<?php
include 'akses.php';
/* $fitur_id = 2;
include '../library/lock-menu.php'; */
include '../library/tgl-indo.php';
$Page = 'Kependudukan';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE NIK='".base64_decode($_GET['id'])."' AND LEFT(KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
	$RowData = mysqli_fetch_array($Edit);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
		
		
		/* Style the form */
		#regForm {
		  background-color: #ffffff;
		  margin: 20px auto;
		  /* padding: 40px; */
		  width: 100%;
		  min-width: 300px;
		}

		/* Style the input fields */
		input {
		  padding: 10px;
		  width: 100%;
		  font-size: 17px;
		  font-family: Raleway;
		  border: 1px solid #aaaaaa;
		}

		/* Mark input boxes that gets an error on validation: */
		input.invalid {
		  background-color: #ffdddd;
		}

		/* Hide all steps by default: */
		.tab {
		  display: none;
		}

		/* Make circles that indicate the steps of the form: */
		.step {
		  height: 15px;
		  width: 15px;
		  margin: 0 2px;
		  background-color: #bbbbbb;
		  border: none; 
		  border-radius: 50%;
		  display: inline-block;
		  opacity: 0.5;
		}

		/* Mark the active step: */
		.step.active {
		  opacity: 1;
		}

		/* Mark the steps that are finished and valid: */
		.step.finish {
		  background-color: #4CAF50;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Data Objek Pajak</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<ul class="nav nav-pills">
						<li <?php if(@$id==null){echo 'class="active"';} ?>>
							<a href="MasterPajak.php"><span class="btn btn-primary">Data NOP</span></a>&nbsp;
						</li>
						<li>
							<a href="#"><span class="btn btn-primary">Import NOP</span></a>&nbsp;
						</li>
						<li>
							<a href="../library/export-excel/template/TemplateDataPenduduk.xls"><span class="btn btn-primary">Download Template</span></a>
						</li>
						<!-- <li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary"><?php /* echo $Sebutan; */ ?></span></a>
						</li> -->
					</ul><br/>
				  <div class="card">
						
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Import Objek Pajak<?php //echo $Sebutan; ?></h3>
							</div>
							<form method="post" action="" enctype="multipart/form-data">
							<div class="card-body">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group-material">
											<label>Import File : </label>
											<div class="input-group">
											<input type="file" name="NamaFile" class="form-control" placeholder="Nama File" value="<?php echo $namaFile ?>" readonly>
											<span class="input-group-btn">
												<button type="submit" class="btn btn-primary" name="SubmitFile">Proses</button>
											</span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group-material">
										<select id="KodeLokasi" name="KodeLokasi" class="form-control" required>	
												<option value="" disabled selected>--- Desa ---</option>
												<?php 
													$menu = mysqli_query($koneksi,"SELECT a.KodeLokasi,b.NamaDesa FROM mstlokasi a JOIN mstdesa b ON a.KodeDesa=b.KodeDesa WHERE LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeLokasi']===@$_REQUEST['KodeLokasi']){
															echo "<option value=\"".$kode['KodeLokasi']."\" selected >".$kode['NamaDesa']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeLokasi']."\" >".$kode['NamaDesa']."</option>\n";
														}
													} 
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<?php if(isset($_POST['SubmitFile'])) {
										ini_set("display_errors",1);
										require_once '../library/excel-reader.php';
										
										$namaFile= @$_FILES['NamaFile']['name'];
										$file_basename = substr($namaFile, 0, strrpos($namaFile, '.')); // strip extention
										$file_ext = substr($namaFile, strrpos($namaFile, '.')); // strip name
										$filesize = @$_FILES['NamaFile']['size'];
										$newfilename = "ImportDataNOP_".date('His').$file_ext;

										$name = @move_uploaded_file(@$_FILES['NamaFile']['tmp_name'],"../library/export-excel/$newfilename");
										
										echo '<div class="table-responsive">';
										
										$data = new Spreadsheet_Excel_Reader("../library/export-excel/$newfilename");
										$htmls='<table class="table table-striped">';
										for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
										{	
											if(count($data->sheets[$i][cells])>0) // checking sheet not empty
											{
												for($j=1;$j<=count($data->sheets[$i][cells]);$j++) // loop used to get each row of the sheet
												{ 
		 
												$htmls.="<tr>";
												for($k=1;$k<=count($data->sheets[$i][cells][$j]);$k++) // This loop is created to get data in a table format.
												{
													$htmls.="<td>";
													$htmls.=$data->sheets[$i][cells][$j][$k];
													$htmls.="</td>";
												}
												$data->sheets[$i][cells][$j][1];
												
												$NOP = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][0]);
												$Tahun = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][1]);
												$NamaWP = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][2]);
												$AlamatWP = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][3]);
												$AlamatOP = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][4]);
												$Bumi = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][5]);
												$Bangunan = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][6]);
												$Pokok = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][7]);			
												
												$htmls.="</tr>";
												}
											}
										echo "<input type='hidden' name='importData' value='$newfilename'>";
	
										}
										$htmls.="</table>";
										echo $htmls;
										echo "";
										echo '<button type="submit" class="btn btn-primary" name="SubmitImport">Import</button>';

										} 
										
										if(isset($_POST['SubmitImport'])){
										$import = @$_POST['importData'];
										ini_set("display_errors",1);
										require_once '../library/excel-reader.php';

										$data = new Spreadsheet_Excel_Reader("../library/export-excel/$import");
										
										echo '<div class="table-responsive">';

										for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
										{	
											if(count($data->sheets[$i][cells])>0) // checking sheet not empty
											{
												
												for($j=2;$j<=count($data->sheets[$i][cells]);$j++) // loop used to get each row of the sheet
												{ 
												 
												$html.="<tr>";
												for($k=1;$k<=count($data->sheets[$i][cells][$j]);$k++) // This loop is created to get data in a table format.
												{
													
												}
												$data->sheets[$i][cells][$j][1];
												
												$NOP = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][1]);
												$Tahun = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][2]);
												$NamaWP = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][3]);
												$AlamatWP = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][4]);
												$AlamatOP = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][5]);
												$Bumi = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][6]);
												$Bangunan = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][7]);
												$Pokok = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][8]);	
												
												$QueryCekData2 = @mysqli_query($koneksi, "SELECT * FROM masterobjekpajak WHERE NoObjekPajak = '".$NOP."' AND Tahun = '".$Tahun."'"); 
												$CekData2 = @mysqli_num_rows($QueryCekData2);
												if($CekData2 > 0){
													
												} else {
													$query = @mysqli_query($koneksi, "insert into masterobjekpajak (NoObjekPajak,Tahun,NamaWP,AlamatWP,AlamatOP,LuasBumi,LuasBangunan,Pokok,KodeLokasi) values ('$NOP','$Tahun','$NamaWP','$AlamatWP','$AlamatOP','$Bumi','$Bangunan','$Pokok','".@$_REQUEST['KodeLokasi']."')");
													if($query){
														@mysqli_query($koneksi, "insert into trbayarpajak (NoObjekPajak,Tahun,KodeLokasi,IsBayar,IsSetor) values ('$NOP','$Tahun','".@$_REQUEST['KodeLokasi']."','0','0')");
													}
												}
												
												$html.="</tr>";
												}
											}
											
										}

										$html.="</table>";

										echo '<script type="text/javascript">alert("Import Data Sukses!"); document.location="MasterPajak.php";</script>';
										}
										?>
									</div>
								</div>
							</div>
							</form>
						</div>
					
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		 $("#KodeLokasi").change(function(){
			var KodeLokasi = $("#KodeLokasi").val();
			$.ajax({
				url: "../library/ambil-desa.php",
				data: "KodeLokasi="+KodeLokasi,
				cache: false,
				success: function(msg){
					$("#KodeDusun").html(msg);
				}
			});
		  });
		});
	</script>
	
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#time1').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time2').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time7').Zebra_DatePicker({format: 'Y-m-d'});
			//$('#Datetime2').Zebra_DatePicker({format: 'Y-m-d H:i', direction: 1});
		});
	</script>
	
	<?php
	if(isset($_POST['Simpan'])){
		echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		
		/* include ('../library/kode-log-server.php');
		//kode jabatan
		$sql_jbt = mysqli_query($koneksi,'SELECT RIGHT(KodeJabatan,5) AS kode FROM mstjabatan WHERE KodeLokasi="'.$login_lokasi.'" ORDER BY KodeJabatan DESC LIMIT 1');  
		$nums_jbt = mysqli_num_rows($sql_jbt);
		 
		if($nums_jbt <> 0)
		 {
		 $data_jbt = mysqli_fetch_array($sql_jbt);
		 $kode_jbt = $data_jbt['kode'] + 1;
		 }else
		 {
		 $kode_jbt = 1;
		 }
		 
		//mulai bikin kode
		 $bikin_kode_jbt = str_pad($kode_jbt, 5, "0", STR_PAD_LEFT);
		 $kode_jadi_jbt = "JBT-".$bikin_kode_jbt;
		
		$query = mysqli_query($koneksi,"INSERT into mstjabatan (KodeJabatan,NamaJabatan,Keterangan,KodeLokasi) 
		VALUES ('$kode_jadi_jbt','".$_POST['Nama']."','".$_POST['Keterangan']."','$login_lokasi')");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Master Jabatan : ".$_POST['Nama']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		} */
	}
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		$nik = htmlspecialchars($_POST['NIK']); $lokasi = htmlspecialchars($_POST['KodeLokasi']); 
		$nama = htmlspecialchars($_POST['Nama']); $tlahir = htmlspecialchars($_POST['TempatLahir']); $tgllahir = $_POST['TanggalLahir']; $jk = htmlspecialchars($_POST['JenisKelamin']); $agama = htmlspecialchars($_POST['Agama']); $stats = htmlspecialchars($_POST['StatusPenduduk']);
		
		$ayah = htmlspecialchars($_POST['NamaAyah']); $ibu = htmlspecialchars($_POST['NamaIbu']); $warga = htmlspecialchars($_POST['Kewarganegaraan']); $statskel = htmlspecialchars($_POST['StatusKeluarga']); $statskaw = htmlspecialchars($_POST['StatusPerkawinan']); $goldarah = htmlspecialchars($_POST['GolDarah']);
		
		$nokk = htmlspecialchars($_POST['NoKK']); $nourut = htmlspecialchars($_POST['NoUrutDiKK']); $noakte = htmlspecialchars($_POST['NoAkteLahir']); $tglakte = $_POST['TanggalAkteLahir']; $didik = htmlspecialchars($_POST['Pendidikan']); $kerja = htmlspecialchars($_POST['Pekerjaan']); $kerja2 = htmlspecialchars($_POST['PekerjaanTambahan']);
		
		$dusun = htmlspecialchars($_POST['KodeDusun']); $rt = htmlspecialchars($_POST['RT']); $rw = htmlspecialchars($_POST['RW']); $jalan = $_POST['NamaJalan']; $norumah = htmlspecialchars($_POST['NoRumah']); $alamat = htmlspecialchars($_POST['Alamat']); 
		
		$query = mysqli_query($koneksi,"UPDATE datapenduduk SET Nama = '$nama', TempatLahir = '$tlahir', TanggalLahir = '$tgllahir', JenisKelamin = '$jk', Agama = '$agama', StatusPenduduk = '$stats', NamaAyah = '$ayah', NamaIbu = '$ibu', WNI = '$warga', StatusKeluarga = '$statskel', StatusPerkawinan = '$statskaw', GolDarah = '$goldarah', NoKK = '$nokk', NoUrutKK = '$nourut', NoAkteLahir = '$noakte', TanggalAkteLahir = '$tglakte', KodePendidikan = '$didik', KodePekerjaan = '$kerja', PekerjaanTambahan = '$kerja2', KodeDusun = '$dusun', RT = '$rt', RW = '$rw', NamaJalan = '$jalan', NoRumah = '$norumah', Alamat = '$alamat' WHERE KodeLokasi = '$lokasi' AND NIK = '$nik'");
		
		if($query){
			echo '<script language="javascript">document.location="DataPenduduk.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Edit Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "DataPenduduk.php";
				  });
				  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		include ('../library/kode-log-server.php');
		$query = mysqli_query($koneksi,"DELETE FROM mstjabatan WHERE KodeJabatan='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Master Jabatan : ".$_GET['nm']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " Hapus data di Master Aparatur terlebih dahulu! ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterJabatan.php";
					  });
					  </script>';
		}
	}
	
	?>
  </body>
</html>