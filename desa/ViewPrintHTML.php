<?php
// Gunakan WriteHTML FPDF
require('../library/fpdf/write-html.php');

$this=new PDF_HTML();
    $this->SetFont('Arial','',12);
    $this->AddPage();
    

include "../library/config.php";
		include "../library/tgl-indo.php";
		
		//get variable
		$kode = base64_decode(@$_GET['kode']);
		$jenis = base64_decode(@$_GET['jns']);
		$lokasi = base64_decode(@$_GET['loc']);
		$border = 0;
		$this->AddPage();
		$this->SetMargins(15,15,15,15);
		$this->SetAutoPageBreak(true,30);
		$this->AliasNbPages();
		
		//header
		$this->Image('..\images\Assets\logo.png',20,5,22); // logo
		$this->SetFont('Times','B','15'); $this->SetTextColor(0,0,0); // warna tulisan
		// font yang digunakan // membuat cell dg panjang 19 dan align center 'C'
		$this->Ln(); $this->Cell(200,0,'P E M E R I N T A H   K A B U P A T E N   N G A N J U K',0,0,'C');
		$this->Ln(); $this->Cell(200,12,'K E C A M A T A N  N G A N J U K',0,0,'C');
		$cari = @mysqli_query($koneksi, "Select NamaDesa, AlamatLokasi from mstdesa inner join mstlokasi on mstlokasi.KodeDesa = mstdesa.KodeDesa WHERE mstlokasi.KodeLokasi='".$lokasi."'") or die(@mysqli_error($cari));
		while($tampil = @mysqli_fetch_array($cari)){
			$namadesa = $tampil['NamaDesa']; $alamat = $tampil['AlamatLokasi'];
			$this->SetFont('Times','B','15');
			$this->Ln(); $this->Cell(200,1,'KELURAHAN '.$namadesa,0,0,'C');
		}
		$this->Ln(); $this->SetFont('Times','','11'); // font yang digunakan
		$this->Cell(200,12,$alamat,0,0,'C');
		
		$this->Ln(11); $this->SetLineWidth(0.75); // tebal garis
		$this->Cell(180,0,'','TB',0,'C',1);
		$this->Ln(1); $this->SetLineWidth(0.1); // tebal garis 
		$this->Cell(180,0,'','TB',0,'L',1);
		
		if($jenis === 'SKAW'){
			$this->Ln(10); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN AHLI WARIS',0,0,'C');
			$this->SetFont('Times','','11');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Keperluan = $tampil['Keperluan']; $Uraian = $tampil['Uraian']; $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat'];}
			
			// baris pendahuluan
			$this->Ln(7.5); $this->MultiCell(0,7.5,$Keperluan);
			
			// baris isi
			$this->Ln(2);
			$this->SetWidths(array(5,5,165,5)); $this->SetAligns(array('J','L','J','J'));
			$Support = "Suport"; $No = 4; $Total = 20;
			for($i=$No;$i<$Total;$i++){
				$ke = $i+1; $urut = $i-3;
				$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
				$arrayHasil = @mysqli_fetch_array($cek);
				if($arrayHasil[$Support.$ke] != null){
					$bagi = explode("#",$arrayHasil[$Support.$ke]);
					$this->SetFont('Times','B','11');
					$this->Row( array( "", $urut.".", $bagi[0].". ".$bagi[1].", Tempat/Tanggal Lahir : ".strtoupper($bagi[2]).", Pekerjaan : ".strtoupper($bagi[3]).", Alamat : ".$bagi[4], "" ) );
				}
			}
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(7.5); $this->MultiCell(0,7.5,$Uraian);
			
			// baris tanda tangan
			$this->Ln(5);
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2, Suport3 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$tgl2 = explode("#", $arraySah['Suport2']); $tgl3 = explode("#", $arraySah['Suport3']);
				$tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				$tglS3 = TanggalIndo(date("Y-m-d", strtotime($tgl3[0])));
				if($arraySah['Suport2'] == null AND $arraySah['Suport3'] == null){
					$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ... ", "" ) );   
				} elseif($arraySah['Suport2'] != null AND $arraySah['Suport3'] == null){
					$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ".$tglS2, "" ) );
				} elseif($arraySah['Suport2'] == null AND $arraySah['Suport3'] != null){
					$this->Row( array( "", "Nganjuk, ".$tglS3, "", "Nganjuk, ... ", "" ) );
				} else {
					$this->Row( array( "", "Nganjuk, ".$tglS3, "", "Nganjuk, ".$tglS2, "" ) );
				}
				
				$this->Ln(1);
				if($arraySah['Suport1'] === '1') {
					$this->Row( array( "", "Nomor : ", "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
				} elseif($arraySah['Suport1'] === '2') {
					$this->Row( array( "", "Nomor : ".$tgl3[1], "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
				} else {
					$this->Row( array( "", "Nomor : ", "", "Nomor : ", "" ) ); 
				}
				
				$this->Ln(4);
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				} elseif($arraySah['Suport1'] === '2'){ 
					$this->Row( array( "", "Dikuatkan oleh Kami,", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) );
				}
			}
			   
			$this->Ln(1);
			$this->Row( array( "", "Camat NGANJUK", "", "Kepala Desa ".$namadesa ) );
			$this->Ln(17.5);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'CAMAT' AND b.KodeLokasi = '001-001'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $camat = $arrayAparat['NamaAparat']; $nip1 = $arrayAparat['NIP']; }
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			$this->Ln(0); $this->Row( array( "", $camat, "", $lurah, "" ) );
			$this->SetFont('Times','','11');
			$this->Ln(-1); $this->Row( array( "", "NIP : ".$nip1, "", "NIP : ".$nip2, "" ) );
			
		}
		
		$pdfz->Output();
    exit;
}?>