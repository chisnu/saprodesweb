<?php
include 'akses.php';
include '../library/tgl-indo.php';

$fitur_id = 5;
include '../library/lock-menu.php';

$Page = 'ProgressSurat';
$Tahun=date('Y');
$HariIni=date('Y-m-d H:i:s');
$JamSekarang = date('H:i:s');
$TanggalSekarang = date('Y-m-d');
		

//cek apakah user sudah pernah membuat tr permohonan msyarakat
$QueryCekData = @mysqli_query($koneksi, "SELECT a.NoTrMohon,a.JenisSurat,b.StandarWaktuPelayanan FROM trpermohonanmasy a JOIN MasterPengurusanSurat b ON (a.JenisSurat)=(b.JenisSurat) WHERE LEFT(a.KodeLokasi,3)='".substr($kode_lokasi_aktif,0,3)."' AND a.IDPend='$id_penduduk_aktif' AND a.StatusPermohonan='PraWAITING'"); 
$numCek = @mysqli_num_rows($QueryCekData); 
if($numCek > 0){
	while($CekData = @mysqli_fetch_array($QueryCekData)){
		$No_Transaksi = $CekData['NoTrMohon'];
		$Jenis_Surat = $CekData['JenisSurat'];
		$Standar_Waktu = $CekData['StandarWaktuPelayanan'];
	}
}

//Hapus Data
/* if(base64_decode(@$_GET['aksi'])==='Hapus'){
	$file = '../images/DokumenSyarat/'.base64_decode(@$_GET['file']);
	$thumb = '../images/DokumenSyarat/thumb_'.base64_decode(@$_GET['file']);
	
	if(file_exists($file)){ unlink($file); }
	if(file_exists($thumb)){ unlink($thumb); }
	$HapusData = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND KodeSyarat='".base64_decode(@$_GET['id'])."' AND NoTrMohon='$No_Transaksi'"); 
	echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil"; </script>';
}

if(base64_decode(@$_GET['aksi'])==='PindahDokumen'){
		$QueryCekData = @mysqli_query($koneksi, "SELECT MAX(NoUrutSyarat) as NoSekarang FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi'"); 
		$CekData = @mysqli_fetch_array($QueryCekData);
		$NoMax = $CekData['NoSekarang'];

		$NoSekarang = $NoMax+1; 
		
		$NmSyarat = base64_decode(@$_GET['NmSy']);
		$KodeDokPenduduk = base64_decode(@$_GET['KodeDok']);
		$KdSyarat = base64_decode(@$_GET['KdSyarat']);
		
		$PindahDokumen = @mysqli_query($koneksi, "INSERT INTO dokumensyaratmohon (NoTrMohon,NoUrutSyarat,KodeLokasi,NamaSyarat,KodeDokPenduduk,IDPend,IsVerified,KodeSyarat) VALUES (
		'$No_Transaksi','$NoSekarang','$kode_lokasi_aktif','$NmSyarat','$KodeDokPenduduk','$id_penduduk_aktif','0','$KdSyarat')");
		
		echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil"; </script>';
	}
*/
	
	if(isset($_POST['CheckAll'])){
		if(@$_REQUEST['ProsesSurat'] === 'TerimaBerkas'){
			$cekbox = @$_POST['cekbox'];
			if($cekbox){
				foreach($cekbox as $value){
					$progress = @$_POST['Progress'.$value];
					
					$TerimaDokumen = @mysqli_query($koneksi, "UPDATE progresssurat SET IsSend=b'0' WHERE KodeLokasi='$login_lokasi' AND NoTrMohon='$value' AND NoUrutProgress='$progress' AND IsSend=b'1'");
					if($TerimaDokumen){
						$QueryCekData = @mysqli_query($koneksi, "SELECT MAX(NoUrutProgress) as NoSekarang, UserName FROM progresssurat WHERE KodeLokasi='$login_lokasi' AND NoTrMohon='$value'"); 
						$CekData = @mysqli_fetch_array($QueryCekData);
						$NoMax = $CekData['NoSekarang']; $NoSekarang = $NoMax+1; $UserMax = $CekData['UserName']; 
						
						// simpan penerimaan berkas
						@mysqli_query($koneksi, "INSERT INTO progresssurat (NoUrutProgress,NoTrMohon,KodeLokasi,StatusProgress,UserName,Waktu,Tanggal,IsSend,IsConfirmed,WaktuConfirm,TglConfirm,UserConfirm)VALUES('$NoSekarang','$value','$login_lokasi','Proses Berkas (DESA)','$UserMax','$TanggalSekarang $JamSekarang','$TanggalSekarang $JamSekarang',b'0',b'1','$TanggalSekarang $JamSekarang','$TanggalSekarang $JamSekarang','$login_id')");
						
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Sukses!",
							text: " Berkas Permohonan Surat telah diterima",
							type: "success"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';	
					} else {
						echo '<script language="javascript">document.location="ProsesSurat.php"; </script>';
					}
				}
							
			}
			else{
				echo '<script language="javascript">document.location="ProsesSurat.php"; </script>';	
			}
		} elseif(@$_REQUEST['ProsesSurat'] === 'AlihkanBerkas') {
			$cekbox = @$_POST['cekbox']; 
			if($cekbox){
				foreach($cekbox as $value){
					$progress = @$_POST['Progress'.$value];
					$tujuan = @$_POST['TujuanSurat'.$value];
					
					$UpdateData = @mysqli_query($koneksi, "UPDATE progresssurat SET IsSend=b'1',SendTo='$tujuan',WaktuSend='$TanggalSekarang $JamSekarang',TglSend='$TanggalSekarang $JamSekarang',UserSender='$login_id',IsConfirmed=b'0' WHERE KodeLokasi='$login_lokasi' AND NoTrMohon='$value' AND NoUrutProgress='$progress' AND StatusProgress='Proses Berkas (DESA)'");
					if($UpdateData){
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Berhasil!",
							text: " Permohonan akan diteruskan ke proses selanjutnya!",
							type: "success"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
						
						}
				}
			}
			else{
				echo '<script language="javascript">document.location="ProsesSurat.php"; </script>';	
			}
		}
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.blue.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	<!-- Select2Master -->
	<link rel="stylesheet" href="../library/select2master/css/select2.css"/>
	<!-- CK Editor -->
	<script type="text/javascript" src="../library/ckeditor/ckeditor.js"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		
		function confirm_hapus() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php?aksi=tampil";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		
		function confirm_proses() {
			var answer = confirm("Apakah Anda yakin untuk memproses data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		function confirm_check() {
			var answer = confirm("Apakah Anda yakin untuk menerima atau mengalihkan data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		function confirm_penduduk() {
			var answer = confirm("Apakah Anda yakin untuk menghapus data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php?aksi=tampil&";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Layanan Surat - Proses Surat</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
				    <ul class="nav nav-pills">
						<li>
							<a href="LayananSurat.php?ProsesSurat=AlihkanBerkas"><span class="btn btn-primary">Kembali</span></a>&nbsp;
						</li>
					</ul><br/>
				  <div class="card">
					
						<?php $sql = @mysqli_query($koneksi, "SELECT a.NoTrMohon, a.TglPermohonan, a.Keterangan1, a.JenisSurat, a.KodeLokasi, a.IDPend, a.StatusPermohonan, b.Keterangan, a.PrediksiTglSelesai, b.KodeNomorSurat, b.StatusSurat FROM trpermohonanmasy a JOIN masterpengurusansurat b ON a.JenisSurat=b.JenisSurat WHERE (a.StatusPermohonan='WAITING' OR a.StatusPermohonan='ON PROGRESS' OR a.StatusPermohonan='DONE' OR a.StatusPermohonan='REJECTED') AND a.KodeLokasi='$login_lokasi' AND NoTrMohon='".base64_decode(@$_GET['kode'])."'");
						$array = @mysqli_fetch_array($sql);
						?>
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Proses <?php echo ucwords($array['Keterangan']); ?></h3>
							</div>
							<div class="card-body">
								<div class="row">
								  <div class="col-sm-6">
									<h5>Informasi Pemohon</h5>
									<?php $sql2 =  mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE IDPend='".$array['IDPend']."'");
									while($array2 = mysqli_fetch_array($sql2)){
										$nik = $array2['NIK'];
										$nama = $array2['Nama'];
										$tlahir = $array2['TempatLahir'];
										$tgllahir = $array2['TanggalLahir'];
										$alamat = $array2['Alamat'];
										$jk = $array2['JenisKelamin'];
										$lokasi = $array2['KodeLokasi'];
										$idlapor = $array2['IDPend'];
									} ?>
									
									<div class="table-responsive">  
									<table class="table table-striped">
									  <tbody>
										<tr><td width="35%">NIK / Nama</td><td width="1%">:</td><td><?php echo $nik." / ".strtoupper($nama);?></td></tr>
										<tr><td>Tempat & Tanggal Lahir</td><td>:</td><td><?php echo ucwords($tlahir).", ".TanggalIndo($tgllahir);?></td></tr>
									  </tbody>
									</table>
									</div><hr>
									
									<?php 
									$tandatangan =  mysqli_query($koneksi, "SELECT Penandatanganan FROM mstlokasi WHERE KodeLokasi='".$array['KodeLokasi']."'");
									while($arrayTtd = mysqli_fetch_array($tandatangan)){
										$TTD = $arrayTtd['Penandatanganan'];
									} 
									$tandatangan2 =  mysqli_query($koneksi, "SELECT Penandatanganan FROM mstlokasi WHERE KodeLokasi='001-000'");
									while($arrayTtd2 = mysqli_fetch_array($tandatangan2)){
										$TTD2 = $arrayTtd2['Penandatanganan'];
									} ?>
									
									<h5>Pengisian Surat</h5>
									<form method="post">
									<?php $sql3 =  mysqli_query($koneksi, "SELECT * FROM trpengurusansurat WHERE NoTrMohon='".$array['NoTrMohon']."' AND KodeLokasi='".$array['KodeLokasi']."' AND JenisSurat='".$array['JenisSurat']."'");
									$array3 = mysqli_fetch_array($sql3); ?>
									
									<!-- <div class="form-group-material">
										<?php /* if($array3['Tanggal'] == null){
											echo '<input type="text" name="TglSurat" id="time7" class="form-control" placeholder="Tanggal Surat" value="'.date("Y-m-d").'" required>';
										} else {
											echo '<input type="text" name="TglSurat" id="time7" class="form-control" placeholder="Tanggal Surat" value="'.substr($array3['Tanggal'],0,11).'" required>';
										} */ ?>
									</div> -->
									<div class="form-group-material">
										<?php if($array3['Keperluan'] == null){
											if($array['JenisSurat'] === 'SKAW') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Berdasarkan keterangan dari Surat Pernyataan Pemohon serta Surat Keterangan dari RT. ... RW. ..., dengan ini menerangkan bahwa Almarhum/Almarhumah ..... telah meninggalkan ahli waris sebagai berikut :</textarea>';
											} elseif($array['JenisSurat'] === 'SKB') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Berdasarkan keterangan dari Surat Pernyataan Pemohon serta Surat Keterangan dari RT. ... RW. ..., pada hari ini ... , tanggal ... , bulan ... , tahun ... , bahwa kami yang bertanda tangan dibawah ini :</textarea>';
											} elseif($array['JenisSurat'] === 'SKBH') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Dengan ini menerangkan berdasarkan keterangan dari Surat Pernyataan Pemohon serta Surat Keterangan dari RT. ... RW. ..., bahwa yang tertera di bawah ini :</textarea>';
											} elseif($array['JenisSurat'] === 'SKBM') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Kami selaku Kepala Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk menerangkan berdasarkan keterangan dari Surat Pernyataan Pemohon dan Surat Keterangan dari RT. ... RW. ..., bahwa penduduk di bawah ini :</textarea>';
											} elseif($array['JenisSurat'] === 'SKBPKB') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Dengan ini diterangkan bahwa berdasarkan keterangan Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ..., adapun Kendaraan Bermotor sebagai berikut :</textarea><br>';
												echo '<input type="text" name="Isi1" class="form-control" placeholder="Nama Kendaraan" value="Merk Kendaraan" required><br>';
												echo '<input type="text" name="Isi2" class="form-control" placeholder="JenisKendaraan/Tahun" value="Jenis Kendaraan/Tahun" required><br>';
												echo '<input type="text" name="Isi3" class="form-control" placeholder="Warna" value="Warna" required><br>';
												echo '<input type="text" name="Isi4" class="form-control" placeholder="Nomor Rangka" value="Nomor Rangka" required><br>';
												echo '<input type="text" name="Isi5" class="form-control" placeholder="Nomor Mesin" value="Nomor Mesin" required><br>';
												echo '<input type="text" name="Isi6" class="form-control" placeholder="Nomor BPKB" value="Nomor BPKB" required><br>';
												echo '<input type="text" name="Isi7" class="form-control" placeholder="Nopol" value="Nopol" required><br>';
												echo '<input type="text" name="Isi8" class="form-control" placeholder="Atas Nama" value="Atas Nama" required><br>';
												echo '<input type="text" name="Isi9" class="form-control" placeholder="Alamat BPKB" value="Alamat BPKB" required>';
											} elseif($array['JenisSurat'] === 'SKD') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Dengan ini menerangkan bahwa berdasarkan keterangan Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ..., bahwa penduduk yang tertera identitasnya di bawah ini, yaitu :</textarea>';
											} elseif($array['JenisSurat'] === 'SKKB') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Yang bertanda tangan di bawah ini Kepala Kelurahan '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk, dengan ini menerangkan berdasarkan Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ..., bahwa penduduk yang tertera di bawah ini :</textarea>';
											} elseif($array['JenisSurat'] === 'SKKT') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo '<p>Yang bertanda tangan di bawah ini Kepala Kelurahan '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk, dengan ini menerangkan berdasarkan Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ... bahwa :</p></textarea>';
											} elseif($array['JenisSurat'] === 'SKLHR') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Yang bertanda tangan di bawah ini Kepala Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk, dengan ini menerangkan berdasarkan keterangan dari Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ... bahwa :</textarea><br>';
											} elseif($array['JenisSurat'] === 'SKMTN') {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Yang bertanda tangan di bawah ini Kepala Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk, dengan ini menerangkan berdasarkan keterangan dari Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ... bahwa :</textarea><br>';
											} elseif($array['JenisSurat'] === 'SKP'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Alamat : Alamat Pindah,<br />
												      Desa/Kelurahan : Nama Desa/Kelurahan,<br />
													  Kecamatan : Nama Kecamatan,<br />
													  Kab/Kota : Nama Kab/Kota,<br />
													  Propinsi : Nama Propinsi</textarea>';
											} elseif($array['JenisSurat'] === 'SKPS'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo '<p>Yang bertanda tangan di bawah ini Kepala Kelurahan '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk, dengan ini menerangkan berdasarkan Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ... bahwa :</p></textarea>';
											} elseif($array['JenisSurat'] === 'SKPTK'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Berdasarkan keterangan dalam Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ... bahwa orang tersebut di atas mempunyai penghasilan Per Bulan Rp. ... ( Terbilang : ... ) dan mempunyai tanggungan keluarga sebagai berikut :</textarea>';
											} elseif($array['JenisSurat'] === 'SKTM'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Berdasarkan Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ... bahwa orang tersebut adalah benar-benar termasuk kriteria masyarakat miskin telah memenuhi 8 kriteria dari 15 kriteria yang ditentukan dalam Surat Keterangan Miskin (SKM).</textarea>';
											} elseif($array['JenisSurat'] === 'SKU'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Berdasarkan keterangan dari Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ..., diterangkan bahwa orang tersebut adalah benar-benar penduduk Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk.</textarea>';
											} elseif($array['JenisSurat'] === 'SKUM'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Berdasarkan keterangan dari Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ..., diterangkan bahwa orang tersebut adalah benar-benar penduduk Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk.</textarea>';
											} elseif($array['JenisSurat'] === 'SKPW'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Berdasarkan keterangan dalam Surat Pernyataan Pemohon dan Surat Keterangan RT. ... RW. ... , diterangkan bahwa yang bersangkutan adalah selaku Perwalian dari :</textarea>';
											} /* elseif($array['JenisSurat'] === 'SKT'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo 'Menindaklanjuti surat Camat Nganjuk pada Tanggal ... Nomor : ... Tentang ... , maka dengan ini kami menugaskan kepada :</textarea><br>';
												echo '<input type="text" name="Isi1" class="form-control" placeholder="Nama" value="Nama" required><br>';
												echo '<input type="text" name="Isi2" class="form-control" placeholder="Jabatan" value="Jabatan" required><br>';
												echo '<input type="text" name="Isi3" class="form-control" placeholder="Alamat" value="Alamat" required><br>';
												echo '<input type="text" name="Isi4" class="form-control" placeholder="Keperluan" value="Keperluan" required><br>';
												echo '<input type="text" name="Isi5" class="form-control" placeholder="Waktu" value="Waktu" required><br>';
												echo '<input type="text" name="Isi6" class="form-control" placeholder="Tempat" value="Tempat" required><br>';
											} */
										} else {
											if($array['JenisSurat'] === 'SKBPKB'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo $array3['Keperluan'];
												echo '</textarea><br>';
												$pecah = explode("#", $array3['Suport1']);
												echo '<input type="text" name="Isi1" class="form-control" placeholder="Nama Kendaraan" value="'.$pecah[0].'" required><br>';
												echo '<input type="text" name="Isi2" class="form-control" placeholder="JenisKendaraan/Tahun" value="'.$pecah[1].'" required><br>';
												echo '<input type="text" name="Isi3" class="form-control" placeholder="Warna" value="'.$pecah[2].'" required><br>';
												echo '<input type="text" name="Isi4" class="form-control" placeholder="Nomor Rangka" value="'.$pecah[3].'" required><br>';
												echo '<input type="text" name="Isi5" class="form-control" placeholder="Nomor Mesin" value="'.$pecah[4].'" required><br>';
												echo '<input type="text" name="Isi6" class="form-control" placeholder="Nomor BPKB" value="'.$pecah[5].'" required><br>';
												echo '<input type="text" name="Isi7" class="form-control" placeholder="Nopol" value="'.$pecah[6].'" required><br>';
												echo '<input type="text" name="Isi8" class="form-control" placeholder="Atas Nama" value="'.$pecah[7].'" required><br>';
												echo '<input type="text" name="Isi9" class="form-control" placeholder="Alamat BPKB" value="'.$pecah[8].'" required>';
											} elseif($array['JenisSurat'] === 'SKLHR'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo $array3['Keperluan'];
												echo '</textarea><br>';
											} elseif($array['JenisSurat'] === 'SKMTN'){
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo $array3['Keperluan'];
												echo '</textarea><br>';
											} /* elseif($array['JenisSurat'] === 'SKT'){
												$pecah = explode("#", $array3['Suport5']);
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
												echo $array3['Keperluan'];
												echo '</textarea><br>';
												echo '<input type="text" name="Isi1" class="form-control" placeholder="Nama" value="'.$pecah[0].'" required><br>';
												echo '<input type="text" name="Isi2" class="form-control" placeholder="Jabatan" value="'.$pecah[1].'" required><br>';
												echo '<input type="text" name="Isi3" class="form-control" placeholder="Alamat" value="'.$pecah[2].'" required><br>';
												echo '<input type="text" name="Isi4" class="form-control" placeholder="Keperluan" value="'.$pecah[3].'" required><br>';
												echo '<input type="text" name="Isi5" class="form-control" placeholder="Waktu" value="'.$pecah[4].'" required><br>';
												echo '<input type="text" name="Isi6" class="form-control" placeholder="Tempat" value="'.$pecah[5].'" required><br>';
											} */ else {
												echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>'.$array3['Keperluan'].'</textarea>';
											}
										} 
										
										if($array['JenisSurat'] === 'SKLHR'){
											if($array3['Suport1'] == null){
												// membuat id otomatis
												$sql = @mysqli_query($koneksi, "SELECT MAX(RIGHT(NIK,5)) AS kode FROM datapenduduk WHERE KodeLokasi='$lokasi' AND LEFT(NIK,8)='".$lokasi."-'"); 
												$nums = @mysqli_num_rows($sql); 
												while($data = @mysqli_fetch_array($sql)){
													if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
												}
												// membuat kode user
												$bikin_kode = str_pad($kode, 5, "0", STR_PAD_LEFT);
												$kode_jadi = $lokasi."-".$bikin_kode;
												
												echo '<input type="hidden" name="NIK" class="form-control" placeholder="NIK" value="'.$kode_jadi.'" readonly>';
												echo '<input type="text" name="Isi1" class="form-control" placeholder="Nama Anak" value="Nama Anak" required><br>';
												echo '<input type="text" name="Isi2" class="form-control" placeholder="Tempat Lahir" value="Tempat Lahir" required><br>';
												echo '<input type="text" name="Isi3" id="time2" class="form-control" placeholder="Tanggal Lahir" value="'.date("Y-m-d").'" required><br>';
												echo '<select name="Isi4" class="form-control" required>
													  <option value="" disabled selected>--- Jenis Kelamin ---</option>';
													  $menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='JNS_KEL' ORDER BY NoUrut ASC");
													  while($kode = mysqli_fetch_array($menu)){
															echo "<option value=\"".$kode['NoUrut']."#".$kode['Uraian']."\" >".$kode['Uraian']."</option>\n";
													  };
												echo '</select><br>';
												echo '<select name="Isi5" class="form-control" required>
													  <option value="" disabled selected>--- Kewarganegaraan ---</option>';
													  $menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' ORDER BY NoUrut ASC");
													  while($kode = mysqli_fetch_array($menu)){
															echo "<option value=\"".$kode['NoUrut']."#".$kode['Uraian']."\" >".$kode['Uraian']."</option>\n";
													  };
												echo '</select><br>';
												echo '<select name="Isi6" class="form-control" required>
													  <option value="" disabled selected>--- Pilih Agama ---</option>';
													  $menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='AGAMA' ORDER BY NoUrut ASC");
													  while($kode = mysqli_fetch_array($menu)){
														    echo "<option value=\"".$kode['NoUrut']."#".$kode['Uraian']."\" >".$kode['Uraian']."</option>\n";
													  };
												echo '</select><br>';
												echo '<input type="text" name="AnakKe" class="form-control" placeholder="AnakKe" required><br>';
												echo '<select id="CariAyah" name="CariAyah" style="width: 100% !important;">
												      <option value=""></option>';
												      $menu = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE StatusPenduduk = 'ADA' AND KodeLokasi='".$array['KodeLokasi']."'");
													  while($kode = mysqli_fetch_array($menu)){
														 echo "<option value=\"".$kode['NIK']."#".$kode['IDPend']."#".$kode['Nama']."\" >".$kode['NIK']." : ".$kode['Nama']."</option>\n";
													  };
												echo '</select><br><br>';
												echo '<select id="CariIbu" name="CariIbu" style="width: 100% !important;">
												      <option value=""></option>';
												      $menu = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE StatusPenduduk = 'ADA' AND KodeLokasi='".$array['KodeLokasi']."'");
													  while($kode = mysqli_fetch_array($menu)){
														 echo "<option value=\"".$kode['NIK']."#".$kode['IDPend']."#".$kode['Nama']."\" >".$kode['NIK']." : ".$kode['Nama']."</option>\n";
													  };
												echo '</select><br><br>';
												echo '<textarea type="text" name="Isi7" class="form-control" rows="10" placeholder="Alamat" required>Alamat</textarea>';
											} else {
												$pecah = explode("#", $array3['Suport1']);
												echo '<input type="hidden" name="NIK" class="form-control" placeholder="NIK" value="'.$pecah[7].'" readonly>';
												echo '<input type="text" name="Isi1" class="form-control" placeholder="Nama Anak" value="'.$pecah[0].'" required><br>';
												echo '<input type="text" name="Isi2" class="form-control" placeholder="Tempat Lahir" value="'.$pecah[1].'" required><br>';
												echo '<input type="text" name="Isi3" id="time2" class="form-control" value="'.$pecah[2].'" required><br>';
												echo '<select name="Isi4" class="form-control" required>
													 <option value="" disabled selected>--- Jenis Kelamin ---</option>';
													  $menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='JNS_KEL' ORDER BY NoUrut ASC");
													  while($kode = mysqli_fetch_array($menu)){
														  if($kode['NoUrut']===$pecah[3]){
															 echo "<option value=\"".$kode['NoUrut']."#".$kode['Uraian']."\" selected>".$kode['Uraian']."</option>\n";
														  }else{
															 echo "<option value=\"".$kode['NoUrut']."#".$kode['Uraian']."\" >".$kode['Uraian']."</option>\n";
														  }
													  }; 	 
												echo '</select><br>';
												echo '<select name="Isi5" class="form-control" required>
													  <option value="" disabled selected>--- Kewarganegaraan ---</option>';
													  $menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' ORDER BY NoUrut ASC");
													  while($kode = mysqli_fetch_array($menu)){
														  if($kode['NoUrut']===$pecah[4]){
															 echo "<option value=\"".$kode['NoUrut']."#".$kode['Uraian']."\" selected>".$kode['Uraian']."</option>\n";
														  }else{
															 echo "<option value=\"".$kode['NoUrut']."#".$kode['Uraian']."\" >".$kode['Uraian']."</option>\n";
														  }
													  };
												echo '</select><br>';
												echo '<select name="Isi6" class="form-control" required>
													  <option value="" disabled selected>--- Pilih Agama ---</option>';
													  $menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='AGAMA' ORDER BY NoUrut ASC");
													  while($kode = mysqli_fetch_array($menu)){
														  if($kode['NoUrut']==$pecah[5]){
															 echo "<option value=\"".$kode['NoUrut']."#".$kode['Uraian']."\" selected>".$kode['Uraian']."</option>\n";
														  }else{
															 echo "<option value=\"".$kode['NoUrut']."#".$kode['Uraian']."\" >".$kode['Uraian']."</option>\n";
														  }
													  };
												echo '</select><br>';
												echo '<input type="text" name="AnakKe" class="form-control" placeholder="AnakKe" value="'.$pecah[8].'" required><br>';
												echo '<select id="CariAyah" name="CariAyah" style="width: 100% !important;">
												      <option value=""></option>';
												      $menu = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE KodeLokasi='".$array['KodeLokasi']."'");
													  while($kode = mysqli_fetch_array($menu)){
														 echo "<option value=\"".$kode['Nama']."\" >".$kode['NIK']." : ".$kode['Nama']."</option>\n";
														 if($kode['NIK']==$pecah[9]){
															 echo "<option value=\"".$kode['NIK']."#".$kode['IDPend']."#".$kode['Nama']."\" selected>".$kode['NIK']." : ".$kode['Nama']."</option>\n";
														  }else{
															 echo "<option value=\"".$kode['NIK']."#".$kode['IDPend']."#".$kode['Nama']."\" >".$kode['NIK']." : ".$kode['Nama']."</option>\n";
														  }
													  };
												echo '</select><br><br>';
												echo '<select id="CariIbu" name="CariIbu" style="width: 100% !important;">
												      <option value=""></option>';
												      $menu = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE KodeLokasi='".$array['KodeLokasi']."'");
													  while($kode = mysqli_fetch_array($menu)){
														  if($kode['NIK']==$pecah[12]){
															 echo "<option value=\"".$kode['NIK']."#".$kode['IDPend']."#".$kode['Nama']."\" selected>".$kode['NIK']." : ".$kode['Nama']."</option>\n";
														  } else {
															 echo "<option value=\"".$kode['NIK']."#".$kode['IDPend']."#".$kode['Nama']."\" >".$kode['NIK']." : ".$kode['Nama']."</option>\n";
														  }
													  };
												echo '</select><br><br>';
												echo '<textarea type="text" name="Isi7" class="form-control" rows="10" placeholder="Alamat" required>'.$pecah[6].'</textarea>';
											}
										} if($array['JenisSurat'] === 'SKMTN'){
											if($array3['Suport1'] == null){
												echo '<select id="CariPenduduk" name="CariPenduduk" style="width: 100% !important;">
												      <option value=""></option>';
												      $menu = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE KodeLokasi='".$array['KodeLokasi']."'");
													  while($kode = mysqli_fetch_array($menu)){
														 echo "<option value=\"".$kode['NIK']."#".$kode['Nama']."#".$kode['JenisKelamin']."#".$kode['Alamat']."#".$kode['IDPend']."\" >".$kode['NIK']." : ".$kode['Nama']."</option>\n";
													  };
												echo '</select><br><br>';
												echo '<input type="text" name="Isi1" id="time2" class="form-control" placeholder="Tanggal Meninggal" value="'.date("Y-m-d").'" required><br>';
												echo '<input type="text" name="Isi2" class="form-control" placeholder="Tempat Meninggal" value="Tempat Meninggal" required><br>';
												echo '<input type="text" name="Isi3" class="form-control" placeholder="Sebab Meninggal" value="Sebab Meninggal" required>';
											} else {
												$pecah = explode("#", $array3['Suport1']);
												$matilama = $pecah[3];
												echo '<select id="CariPenduduk" name="CariPenduduk" style="width: 100% !important;" >
												      <option value=""></option>';
												      $menu = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE KodeLokasi='".$array['KodeLokasi']."'");
													  while($kode = mysqli_fetch_array($menu)){
														  if($kode['NIK']==$pecah[3]){
															 echo "<option value=\"".$kode['NIK']."#".$kode['Nama']."#".$kode['JenisKelamin']."#".$kode['Alamat']."#".$kode['IDPend']."\" selected>".$kode['NIK']." : ".$kode['Nama']."</option>\n";
														  } else {
															 echo "<option value=\"".$kode['NIK']."#".$kode['Nama']."#".$kode['JenisKelamin']."#".$kode['Alamat']."#".$kode['IDPend']."\" >".$kode['NIK']." : ".$kode['Nama']."</option>\n";
														  }
													  };
												echo '</select><br><br>';
												echo '<input type="text" name="Isi1" id="time2" class="form-control" placeholder="Tanggal Meninggal" value="'.$pecah[0].'" required><br>';
												echo '<input type="text" name="Isi2" class="form-control" placeholder="Tempat Meninggal" value="'.$pecah[1].'" required><br>';
												echo '<input type="text" name="Isi3" class="form-control" placeholder="Sebab Meninggal" value="'.$pecah[2].'" required>';
											}
										}
										?>
										
										<script type="text/javascript">
											var editor = CKEDITOR.replace('ckeditor');
											CKFinder.setupCKEditor(editor, 'ckfinder/');    
										</script>
									</div>
									<?php if($array3['Uraian'] == null){
										echo '<div class="form-group-material">';
											if($array['JenisSurat'] === 'SKAW') {
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Adapun Surat Keterangan Ahli Waris ini diberikan untuk keperluan ... . Demikian orang tersebut diatas adalah benar-benar ahli waris dari almarhum/almarhumah, dan apabila dikemudian hari ternyata keterangan ini tidak benar, maka kami sanggup dituntut sesuai peraturan undang-undang yang berlaku, serta semua pihak yang mengesahkan Surat Keterangan ini kami nyatakan bebas dari segala tuntutan.</textarea>';
											} elseif($array['JenisSurat'] === 'SKB') {
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo '<p>Kedua belah pihak atas kehendak sendiri tanpa ada tekanan siapapun beritikad baik dan mengadakan kesepakatan bersama sebagai berikut :<br />
												1. Permintaan maaf dari salah satu pihak atau saling memaafkan dari kedua belah pihak.<br />
												2. ...<br />
												3. ...<br />
												</p></textarea>';
											} elseif($array['JenisSurat'] === 'SKBH') {
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Betul bahwa orang tersebut di atas terdaftar sebagai penduduk Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk dan yang bersangkutan diatas telah kehilangan barang yaitu ... . Dengan terbitnya surat keterangan ini, dipergunakan untuk keperluan ... . Demikian untuk surat ini menjadi bahan pemeriksaan. Terima kasih.</textarea>';
											} elseif($array['JenisSurat'] === 'SKBM') {
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Betul bahwa orang tersebut di atas adalah Penduduk Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk, dan benar sampai saat ini belum pernah MENIKAH. Demikian Surat Keterangan ini dibuat untuk menjadikan periksa dan dipergunakan untuk seperlunya.</textarea>';
											} elseif($array['JenisSurat'] === 'SKBPKB') {
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Adalah benar kendaraan milik Pemohon, dan apabila nanti di kemudian hari ada masalah dengan Kendaaraan Bermotor tersebut diatas, pemohon sanggup dituntut sesuai dengan hukum yang berlaku dan tidak akan melibatkan yang ikut mengesahkan Surat Keterangan saya ini.
												Demikian surat keterangan ini saya di buat dengan sebenarnya dan dipergunakan sebagaimana mestinya.</textarea>';
											} elseif($array['JenisSurat'] === 'SKD') {
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Betul bahwa orang tersebut diatas adalah benar-benar terdaftar sebagai penduduk Kelurahan/Desa '.$login_desa.' dan berdomisili di Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk. Demikian surat keterangan ini di buat untuk dipergunakan seperlunya.</textarea>';
											} elseif($array['JenisSurat'] === 'SKKB') {
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Bahwa orang tersebut di atas benar-benar terdaftar sebagai Penduduk Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk dan sepanjang pengetahuan kami orang tersebut di atas selama bertempat tinggal di Kelurahan/Desa '.$login_desa.' berkelakuan baik dan tidak pernah tersangkut urusan Polisi. Surat keterangan ini dipergunakan untuk keperluan ... dan berlaku sejak dikeluarkannya surat ini hingga tanggal ... . Demikian untuk menjadikan bahan pemeriksaan.</textarea>';
											} elseif($array['JenisSurat'] === 'SKKT') {
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo '<p> Adalah benar-benar penduduk Kelurahan/Desa '.$login_desa.' yang memiliki/menguasi tanah berupa Akte Jual Beli yang tercatat pada buku Krawangan Kelurahan/Desa Letter C Desa Nomor Persil 20 b Klas 160 meter persegi, dengan batas-batas dan ketentuan sebagai berikut :<br />
												&nbsp; &nbsp;- Sebelah Utara, (Batas Sebelah Utara)<br />
												&nbsp; &nbsp;- Sebelah Timur, (Batas Sebelah Timur)<br />
												&nbsp; &nbsp;- Sebelah Selatan, (Batas Sebelah Selatan)<br />
												&nbsp; &nbsp;- Sebelah Barat, (Batas Sebelah Barat)</p></textarea>';
											} /*  elseif($array['JenisSurat'] === 'SKLHR'){
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo '&nbsp;&nbsp;&nbsp;Demikian surat keterangan ini dipergunakan sebagaimana mestinya.</textarea>';
											} elseif($array['JenisSurat'] === 'SKMTN'){
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo '&nbsp;&nbsp;&nbsp;Demikian surat keterangan ini dipergunakan sebagaimana mestinya.</textarea>';
											}  */ elseif($array['JenisSurat'] === 'SKP'){
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Alasan Pindah (Isi Dengan Uraian Mengenai Alasan Kepindahan)</textarea>';
											} elseif($array['JenisSurat'] === 'SKPS'){
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Betul orang tersebut di atas adalah penduduk Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk, orang tersebut di atas belum mempunyai/habis masa berlakunya Kartu Tanda Penduduk (KTP) asli dan masih dalam proses, maka diberikan surat keterangan ini sebagai Tanda Pengenal Sementara berlaku selama 3 (tiga) bulan terhitung mulai tanggal dikeluarkannya surat keterangan ini. Demikian surat keterangan ini diberikan untuk dipergunakan seperlunya.</textarea>';
											} elseif($array['JenisSurat'] === 'SKPTK'){
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Surat keterangan ini dipergunakan untuk mengajukan permohonan memperoleh beasiswa anaknya di Sekolah/Kampus ... atas nama ... .
												Demikian surat keterangan ini untuk dipergunakan sebagaimana mestinya.</textarea>';
											} elseif($array['JenisSurat'] === 'SKTM'){
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Adapun surat keterangan ini dipergunakan untuk keperluan ... . Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.</textarea>';
											} elseif($array['JenisSurat'] === 'SKU'){
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Orang tersebut memiliki usaha ... yang bergerak di bidang ... , yang bertempat di ... Kelurahan/Desa '.$login_desa.', Kecamatan Nganjuk, Kabupaten Nganjuk. Demikian surat keterangan ini dibuat untuk dipergunakan seperlunya.</textarea>';
											} elseif($array['JenisSurat'] === 'SKUM'){
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Adapun surat keterangan ini dibuat untuk kepentingan ... . Demikian surat keterangan ini dibuat untuk dipergunakan seperlunya.</textarea>';
											} elseif($array['JenisSurat'] === 'SKPW'){
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
												echo 'Dalam hal ini selaku Perwalian ( mewakili anak kandung di bawah umur ) bertindak dalam penyelesaian harta peninggalan dari Almarhum/almarhumah ... . Selanjutnya Surat Keterangan Perwalian ini saya buat dengan sadar dan benar tanpa ada tekanan dan paksaan dari manapun dan di kemudian hari bila ada ketidakbenarannya, saya bersedia dituntut dihadapan Pejabat yang berwenang.</textarea>';
											} /* elseif($array['JenisSurat'] === 'SKT'){
												echo 'Demikian untuk menjadikan perhatian sepenuhnya.';
											} */
											
											echo '<script type="text/javascript">
												var editor2 = CKEDITOR.replace("ckeditor2");
												CKFinder.setupCKEditor(editor2, "ckfinder/");    
											</script>
											</div>';
											
											if($array['JenisSurat'] === 'SKB') { 
											echo '<div class="form-group-material">
												<textarea type="text" id="ckeditor3" name="Uraian2" class="form-control" rows="10" placeholder="Uraian Penutup" required>
													<p>Demikian surat kesepakatan bersama ini dibuat dan ditandatangani oleh kedua belah pihak dihadapan para saksi dan para Petugas Polmas dan Petugas Kelurahan/Desa yang turut serta menandatangani kesepakatan ini.</p>
												</textarea>
												<script type="text/javascript">
													var editor3 = CKEDITOR.replace("ckeditor3");
													CKFinder.setupCKEditor(editor3, "ckfinder/");    
												</script>
											</div>';
											} elseif($array['JenisSurat'] === 'SKKT') {
											echo '<div class="form-group-material">
												<textarea type="text" id="ckeditor3" name="Uraian2" class="form-control" rows="10" placeholder="Uraian Penutup" required>
													<p>Keterangan :<br />
													1. Tanah tersebut benar-benar milik yang bersangkutan dan tidak dalam keadaan sengketa.<br />
													2. Tanah berasal dari Orang Tua.</br>
													3. Tanah belum terdaftar/didaftarkan Haknya ke BPN (belum diterbitkan SHM,SHGU, Lainnya).<br />
													4. Buku pendukung kepemilikan sementara saat ini berupa,<br />
													&nbsp; &nbsp; a. Surat Keterangan AKTE Jual Beli.<br />
													&nbsp; &nbsp; b. Surat Ketetapan IPEDA Perkotaan Tahun 1983 Nomor : 541/1169/N/O1/03/1983.<br /></p>
												</textarea>
												<script type="text/javascript">
													var editor3 = CKEDITOR.replace("ckeditor3");
													CKFinder.setupCKEditor(editor3, "ckfinder/");    
												</script>
											</div>';	
											}
											
										} else {
											if($array['JenisSurat'] === 'SKB' OR $array['JenisSurat'] === 'SKKT') { 
												$bagi_uraian = explode("#", $array3['Uraian']);
												echo '<div class="form-group-material">';
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>'.$bagi_uraian[0].'</textarea>';
												echo '<script type="text/javascript">
													var editor2 = CKEDITOR.replace("ckeditor2");
													CKFinder.setupCKEditor(editor2, "ckfinder/");    
												</script>
												</div>';
												
												echo '<div class="form-group-material">
													<textarea type="text" id="ckeditor3" name="Uraian2" class="form-control" rows="10" placeholder="Uraian Penutup" required>'.$bagi_uraian[1].'</textarea>
													<script type="text/javascript">
														var editor3 = CKEDITOR.replace("ckeditor3");
														CKFinder.setupCKEditor(editor3, "ckfinder/");    
													</script>
												</div>';
											} else {
												echo '<div class="form-group-material">';
												echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>'.$array3['Uraian'].'</textarea>';
												echo '<script type="text/javascript">
													var editor2 = CKEDITOR.replace("ckeditor2");
													CKFinder.setupCKEditor(editor2, "ckfinder/");    
												</script>
												</div>';
											}
										} ?>
										
									<button type="submit" class="btn btn-primary" name="SimpanPengurusan">Simpan</button>&nbsp;
									<button type="submit" class="btn btn-warning" name="ResetPengurusan">Reset</button>
									
									<?php if(isset($_POST['SimpanPengurusan'])){
										// deklarasi post data
										if($array['JenisSurat'] === 'SKBPKB'){
											$keperluan = $_POST['Keperluan'];
											$suport1 = htmlspecialchars($_POST['Isi1'])."#".htmlspecialchars($_POST['Isi2'])."#".htmlspecialchars($_POST['Isi3'])."#".htmlspecialchars($_POST['Isi4'])."#".htmlspecialchars($_POST['Isi5'])."#".htmlspecialchars($_POST['Isi6'])."#".htmlspecialchars($_POST['Isi7'])."#".htmlspecialchars($_POST['Isi8'])."#".htmlspecialchars($_POST['Isi9']);
										} elseif($array['JenisSurat'] === 'SKLHR'){ 
											$isi4 = explode("#", htmlspecialchars($_POST['Isi4']));
											$isi5 = explode("#", htmlspecialchars($_POST['Isi5']));
											$isi6 = explode("#", htmlspecialchars($_POST['Isi6']));
											$ayah = explode("#", htmlspecialchars($_POST['CariAyah']));
											$ibu = explode("#", htmlspecialchars($_POST['CariIbu']));
											
											$keperluan = $_POST['Keperluan'];
											$suport1 = htmlspecialchars($_POST['Isi1'])."#".htmlspecialchars($_POST['Isi2'])."#".htmlspecialchars($_POST['Isi3'])."#".$isi4[0]."#".$isi5[0]."#".$isi6[0]."#".htmlspecialchars($_POST['Isi7'])."#".htmlspecialchars($_POST['NIK'])."#".htmlspecialchars($_POST['AnakKe'])."#".htmlspecialchars($_POST['CariAyah'])."#".htmlspecialchars($_POST['CariIbu']);
										} elseif($array['JenisSurat'] === 'SKMTN'){ 
											$keperluan = $_POST['Keperluan'];
											$kematian = explode("#", htmlspecialchars($_POST['CariPenduduk']));
											$suport1 = htmlspecialchars($_POST['Isi1'])."#".htmlspecialchars($_POST['Isi2'])."#".htmlspecialchars($_POST['Isi3'])."#".htmlspecialchars($_POST['CariPenduduk']);
										} /* elseif($array['JenisSurat'] === 'SKT'){ 
											$keperluan = $_POST['Keperluan'];
											$suport5 = htmlspecialchars($_POST['Isi1'])."#".htmlspecialchars($_POST['Isi2'])."#".htmlspecialchars($_POST['Isi3'])."#".htmlspecialchars($_POST['Isi4'])."#".htmlspecialchars($_POST['Isi5'])."#".htmlspecialchars($_POST['Isi6']);
										}  */
										else {
											$keperluan = $_POST['Keperluan']; 
										}
										
										if($array['JenisSurat'] === 'SKB' OR $array['JenisSurat'] === 'SKKT'){
											$uraian = $_POST['Uraian']."#".$_POST['Uraian2'];
										} elseif($array['JenisSurat'] === 'SKLHR' OR $array['JenisSurat'] === 'SKMTN'){
											$uraian = null;
										} else {
											$uraian = $_POST['Uraian'];
										}
										
										$tanggal = date("Y-m-d"); 
										
										// cek apakah sudah ada permohonan
										$cek = @mysqli_query($koneksi, "SELECT * from trpengurusansurat where NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
										$num = @mysqli_num_rows($cek);
										if($num <> 0){
											// update transaksi pengurusan surat
											if($array['JenisSurat'] === 'SKLHR' OR $array['JenisSurat'] === 'SKMTN'  OR $array['JenisSurat'] === 'SKBPKB' /* OR $array['JenisSurat'] === 'SKT' */) {
												$UpdateTrPengurusan = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET Tanggal='".$tanggal."', Uraian='".$_POST['Uraian']."', Keperluan='".$keperluan."', Suport1='".$suport1."', Penandatanganan='".$TTD."' WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
											} elseif($array['JenisSurat'] === 'SKAW' OR $array['JenisSurat'] === 'SKPW') {
												$UpdateTrPengurusan = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET Tanggal='".$tanggal."', Uraian='".$uraian."', Keperluan='".$keperluan."', Penandatanganan='".$TTD."#".$TTD2."' WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
											} else {
												$UpdateTrPengurusan = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET Tanggal='".$tanggal."', Uraian='".$uraian."', Keperluan='".$keperluan."', Penandatanganan='".$TTD."' WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
											} 
											if($UpdateTrPengurusan){
												if($array['JenisSurat'] === 'SKLHR'){
													$lahir = @mysqli_query($koneksi, "UPDATE datapenduduk SET Nama = '".htmlspecialchars($_POST['Isi1'])."', TempatLahir = '".htmlspecialchars($_POST['Isi2'])."', TanggalLahir = '".$_POST['Isi3']."', JenisKelamin = '".$isi4[0]."', WNI = '".$isi5[0]."', Agama = '".$isi6[0]."', NamaIbu = '".$ibu[2]."', NamaAyah = '".$ayah[2]."', AnakKe = '".$_POST['AnakKe']."', Alamat = '".$_POST['Isi7']."' WHERE NIK = '".$_POST['NIK']."' AND KodeLokasi = '".$array['KodeLokasi']."' AND StatusPenduduk = 'ADA'");
													if($lahir){
														@mysqli_query($koneksi, "UPDATE trkelahiran SET TanggalInput = '".$_POST['Isi3']."', TanggalLahir = '".$_POST['Isi3']."', NamaAyah = '".$ayah[2]."', NamaIbu = '".$ibu[2]."', IDPendAyah = '".$ayah[1]."', IDPendIbu = '".$ibu[1]."', AnakKe = '".htmlspecialchars($_POST['AnakKe'])."', TempatDilahirkan = '".htmlspecialchars($_POST['Isi2'])."', Alamat = '".htmlspecialchars($_POST['Isi7'])."' WHERE KodeLokasi = '".$array['KodeLokasi']."' AND NoTrMohon = '".$array['NoTrMohon']."'");
													}
													
												} elseif($array['JenisSurat'] === 'SKMTN') {
													@mysqli_query($koneksi, "UPDATE datapenduduk SET StatusPenduduk = 'ADA' WHERE NIK = '".$matilama."' AND KodeLokasi = '".$array['KodeLokasi']."'");
													@mysqli_query($koneksi, "UPDATE datapenduduk SET StatusPenduduk = 'MENINGGAL' WHERE NIK = '".$kematian[0]."' AND KodeLokasi = '".$array['KodeLokasi']."'");
													@mysqli_query($koneksi, "UPDATE trkematian SET TanggalInput = '".$_POST['Isi1']."', TanggalMeninggal = '".$_POST['Isi1']."', TempatMeninggal = '".htmlspecialchars($_POST['Isi2'])."', SebabMeninggal = '".htmlspecialchars($_POST['Isi3'])."', IDPend = '".$kematian[4]."' WHERE KodeLokasi = '".$array['KodeLokasi']."' AND NoTrMohon = '".$array['NoTrMohon']."'");
													
												} elseif($array['JenisSurat'] === 'SKP') {
													@mysqli_query($koneksi, "UPDATE datapenduduk SET StatusPenduduk = 'PINDAH_KELUAR' WHERE NIK = '".$nik."' AND KodeLokasi = '".$array['KodeLokasi']."'");
													
													@mysqli_query($koneksi, "UPDATE mutasikeluar SET AlamatTujuan = '".$keperluan."', TanggalInput = '".date('Y-m-d')."', TanggalPindah = '".date('Y-m-d')."', AlasanPindah = '".$uraian."' WHERE KodeLokasi = '".$array['KodeLokasi']."' AND NoTrMohon = '".$array['NoTrMohon']."'");
													
													@mysqli_query($koneksi, "UPDATE anggotakeluar SET TanggalPindah = '".date('Y-m-d')."' WHERE KodeLokasi = '".$array['KodeLokasi']."' AND NoTrMohon = '".$array['NoTrMohon']."'");
										
												}
												echo '<script type="text/javascript">
												  sweetAlert({
													title: "Update Surat Sukses!",
													text: " Data Surat Telah Diperbaharui!",
													type: "success"
												  },
												  function () {
													window.location.href = "ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'";
												  });
												  </script>';
												/* echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil&kd='.base64_encode($array['NoTrMohon']).'"; </script>'; */	
											}
										} else { 
											// buat nomor transaksi pengurusan surat
											$Pengurusan = @mysqli_query($koneksi, "SELECT MAX(NoTransaksiSurat) as NoSurat FROM trpengurusansurat WHERE KodeLokasi='".$array['KodeLokasi']."' AND JenisSurat='".$array['JenisSurat']."'"); 
											$nums = @mysqli_num_rows($pengurusan); 
											while($CekData = @mysqli_fetch_array($Pengurusan)){
												if($nums === 0){ $kode = 1; } else { $kode = $CekData['NoSurat'] + 1; }
											}
											$NoSekarang = $kode; 
											
											// simpan transaksi pengurusan surat
											if($array['JenisSurat'] === 'SKLHR' OR $array['JenisSurat'] === 'SKMTN' /* OR $array['JenisSurat'] === 'SKT' */){
												$SimpanTrPengurusan = @mysqli_query($koneksi, "INSERT INTO trpengurusansurat(NoTransaksiSurat,Tanggal,Uraian,Keperluan,KodeLokasi,JenisSurat,NoTrMohon,StatusSurat,Suport1,Penandatanganan) VALUES ('$NoSekarang','".$tanggal."','".$uraian."','".$keperluan."','".$array['KodeLokasi']."','".$array['JenisSurat']."','".$array['NoTrMohon']."','0','".$suport1."','".$TTD."')");
											} elseif($array['JenisSurat'] === 'SKAW' OR $array['JenisSurat'] === 'SKPW') { 
												$SimpanTrPengurusan = @mysqli_query($koneksi, "INSERT INTO trpengurusansurat(NoTransaksiSurat,Tanggal,Uraian,Keperluan,KodeLokasi,JenisSurat,NoTrMohon,StatusSurat,Penandatanganan) VALUES ('$NoSekarang','".$tanggal."','".$uraian."','".$keperluan."','".$array['KodeLokasi']."','".$array['JenisSurat']."','".$array['NoTrMohon']."','0','".$TTD."#".$TTD2."')");
											} else { 
												$SimpanTrPengurusan = @mysqli_query($koneksi, "INSERT INTO trpengurusansurat(NoTransaksiSurat,Tanggal,Uraian,Keperluan,KodeLokasi,JenisSurat,NoTrMohon,StatusSurat,Penandatanganan) VALUES ('$NoSekarang','".$tanggal."','".$uraian."','".$keperluan."','".$array['KodeLokasi']."','".$array['JenisSurat']."','".$array['NoTrMohon']."','0','".$TTD."')");
											} 
											if($SimpanTrPengurusan){
												if($array['JenisSurat'] === 'SKLHR'){
													// membuat id otomatis
													$sql2 = @mysqli_query($koneksi, "SELECT MAX(IDPend) AS kode FROM datapenduduk WHERE KodeLokasi='$login_lokasi'"); 
													$data2 = @mysqli_fetch_array($sql2);
													$no_urut = $data2['kode']+1;
												
													$lahir = @mysqli_query($koneksi, "INSERT into datapenduduk(IDPend,Nama,NIK,TempatLahir,TanggalLahir,JenisKelamin,StatusKeluarga,StatusPenduduk,WNI,Agama,NamaIbu,NamaAyah,StatusPerkawinan,KodePendidikan,KodePekerjaan,PasswordPenduduk,KodeLokasi,AnakKe,Alamat) values ('$no_urut','".htmlspecialchars($_POST['Isi1'])."','".$_POST['NIK']."','".htmlspecialchars($_POST['Isi2'])."','".$_POST['Isi3']."','".$isi4[0]."','4','ADA','".$isi5[0]."','".$isi6[0]."','".$ibu[2]."','".$ayah[2]."','2','1',null,'".base64_encode('nganjuk12345')."','".$array['KodeLokasi']."','".htmlspecialchars($_POST['AnakKe'])."','".htmlspecialchars($_POST['Isi7'])."')");
													if($lahir){
														// membuat id otomatis
														$sql3 = @mysqli_query($koneksi, "SELECT MAX(KodeKelahiran) AS kode2 FROM trkelahiran WHERE KodeLokasi='$login_lokasi'"); 
														$data3 = @mysqli_fetch_array($sql3);
														$no_urut2 = $data3['kode2']+1;
														
														@mysqli_query($koneksi, "INSERT into trkelahiran(KodeKelahiran,TanggalInput,TanggalLahir,NamaAyah,NamaIbu,IDPendAyah,IDPendIbu,AnakKe,TempatDilahirkan,Alamat,KodeLokasi,NoTrMohon,IDPend,IsAktif) values ('$no_urut2','".$_POST['Isi3']."','".$_POST['Isi3']."','".$ayah[2]."','".$ibu[2]."','".$ayah[1]."','".$ibu[1]."','".htmlspecialchars($_POST['AnakKe'])."','".htmlspecialchars($_POST['Isi2'])."','".htmlspecialchars($_POST['Isi7'])."','".$array['KodeLokasi']."','".$array['NoTrMohon']."','".$no_urut."','1')");
													}
												} elseif($array['JenisSurat'] === 'SKMTN') {
													$mati = @mysqli_query($koneksi, "UPDATE datapenduduk SET StatusPenduduk = 'MENINGGAL' WHERE NIK = '".$kematian[0]."' AND KodeLokasi = '".$array['KodeLokasi']."'");
													if($mati){
														// membuat id otomatis
														$sql3 = @mysqli_query($koneksi, "SELECT MAX(KodeKematian) AS kode2 FROM trkematian WHERE KodeLokasi='$login_lokasi'"); 
														$data3 = @mysqli_fetch_array($sql3);
														$no_urut2 = $data3['kode2']+1;
														
														@mysqli_query($koneksi, "INSERT into trkematian(KodeKematian,TanggalInput,TanggalMeninggal,SebabMeninggal,TempatMeninggal,IdPendPelapor,IDPend,KodeLokasi,NoTrMohon,IsAktif) values ('$no_urut2','".$_POST['Isi1']."','".$_POST['Isi1']."','".htmlspecialchars($_POST['Isi3'])."','".htmlspecialchars($_POST['Isi2'])."','".$array['IDPend']."','".$kematian[4]."','".$array['KodeLokasi']."','".$array['NoTrMohon']."','1')");
													}
												} elseif($array['JenisSurat'] === 'SKP') {
													$pindah = @mysqli_query($koneksi, "UPDATE datapenduduk SET StatusPenduduk = 'PINDAH_KELUAR' WHERE NIK = '".$nik."' AND KodeLokasi = '".$array['KodeLokasi']."'");
													if($pindah){
														// membuat id otomatis
														$sql3 = @mysqli_query($koneksi, "SELECT MAX(KodeMutasiKeluar) AS kode2 FROM mutasikeluar WHERE KodeLokasi='$login_lokasi'"); 
														$data3 = @mysqli_fetch_array($sql3);
														$no_urut2 = $data3['kode2']+1;
														
														@mysqli_query($koneksi, "INSERT into mutasikeluar(KodeMutasiKeluar,AlamatTujuan,TanggalInput,TanggalPindah,AlasanPindah,IDPend,KodeLokasi,NoTrMohon,IsAktif) values ('$no_urut2','".$keperluan."','".date('Y-m-d')."','".date('Y-m-d')."','".$uraian."','".$array['IDPend']."','".$array['KodeLokasi']."','".$array['NoTrMohon']."','1')");
													}
												}
												echo '<script type="text/javascript">
												  sweetAlert({
													title: "Simpan Surat Sukses!",
													text: " Data Surat Telah Dibuat!",
													type: "success"
												  },
												  function () {
													window.location.href = "ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'";
												  });
												  </script>';
												/* echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil&kd='.base64_encode($array['NoTrMohon']).'"; </script>';	 */
											}
										}
									} 
									if(isset($_POST['ResetPengurusan'])){
										// cek apakah sudah ada permohonan
										$cek = @mysqli_query($koneksi, "SELECT * from trpengurusansurat where NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
										$num = @mysqli_num_rows($cek);
										if($num <> 0){
											$UpdateTrPengurusan = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET Uraian=null, Keperluan=null WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
											if($UpdateTrPengurusan){
												echo '<script type="text/javascript">
												  sweetAlert({
													title: "Reset Surat Sukses!",
													text: " Data Surat Telah Direset Ulang!",
													type: "success"
												  },
												  function () {
													window.location.href = "ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'";
												  });
												  </script>';
												/* echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil&kd='.base64_encode($array['NoTrMohon']).'"; </script>';	 */
											}
										}
									}
									?>
									</form>
								  </div>
								  
								  <?php if($array['JenisSurat'] === 'SKAW' OR $array['JenisSurat'] === 'SKB' /* OR $array['JenisSurat'] === 'SKMTN' OR $array['JenisSurat'] === 'SKLHR' */ OR $array['JenisSurat'] === 'SKP' OR $array['JenisSurat'] === 'SKPTK' OR $array['JenisSurat'] === 'SKPW' ){ ?>
								  <div class="col-sm-6">
								    <?php if($array['JenisSurat'] === 'SKAW'){
										echo '<h5>Data Ahli Waris</h5>';
									} elseif($array['JenisSurat'] === 'SKB'){
										echo '<h5>Data Pihak Terkait</h5>';
									} /*elseif($array['JenisSurat'] === 'SKLHR'){
										echo '<h5>Data Penduduk Lahir</h5>'; 
									} elseif($array['JenisSurat'] === 'SKMTN'){
										echo '<h5>Data Penduduk Meninggal</h5>';
									} */ elseif($array['JenisSurat'] === 'SKP'){
										echo '<h5>Data Penduduk Pindah</h5>';
									} elseif($array['JenisSurat'] === 'SKPTK'){
										echo '<h5>Data Tanggungan Keluarga</h5>';
									} elseif($array['JenisSurat'] === 'SKPW'){
										echo '<h5>Data Perwalian</h5>';
									} ?>
									
									<div class="form-group">
									  <?php $jsArray = "var arrData = new Array();\n"; ?>
									  <select id="CariPenduduk" name="CariPenduduk" onchange="changeValue(this.value)" style="width: 100% !important;">
									  <option value=""></option>
									  <?php
										$menu = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE StatusPenduduk = 'ADA' AND KodeLokasi='".$array['KodeLokasi']."' ORDER BY Nama");
										while($kode = mysqli_fetch_array($menu)){
										  echo "<option value=\"".$kode['NIK']."\" >".$kode['NIK']." : ".$kode['Nama']."</option>\n";
										    $menu2 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' AND NoUrut='".$kode['WNI']."'");
										    $kode2 = mysqli_fetch_array($menu2);
											$menu3 = mysqli_query($koneksi,"SELECT NamaPekerjaan FROM mstpekerjaan WHERE KodePekerjaan='".$kode['KodePekerjaan']."'");
										    $kode3 = mysqli_fetch_array($menu3);
											$menu4 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='JNS_KEL' AND NoUrut='".$kode['JenisKelamin']."'");
										    $kode4 = mysqli_fetch_array($menu4);
											$menu5 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='HUB_KELUARGA' AND NoUrut='".$kode['StatusKeluarga']."'");
										    $kode5 = mysqli_fetch_array($menu5);
											$menu6 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='STATUS_PERKAWINAN' AND NoUrut='".$kode['StatusPerkawinan']."'");
										    $kode6 = mysqli_fetch_array($menu6);
											$menu7 = mysqli_query($koneksi,"SELECT NamaPendidikan FROM mstpendidikan WHERE KodePendidikan='".$kode['KodePendidikan']."'");
										    $kode7 = mysqli_fetch_array($menu7);
											$menu8 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='AGAMA' AND NoUrut='".$kode['Agama']."'");
										    $kode8 = mysqli_fetch_array($menu8);
											$jsArray .= "arrData['".$kode['NIK']."'] = {
											  nik:'".addslashes($kode['NIK'])."',
											  nama:'".addslashes($kode['Nama'])."',
											  jenis:'".addslashes($kode4['Uraian'])."',
											  warga:'".addslashes($kode2['Uraian'])."',
											  tlahir:'".addslashes($kode['TempatLahir'])."', 
											  tgllahir:'".addslashes(date("Y-m-d", strtotime($kode['TanggalLahir'])))."',
											  kerja:'".addslashes($kode3['NamaPekerjaan'])."',
											  alamat:'".addslashes($kode['Alamat'])."',
											  status:'".addslashes($kode5['Uraian'])."',
											  kawin:'".addslashes($kode6['Uraian'])."',
											  didik:'".addslashes($kode7['NamaPendidikan'])."',
											  agama:'".addslashes($kode8['Uraian'])."',
											  idpend:'".addslashes($kode['IDPend'])."'
											};\n";
										}
									  ?>
									  </select>
									</div>
										
									</form>
									<div class="table-responsive">  
									<table class="table table-striped" width="100%">
									  <thead>
										<tr>
										  <?php if($array['JenisSurat'] === 'SKP'){
											 echo '<th colspan="1"></th>';
										  } else {
											 echo '<th colspan="2"></th>'; 
										  } ?>
										  
										  <th>NIK</th>
										  <th>Nama</th>
										  <th>Jenis Kelamin</th>
										  <th>Kewarganegaraan</th>
										  <th>Tempat Lahir</th>
										  <th>Tanggal Lahir</th>
										  <th>Agama</th>
										  <th>Pekerjaan</th>
										  <th>Pendidikan</th>
										  <th>Alamat</th>
										  <th>Status Keluarga</th>
										  <th>Status Perkawinan</th>
										</tr>
									  </thead>
									  <tbody>
									  	<?php $Support = "Suport"; $No = 1; $Total = 20;
										for($i=$No;$i<=$Total;$i++){
											$cek = @mysqli_query($koneksi, "SELECT ".$Support.$i." from trpengurusansurat where NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
											$arrayHasil = @mysqli_fetch_array($cek);
											if($arrayHasil[$Support.$i] != null){
												$bagi = explode("#",$arrayHasil[$Support.$i]);
												echo '<tr><form method="post">';
												if($array['JenisSurat'] === 'SKP'){
													echo '<td><button type="submit" class="btn btn-danger fa fa-close" name="HapusData'.$i.'" onclick="return confirm_penduduk()" title="Delete"/></td>';
												} else {
													echo '<td><button type="submit" class="btn btn-danger fa fa-close" name="HapusData'.$i.'" onclick="return confirm_penduduk()" title="Delete"/></td>';
													echo '<td><button type="submit" class="btn btn-primary fa fa-save" name="GantiData'.$i.'" title="Update"/></td>';
												}
												echo '<td><input type="text" name="suport'.$i.'ke10" class="form-control" style="width:200px;" placeholder="NIK" value="'.$bagi[10].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke0" class="form-control" style="width:200px;" placeholder="Nama" value="'.$bagi[0].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke1" class="form-control" style="width:100px;" placeholder="Jenis Kelamin" value="'.$bagi[1].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke2" class="form-control" style="width:100px;" placeholder="Kewarganegaraan" value="'.$bagi[2].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke3" class="form-control" style="width:250px;" placeholder="Tempat Lahir" value="'.$bagi[3].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke4" class="form-control" style="width:250px;" placeholder="Tanggal Lahir" value="'.$bagi[4].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke11" class="form-control" style="width:250px;" placeholder="Agama" value="'.$bagi[11].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke5" class="form-control" style="width:200px;" placeholder="Pekerjaan" value="'.$bagi[5].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke9" class="form-control" style="width:200px;" placeholder="Pendidikan" value="'.$bagi[9].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke6" class="form-control" style="width:400px;" placeholder="Alamat" value="'.$bagi[6].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke7" class="form-control" style="width:200px;" placeholder="Status Keluarga" value="'.$bagi[7].'"required></td>';
												echo '<td><input type="text" name="suport'.$i.'ke8" class="form-control" style="width:200px;" placeholder="Status Perkawinan" value="'.$bagi[8].'"required></td>';
												echo '<td><input type="hidden" name="suport'.$i.'ke12" class="form-control" style="width:200px;" placeholder="ID Penduduk" value="'.$bagi[12].'"required></td>';
												echo '</form></tr>';
												
												if(isset($_POST['GantiData'.$i])){
													$UpdatePenduduk = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET ".$Support.$i."='".htmlspecialchars($_POST['suport'.$i.'ke0'])."#".htmlspecialchars($_POST['suport'.$i.'ke1'])."#".htmlspecialchars($_POST['suport'.$i.'ke2'])."#".htmlspecialchars($_POST['suport'.$i.'ke3'])."#".htmlspecialchars($_POST['suport'.$i.'ke4'])."#".htmlspecialchars($_POST['suport'.$i.'ke5'])."#".htmlspecialchars($_POST['suport'.$i.'ke6'])."#".htmlspecialchars($_POST['suport'.$i.'ke7'])."#".htmlspecialchars($_POST['suport'.$i.'ke8'])."#".htmlspecialchars($_POST['suport'.$i.'ke9'])."#".htmlspecialchars($_POST['suport'.$i.'ke10'])."#".htmlspecialchars($_POST['suport'.$i.'ke11'])."#".htmlspecialchars($_POST['suport'.$i.'ke12'])."' WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
													if($UpdatePenduduk){
														echo '<script language="javascript">document.location="ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'"; </script>';	
													}
												}
												
												if(isset($_POST['HapusData'.$i])){
													$HapusPenduduk = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET ".$Support.$i."='". null ."' WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
													if($HapusPenduduk){
														if($array['JenisSurat'] === 'SKP') {
															// cari kode mutasi keluar
															$cari = @mysqli_query($koneksi, "SELECT * FROM mutasikeluar WHERE KodeLokasi='".$array['KodeLokasi']."' AND NoTrMohon='".$array['NoTrMohon']."'"); 
															$cari2 = @mysqli_fetch_array($cari);
															
															@mysqli_query($koneksi, "DELETE FROM anggotakeluar WHERE NIK = '".$bagi[10]."' AND KodeMutasiKeluar = '".$cari2['KodeMutasiKeluar']."' AND KodeLokasi = '".$array['KodeLokasi']."' AND NoTrMohon = '".$array['NoTrMohon']."'");
															
															@mysqli_query($koneksi, "UPDATE datapenduduk SET StatusPenduduk = 'ADA' WHERE NIK = '".$bagi[10]."' AND KodeLokasi = '".$array['KodeLokasi']."'");
														}
														echo '<script language="javascript">document.location="ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'"; </script>';	
													}
												}
												
												echo '<script>
												function confirm_penduduk() {
														var answer = confirm("Apakah Anda yakin untuk menghapus data ini ?")
														if (answer == true){
															window.location = "ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'";
															}
														else{
														alert("Terima Kasih !"); return false; 	
															}
														}
												</script>';
											} 
										}
										?>
										
										<tr>
										<form method="post">
										<?php
										if($array['JenisSurat'] === 'SKB'){
										$cek = @mysqli_query($koneksi, "SELECT Suport2 from trpengurusansurat where NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
										$arrayHasil = @mysqli_fetch_array($cek);
										if($arrayHasil['Suport2'] != null){
											echo '';
										} else {
										?>
										  <td colspan="2" align="right"><button type="submit" class="btn btn-primary fa fa-plus" name="MasukkanData" title="Update"/></td>
										  <td><input type="text" id="arrNik" name="waris11" class="form-control" style="width:200px;" placeholder="NIK" required></td>
										  <td><input type="text" id="arrNama" name="waris1" class="form-control" style="width:200px;" placeholder="Nama" required></td>
										  <td><input type="text" id="arrJenis" name="waris2" class="form-control" style="width:100px;" placeholder="Jenis Kelamin" required></td>
										  <td><input type="text" id="arrWarga" name="waris3" class="form-control" style="width:100px;" placeholder="Kewarganegaraan" required></td>
										  <td><input type="text" id="arrTlahir" name="waris4" class="form-control" style="width:250px;" placeholder="Tempat Lahir" required></td>
										  <td><input type="text" id="arrTgllahir" name="waris5" class="form-control" style="width:250px;" placeholder="Tanggal Lahir" required></td>
										  <td><input type="text" id="arrAgama" name="waris12" class="form-control" style="width:250px;" placeholder="Agama" required></td>
										  <td><input type="text" id="arrKerja" name="waris6" class="form-control" style="width:200px;" placeholder="Pekerjaan" required></td>
										  <td><input type="text" id="arrDidik" name="waris10" class="form-control" style="width:200px;" placeholder="Pendidikan" required></td>
										  <td><input type="text" id="arrAlamat" name="waris7" class="form-control" style="width:400px;" placeholder="Alamat" required></td>
										  <td><input type="text" id="arrStatus" name="waris8" class="form-control" style="width:200px;" placeholder="Status Keluarga" required></td>
										  <td><input type="text" id="arrKawin" name="waris9" class="form-control" style="width:200px;" placeholder="Status Perkawinan" required></td>
										  <td><input type="hidden" id="arrID" name="waris13" class="form-control" style="width:200px;" placeholder="ID Penduduk" required></td>
										  <script type="text/javascript">  
											<?php echo $jsArray; ?>
											function changeValue(id){
											document.getElementById('arrNama').value = arrData[id].nama;
											document.getElementById('arrJenis').value = arrData[id].jenis;
											document.getElementById('arrWarga').value = arrData[id].warga;
											document.getElementById('arrTlahir').value = arrData[id].tlahir;
											document.getElementById('arrTgllahir').value = arrData[id].tgllahir;
											document.getElementById('arrAgama').value = arrData[id].agama;
											document.getElementById('arrKerja').value = arrData[id].kerja;
											document.getElementById('arrDidik').value = arrData[id].didik;
											document.getElementById('arrAlamat').value = arrData[id].alamat;
											document.getElementById('arrStatus').value = arrData[id].status;
											document.getElementById('arrKawin').value = arrData[id].kawin;
											document.getElementById('arrNik').value = arrData[id].nik;
											document.getElementById('arrID').value = arrData[id].idpend;
											};
										  </script>
										<?php } } else { 
										if($array['JenisSurat'] === 'SKP'){
										  echo '<td colspan="1" align="right"><button type="submit" class="btn btn-primary fa fa-plus" name="MasukkanData" title="Update"/></td>';
										} else {
										  echo '<td colspan="2" align="right"><button type="submit" class="btn btn-primary fa fa-plus" name="MasukkanData" title="Update"/></td>';
										} ?>
										  <td><input type="text" id="arrNik" name="waris11" class="form-control" style="width:200px;" placeholder="NIK" required></td>
										  <td><input type="text" id="arrNama" name="waris1" class="form-control" style="width:200px;" placeholder="Nama" required></td>
										  <td><input type="text" id="arrJenis" name="waris2" class="form-control" style="width:100px;" placeholder="Jenis Kelamin" required></td>
										  <td><input type="text" id="arrWarga" name="waris3" class="form-control" style="width:100px;" placeholder="Kewarganegaraan" required></td>
										  <td><input type="text" id="arrTlahir" name="waris4" class="form-control" style="width:250px;" placeholder="Tempat Lahir" required></td>
										  <td><input type="text" id="arrTgllahir" name="waris5" class="form-control" style="width:250px;" placeholder="Tanggal Lahir" required></td>
										  <td><input type="text" id="arrAgama" name="waris12" class="form-control" style="width:250px;" placeholder="Agama" required></td>
										  <td><input type="text" id="arrKerja" name="waris6" class="form-control" style="width:200px;" placeholder="Pekerjaan" required></td>
										  <td><input type="text" id="arrDidik" name="waris10" class="form-control" style="width:200px;" placeholder="Pendidikan" required></td>
										  <td><input type="text" id="arrAlamat" name="waris7" class="form-control" style="width:400px;" placeholder="Alamat" required></td>
										  <td><input type="text" id="arrStatus" name="waris8" class="form-control" style="width:200px;" placeholder="Status Keluarga" required></td>
										  <td><input type="text" id="arrKawin" name="waris9" class="form-control" style="width:200px;" placeholder="Status Perkawinan" required></td>
										  <td><input type="hidden" id="arrID" name="waris13" class="form-control" style="width:200px;" placeholder="ID Penduduk" required></td>
										  <script type="text/javascript">  
											<?php echo $jsArray; ?>
											function changeValue(id){
											document.getElementById('arrNama').value = arrData[id].nama;
											document.getElementById('arrJenis').value = arrData[id].jenis;
											document.getElementById('arrWarga').value = arrData[id].warga;
											document.getElementById('arrTlahir').value = arrData[id].tlahir;
											document.getElementById('arrTgllahir').value = arrData[id].tgllahir;
											document.getElementById('arrAgama').value = arrData[id].agama;
											document.getElementById('arrKerja').value = arrData[id].kerja;
											document.getElementById('arrDidik').value = arrData[id].didik;
											document.getElementById('arrAlamat').value = arrData[id].alamat;
											document.getElementById('arrStatus').value = arrData[id].status;
											document.getElementById('arrKawin').value = arrData[id].kawin;
											document.getElementById('arrNik').value = arrData[id].nik;
											document.getElementById('arrID').value = arrData[id].idpend;
											};
										  </script>
										<?php } ?>
										  
										 	<?php if(isset($_POST['MasukkanData'])){
											$cek = @mysqli_query($koneksi, "SELECT * from trpengurusansurat where NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
											$num = @mysqli_num_rows($cek);
											if($num <> 0){
												$Support = "Suport"; $No = 1; $Total = 20;
												for($i=$No;$i<=$Total;$i++){
													$cek = @mysqli_query($koneksi, "SELECT ".$Support.$i." from trpengurusansurat where NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
													$arrayHasil = @mysqli_fetch_array($cek);
													if($arrayHasil[$Support.$i] == null){
														$UpdateTrPengurusan = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET ".$Support.$i."='".htmlspecialchars($_POST['waris1'])."#".htmlspecialchars($_POST['waris2'])."#".htmlspecialchars($_POST['waris3'])."#".htmlspecialchars($_POST['waris4'])."#".htmlspecialchars($_POST['waris5'])."#".htmlspecialchars($_POST['waris6'])."#".htmlspecialchars($_POST['waris7'])."#".htmlspecialchars($_POST['waris8'])."#".htmlspecialchars($_POST['waris9'])."#".htmlspecialchars($_POST['waris10'])."#".htmlspecialchars($_POST['waris11'])."#".htmlspecialchars($_POST['waris12'])."#".htmlspecialchars($_POST['waris13'])."' WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
														if($UpdateTrPengurusan){
															if($array['JenisSurat'] === 'SKP') {
															$pindah = @mysqli_query($koneksi, "UPDATE datapenduduk SET StatusPenduduk = 'PINDAH_KELUAR' WHERE NIK = '".htmlspecialchars($_POST['waris11'])."' AND KodeLokasi = '".$array['KodeLokasi']."'");
															if($pindah){
																// cari kode mutasi keluar
																$cari = @mysqli_query($koneksi, "SELECT * FROM mutasikeluar WHERE KodeLokasi='".$array['KodeLokasi']."' AND NoTrMohon='".$array['NoTrMohon']."'"); 
																$cari2 = @mysqli_fetch_array($cari);
																
																// isi tabel anggota keluar
																$sql3 = @mysqli_query($koneksi, "SELECT MAX(NoUrut) AS kode2 FROM anggotakeluar WHERE KodeLokasi='".$array['KodeLokasi']."' AND NoTrMohon='".$array['NoTrMohon']."'"); 
																$data3 = @mysqli_fetch_array($sql3);
																$no_urut2 = $data3['kode2']+1;
																
																@mysqli_query($koneksi, "INSERT into anggotakeluar(NoUrut,TanggalPindah,NIK,Nama,SHDK,KodeMutasiKeluar,KodeLokasi,NoTrMohon,IDPend,IsAktif) values ('$no_urut2','".date('Y-m-d')."','".htmlspecialchars($_POST['waris11'])."','".htmlspecialchars($_POST['waris1'])."','".htmlspecialchars($_POST['waris8'])."','".$cari2['KodeMutasiKeluar']."','".$array['KodeLokasi']."','".$array['NoTrMohon']."','".htmlspecialchars($_POST['waris13'])."','1')");
															}
															}
															echo '<script language="javascript">document.location="ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'"; </script>';	
														}
														exit;
													} else {
														echo "";
													}
												}
											} else {
												// Buat Transaksi Surat
												$Pengurusan = @mysqli_query($koneksi, "SELECT MAX(NoTransaksiSurat) as NoSurat FROM trpengurusansurat WHERE KodeLokasi='".$array['KodeLokasi']."' AND JenisSurat='".$array['JenisSurat']."'"); 
												$nums = @mysqli_num_rows($pengurusan); 
												while($CekData = @mysqli_fetch_array($Pengurusan)){
													if($nums === 0){ $kode = 1; } else { $kode = $CekData['NoSurat'] + 1; }
												}
												$NoSekarang = $kode; 
												
												// simpan transaksi pengurusan surat
												if($array['JenisSurat'] === 'SKAW' OR $array['JenisSurat'] === 'SKPW') {
													$SimpanTrPengurusan = @mysqli_query($koneksi, "INSERT INTO trpengurusansurat(NoTransaksiSurat,Tanggal,KodeLokasi,JenisSurat,NoTrMohon,StatusSurat,Penandatanganan) VALUES ('$NoSekarang','".date('Y-m-d')."','".$array['KodeLokasi']."','".$array['JenisSurat']."','".$array['NoTrMohon']."','0','".$TTD."#".$TTD2."')");
												} else {
													$SimpanTrPengurusan = @mysqli_query($koneksi, "INSERT INTO trpengurusansurat(NoTransaksiSurat,Tanggal,KodeLokasi,JenisSurat,NoTrMohon,StatusSurat,Penandatanganan) VALUES ('$NoSekarang','".date('Y-m-d')."','".$array['KodeLokasi']."','".$array['JenisSurat']."','".$array['NoTrMohon']."','0','".$TTD."')");
												}
												if($SimpanTrPengurusan){
													$Support = "Suport"; $No = 1; $Total = 20;
													for($i=$No;$i<=$Total;$i++){
														$cek = @mysqli_query($koneksi, "SELECT ".$Support.$i." from trpengurusansurat where NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
														$arrayHasil = @mysqli_fetch_array($cek);
														if($arrayHasil[$Support.$i] == null){
															$UpdateTrPengurusan = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET ".$Support.$i."='".htmlspecialchars($_POST['waris1'])."#".htmlspecialchars($_POST['waris2'])."#".htmlspecialchars($_POST['waris3'])."#".htmlspecialchars($_POST['waris4'])."#".htmlspecialchars($_POST['waris5'])."#".htmlspecialchars($_POST['waris6'])."#".htmlspecialchars($_POST['waris7'])."#".htmlspecialchars($_POST['waris8'])."#".htmlspecialchars($_POST['waris9'])."#".htmlspecialchars($_POST['waris10'])."#".htmlspecialchars($_POST['waris11'])."#".htmlspecialchars($_POST['waris12'])."#".htmlspecialchars($_POST['waris13'])."' WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
															if($UpdateTrPengurusan){
																if($array['JenisSurat'] === 'SKP') {
																$pindah = @mysqli_query($koneksi, "UPDATE datapenduduk SET StatusPenduduk = 'PINDAH_KELUAR' WHERE NIK = '".htmlspecialchars($_POST['waris11'])."' AND KodeLokasi = '".$array['KodeLokasi']."'");
																if($pindah){
																	// isi tabel mutasi keluar
																	$sql3 = @mysqli_query($koneksi, "SELECT MAX(KodeMutasiKeluar) AS kode2 FROM mutasikeluar WHERE KodeLokasi='$login_lokasi'"); 
																	$data3 = @mysqli_fetch_array($sql3);
																	$no_urut2 = $data3['kode2']+1;
																	
																	@mysqli_query($koneksi, "INSERT into mutasikeluar(KodeMutasiKeluar,IDPend,KodeLokasi,NoTrMohon,IsAktif) values ('$no_urut2','".$array['IDPend']."','".$array['KodeLokasi']."','".$array['NoTrMohon']."','1')");
																	
																	// isi tabel anggota keluar
																	$sql4 = @mysqli_query($koneksi, "SELECT MAX(NoUrut) AS kode3 FROM anggotakeluar WHERE KodeLokasi='".$array['KodeLokasi']."' AND NoTrMohon='".$array['NoTrMohon']."'"); 
																	$data4 = @mysqli_fetch_array($sql4);
																	$no_urut3 = $data4['kode3']+1;
																	
																	@mysqli_query($koneksi, "INSERT into anggotakeluar(NoUrut,TanggalPindah,NIK,Nama,SHDK,KodeMutasiKeluar,KodeLokasi,NoTrMohon,IDPend,IsAktif) values ('$no_urut3','".date('Y-m-d')."','".htmlspecialchars($_POST['waris11'])."','".htmlspecialchars($_POST['waris1'])."','".htmlspecialchars($_POST['waris8'])."','$no_urut2','".$array['KodeLokasi']."','".$array['NoTrMohon']."','".htmlspecialchars($_POST['waris13'])."','1')");
																}
																}
																echo '<script language="javascript">document.location="ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'"; </script>';	
															}
															exit;
														} else {
															echo "";
														}
													}
													echo '<script language="javascript">document.location="ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'"; </script>';	
												}
											}
											}
											?>
										</form>
										</tr>
									  </tbody>
									</table>
									</div>
									</form>
								
								  </div>
								  <?php } else { echo ""; } ?>
								</div>
							</div>
						 
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
	<!-- Modal Popup untuk Edit--> 
	<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
	
	<div id="ModalEditDokumen" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
	<div id="ModalEditTracking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="komponen/js/front.js"></script>
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#time1').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time2').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time7').Zebra_DatePicker({format: 'Y-m-d'});
			//$('#Datetime2').Zebra_DatePicker({format: 'Y-m-d H:i', direction: 1});
		});
	</script>
	<script src="../library/select2master/js/select2.min.js"></script>
	  <script>
	  $(document).ready(function () {
		$("#CariPenduduk").select2({
			placeholder: "NIK atau Nama Penduduk"
		});
	  });
	  $(document).ready(function () {
		$("#CariAyah").select2({
			placeholder: "NIK atau Nama Ayah"
		});
	  });
	  $(document).ready(function () {
		$("#CariIbu").select2({
			placeholder: "NIK atau Nama Ibu"
		});
	  });
	  </script>
	<!-- Javascript untuk popup modal Edit--> 
	<script type="text/javascript">
	   // open modal lihat dokumen
	   $(document).ready(function () {
	   $(".open_modal").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  	   $.ajax({
					   url: "LihatDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		// open modal lihat dokumen
	   $(document).ready(function () {
	   $(".open_modal_input").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  	   $.ajax({
					   url: "CariPenduduk.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		// open modal lihat progress
	   $(document).ready(function () {
	   $(".open_modal_tracking").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  var user  = $(this).data("user");
		  	   $.ajax({
					   url: "TrackingDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat,User: user},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
	  /* // open modal unggah dokumensyarat 
		$(document).ready(function () {
	   $(".open_modal").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var nm_syarat = $(this).data("namasyarat");
		  var kd_syarat = $(this).data("kodesyarat");
			   $.ajax({
					   url: "UnggahDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,NamaSyarat: nm_syarat,KodeSyarat: kd_syarat},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		//open modal dokumensyarat
		$(document).ready(function () {
	    $(".open_modal_dokumen").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var nm_syarat = $(this).data("namasyarat");
		  var kd_syarat = $(this).data("kodesyarat");
			   $.ajax({
					   url: "LihatDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,NamaSyarat: nm_syarat,KodeSyarat: kd_syarat},
					   success: function (ajaxData){
					   $("#ModalEditDokumen").html(ajaxData);
					   $("#ModalEditDokumen").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		//open modal tracking
		$(document).ready(function () {
	    $(".open_modal_tracking").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var nm_surat = $(this).data("namasurat");
			   $.ajax({
					   url: "TrackingDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,NamaiSurat:nm_surat},
					   success: function (ajaxData){
					   $("#ModalEditTracking").html(ajaxData);
					   $("#ModalEditTracking").modal('show',{backdrop: 'true'});
				   }
				});
			});
		}); */
	</script>
	
	<script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>
	
	<?php
	if(isset($_POST['BuatSurat'])){
		// cek apakah sudah ada permohonan
		$cek = @mysqli_query($koneksi, "SELECT * from trpermohonanmasy where IDPend='$id_penduduk_aktif' AND JenisSurat='".$_POST['JenisSurat']."' AND (StatusPermohonan != 'REJECTED' OR StatusPermohonan != 'DONE')");
		$num = @mysqli_num_rows($cek);
		if($num > 0){
			echo '<script type="text/javascript">swal( "Permohonan Sudah Ada!", " Silahkan Tunggu Permohonan Selesai Diproses ", "error" ); </script>';
		}else{ 
			// membuat id otomatis
			$sql = @mysqli_query($koneksi, "SELECT MAX(RIGHT(NoTrMohon,8)) AS kode FROM trpermohonanmasy WHERE KodeLokasi='$kode_lokasi_aktif' AND LEFT(NoTrMohon,7)='MH-$Tahun'"); 
			$nums = @mysqli_num_rows($sql); 
			while($data = @mysqli_fetch_array($sql)){
			if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
			}
			// membuat kode user
			$bikin_kode = str_pad($kode, 8, "0", STR_PAD_LEFT);
			$kode_jadi = "MH-".$Tahun."-".$bikin_kode;
			
			//simpan trpermohonan masyarakat
			$SimpanData = @mysqli_query($koneksi, "INSERT INTO trpermohonanmasy (NoTrMohon,KodeLokasi,JenisSurat,IDPend,StatusPermohonan)VALUES('$kode_jadi','$kode_lokasi_aktif','".$_POST['JenisSurat']."','$id_penduduk_aktif','PraWAITING')"); 
			
			echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil"; </script>';	
		}
	}
	
	if(isset($_POST['GantiSurat'])){
		$cek = @mysqli_query($koneksi, "SELECT * from trpermohonanmasy where IDPend='$id_penduduk_aktif' AND JenisSurat='".$_POST['JenisSurat']."' AND (StatusPermohonan != 'REJECTED' OR StatusPermohonan != 'DONE')");
		$num = @mysqli_num_rows($cek);
		if($num > 0){
			echo '<script type="text/javascript">swal( "Permohonan Sudah Ada!", " Silahkan Tunggu Permohonan Selesai Diproses ", "error" ); </script>';
		}else{ 
			//hapus dokumensyarat mohon
			$HapusDokumenSyarat = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			if($HapusDokumenSyarat){
				//update trmohonmsy
				$UpdateData = @mysqli_query($koneksi, "UPDATE trpermohonanmasy SET JenisSurat='".$_POST['JenisSurat']."' WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi'"); 
			}
			
			echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil"; </script>';
		}
	}
	
	if(isset($_POST['KirimPermohonan'])){
		$cek = @mysqli_query($koneksi, "SELECT * from dokumensyaratmohon where NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
		$num = @mysqli_num_rows($cek);
		if($num === 0){
			echo '<script type="text/javascript">swal( "Maaf!", " Dokumen Persyaratan Anda Belum Lengkap ", "error" ); </script>';
		}else{ 
		
			$JamSekarang = date('H:i:s');
			$TanggalSekarang = date('Y-m-d');
			$JamLayananKantor = date('Y-m-d').' 12:00:00'; //jam tutup lyn hari ini
			
			$NamaHariIni = date('D', strtotime($HariIni)); //nama hari
			
			if(strtotime($HariIni) > strtotime($JamLayananKantor)){ //jika input diatas jam 12 siang maka diproses besok
				//Cek Ada berapa banyak hari sabtu
				$TglLibur = array();
				for($i=0; $i < $Standar_Waktu; $i++){
					$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
					$NmHari = date('D', strtotime($TglHari));
					$TglLibur[]=$NmHari;
				}
				//cari ada berapa banayak hari saturday
				$Count = array_count_values($TglLibur); 
				$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
				
				$TotalLamaPelayanan = $Standar_Waktu+$JmlTglLibur;
				$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
				
			}else{ //surat diproses hari ini
				//Cek Ada berapa banyak hari sabtu
				$StandarWaktuBaru  = $Standar_Waktu-1;
				$TglLibur = array();
				for($i=0; $i < $StandarWaktuBaru; $i++){
					$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
					$NmHari = date('D', strtotime($TglHari));
					$TglLibur[]=$NmHari;
				}
				//cari ada berapa banayak hari saturday
				$Count = array_count_values($TglLibur); 
				$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
				
				$TotalLamaPelayanan = $StandarWaktuBaru+$JmlTglLibur;
				$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
			}
			 
			//insert to progress surat & update trpermohonanmsy
			$UpdateTrPermohonan = @mysqli_query($koneksi, "UPDATE trpermohonanmasy SET TglPermohonan='$HariIni',PrediksiTglSelesai='$PrediksiTglSelesai', Keterangan1='".$_POST['Ket']."', StatusPermohonan='WAITING' WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			
			if($UpdateTrPermohonan){
				$InsertData = @mysqli_query($koneksi, "INSERT INTO progresssurat (NoUrutProgress,NoTrMohon,KodeLokasi,StatusProgress,Waktu,Tanggal,SendTo,UserSender,IsSend,WaktuSend,TglSend,IsConfirmed)
				VALUES('1','$No_Transaksi','$kode_lokasi_aktif','Pemohon/Masyarakat','$JamSekarang','$TanggalSekarang','Front Office (DESA)','$id_penduduk_aktif','1','$JamSekarang','$HariIni','0')");
				
				if($InsertData){
					echo '<script type="text/javascript">
					  sweetAlert({
						title: "Permohonan Terkirim!",
						text: " Permohonan surat akan diproses pada jam kerja aktif!",
						type: "success"
					  },
					  function () {
						window.location.href = "ProsesSurat.php";
					  });
					  </script>';
				}else{
					echo '<script type="text/javascript">
						  sweetAlert({
							title: "Simpan Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
				} 
			}
		}
	}
	
	if(isset($_POST['BatalkanPermohonan'])){
		//hapus dokumensyarat mohon
		$HapusDokumenSyarat = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
		if($HapusDokumenSyarat){
			$HapusTrPermohonan = @mysqli_query($koneksi, "DELETE FROM trpermohonanmasy WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			if($HapusTrPermohonan){
				echo '<script type="text/javascript">
				  sweetAlert({
					title: "Hapus Data Berhasil",
					text: " ",
					type: "success"
				  },
				  function () {
					window.location.href = "ProsesSurat.php";
				  });
				  </script>';
			}else{
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "ProsesSurat.php";
					  });
					  </script>';
			}
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "ProsesSurat.php";
					  });
					  </script>';
		}
	}
	
	//Hapus Transaksi
	if(base64_decode(@$_GET['aksi'])==='HapusTransaksi'){
		//cek apakah status permohonan waiting
		$QueryCekStatus = @mysqli_query($koneksi, "SELECT NoTrMohon FROM trpermohonanmasy WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."' AND StatusPermohonan='WAITING'"); 
		$numCekStatus = @mysqli_num_rows($QueryCekData); 
		if($numCekStatus > 0){
			$HapusData = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'"); 
			
			if($HapusData){
				$HapusProgress = @mysqli_query($koneksi, "DELETE FROM progresssurat WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'"); 
				
				if($HapusProgress){
					$HapusTrans = @mysqli_query($koneksi, "DELETE FROM trpermohonanmasy WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'"); 
					
					if($HapusProgress){
						echo '<script language="javascript">document.location="ProsesSurat.php"; </script>';
					}else{
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
					}
				}else{
					echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
				}
			}else{
				echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
			}
		}else{
			echo '<script type="text/javascript">
			  sweetAlert({
				title: "Hapus Data Gagal!",
				text: " Permohonan Anda sedang dalam prosess verifikasi ",
				type: "error"
			  },
			  function () {
				window.location.href = "ProsesSurat.php";
			  });
			  </script>';
		}
	}
	
	
	?>
  </body>
</html>