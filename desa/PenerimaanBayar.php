<?php
include 'akses.php';
include '../library/tgl-indo.php';

$fitur_id = 11;
include '../library/lock-menu.php';

$Page = 'Pajak';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM masterobjekpajak WHERE NoObjekPajak='".base64_decode($_GET['id'])."' AND KodeLokasi='".base64_decode($_GET['kode'])."'");
	$RowData = mysqli_fetch_array($Edit);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.blue.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
		
		
		/* Style the form */
		#regForm {
		  background-color: #ffffff;
		  margin: 20px auto;
		  /* padding: 40px; */
		  width: 100%;
		  min-width: 300px;
		}

		/* Style the input fields */
		input {
		  padding: 10px;
		  width: 100%;
		  font-size: 17px;
		  font-family: Raleway;
		  border: 1px solid #aaaaaa;
		}

		/* Mark input boxes that gets an error on validation: */
		input.invalid {
		  background-color: #ffdddd;
		}

		/* Hide all steps by default: */
		.tab {
		  display: none;
		}

		/* Make circles that indicate the steps of the form: */
		.step {
		  height: 15px;
		  width: 15px;
		  margin: 0 2px;
		  background-color: #bbbbbb;
		  border: none; 
		  border-radius: 50%;
		  display: inline-block;
		  opacity: 0.5;
		}

		/* Mark the active step: */
		.step.active {
		  opacity: 1;
		}

		/* Mark the steps that are finished and valid: */
		.step.finish {
		  background-color: #4CAF50;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Penerimaan Objek Pajak</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<!-- <ul class="nav nav-pills">
						<!-- <li <?php /*if(@$id==null){echo 'class="active"';} */?>>
							<a href="#home-pills" data-toggle="tab"><span class="btn btn-primary">Data NOP</span></a>&nbsp;
						</li>
						<li>
							<a href="BagiPajak.php"><span class="btn btn-primary">Distribusi SPPT</span></a>&nbsp;
						</li>
						<!-- <li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary"><?php /* echo $Sebutan; */ ?></span></a>
						</li> --
					</ul><br/> -->
				  <div class="card">
					<div class="tab-content">
						<div class="tab-pane fade <?php if(@$_GET['id']==null){ echo 'in active show'; }?>" id="home-pills">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Objek Pajak</h3>
							</div>
							<div class="card-body">							  
								<div class="col-lg-8">
									<form method="post" action="">
										<div class="form-group input-group">
											<input type="text" name="tahun" id="time1" class="form-control" value="<?php echo @$_REQUEST['tahun']; ?>" placeholder="Pilih Tahun" required>&nbsp;&nbsp;
											<input type="text" name="keyword" class="form-control" placeholder="Nama..." value="<?php echo @$_REQUEST['keyword']; ?>">
											<span class="input-group-btn">
												<button class="btn btn-primary" type="submit">Cari</button>
											</span>
										</div>
									</form>
								</div>
							  <div class="table-responsive">  
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>No</th>
									  <th>NOP</th>
									  <th>Tahun</th>
									  <th>Alamat OP</th>
									  <!-- <th>Luas Bumi/Bangunan</th> -->
									  <th>Pokok</th>
									  <th>Lokasi</th>
									  <th>Petugas</th>
									  <th>Aksi</th>
									</tr>
								  </thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										$tahun= @$_REQUEST['tahun'];
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword= @$_REQUEST['keyword'];
											if(@$_REQUEST['tahun'] === null){
												$reload = "MasterPajak.php?pagination=true&keyword=$keyword";
												$sql =  "SELECT * FROM masterobjekpajak a JOIN mstlokasi b ON b.KodeLokasi=a.KodeLokasi JOIN trbayarpajak c ON (c.NoObjekPajak,c.Tahun)=(a.NoObjekPajak,a.Tahun) WHERE c.UserName = '$login_id' AND a.Tahun = '".date('Y')."' AND (a.NoObjekPajak LIKE '%$keyword%' OR a.NamaWP LIKE '%$keyword%') AND a.KodeLokasi ='".$login_lokasi."' ORDER BY a.NoObjekPajak";
											} else {
												$reload = "MasterPajak.php?pagination=true&keyword=$keyword&tahun=$tahun";
												$sql =  "SELECT * FROM masterobjekpajak a JOIN mstlokasi b ON b.KodeLokasi=a.KodeLokasi JOIN trbayarpajak c ON (c.NoObjekPajak,c.Tahun)=(a.NoObjekPajak,a.Tahun) WHERE c.UserName = '$login_id' AND a.Tahun = '$tahun' AND (a.NoObjekPajak LIKE '%$keyword%' OR a.NamaWP LIKE '%$keyword%') AND a.KodeLokasi ='".$login_lokasi."' ORDER BY a.NoObjekPajak";
											}
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											if(@$_REQUEST['tahun'] === null){
												$reload = "MasterPajak.php?pagination=true";
												$sql =  "SELECT * FROM masterobjekpajak a JOIN mstlokasi b ON b.KodeLokasi=a.KodeLokasi JOIN trbayarpajak c ON (c.NoObjekPajak,c.Tahun)=(a.NoObjekPajak,a.Tahun) WHERE c.UserName = '$login_id' AND a.Tahun = '".date('Y')."' AND a.KodeLokasi ='".$login_lokasi."' ORDER BY a.NoObjekPajak";
											} else {
												$reload = "MasterPajak.php?pagination=true&tahun=$tahun";
												$sql =  "SELECT * FROM masterobjekpajak a JOIN mstlokasi b ON b.KodeLokasi=a.KodeLokasi JOIN trbayarpajak c ON (c.NoObjekPajak,c.Tahun)=(a.NoObjekPajak,a.Tahun) WHERE c.UserName = '$login_id' AND a.Tahun = '$tahun' AND a.KodeLokasi ='".$login_lokasi."' ORDER BY a.NoObjekPajak";
											}
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 20; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php if($tcount == null OR $tcount === 0){
											echo '<tr class="odd gradeX"><td colspan="9" align="center"><br><h5>Tidak Ada Data</h5><br></td></tr>';
										} else {
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td>
												<?php echo $data ['NoObjekPajak']."<br><strong>".strtoupper($data['NamaWP'])."</strong>"; ?>
											</td>
											<td>
												<?php echo $data ['Tahun']; ?>
											</td>
											<td>
												<?php echo ucwords($data ['AlamatOP']); ?>
											</td>
											<!-- <td align="right">
												<?php /* echo number_format($data ['LuasBumi'])."<br>".number_format($data ['LuasBangunan']); */ ?>
											</td> -->
											<td align="right">
												<?php echo "<strong>".number_format($data ['Pokok'])."</strong>"; ?>
											</td>
											<td>
												<?php $CariData = @mysqli_query($koneksi, "SELECT NamaDesa FROM mstdesa WHERE KodeDesa= '".$data['KodeDesa']."'"); 
												$array = @mysqli_fetch_array($CariData);
												echo $array ['NamaDesa']; ?>
											</td>
											<td>
												<?php if($data['UserName'] == null OR $data['UserName'] === ''){ echo '-'; } else { echo ucwords($data ['UserName']); } ?>
											</td>
											<td>	
												<?php if($data['IsBayar'] === '1'){ 
													echo '<a href="#"><span class="btn btn-sm btn-success" title="Terbayar">Terbayar</span><br>';
													if($data['Tanggal'] == null){
														echo 'Tidak Ada Tanggal';
													} else {
														echo 'Pada '.TanggalIndo($data['Tanggal']);
													}
												} else { ?>
												<a href="#" class='open_modal_verif' data-nop='<?php echo $data['NoObjekPajak'];?>' data-kodelokasi='<?php echo $data['KodeLokasi'];?>' data-tahun='<?php echo $data['Tahun'];?>'>
												<span class="btn btn-sm btn-primary fa fa-refresh" title="Terima Bayar"></span></a>
												<?php } ?>
												<!-- <a href="MasterPajak.php?id=<?php /* echo base64_encode($data['NoObjekPajak']); ?>&kode=<?php echo base64_encode($data['KodeLokasi']); */ ?>"><span class="btn btn-sm btn-warning fa fa-edit" title="Edit"></span></a>
												<a href="LayananSurat.php"><span class="btn btn-sm btn-info fa fa-search" title="Detail"></span></a> -->
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										}
										?>
									</tbody>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							  </div>
							</div>
						</div>
						<!-- <div class="tab-pane fade <?php /* if(@$_GET['id']!=null){ echo 'in active show'; }?>" id="tambah-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Data NOP<?php //echo $Sebutan; ?></h3>
							</div>
						<form method="post" action="">
							<div class="card-body">
								<div class="row">
								  <div class="col-lg-6">
										<div class="form-group-material">
										  <input type="text" name="NOP" class="form-control" placeholder="NIK" value="<?php echo @$RowData['NoObjekPajak'];?>" readonly>
										  <input type="hidden" name="KodeLokasi" class="form-control" placeholder="Kode Lokasi" value="<?php echo @$RowData['KodeLokasi'];?>" readonly>
										  <input type="hidden" name="Tahun" class="form-control" placeholder="Tahun" value="<?php echo @$RowData['Tahun'];?>" readonly>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NamaWP" class="form-control" placeholder="Nama WP" value="<?php echo @$RowData['NamaWP'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="AlamatWP" class="form-control" placeholder="Alamat WP" value="<?php echo @$RowData['AlamatWP'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="AlamatOP" class="form-control" placeholder="Alamat OP" value="<?php echo @$RowData['AlamatOP'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Bumi" class="form-control" placeholder="Luas Bumi" value="<?php echo @$RowData['LuasBumi'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Bangunan" class="form-control" placeholder="Luas Bangunan" value="<?php echo @$RowData['LuasBangunan'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Pokok" class="form-control" placeholder="Pokok" value="<?php echo @$RowData['Pokok']; */ ?>" required>
										</div>
										
									</div>
									
								  </div>
								  &nbsp;<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button>
								</div>
							</div> 
						</form>								
							<?php
							/* if(@$_GET['id']==null){
								echo '<button type="submit" class="btn btn-primary" name="Simpan">Simpan</button>';
							}else{
								echo '<input type="hidden" name="KodeJabatan" value="'.$RowData['KodeJabatan'].'"> ';
								echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
								echo '<a href="UserLogin.php"><span class="btn btn-warning">Batalkan</span></a>';
							} */
							?>
						</div> -->
						
						<!-- <div class="tab-pane fade" id="import-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Import Data Penduduk<?php /* //echo $Sebutan; ?></h3>
							</div>
							<form method="post" action="">
							<div class="card-body">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group-material">
											<label>Import File : </label>
											<div class="input-group">
											<input type="file" name="NamaFile" class="form-control" placeholder="Nama File" value="<?php echo $namaFile ?>" readonly>
											<span class="input-group-btn">
												<button type="submit" class="btn btn-primary" name="SubmitFile">Proses</button>
											</span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<?php if(isset($_POST['SubmitFile'])) {
										echo '<div class="table-responsive">';
										
										ini_set("display_errors",1);
										require_once '../library/excel-reader.php';
										
										$data = new Spreadsheet_Excel_Reader("../library/export-excel/$newfilename");
										$htmls='<table class="table table-striped">';
										for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
										{	
											if(count($data->sheets[$i][cells])>0) // checking sheet not empty
											{
												for($j=1;$j<=count($data->sheets[$i][cells]);$j++) // loop used to get each row of the sheet
												{ 
		 
												$htmls.="<tr>";
												for($k=1;$k<=count($data->sheets[$i][cells][$j]);$k++) // This loop is created to get data in a table format.
												{
													$htmls.="<td>";
													$htmls.=$data->sheets[$i][cells][$j][$k];
													$htmls.="</td>";
												}
												$data->sheets[$i][cells][$j][1];
												$no = 1;
												$NamaPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][0]);
												$NIKPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][1]);
												$TLahirPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][2]);
												$TglLahirPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][3]);
												$JKPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][4]);
												$NamaIbuPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][5]);
												$NamaAyahPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][6]);
												$NoKKPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][7]);			
												$NoUrutKKPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][8]);
												$AlamatPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][9]);
												
												$htmls.="</tr>";
												$no++;
												}
											}
										echo "<input type='hidden' name='importData' value='$newfilename'>";
	
										}
										$htmls.="</table>";
										echo $htmls;
										echo "";
										echo '<button type="submit" class="btn btn-primary" name="SubmitImport">Import</button>';

										} */
										?>
									</div>
								</div>
							</div>
							</form>
						</div>
					</div> -->
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
	
	<!-- Modal Popup untuk Edit--> 
	<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>

    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		 $("#KodeLokasi").change(function(){
			var KodeLokasi = $("#KodeLokasi").val();
			$.ajax({
				url: "../library/ambil-desa.php",
				data: "KodeLokasi="+KodeLokasi,
				cache: false,
				success: function(msg){
					$("#KodeDusun").html(msg);
				}
			});
		  });
		});
	</script>
	
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#time1').Zebra_DatePicker({format: 'Y'});
			$('#time2').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time7').Zebra_DatePicker({format: 'Y-m-d'});
			//$('#Datetime2').Zebra_DatePicker({format: 'Y-m-d H:i', direction: 1});
		});
		
		 // open modal lihat dokumen
	   $(document).ready(function () {
	   $(".open_modal_verif").click(function(e) {
		  var nop = $(this).data("nop");
		  var kode_lok = $(this).data("kodelokasi");
		  var tahun  = $(this).data("tahun");
		  	   $.ajax({
					   url: "VerifikasiBayar.php",
					   type: "GET",
					   data : {Nop: nop,KodeLokasi: kode_lok,Tahun: tahun},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
	
	<?php
	/* if(isset($_POST['Simpan'])){
		echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		 */
		/* include ('../library/kode-log-server.php');
		//kode jabatan
		$sql_jbt = mysqli_query($koneksi,'SELECT RIGHT(KodeJabatan,5) AS kode FROM mstjabatan WHERE KodeLokasi="'.$login_lokasi.'" ORDER BY KodeJabatan DESC LIMIT 1');  
		$nums_jbt = mysqli_num_rows($sql_jbt);
		 
		if($nums_jbt <> 0)
		 {
		 $data_jbt = mysqli_fetch_array($sql_jbt);
		 $kode_jbt = $data_jbt['kode'] + 1;
		 }else
		 {
		 $kode_jbt = 1;
		 }
		 
		//mulai bikin kode
		 $bikin_kode_jbt = str_pad($kode_jbt, 5, "0", STR_PAD_LEFT);
		 $kode_jadi_jbt = "JBT-".$bikin_kode_jbt;
		
		$query = mysqli_query($koneksi,"INSERT into mstjabatan (KodeJabatan,NamaJabatan,Keterangan,KodeLokasi) 
		VALUES ('$kode_jadi_jbt','".$_POST['Nama']."','".$_POST['Keterangan']."','$login_lokasi')");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Master Jabatan : ".$_POST['Nama']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		} */
	/* } */
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		$nop = htmlspecialchars($_POST['NOP']); $nama = htmlspecialchars($_POST['NamaWP']); $lokasi = htmlspecialchars($_POST['KodeLokasi']);
		$alamatWP = htmlspecialchars($_POST['AlamatWP']); $alamatOP = htmlspecialchars($_POST['AlamatOP']); $tahun = htmlspecialchars($_POST['Tahun']); 
		$bumi = htmlspecialchars($_POST['Bumi']); $bangunan = htmlspecialchars($_POST['Bangunan']); $pokok = htmlspecialchars($_POST['Pokok']);
		
		$query = mysqli_query($koneksi,"UPDATE masterobjekpajak SET NamaWP = '$nama', AlamatWP = '$alamatWP', AlamatOP = '$alamatOP', LuasBumi = '$bumi', LuasBangunan = '$bangunan', Pokok = '$pokok' WHERE NoObjekPajak = '$nop' AND Tahun = '$tahun' AND KodeLokasi = '$lokasi'");
		
		if($query){
			echo '<script language="javascript">document.location="MasterPajak.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Edit Data Gagal!",
					text: " ",
					type: "error"
				  });
				  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		include ('../library/kode-log-server.php');
		$query = mysqli_query($koneksi,"DELETE FROM mstjabatan WHERE KodeJabatan='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Master Jabatan : ".$_GET['nm']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " Hapus data di Master Aparatur terlebih dahulu! ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterJabatan.php";
					  });
					  </script>';
		}
	}
	
	?>
  </body>
</html>