<?php
include 'akses.php';
include '../library/tgl-indo.php';

$fitur_id = 19;
include '../library/lock-menu.php';

$Page = 'Kependudukan';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE NIK='".base64_decode($_GET['id'])."' AND LEFT(KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
	$RowData = mysqli_fetch_array($Edit);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.blue.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
		
		
		/* Style the form */
		#regForm {
		  background-color: #ffffff;
		  margin: 20px auto;
		  /* padding: 40px; */
		  width: 100%;
		  min-width: 300px;
		}

		/* Style the input fields */
		input {
		  padding: 10px;
		  width: 100%;
		  font-size: 17px;
		  font-family: Raleway;
		  border: 1px solid #aaaaaa;
		}

		/* Mark input boxes that gets an error on validation: */
		input.invalid {
		  background-color: #ffdddd;
		}

		/* Hide all steps by default: */
		.tab {
		  display: none;
		}

		/* Make circles that indicate the steps of the form: */
		.step {
		  height: 15px;
		  width: 15px;
		  margin: 0 2px;
		  background-color: #bbbbbb;
		  border: none; 
		  border-radius: 50%;
		  display: inline-block;
		  opacity: 0.5;
		}

		/* Mark the active step: */
		.step.active {
		  opacity: 1;
		}

		/* Mark the steps that are finished and valid: */
		.step.finish {
		  background-color: #4CAF50;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Data Penduduk (B.1)</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<ul class="nav nav-pills">
						<li <?php if(@$id==null){echo 'class="active"';} ?>>
							<a href="DataPenduduk.php"><span class="btn btn-primary">Data Penduduk</span></a>&nbsp;
						</li>
						<!-- <li>
							<a href="ImportPenduduk.php"><span class="btn btn-primary">Import Penduduk</span></a>&nbsp;
						</li>
						<li>
							<a href="../library/export-excel/template/TemplateDataPenduduk.xls"><span class="btn btn-primary">Download Template</span></a>
						</li>
						<li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary"><?php /* echo $Sebutan; */ ?></span></a>
						</li> -->
					</ul><br/>
				  <div class="card">
					<div class="tab-content">
						<div class="tab-pane fade <?php if(@$_GET['id']==null){ echo 'in active show'; }?>" id="home-pills">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Data Penduduk (B.1)</h3>
							</div>
							<div class="card-body">							  
								<div class="col-lg-4 offset-lg-8">
									<form method="post" action="">
										<div class="form-group input-group">
											<!-- <select type="hidden" name="lokasi" class="form-control" required>	
												<option value="" disabled selected>--- Pilih Desa ---</option>
												<?php
													/* $menu = mysqli_query($koneksi,"SELECT a.KodeLokasi,b.NamaDesa FROM mstlokasi a JOIN mstdesa b ON a.KodeDesa=b.KodeDesa WHERE LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeLokasi']==@$_REQUEST['lokasi']){
															echo "<option value=\"".$kode['KodeLokasi']."\" selected >".$kode['NamaDesa']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeLokasi']."\" >".$kode['NamaDesa']."</option>\n";
														}
													} */
												?>
											</select>&nbsp;&nbsp; -->
											<input type="text" name="keyword" class="form-control" placeholder="Nama..." value="<?php echo @$_REQUEST['keyword']; ?>">
											<span class="input-group-btn">
												<button class="btn btn-primary" type="submit">Cari</button>
											</span>
										</div>
									</form>
								</div>
							  <div class="table-responsive">  
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>No</th>
									  <th>NIK/Nama</th>
									  <th>Tempat/Tanggal Lahir</th>
									  <th>JK</th>
									  <th>SHDK</th>
									  <th>Agama</th>
									  <th>Pendidikan Terakhir</th>
									  <th>Pekerjaan</th>
									  <th>Ayah</th>
									  <th>Ibu</th>
									  <th>Desa</th>
									  <th>No KK</th>
									  <th>Status Penduduk</th>
									  <th>Aksi</th>
									</tr>
								  </thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										/* $lokasi= @$_REQUEST['lokasi']; */
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword= @$_REQUEST['keyword'];
											/* if(@$_REQUEST['lokasi'] === null){ */
												$reload = "DataPenduduk.php?pagination=true&keyword=$keyword";
												$sql =  "SELECT a.NIK,a.Nama,a.TempatLahir,a.TanggalLahir,a.JenisKelamin,a.NamaAyah, a.NamaIbu,a.RT,a.RW,a.NoKK ,a.StatusPenduduk,a.StatusKeluarga,a.Agama,d.NamaPendidikan,e.NamaPekerjaan,f.KodeDesa FROM datapenduduk a LEFT JOIN mstpendidikan d ON a.KodePendidikan=d.KodePendidikan LEFT JOIN mstpekerjaan e ON a.KodePekerjaan=e.KodePekerjaan LEFT JOIN mstlokasi f ON a.KodeLokasi=f.KodeLokasi WHERE (a.NIK LIKE '%$keyword%' OR a.Nama LIKE '%$keyword%') AND a.KodeLokasi='".$login_lokasi."' GROUP BY a.NIK, a.KodeLokasi ORDER BY f.KodeLokasi, a.NIK, a.Nama";
											/* } else {
												$reload = "DataPenduduk.php?pagination=true&keyword=$keyword&lokasi=$lokasi";
												$sql =  "SELECT a.NIK,a.Nama,a.TempatLahir,a.TanggalLahir,a.JenisKelamin,a.NamaAyah, a.NamaIbu,a.RT,a.RW,a.NoKK ,a.StatusPenduduk,a.StatusKeluarga,a.Agama,d.NamaPendidikan,e.NamaPekerjaan,f.KodeDesa FROM datapenduduk a LEFT JOIN mstpendidikan d ON a.KodePendidikan=d.KodePendidikan LEFT JOIN mstpekerjaan e ON a.KodePekerjaan=e.KodePekerjaan LEFT JOIN mstlokasi f ON a.KodeLokasi=f.KodeLokasi WHERE (a.NIK LIKE '%$keyword%' OR a.Nama LIKE '%$keyword%') AND a.KodeLokasi='".$lokasi."' GROUP BY a.NIK, a.KodeLokasi ORDER BY f.KodeLokasi, a.NIK, a.Nama";
											} */
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											/* if(@$_REQUEST['lokasi'] === null){ */
												$reload = "DataPenduduk.php?pagination=true";
												$sql =  "SELECT a.NIK,a.Nama,a.TempatLahir,a.TanggalLahir,a.JenisKelamin,a.NamaAyah, a.NamaIbu,a.RT,a.RW,a.NoKK ,a.StatusPenduduk,a.StatusKeluarga,a.Agama,d.NamaPendidikan,e.NamaPekerjaan,f.KodeDesa FROM datapenduduk a LEFT JOIN mstpendidikan d ON a.KodePendidikan=d.KodePendidikan LEFT JOIN mstpekerjaan e ON a.KodePekerjaan=e.KodePekerjaan LEFT JOIN mstlokasi f ON a.KodeLokasi=f.KodeLokasi WHERE a.KodeLokasi='".$login_lokasi."' GROUP BY a.NIK, a.KodeLokasi ORDER BY f.KodeLokasi, a.NIK, a.Nama";
											/* } else {
												$reload = "DataPenduduk.php?pagination=true&lokasi=$lokasi";
												$sql =  "SELECT a.NIK,a.Nama,a.TempatLahir,a.TanggalLahir,a.JenisKelamin,a.NamaAyah, a.NamaIbu,a.RT,a.RW,a.NoKK ,a.StatusPenduduk,a.StatusKeluarga,a.Agama,d.NamaPendidikan,e.NamaPekerjaan,f.KodeDesa FROM datapenduduk a LEFT JOIN mstpendidikan d ON a.KodePendidikan=d.KodePendidikan LEFT JOIN mstpekerjaan e ON a.KodePekerjaan=e.KodePekerjaan LEFT JOIN mstlokasi f ON a.KodeLokasi=f.KodeLokasi WHERE a.KodeLokasi='".$lokasi."' GROUP BY a.NIK, a.KodeLokasi ORDER BY f.KodeLokasi, a.NIK, a.Nama";
											} */
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 20; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php if($tcount == null OR $tcount === 0){
											echo '<tr class="odd gradeX"><td colspan="14" align="center"><br><h5>Tidak Ada Data</h5><br></td></tr>';
										} else {
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td>
												<?php echo $data ['NIK']."<br><strong>".strtoupper($data['Nama'])."</strong>"; ?>
											</td>
											<td>
												<?php echo $data ['TempatLahir'].", <br>".TanggalIndo($data ['TanggalLahir']); ?>
											</td>
											<td>
												<?php if($data ['JenisKelamin']==='1'){ echo 'Laki-laki';}else{echo 'Perempuan';} ?>
											</td>
											<td>
												<?php if($data['StatusKeluarga'] == null OR $data['StatusKeluarga'] === ""  OR $data['StatusKeluarga'] === '0'){ 
													echo "-"; 
												} else { 
												$caridata = mysqli_query($koneksi, "SELECT Uraian FROM mstlistdata WHERE JenisList = 'HUB_KELUARGA' AND NoUrut = '".$data ['StatusKeluarga']."'");
												while($array = mysqli_fetch_array($caridata)){
													echo $array ['Uraian']; 
												}
												}
												?>
											</td>
											<td>
												<?php if($data['Agama'] == null OR $data['Agama'] === ""  OR $data['Agama'] === '0'){ 
													echo "-"; 
												} else { 
												$caridata = mysqli_query($koneksi, "SELECT Uraian FROM mstlistdata WHERE JenisList = 'AGAMA' AND NoUrut = '".$data ['Agama']."'");
												while($array = mysqli_fetch_array($caridata)){
													echo $array ['Uraian']; 
												}
												}
												?>
												
											</td>
											<td>
												<?php if($data['NamaPendidikan'] == null OR $data['NamaPendidikan'] === ""){ echo "-"; } else { echo $data ['NamaPendidikan']; } ?>
											</td>
											<td>
												<?php if($data['NamaPekerjaan'] == null OR $data['NamaPekerjaan'] === ""){ echo "-"; } else { echo $data ['NamaPekerjaan']; } ?>
											</td>
											<td>
												<?php if($data['NamaAyah'] == null OR $data['NamaAyah'] === ""){ echo "-"; } else { echo $data ['NamaAyah']; } ?>
											</td>
											<td>
												<?php if($data['NamaIbu'] == null OR $data['NamaIbu'] === ""){ echo "-"; } else { echo $data ['NamaIbu']; } ?>
											</td>
											<td>
												<?php $caridesa = mysqli_query($koneksi, "SELECT NamaDesa FROM mstdesa WHERE KodeDesa = '".$data ['KodeDesa']."'");
												while($array = mysqli_fetch_array($caridesa)){
													echo $array ['NamaDesa']; 
												}
												?>
											</td>
											<td>
												<?php if($data['NoKK'] == null OR $data['NoKK'] === ""){ echo "-"; } else { echo $data ['NoKK']; } ?>
											</td>
											<td>
												<?php echo $data ['StatusPenduduk']; ?>
											</td>
											<td width="150px" align="center">										
												<a href="DataPenduduk.php?id=<?php echo base64_encode($data['NIK']); ?>"><span class="btn btn-sm btn-warning fa fa-edit" title="Edit"></span></a>
												<!-- <a href="LayananSurat.php"><span class="btn btn-sm btn-info fa fa-search" title="Detail"></span></a> -->
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										}
										?>
									</tbody>
								</table>
								
							  </div><br><div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							</div>
						</div>
						<div class="tab-pane fade <?php if(@$_GET['id']!=null){ echo 'in active show'; }?>" id="tambah-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Edit Penduduk<?php //echo $Sebutan; ?></h3>
							</div>
						<form method="post" action="">
							<div class="card-body">
								<div class="row">
								  <div class="col-lg-3">
										<div class="form-group-material">
										  <input type="text" name="NIK" class="form-control" placeholder="NIK" value="<?php echo @$RowData['NIK'];?>" readonly>
										  <input type="hidden" name="KodeLokasi" class="form-control" placeholder="Kode Lokasi" value="<?php echo @$RowData['KodeLokasi'];?>" readonly>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Nama" class="form-control" placeholder="Nama" value="<?php echo @$RowData['Nama'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="TempatLahir" class="form-control" placeholder="Tempat Lahir" value="<?php echo @$RowData['TempatLahir'];?>" required>
										</div>
										<div class="form-group-material">
										<?php if(@$RowData['TanggalLahir'] !== null){
											echo '<input type="text" name="TanggalLahir" id="time7" class="form-control" placeholder="Tanggal Lahir" value="'.substr($RowData['TanggalLahir'],0,11).'" required>';
										} else {
											echo '<input type="text" name="TanggalLahir" id="time7" class="form-control" placeholder="Tanggal Lahir" value="'.date('Y-m-d').'" required>';
										} ?>
										</div>
										<div class="form-group-material">
											<select name="JenisKelamin" class="form-control" required>
												<option value="" disabled selected>--- Jenis Kelamin ---</option>
												<option value="1" <?php if(@$RowData['JenisKelamin']==='1'){echo 'selected';} ?>>Laki-laki</option>
												<option value="2" <?php if(@$RowData['JenisKelamin']==='2'){echo 'selected';} ?>>Perempuan</option>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Agama" class="form-control" required>
												<option value="" disabled selected>--- Pilih Agama ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='AGAMA' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['Agama']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="StatusPenduduk" class="form-control" required>
												<option value="" disabled selected>--- StatusPenduduk ---</option>
												<option value="ADA" <?php if(@$RowData['StatusPenduduk']==='ADA'){echo 'selected';} ?>>Ada</option>
												<option value="PINDAH_MASUK" <?php if(@$RowData['StatusPenduduk']==='PINDAH_MASUK'){echo 'selected';} ?>>Pindah Masuk</option>
												<option value="PINDAH_KELUAR" <?php if(@$RowData['StatusPenduduk']==='PINDAH_KELUAR'){echo 'selected';} ?>>Pindah Keluar</option>
												<option value="MENINGGAL" <?php if(@$RowData['StatusPenduduk']==='MENINGGAL'){echo 'selected';} ?>>Meninggal</option>
											</select>
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group-material">
										  <input type="text" name="NamaAyah" class="form-control" placeholder="Nama Ayah" value="<?php echo @$RowData['NamaAyah'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NamaIbu" class="form-control" placeholder="Nama Ibu" value="<?php echo @$RowData['NamaIbu'];?>" required>
										</div>
										<div class="form-group-material">
											<select name="Kewarganegaraan" class="form-control" required>
												<option value="" disabled selected>--- Kewarganegaraan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['Agama']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										
										<div class="form-group-material">
											<select name="StatusKeluarga" class="form-control" required>
												<option value="" disabled selected>--- Status Keluarga ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='HUB_KELUARGA' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['StatusKeluarga']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="StatusPerkawinan" class="form-control" required>
												<option value="" disabled selected>--- Status Perkawinan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='STATUS_PERKAWINAN' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['StatusPerkawinan']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="GolDarah" class="form-control" required>
												<option value="" disabled selected>--- Gol Darah ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='GOL_DARAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['GolDarah']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="AnakKe" class="form-control" placeholder="Anak Ke" value="<?php echo @$RowData['AnakKe'];?>">
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group-material">
										  <input type="text" name="NoKK" class="form-control" placeholder="No KK" value="<?php echo @$RowData['NoKK'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NoUrutDiKK" class="form-control" placeholder="No Urut di KK" value="<?php echo @$RowData['NoUrutKK'];?>" required>
										</div>
										
										<div class="form-group-material">
										  <input type="text" name="NoAkteLahir" class="form-control" placeholder="No Akte Kelahiran" value="<?php echo @$RowData['NoAkteLahir'];?>">
										</div>
										<div class="form-group-material">
										<?php if(@$RowData['TanggalAkteLahir'] !== null){
											echo '<input type="text" name="TanggalAkteLahir" id="time2" class="form-control" placeholder="Tanggal Akte Lahir" value="'.substr($RowData['TanggalLahir'],0,11).'" required>';
										} else {
											echo '<input type="text" name="TanggalAkteLahir" id="time2" class="form-control" placeholder="Tanggal Akte Lahir" value="'.date('Y-m-d').'" required>';
										} ?>
										</div>
										<div class="form-group-material">
											<select name="Pendidikan" class="form-control" required>
												<option value="" disabled selected>--- Pendidikan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT KodePendidikan,NamaPendidikan FROM mstpendidikan ORDER BY KodePendidikan ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodePendidikan']===@$RowData['KodePendidikan']){
															echo "<option value=\"".$kode['KodePendidikan']."\" selected>".$kode['NamaPendidikan']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodePendidikan']."\" >".$kode['NamaPendidikan']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Pekerjaan" class="form-control" required>
												<option value="" disabled selected>--- Pekerjaan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT KodePekerjaan,NamaPekerjaan FROM mstpekerjaan ORDER BY KodePekerjaan ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodePekerjaan']===@$RowData['KodePekerjaan']){
															echo "<option value=\"".$kode['KodePekerjaan']."\" selected>".$kode['NamaPekerjaan']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodePekerjaan']."\" >".$kode['NamaPekerjaan']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="PekerjaanTambahan" class="form-control" placeholder="Pekerjaan Tambahan" value="<?php echo @$RowData['PekerjaanTambahan'];?>">
										</div>
									</div>
									<div class="col-lg-3">
										<!-- <div class="form-group-material">						
											<select id="KodeLokasi" name="KodeLokasi" class="form-control" required>	
												<option value="" disabled selected>--- Desa ---</option>
												<?php /*
													$menu = mysqli_query($koneksi,"SELECT a.KodeLokasi,b.NamaDesa FROM mstlokasi a JOIN mstdesa b ON a.KodeDesa=b.KodeDesa WHERE LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeLokasi']===@$RowData['KodeLokasi']){
															echo "<option value=\"".$kode['KodeLokasi']."\" selected >".$kode['NamaDesa']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeLokasi']."\" >".$kode['NamaDesa']."</option>\n";
														}
													} */
												?>
											</select>
										</div> -->
										<div class="form-group-material">
											<select id="KodeDusun" name="KodeDusun" class="form-control">	
												<option value="" disabled selected>--- Dusun ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT * FROM mstdusun WHERE KodeLokasi='".@$RowData['KodeLokasi']."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeDusun']==@$RowData['KodeDusun']){
															echo "<option value=\"".$kode['KodeDusun']."\" selected >".$kode['NamaDusun']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeDusun']."\" >".$kode['NamaDusun']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="RT" class="form-control" placeholder="RT" value="<?php echo @$RowData['RT'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="RW" class="form-control" placeholder="RW" value="<?php echo @$RowData['RW'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="NamaJalan" class="form-control" placeholder="Nama Jalan" value="<?php echo @$RowData['NamaJalan'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="NoRumah" class="form-control" placeholder="No Rumah" value="<?php echo @$RowData['NoRumah'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="Alamat" class="form-control" placeholder="Alamat" value="<?php echo @$RowData['Alamat'];?>">
										</div>
										<!-- <div class="form-group-material">
											<select name="StatusKepemilikanRumah" class="form-control">
												<option value="" disabled selected>--- Kepemilikan Rumah ---</option>
												<?php /*
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='KEPEMILIKAN_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['StatusPemilikRumah']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Dinding" class="form-control" >
												<option value="" disabled selected>--- Dinding ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='DINDING_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Dinding']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Lantai" class="form-control" >
												<option value="" disabled selected>--- Lantai ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='LANTAI_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Lantai']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Atap" class="form-control" >
												<option value="" disabled selected>--- Atap ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='ATAP_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Lantai']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="AsetLainnya" class="form-control" >
												<option value="" disabled selected>--- Aset Lainnya ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='ASET_KELUARGA' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Lantai']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Suku" class="form-control" placeholder="Suku" value="<?php echo @$RowData['Alamat']; */?>">
										</div> -->
								  </div>
								  &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button>
								</div>
							</div>
						</form>								
							<?php
							/* if(@$_GET['id']==null){
								echo '<button type="submit" class="btn btn-primary" name="Simpan">Simpan</button>';
							}else{
								echo '<input type="hidden" name="KodeJabatan" value="'.$RowData['KodeJabatan'].'"> ';
								echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
								echo '<a href="UserLogin.php"><span class="btn btn-warning">Batalkan</span></a>';
							} */
							?>
						</div>
						
						<div class="tab-pane fade" id="import-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Import Data Penduduk<?php //echo $Sebutan; ?></h3>
							</div>
							<form method="post" action="">
							<div class="card-body">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group-material">
											<label>Import File : </label>
											<div class="input-group">
											<input type="file" name="NamaFile" class="form-control" placeholder="Nama File" value="<?php echo $namaFile ?>" readonly>
											<span class="input-group-btn">
												<button type="submit" class="btn btn-primary" name="SubmitFile">Proses</button>
											</span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<?php if(isset($_POST['SubmitFile'])) {
										echo '<div class="table-responsive">';
										
										ini_set("display_errors",1);
										require_once '../library/excel-reader.php';
										
										$data = new Spreadsheet_Excel_Reader("../library/export-excel/$newfilename");
										$htmls='<table class="table table-striped">';
										for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
										{	
											if(count($data->sheets[$i][cells])>0) // checking sheet not empty
											{
												for($j=1;$j<=count($data->sheets[$i][cells]);$j++) // loop used to get each row of the sheet
												{ 
		 
												$htmls.="<tr>";
												for($k=1;$k<=count($data->sheets[$i][cells][$j]);$k++) // This loop is created to get data in a table format.
												{
													$htmls.="<td>";
													$htmls.=$data->sheets[$i][cells][$j][$k];
													$htmls.="</td>";
												}
												$data->sheets[$i][cells][$j][1];
												$no = 1;
												$NamaPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][0]);
												$NIKPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][1]);
												$TLahirPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][2]);
												$TglLahirPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][3]);
												$JKPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][4]);
												$NamaIbuPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][5]);
												$NamaAyahPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][6]);
												$NoKKPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][7]);			
												$NoUrutKKPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][8]);
												$AlamatPD = mysqli_real_escape_string($koneksi,$data->sheets[$i][cells][$j][9]);
												
												$htmls.="</tr>";
												$no++;
												}
											}
										echo "<input type='hidden' name='importData' value='$newfilename'>";
	
										}
										$htmls.="</table>";
										echo $htmls;
										echo "";
										echo '<button type="submit" class="btn btn-primary" name="SubmitImport">Import</button>';

										}
										?>
									</div>
								</div>
							</div>
							</form>
						</div>
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		 $("#KodeLokasi").change(function(){
			var KodeLokasi = $("#KodeLokasi").val();
			$.ajax({
				url: "../library/ambil-desa.php",
				data: "KodeLokasi="+KodeLokasi,
				cache: false,
				success: function(msg){
					$("#KodeDusun").html(msg);
				}
			});
		  });
		});
	</script>
	
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#time1').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time2').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time7').Zebra_DatePicker({format: 'Y-m-d'});
			//$('#Datetime2').Zebra_DatePicker({format: 'Y-m-d H:i', direction: 1});
		});
	</script>
	
	<?php
	if(isset($_POST['Simpan'])){
		echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		
		/* include ('../library/kode-log-server.php');
		//kode jabatan
		$sql_jbt = mysqli_query($koneksi,'SELECT RIGHT(KodeJabatan,5) AS kode FROM mstjabatan WHERE KodeLokasi="'.$login_lokasi.'" ORDER BY KodeJabatan DESC LIMIT 1');  
		$nums_jbt = mysqli_num_rows($sql_jbt);
		 
		if($nums_jbt <> 0)
		 {
		 $data_jbt = mysqli_fetch_array($sql_jbt);
		 $kode_jbt = $data_jbt['kode'] + 1;
		 }else
		 {
		 $kode_jbt = 1;
		 }
		 
		//mulai bikin kode
		 $bikin_kode_jbt = str_pad($kode_jbt, 5, "0", STR_PAD_LEFT);
		 $kode_jadi_jbt = "JBT-".$bikin_kode_jbt;
		
		$query = mysqli_query($koneksi,"INSERT into mstjabatan (KodeJabatan,NamaJabatan,Keterangan,KodeLokasi) 
		VALUES ('$kode_jadi_jbt','".$_POST['Nama']."','".$_POST['Keterangan']."','$login_lokasi')");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Master Jabatan : ".$_POST['Nama']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		} */
	}
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		$nik = htmlspecialchars($_POST['NIK']); $lokasi = htmlspecialchars($_POST['KodeLokasi']); 
		$nama = htmlspecialchars($_POST['Nama']); $tlahir = htmlspecialchars($_POST['TempatLahir']); $tgllahir = $_POST['TanggalLahir']; $jk = htmlspecialchars($_POST['JenisKelamin']); $agama = htmlspecialchars($_POST['Agama']); $stats = htmlspecialchars($_POST['StatusPenduduk']);
		
		$ayah = htmlspecialchars($_POST['NamaAyah']); $ibu = htmlspecialchars($_POST['NamaIbu']); $warga = htmlspecialchars($_POST['Kewarganegaraan']); $statskel = htmlspecialchars($_POST['StatusKeluarga']); $statskaw = htmlspecialchars($_POST['StatusPerkawinan']); $goldarah = htmlspecialchars($_POST['GolDarah']); $anakke = htmlspecialchars($_POST['AnakKe']);
		
		$nokk = htmlspecialchars($_POST['NoKK']); $nourut = htmlspecialchars($_POST['NoUrutDiKK']); $noakte = htmlspecialchars($_POST['NoAkteLahir']); $tglakte = $_POST['TanggalAkteLahir']; $didik = htmlspecialchars($_POST['Pendidikan']); $kerja = htmlspecialchars($_POST['Pekerjaan']); $kerja2 = htmlspecialchars($_POST['PekerjaanTambahan']);
		
		$dusun = htmlspecialchars($_POST['KodeDusun']); $rt = htmlspecialchars($_POST['RT']); $rw = htmlspecialchars($_POST['RW']); $jalan = $_POST['NamaJalan']; $norumah = htmlspecialchars($_POST['NoRumah']); $alamat = htmlspecialchars($_POST['Alamat']); 
		
		$query = mysqli_query($koneksi,"UPDATE datapenduduk SET Nama = '$nama', TempatLahir = '$tlahir', TanggalLahir = '$tgllahir', JenisKelamin = '$jk', Agama = '$agama', StatusPenduduk = '$stats', NamaAyah = '$ayah', NamaIbu = '$ibu', WNI = '$warga', StatusKeluarga = '$statskel', StatusPerkawinan = '$statskaw', GolDarah = '$goldarah', NoKK = '$nokk', NoUrutKK = '$nourut', NoAkteLahir = '$noakte', TanggalAkteLahir = '$tglakte', KodePendidikan = '$didik', KodePekerjaan = '$kerja', PekerjaanTambahan = '$kerja2', KodeDusun = '$dusun', RT = '$rt', RW = '$rw', NamaJalan = '$jalan', NoRumah = '$norumah', Alamat = '$alamat', AnakKe = '$anakke' WHERE KodeLokasi = '$lokasi' AND NIK = '$nik'");
		
		if($query){
			echo '<script language="javascript">document.location="DataPenduduk.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Edit Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "DataPenduduk.php";
				  });
				  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		include ('../library/kode-log-server.php');
		$query = mysqli_query($koneksi,"DELETE FROM mstjabatan WHERE KodeJabatan='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Master Jabatan : ".$_GET['nm']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " Hapus data di Master Aparatur terlebih dahulu! ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterJabatan.php";
					  });
					  </script>';
		}
	}
	
	?>
  </body>
</html>