<?php  
session_start();  
ob_start();  
include '../library/config.php';
include '../library/tgl-indo.php';

//get variable
$kode = base64_decode(@$_GET['kode']);
$jenis = base64_decode(@$_GET['jns']);
$lokasi = base64_decode(@$_GET['loc']);

?>  	

<html xmlns="http://www.w3.org/1999/xhtml"> <!-- Bagian halaman HTML yang akan konvert -->  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />  
<?php include 'title.php';?>   
</head>  
<body> 
	<!-- Bagian Head Print Out -->
	<br><br>
	<table border="0" style="width: 100%;" align="center" cellpadding="0" cellspacing="0" >
		<tr valign="top">
		<td style="width: 5%;" align="center"></td>
			<td style="width: 13%;" align="center"><img src="../images/Assets/logo.png" style="width: 75%;" /></td>
			<td style="width: 77%;" align="center">
				<span style="font-size: 22px; font-weight: bold; line-height: 25px;">P E M E R I N T A H &nbsp; K A B U P A T E N &nbsp; N G A N J U K</span><br>
				<span style="font-size: 22px; font-weight: bold; line-height: 25px;">K E C A M A T A N &nbsp; N G A N J U K</span><br>
				<?php $cari = @mysqli_query($koneksi, "Select NamaDesa, AlamatLokasi from mstdesa inner join mstlokasi on mstlokasi.KodeDesa = mstdesa.KodeDesa WHERE mstlokasi.KodeLokasi='".$lokasi."'") or die(@mysqli_error($cari));
				while($tampil = @mysqli_fetch_array($cari)){
					echo '<span style="font-size: 20px; font-weight: bold; line-height: 25px;">KELURAHAN '.$tampil['NamaDesa'].'</span><br>';
					echo '<span style="font-size: 16px;">'.ucwords($tampil['AlamatLokasi']).'</span><br>';
				} ?>
			</td>
			<td style="width: 5%;"></td>
		</tr>
		<tr><td style="width: 2%;"></td><td colspan="2"><hr></td><td style="width: 2%;"></td></tr>
	</table>
	
	<!-- Bagian Isi Surat -->
	<table border="0" style="width: 100%;" align="center" cellpadding="0" cellspacing="0" >
	<?php $cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari)); while($tampil = @mysqli_fetch_array($cari)){ $Keperluan = strip_tags($tampil['Keperluan']); $Uraian = $tampil['Uraian']; $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat'];} ?>
		<tr valign="top"><td colspan="3" style="width: 100%;" align="center"><br><span style="font-size: 18px; text-decoration: underline; font-weight: bold;">SURAT KETERANGAN AHLI WARIS</span></td></tr>
		<tr valign="top">
			<td style="width: 7.5%;"></td>
			<td><p style="padding-left: 94px; padding-right: 94px; top: 275px; font-size: 14px; text-align: center;
    ">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at velit dignissim, sollicitudin erat a, pretium turpis. Suspendisse placerat, mauris nec porta ultricies, lectus diam aliquam nulla, eu tristique mauris elit a eros. Aenean semper vestibulum elit, non ullamcorper orci mattis sed. Nullam tempor facilisis nibh, at accumsan ipsum pharetra id. Cras ultricies eget est et blandit. Duis quam tellus, posuere at quam sit amet, posuere euismod velit. Sed vel tellus vel tortor lacinia aliquet. Etiam sed urna ut mi tincidunt mattis ac non justo. In eu est eget est tristique auctor nec vel enim. Etiam non finibus turpis. Suspendisse potenti.</p><br/></td>
			<td style="width: 7.5%;"></td>
		</tr>
		
	</table>
	
	<!-- Bagian Footer Surat -->
	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
            <tr>
                <th style="width: 20px; text-align: center; border: solid 1px #000;"><strong>No</strong></th>
                <th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>No.Dok. Kerjasama</strong></th>
                <th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Nama Program</strong></th>
                <th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Mitra Kerjasama</strong></th>
				<th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Bidang Kerjasama</strong></th>
				<th style="width: 20%; text-align: center; border: solid 1px #000;"><strong>Ruang Lingkup</strong></th>
				<th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Periode Kerjasama</strong></th>
				<th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Tindak Lanjut</strong></th>
				<th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Kendala</strong></th>
				<th style="width: 5%; text-align: center; border: solid 1px #000;"><strong>Ket</strong></th>
            </tr>
        </thead>
        <tbody>
            <?php
			/*$sql =  "SELECT * FROM kerjasama WHERE date_format(TanggalMulai, '%Y')='".$_POST['PrintTahun']."' ORDER BY KodeKerjasama DESC";
			$result = mysqli_query($koneksi,$sql);
			while($data=mysqli_fetch_array($result)){
			?>
			<tr>
                <td style="width: 20px; text-align: left; border: solid 1px #000">
                    <?php echo ++$no_urut;?>
                </td>
                <td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php 
						echo "No: ".$data ['NoPerjanjian1']; echo '<br/>';
						echo "No: ".$data ['NoPerjanjian2'];
						if($data ['NoPerjanjian3']!=null){
							echo '<br/>';
							echo "No: ".$data ['NoPerjanjian3'];
						}
						
						if($data ['NoPerjanjian4']!=null){
							echo '<br/>';
							echo "No: ".$data ['NoPerjanjian4'];
						}
					?>
                </td> 
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['NamaProgram']; ?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['MitraKerjasama']; ?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['BidangKerjasama']; ?>
                </td>
				<td style="width: 20%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['Kegiatan']; ?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data['PeriodeKerjasama']; echo '<br/>'; echo TanggalIndo($data ['TanggalMulai']); echo ' s/d '; echo TanggalIndo($data ['TanggalSelesai']);?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['TindakLanjut']; ?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['Kendala']; ?>
                </td>
				<td style="width: 5%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['Keterangan']; ?>
                </td>
            </tr>
			<?php } */?>
        </tbody>
    </table>
</body>  
</html><!-- Akhir halaman HTML yang akan di konvert -->  
<?php  
$filename="Rekapitulasi Kerjasama Tahun ".$_POST['PrintTahun'].".pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya  
//==========================================================================================================  
//Copy dan paste langsung script dibawah ini,untuk mengetahui lebih jelas tentang fungsinya silahkan baca-baca tutorial tentang HTML2PDF  
//==========================================================================================================  
$content = ob_get_clean();  
$content = '<page style="font-family: Cambria,"Times New Roman",serif;">'.($content).'</page>';  
 require_once('../library/html2pdf/html2pdf.class.php');  
 try  
 {  
  $html2pdf = new HTML2PDF('P','F4','en', false, 'ISO-8859-15',array(1,0));  
  $html2pdf->setDefaultFont('Times','11');  
  $html2pdf->writeHTML($content, isset($_GET['vuehtml']));  
  $html2pdf->Output($filename);  
 }  
 catch(HTML2PDF_exception $e) { echo $e; }  
?>  