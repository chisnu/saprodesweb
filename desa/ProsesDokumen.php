<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $NoTrMohon	=@$_GET['NoTrMohon'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $IDPend	=@$_GET['IDPend'];
 $JenisSurat=@$_GET['JenisSurat'];
 // $Aksi	=@$_GET['Aksi']; //tinjau ulang digunakan jika dokumen di rejected 
?>

<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Proses Dokumen</h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
								<label>Dokumen untuk <strong><?php echo $JenisSurat;?></strong></label>
								<?php // cari user
								$CariData = @mysqli_query($koneksi, "SELECT Nama, NIK FROM datapenduduk WHERE IDPend = '".$IDPend."'"); 
								$array = @mysqli_fetch_array($CariData);
									echo "<br>Nama : ".$array['Nama']." (<strong>".$array['NIK']."</strong>)";
									echo "<br>Nomor : ".$NoTrMohon."<br><br>";
								?>
								
								<form method="post" action="">
									<select class="form-control" name="JenisSurat" autocomplete="off" required>
										<option value="">-- Pilih Proses --</option>
										<option value="Verifikasi Berkas (DESA)">Verifikasi Berkas</option>
										<option value="Proses Berkas (DESA)">Proses Berkas</option>
										<option value="Pengesahan (DESA)">Pengesahan</option>
										<option value="Penomoran (DESA)">Penomoran</option>
									</select>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
