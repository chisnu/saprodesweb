<?php
include 'akses.php';
$Page = 'UbahPassword';
$Tahun=date('Y');
$HariIni=date('Y-m-d H:i:s');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.blue.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	
    <link rel="stylesheet" href="../komponen/css/custom.css">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
		
		label{
			color:#796AEE;;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Ubah Password</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables">
            <div class="container-fluid">
				<div class="row">
					<div class="col-lg-6">
					  <div class="card">
						<div class="card-body">
							 <form method="post" action="">
								<div class="form-group-material">
								  <input type="password" name="PasswordLama" class="form-control" placeholder="Password Lama" required>
								</div>
								<div class="form-group-material">
								  <input type="password" name="PasswordBaru" class="form-control" placeholder="Password Baru" required>
								</div>
								<div class="form-group-material">
								  <input type="password" name="KonfirmasiPasswordBaru" class="form-control" placeholder="Konfirmasi Password Baru" required>
								</div>
								<button type="submit" class="btn btn-primary" name="SimpanPassword">Simpan Password</button>
							  </form>
						</div>
					  </div>
					</div>					
				</div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<?php
		if(isset($_POST['SimpanPassword'])){
			if(base64_encode($_POST['PasswordLama']) === $kode_password){
				if(base64_encode($_POST['PasswordBaru'])===base64_encode($_POST['KonfirmasiPasswordBaru'])){
					$UpdatePass = @mysqli_query($koneksi, "UPDATE userlogin SET UserPsw='".base64_encode($_POST['KonfirmasiPasswordBaru'])."' WHERE UserName='$login_id' AND KodeLokasi='$login_lokasi'"); 
					echo '<script type="text/javascript">
					  sweetAlert({
						title: "Ubah Password Berhasil",
						text: " Password baru dapat digunakan setelah anda login lagi. ",
						type: "success"
					  },
					  function () {
						window.location.href = "UbahPassword.php";
					  });
					  </script>';
				}else{
					echo '<script type="text/javascript">
					  sweetAlert({
						title: "Ubah Password Gagal!",
						text: " Password Baru tidak sama dengan Konfirmasi Password Baru ",
						type: "error"
					  },
					  function () {
						window.location.href = "UbahPassword.php";
					  });
					  </script>';
				}
			}else{
				echo '<script type="text/javascript">
				  sweetAlert({
					title: "Ubah Password Gagal!",
					text: " Password Lama Salah ",
					type: "error"
				  },
				  function () {
					window.location.href = "UbahPassword.php";
				  });
				  </script>';
			}
		}
	?>
  </body>
</html>