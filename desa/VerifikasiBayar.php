<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $Nop	=@$_GET['Nop'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $Tahun=@$_GET['Tahun'];
 // $Aksi	=@$_GET['Aksi']; //tinjau ulang digunakan jika dokumen di rejected 
?>

<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Terima Pembayaran PBB</h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
								<label>Nomor Objek Pajak : <strong><?php echo $Nop;?></strong></label>
								<?php // cari user
								$CariData = @mysqli_query($koneksi, "SELECT * FROM masterobjekpajak WHERE NoObjekPajak = '".$Nop."' AND KodeLokasi = '".$KodeLokasi."' AND Tahun = '".$Tahun."'"); 
								$array = @mysqli_fetch_array($CariData);
									echo "<br>Nama : ".$array['NamaWP'];
									echo "<br>Tahun : ".$array['Tahun'];
									echo "<br>Pokok Pajak : Rp. ".number_format($array['Pokok'])."<br><br>";
								?>
								<!--<form action="upload/processupload.php" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
									<input name="NoTrMohon" type="hidden" value="<?php echo $NoTrMohon;?>" />
									<input name="KodeLokasi" type="hidden" value="<?php echo $KodeLokasi;?>" />
									<input name="IDPend" type="hidden" value="<?php echo $IDPend;?>" />
									<input name="NamaSyarat" type="hidden" value="<?php echo $NmSyarat;?>" />
									<input name="KodeSyarat" type="hidden" value="<?php echo $KdSyarat;?>" />
									<br/>
									<div class="form-group-material">
									  <input type="text" name="Ket" class="form-control" placeholder="Keterangan">
									</div>
								</form>-->
								<form method="post" action="VerifikasiDokumen.php?type=<?php echo base64_encode('pembayaranPBB'); ?>">
								<div class="table">  
									<table class="table table-striped">
									  <tbody>
												<tr>
												  <?php // cek apakah sudah lengkap
														echo '<td align="center">';
														if(@$array['IsBayar'] === '1'){
															echo '<input type="checkbox" name="sah" value="'.$data_syarat["Suport1"].'" checked="checked" disabled />&nbsp;&nbsp;';
															echo '<span class="btn btn-success">Sudah Terbayar</span>';
														} else {
															echo '<input type="checkbox" name="sah" value="1">&nbsp;Objek Pajak Dibayar</input><br><br>';
															echo '<input name="Nop" type="hidden" value="'.$Nop.'" />';
															echo '<input name="KodeLokasi" type="hidden" value="'.$KodeLokasi.'" />';
															echo '<input name="Tahun" type="hidden" value="'.$Tahun.'" />';
															echo '<button class="btn btn-warning" name="Pengesahan">Ceklist dan Klik untuk Konfirmasi Pembayaran</button>';
														}
														echo '</td>';
												  ?>
												</tr>
									 </tbody>
									</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<div id="ModalEditImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>

<script type="text/javascript">
	   $(document).ready(function () {
	   $(".open_modal_image").click(function(e) {
		  // var kd_dok = $(this).data("kodedok");
		  // var id_pend  = $(this).data("idpend");
		  // var dok  = $(this).data("filedok");
		  var no_trmohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("lokasi");
			   $.ajax({
					   url: "DetilDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_trmohon,KodeLokasi: kode_lok},
					   success: function (ajaxData){
					   $("#ModalEditImage").html(ajaxData);
					   $("#ModalEditImage").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
	
	<script>
		function confirm_verif() {
			var answer = confirm("Apakah Anda yakin untuk memverifikasi data ini ?")
			if (answer == true){
				window.location = "VerifikasiSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
	
	<script>
		function cek(syarat){
			for(i=0; i < syarat.length; i++){
				syarat[i].checked = true;
			}
		}
		function uncek(syarat){
			for(i=0; i < syarat.length; i++){
				syarat[i].checked = false;
			}
		}
	</script>