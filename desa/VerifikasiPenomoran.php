<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $NoTrMohon	=@$_GET['NoTrMohon'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $IDPend	=@$_GET['IDPend'];
 $JenisSurat=@$_GET['JenisSurat'];
 // $Aksi	=@$_GET['Aksi']; //tinjau ulang digunakan jika dokumen di rejected 
?>

<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">

<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Penomoran</h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
								<label>Dokumen untuk <strong><?php echo $JenisSurat;?></strong></label>
								<?php // cari user
								$CariData = @mysqli_query($koneksi, "SELECT Nama, NIK FROM datapenduduk WHERE IDPend = '".$IDPend."'"); 
								$array = @mysqli_fetch_array($CariData);
									echo "<br>Nama : ".$array['Nama']." (<strong>".$array['NIK']."</strong>)";
									echo "<br>Nomor : ".$NoTrMohon."<br><br>";
								?>
								<!--<form action="upload/processupload.php" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
									<input name="NoTrMohon" type="hidden" value="<?php echo $NoTrMohon;?>" />
									<input name="KodeLokasi" type="hidden" value="<?php echo $KodeLokasi;?>" />
									<input name="IDPend" type="hidden" value="<?php echo $IDPend;?>" />
									<input name="NamaSyarat" type="hidden" value="<?php echo $NmSyarat;?>" />
									<input name="KodeSyarat" type="hidden" value="<?php echo $KdSyarat;?>" />
									<br/>
									<div class="form-group-material">
									  <input type="text" name="Ket" class="form-control" placeholder="Keterangan">
									</div>
								</form>-->
								<form method="post" action="VerifikasiDokumen.php?type=<?php echo base64_encode('penomoran'); ?>">
								<?php $sql3 =  mysqli_query($koneksi, "SELECT a.Tanggal, a.NoManualSurat, a.NoAgendaSurat, b.PrediksiTglSelesai, b.StatusPermohonan FROM trpengurusansurat a join trpermohonanmasy b on b.NoTrMohon = a.NoTrMohon AND a.KodeLokasi = b.KodeLokasi WHERE a.NoTrMohon='".$NoTrMohon."' AND a.KodeLokasi='".$KodeLokasi."' AND a.JenisSurat='".$JenisSurat."'");
									while($array3 = mysqli_fetch_array($sql3)){ ?>
									
									<div class="form-group-material">
										<select name="StatusPermohonan" class="form-control" required>
											<option value="ON PROGRESS" <?php if(@$array3['StatusPermohonan']==='ON PROGRESS'){ echo 'selected="selected"'; } ?>>SELESAI</option>
											<option value="REJECTED" <?php if(@$array3['StatusPermohonan']==='REJECTED'){ echo 'selected="selected"'; } ?>>DITOLAK</option>
										</select>
									</div>
									<div class="form-group-material">
										<?php if($array3['Tanggal'] == null){
											echo '<input type="text" name="TglSurat" id="time7" class="form-control" placeholder="Tanggal Surat" value="'.substr($array3['PrediksiTglSelesai'],0,11).'" required>';
										} else {
											echo '<input type="text" name="TglSurat" id="time7" class="form-control" placeholder="Tanggal Surat" value="'.substr($array3['Tanggal'],0,11).'" required>';
										} ?>
									</div>
									<div class="form-group-material">
										<?php if($array3['NoManualSurat'] == null){
											echo '<input type="text" name="KodeSurat" class="form-control" placeholder="Kode Surat" value="'.$array['KodeNomorSurat'].'" required>';
										} else {
											echo '<input type="text" name="KodeSurat" class="form-control" placeholder="Kode Surat" value="'.$array3['NoManualSurat'].'" required>';
										} ?>
									</div>
									<div class="form-group-material">
										<?php if($array3['NoAgendaSurat'] == null){
											echo '<input type="text" name="KodeAgendaSurat" class="form-control" placeholder="Kode Agenda Surat" value="'.$array['KodeNomorSurat'].'" required>';
										} else {
											echo '<input type="text" name="KodeAgendaSurat" class="form-control" placeholder="Kode Agenda Surat" value="'.$array3['NoManualSurat'].'" required>';
										} ?>
									</div>
									<div class="form-group-material">
										<?php if($array3['Keperluan'] == null){
											echo '<textarea type="text" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>Yang bertanda tangan dibawah ini adalah Ahli Waris yang sah dari ';
											if($jk === '1'){ echo 'almarhum '.strtoupper($nama); } else { echo 'almarhumah '.strtoupper($nama); }
											echo ' tempat tinggal terakhir di ';
											if($alamat != null OR $alamat !== ''){ echo ucwords($alamat); } else { echo '...'; }
											echo ' yang meninggal pada ..... di ';
											if($alamat != null OR $alamat !== ''){ echo ucwords($alamat); } else { echo '...'; } 
											echo ', dan semasa hidupnya almarhum/almarhumah telah kawin sah dengan ... dan mempunyai ahli waris sebagai berikut :</textarea>';
										} else {
											echo '<textarea type="text" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>'.$array3['Keperluan'].'</textarea>';
										} ?>
									</div>
									<div class="form-group-material">
										<?php if($array3['Uraian'] == null){
											echo '<textarea type="text" name="Uraian" class="form-control" rows="10" placeholder="Keterangan" value="" required>Demikian orang tersebut diatas adalah benar-benar ahli waris dari ';
											if($jk === '1'){ echo 'almarhum '.strtoupper($nama); } else { echo 'almarhumah '.strtoupper($nama); } 
											echo ' dan apabila dikemudian hari ternyata keterangan ini tidak benar, maka kami sanggup dituntut sesuai peraturan undang-undang yang berlaku, serta semua pihak yang mengesahkan Surat Keterangan ini kami nyatakan bebas dari segala tuntutan.</textarea>';
										} else {
											echo '<textarea type="text" name="Uraian" class="form-control" rows="10" placeholder="Uraian" value="" required>'.$array3['Uraian'].'</textarea>';
										} ?>
									</div>
									
									<?php } ?>
										
									<button type="submit" class="btn btn-primary" name="SimpanPengurusan">Simpan</button>
								</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<div id="ModalEditImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
	

<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#time1').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time2').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time7').Zebra_DatePicker({format: 'Y-m-d'});
			//$('#Datetime2').Zebra_DatePicker({format: 'Y-m-d H:i', direction: 1});
		});
	</script>
	
<script type="text/javascript">
	   $(document).ready(function () {
	   $(".open_modal_image").click(function(e) {
		  // var kd_dok = $(this).data("kodedok");
		  // var id_pend  = $(this).data("idpend");
		  // var dok  = $(this).data("filedok");
		  var no_trmohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("lokasi");
			   $.ajax({
					   url: "DetilDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_trmohon,KodeLokasi: kode_lok},
					   success: function (ajaxData){
					   $("#ModalEditImage").html(ajaxData);
					   $("#ModalEditImage").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
	
	<script>
		function confirm_verif() {
			var answer = confirm("Apakah Anda yakin untuk memverifikasi data ini ?")
			if (answer == true){
				window.location = "VerifikasiSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
	
	<script>
		function cek(syarat){
			for(i=0; i < syarat.length; i++){
				syarat[i].checked = true;
			}
		}
		function uncek(syarat){
			for(i=0; i < syarat.length; i++){
				syarat[i].checked = false;
			}
		}
	</script>