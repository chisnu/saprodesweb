<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $NoTrMohon	=@$_GET['NoTrMohon'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $IDPend	=@$_GET['IDPend'];
 $JenisSurat=@$_GET['JenisSurat'];
 // $Aksi	=@$_GET['Aksi']; //tinjau ulang digunakan jika dokumen di rejected 
?>

<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Lihat Dokumen</h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
								<label>Dokumen untuk <strong><?php echo $JenisSurat;?></strong></label>
								<?php // cari user
								$CariData = @mysqli_query($koneksi, "SELECT Nama, NIK FROM datapenduduk WHERE IDPend = '".$IDPend."'"); 
								$array = @mysqli_fetch_array($CariData);
									echo "<br>Nama : ".$array['Nama']." (<strong>".$array['NIK']."</strong>)";
									echo "<br>Nomor : ".$NoTrMohon."<br><br>";
								?>
								<!--<form action="upload/processupload.php" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
									<input name="NoTrMohon" type="hidden" value="<?php echo $NoTrMohon;?>" />
									<input name="KodeLokasi" type="hidden" value="<?php echo $KodeLokasi;?>" />
									<input name="IDPend" type="hidden" value="<?php echo $IDPend;?>" />
									<input name="NamaSyarat" type="hidden" value="<?php echo $NmSyarat;?>" />
									<input name="KodeSyarat" type="hidden" value="<?php echo $KdSyarat;?>" />
									<br/>
									<div class="form-group-material">
									  <input type="text" name="Ket" class="form-control" placeholder="Keterangan">
									</div>
								</form>-->
								<div class="table-responsive">  
									<table class="table table-striped">
									  <thead>
										<tr>
										  <th>No</th>
										  <th>Nama Syarat</th>
										  <th>Keterangan</th>
										  <th>File</th>
										</tr>
									  </thead>
									  <tbody>
										<?php 
											$no =1;
											$sql_syarat = @mysqli_query($koneksi, "SELECT * FROM mstsyaratsurat WHERE JenisSurat='$JenisSurat' AND LEFT(KodeLokasi,3)='".substr($KodeLokasi,0,3)."'"); 
											while($data_syarat = @mysqli_fetch_array($sql_syarat)){
										?>
												<tr>
												  <th scope="row"><?php echo $no++; ?></th>
												  <td><?php echo $data_syarat['NamaSyarat'];?></td>
												  <?php
												  // cek apakah sudah lengkap
														$CekLengkap = @mysqli_query($koneksi, "SELECT * FROM dokumensyaratmohon WHERE KodeLokasi='$KodeLokasi AND NoTrMohon='$NoTrMohon' AND KodeSyarat='".$data_syarat['KodeSyarat']."'"); 
														
														$numLengkap = @mysqli_num_rows($CekLengkap); 
														if($numLengkap = 0){
															echo "<td><font color='#3498db'>Belum</font></td>";
														}else{
															echo "<td><font color='#3498db'>Lengkap</font></td>";
														}
												  ?>
												  <td width="50px">
														<a href="#" class='open_modal_image' data-notrmohon='<?php echo $NoTrMohon;?>' data-lokasi='<?php echo $KodeLokasi;?>' data-kode='<?php echo $data_syarat['KodeSyarat'];?>'>
														<span class="btn btn-info btn-sm fa fa-search" title="Lihat Dokumen"></span>
														</a>
												  </td>
												</tr>
										<?php } ?>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<div id="ModalEditImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
<script type="text/javascript">
	   $(document).ready(function () {
	   $(".open_modal_image").click(function(e) {
		  // var kd_dok = $(this).data("kodedok");
		  // var id_pend  = $(this).data("idpend");
		  // var dok  = $(this).data("filedok");
		  var no_trmohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("lokasi");
		  var kode_syr = $(this).data("kode");
			   $.ajax({
					   url: "DetilDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_trmohon,KodeLokasi: kode_lok,KodeSyarat: kode_syr},
					   success: function (ajaxData){
					   $("#ModalEditImage").html(ajaxData);
					   $("#ModalEditImage").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>