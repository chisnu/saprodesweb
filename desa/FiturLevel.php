<?php
include 'akses.php';
$Page = 'Security';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(base64_decode(@$_GET['aksi'])=='NonAktif'){
	$Hapus = mysqli_query($koneksi,"DELETE FROM fiturlevel WHERE LevelID='".base64_decode($_GET['id'])."' AND FiturID='".base64_decode($_GET['data'])."' AND KodeLokasi='$login_lokasi'");
	if($Hapus){
		 echo '<script> window.location="FiturLevel.php?id='.$_GET['id'].'"</script>';
	}else{
		 echo '<script>alert("Set Hak Akses Gagal"); window.location="FiturLevel.php?id='.$_GET['id'].'"</script>';
	}
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.blue.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th,td {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "AksesLevel.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Akses Level</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-8">
				  <div class="card">
					<div class="card-header d-flex align-items-center">
					  <h3 class="h4">Set Fitur Akses</h3>
					</div>
					<div class="card-body">	
					  <div class="table-responsive">  
						<table class="table table-striped">
						  <thead>
							<tr>
							  <th>No</th>
							  <th>Menu Akses</th>
                              <th>Hak Akses</th>
							</tr>
						  </thead>
							<?php
								include '../library/pagination1.php';
								// mengatur variabel reload dan sql
								$reload = "UserLogin.php?pagination=true";
								$sql =  "SELECT * FROM serverfitur WHERE JenisUnit='Desa' ORDER BY FiturID ASC";
								$result = mysqli_query($koneksi,$sql);
								
								//pagination config start
								$rpp = 50; // jumlah record per halaman
								$page = intval(@$_GET["page"]);
								if($page<=0) $page = 1;  
								$tcount = mysqli_num_rows($result);
								$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
								$count = 0;
								$i = ($page-1)*$rpp;
								$no_urut = ($page-1)*$rpp;
								//pagination config end				
							?>
							<tbody>
								<form action="SimpanFiturAkses.php?id=<?php echo $_GET['id']; ?>" method="post">
								<?php
								while(($count<$rpp) && ($i<$tcount)) {
									mysqli_data_seek($result,$i);
									$data = mysqli_fetch_array($result);
								?>
								<tr class="odd gradeX">
									<td class="text-left" width="50px">
										<?php echo ++$no_urut;?> 
									</td>
									<td class="text-left" >
										<strong><?php echo $data ['FiturName']; ?></strong>
									</td>
									<td align="center">
										<?php
											$cek2 = mysqli_query($koneksi,"select * from fiturlevel where LevelID='".base64_decode($_GET['id'])."' AND FiturID='".$data ['FiturID']."' AND KodeLokasi='$login_lokasi'");									
											$num2 = mysqli_num_rows($cek2);
											if($num2 == 1 ){
										?>
											<a href="FiturLevel.php?id=<?php echo $_GET['id'];?>&data=<?php echo base64_encode($data ['FiturID']);?>&aksi=<?php echo base64_encode('NonAktif');?>" title='Klik tombol untuk menonaktifkan fitur akses menu ini !'><i class='btn btn-success btn-sm'><span class='fa fa-check'></span> Akses Verified</i></a>
										<?php }else{ ?>
											<input type="checkbox" id="cekbox" name="cekbox[]" value="<?php echo $data['FiturID'] ?>"/>
										<?php } ?>
									</td>
								</tr>
								<?php
									$i++; 
									$count++;
								}
								?>
							</tbody>
								<input type="button" class="btn btn-md btn-warning" onclick="cek(this.form.cekbox)" value="Select All" />&nbsp;
								<input type="button" class="btn btn-md btn-danger" onclick="uncek(this.form.cekbox)" value="Clear All" />&nbsp;
								<input type="submit" class="btn btn-md btn-success" value="Simpan" name="submit" />
								<br/><br/>
							</form>
						</table>
						<!--<div><?php echo paginate_one($reload, $page, $tpages); ?></div>-->
					  </div>
					</div>
						
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	<script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>
  </body>
</html>