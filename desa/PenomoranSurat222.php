<?php
include 'akses.php';
include '../library/tgl-indo.php';

/* $fitur_id = 2;
include '../library/lock-menu.php'; */

$Page = 'ProgressSurat';
$Tahun=date('Y');
$HariIni=date('Y-m-d H:i:s');
$JamSekarang = date('H:i:s');
$TanggalSekarang = date('Y-m-d');
		

//cek apakah user sudah pernah membuat tr permohonan msyarakat
$QueryCekData = @mysqli_query($koneksi, "SELECT a.NoTrMohon,a.JenisSurat,b.StandarWaktuPelayanan FROM trpermohonanmasy a JOIN MasterPengurusanSurat b ON (a.JenisSurat)=(b.JenisSurat) WHERE LEFT(a.KodeLokasi,3)='".substr($kode_lokasi_aktif,0,3)."' AND a.IDPend='$id_penduduk_aktif' AND a.StatusPermohonan='PraWAITING'"); 
$numCek = @mysqli_num_rows($QueryCekData); 
if($numCek > 0){
	while($CekData = @mysqli_fetch_array($QueryCekData)){
		$No_Transaksi = $CekData['NoTrMohon'];
		$Jenis_Surat = $CekData['JenisSurat'];
		$Standar_Waktu = $CekData['StandarWaktuPelayanan'];
	}
}

//Hapus Data
/* if(base64_decode(@$_GET['aksi'])==='Hapus'){
	$file = '../images/DokumenSyarat/'.base64_decode(@$_GET['file']);
	$thumb = '../images/DokumenSyarat/thumb_'.base64_decode(@$_GET['file']);
	
	if(file_exists($file)){ unlink($file); }
	if(file_exists($thumb)){ unlink($thumb); }
	$HapusData = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND KodeSyarat='".base64_decode(@$_GET['id'])."' AND NoTrMohon='$No_Transaksi'"); 
	echo '<script language="javascript">document.location="PenomoranSurat.php?aksi=tampil"; </script>';
}

if(base64_decode(@$_GET['aksi'])==='PindahDokumen'){
		$QueryCekData = @mysqli_query($koneksi, "SELECT MAX(NoUrutSyarat) as NoSekarang FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi'"); 
		$CekData = @mysqli_fetch_array($QueryCekData);
		$NoMax = $CekData['NoSekarang'];

		$NoSekarang = $NoMax+1; 
		
		$NmSyarat = base64_decode(@$_GET['NmSy']);
		$KodeDokPenduduk = base64_decode(@$_GET['KodeDok']);
		$KdSyarat = base64_decode(@$_GET['KdSyarat']);
		
		$PindahDokumen = @mysqli_query($koneksi, "INSERT INTO dokumensyaratmohon (NoTrMohon,NoUrutSyarat,KodeLokasi,NamaSyarat,KodeDokPenduduk,IDPend,IsVerified,KodeSyarat) VALUES (
		'$No_Transaksi','$NoSekarang','$kode_lokasi_aktif','$NmSyarat','$KodeDokPenduduk','$id_penduduk_aktif','0','$KdSyarat')");
		
		echo '<script language="javascript">document.location="PenomoranSurat.php?aksi=tampil"; </script>';
	}
*/
	
	
	if(isset($_POST['CheckAll'])){
		if(@$_REQUEST['ProsesSurat'] === 'TerimaBerkas'){
			$cekbox = @$_POST['cekbox'];
			if($cekbox){
				foreach($cekbox as $value){
					$TerimaDokumen = @mysqli_query($koneksi, "UPDATE progresssurat SET IsSend=b'0' WHERE KodeLokasi='$login_lokasi' AND NoTrMohon='$value' AND IsSend=b'1'");
					if($TerimaDokumen){
						$QueryCekData = @mysqli_query($koneksi, "SELECT MAX(NoUrutProgress) as NoSekarang, UserName FROM progresssurat WHERE KodeLokasi='$login_lokasi' AND NoTrMohon='$value'"); 
						$CekData = @mysqli_fetch_array($QueryCekData);
						$NoMax = $CekData['NoSekarang']; $NoSekarang = $NoMax+1; $UserMax = $CekData['UserName']; 
						
						// simpan penerimaan berkas
						@mysqli_query($koneksi, "INSERT INTO progresssurat (NoUrutProgress,NoTrMohon,KodeLokasi,StatusProgress,UserName,Waktu,Tanggal,IsSend,IsConfirmed,WaktuConfirm,TglConfirm,UserConfirm)VALUES('$NoSekarang','$value','$login_lokasi','Penomoran (DESA)','$UserMax','$TanggalSekarang $JamSekarang','$TanggalSekarang $JamSekarang',b'0',b'1','$TanggalSekarang $JamSekarang','$TanggalSekarang $JamSekarang','$login_id')");
						
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Sukses!",
							text: " Berkas Permohonan Surat telah diterima",
							type: "success"
						  },
						  function () {
							window.location.href = "PenomoranSurat.php";
						  });
						  </script>';	
					} else {
						echo '<script language="javascript">document.location="PenomoranSurat.php"; </script>';
					}
				}
							
			}
			else{
				echo '<script language="javascript">document.location="PenomoranSurat.php"; </script>';	
			}
		} elseif(@$_REQUEST['ProsesSurat'] === 'AlihkanBerkas') {
			$cekbox = @$_POST['cekbox'];
			$tujuan = @$_POST['TujuanSurat'];
			if($cekbox){
				foreach($cekbox as $key => $value){
					$UpdateData = @mysqli_query($koneksi, "UPDATE progresssurat SET IsSend=b'1',SendTo='$tujuan[$key]',WaktuSend='$TanggalSekarang $JamSekarang',TglSend='$TanggalSekarang $JamSekarang',UserSender='$login_id',IsConfirmed=b'0' WHERE KodeLokasi='$login_lokasi' AND NoTrMohon='$value' AND StatusProgress='Penomoran (DESA)'");
					if($UpdateData){
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Berhasil!",
							text: " Permohonan akan diteruskan ke proses selanjutnya!",
							type: "success"
						  },
						  function () {
							window.location.href = "PenomoranSurat.php";
						  });
						  </script>';
						
						}
				}
			}
			else{
				echo '<script language="javascript">document.location="PenomoranSurat.php"; </script>';	
			}
		}
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "PenomoranSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		
		function confirm_hapus() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "PenomoranSurat.php?aksi=tampil";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		
		function confirm_proses() {
			var answer = confirm("Apakah Anda yakin untuk memproses data ini ?")
			if (answer == true){
				window.location = "PenomoranSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		function confirm_check() {
			var answer = confirm("Apakah Anda yakin untuk menerima atau mengalihkan data ini ?")
			if (answer == true){
				window.location = "PenomoranSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Layanan Surat - Penomoran Surat</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
				    <ul class="nav nav-pills">
						<li <?php if(@$id==null){echo 'class="active"';} ?>>
							<a href="#home-pills" data-toggle="tab"><span class="btn btn-primary">Data Permohonan</span></a>&nbsp;
						</li>
						<!-- <li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary">Alihkan Berkas</span></a>
						</li> -->
					</ul><br/>
				  <div class="card">
					<div class="tab-content">
						<div class="tab-pane fade <?php if(@$_GET['aksi']==null){ echo 'in active show'; }?>" id="home-pills">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Data Permohonan</h3>
							</div>
							<div class="card-body">							  
								<!-- <div class="col-lg-4 offset-lg-8">
									<form method="post" action="">
										<div class="form-group input-group">						
											<input type="text" name="keyword" class="form-control" placeholder="Nama Surat" value="<?php echo @$_REQUEST['keyword']; ?>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="submit">Cari</button>
											</span>
										</div>
									</form>
								</div> -->
							  <div class="table-responsive">  
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th></th>
									  <th>No</th>
									  <!-- <th>Kode Transaksi</th> -->
									   <?php if(@$_REQUEST['ProsesSurat'] === 'AlihkanBerkas'){
										  echo "<th>Tujuan</th>";
									  } else {
										  echo "<th>Dari</th>";
									  } ?>
									  <th>Nama Surat</th>
									  <th>Estimasi Selesai</th>
									  <th>Status Permohonan</th>
									  <th>Aksi</th>
									</tr>
								  </thead>
								  
									<!-- select all -->
									<form method="post">
									<div class="row">
										<div class="col-lg-3">
											<select class="form-control" name="ProsesSurat" onchange="this.form.submit()" autocomplete="off" required>
											<option value="TerimaBerkas" <?php if(@$_REQUEST['ProsesSurat'] === 'TerimaBerkas'){ echo 'selected="selected"'; } ?>>Terima Berkas</option>
											<option value="AlihkanBerkas" <?php if(@$_REQUEST['ProsesSurat'] === 'AlihkanBerkas'){ echo 'selected="selected"'; } ?>>Alihkan Berkas</option>
											</select>
										</div>
										<div class="col-lg-5">
										<div class="form-group input-group">						
											<input type="text" name="keyword" class="form-control" placeholder="Nama Surat" value="<?php echo @$_REQUEST['keyword']; ?>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="submit">Cari</button>
											</span>
										</div>
										</div>
										<div class="col-lg-4">
											<input type="button" class="btn btn-warning" onclick="cek(this.form.cekbox)" value="Select All" />&nbsp;
											<input type="button" class="btn btn-danger" onclick="uncek(this.form.cekbox)" value="Clear All" />&nbsp;
											<input type="submit" class="btn btn-info" onclick="return confirm_check()" value="Proses" name="CheckAll" />
										</div>
									</div>
								<?php 
									//procedure paging
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if((isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>"")){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword= @$_REQUEST['keyword'];
										$reload = "PenomoranSurat.php?pagination=true&keyword=$keyword";
										if(@$_REQUEST['ProsesSurat'] === 'AlihkanBerkas'){
											$sql =  "SELECT a.NoTrMohon, a.TglPermohonan, a.Keterangan1, a.JenisSurat, a.KodeLokasi, a.IDPend, a.StatusPermohonan, b.Keterangan, 
											a.PrediksiTglSelesai FROM trpermohonanmasy a JOIN masterpengurusansurat b ON a.JenisSurat=b.JenisSurat JOIN progresssurat c ON a.NoTrMohon=c.NoTrMohon WHERE (a.StatusPermohonan='WAITING' OR a.StatusPermohonan='ON PROGRESS' OR a.StatusPermohonan='REJECTED') AND b.Keterangan LIKE '%$keyword%' AND a.KodeLokasi='$login_lokasi' AND c.StatusProgress='Penomoran (DESA)' AND c.IsConfirmed=b'1' ORDER BY a.TglPermohonan DESC";
										}else{
											$sql =  "SELECT a.NoTrMohon, a.TglPermohonan, a.Keterangan1, a.JenisSurat, a.KodeLokasi, a.IDPend, a.StatusPermohonan, b.Keterangan, 
											a.PrediksiTglSelesai, c.StatusProgress FROM trpermohonanmasy a JOIN masterpengurusansurat b ON a.JenisSurat=b.JenisSurat JOIN progresssurat c ON a.NoTrMohon=c.NoTrMohon WHERE (a.StatusPermohonan='WAITING' OR a.StatusPermohonan='ON PROGRESS' OR a.StatusPermohonan='REJECTED') AND b.Keterangan LIKE '%$keyword%' AND a.KodeLokasi='$login_lokasi' AND c.SendTo='Penomoran (DESA)' AND c.IsSend=b'1' ORDER BY a.TglPermohonan DESC";
										}
										$result = mysqli_query($koneksi, $sql);
									}else{
										//jika tidak ada pencarian pakai ini
										$reload = "PenomoranSurat.php?pagination=true";
										if(@$_REQUEST['ProsesSurat'] === 'AlihkanBerkas'){
											$sql =  "SELECT a.NoTrMohon, a.TglPermohonan, a.Keterangan1, a.JenisSurat, a.KodeLokasi, a.IDPend, a.StatusPermohonan, b.Keterangan, 
											a.PrediksiTglSelesai FROM trpermohonanmasy a JOIN masterpengurusansurat b ON a.JenisSurat=b.JenisSurat JOIN progresssurat c ON a.NoTrMohon=c.NoTrMohon WHERE (a.StatusPermohonan='WAITING' OR a.StatusPermohonan='ON PROGRESS' OR a.StatusPermohonan='REJECTED') AND a.KodeLokasi='$login_lokasi' AND c.StatusProgress='Penomoran (DESA)' AND c.IsConfirmed=b'1' ORDER BY a.TglPermohonan DESC";
										} else {
											$sql =  "SELECT a.NoTrMohon, a.TglPermohonan, a.Keterangan1, a.JenisSurat, a.KodeLokasi, a.IDPend, a.StatusPermohonan, b.Keterangan, 
											a.PrediksiTglSelesai, c.StatusProgress FROM trpermohonanmasy a JOIN masterpengurusansurat b ON a.JenisSurat=b.JenisSurat JOIN progresssurat c ON a.NoTrMohon=c.NoTrMohon WHERE (a.StatusPermohonan='WAITING' OR a.StatusPermohonan='ON PROGRESS' OR a.StatusPermohonan='REJECTED') AND a.KodeLokasi='$login_lokasi' AND c.SendTo='Penomoran (DESA)' AND c.IsSend=b'1' ORDER BY a.TglPermohonan DESC";
										}
										$result = mysqli_query($koneksi, $sql);
									}
									
									//pagination config start
									$rpp = 10; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end	
								
									?>
									
									<tbody>
									<?php if($tcount == null OR $tcount === 0){
										echo '<tr class="odd gradeX"><td colspan="7" align="center"><br><h5>Tidak Ada Data</h5><br></td></tr>';
									} else {
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
									$data = mysqli_fetch_array($result);
									?>
									
									<tr class="odd gradeX">
										<td><input type="checkbox" id="cekbox" name="cekbox[]" value="<?php echo $data['NoTrMohon'] ?>"/></td>
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<!-- <td align="center">
											<?php echo $data['NoTrMohon'];?>
										</td> -->
										<td align="center">
											<?php if(@$_REQUEST['ProsesSurat'] === 'AlihkanBerkas'){
												echo '<select class="form-control" name="TujuanSurat[]" autocomplete="off" required>
														<option value="Verifikasi Berkas (DESA)">Verifikasi</option>
														<option value="Proses Berkas (DESA)">Proses</option>
														<option value="Pengesahan (DESA)">Pengesahan</option>
													  </select>';
														// <option value="Front Office (KEC)">Kecamatan</option>
											} else {
												echo $data['StatusProgress'];
											} ?>
										</td>
										<td>
											<?php echo $data['Keterangan']." atas user :<br>";
											// cari user
											$CariData = @mysqli_query($koneksi, "SELECT Nama, NIK FROM datapenduduk WHERE IDPend = '".$data['IDPend']."'"); 
											$array = @mysqli_fetch_array($CariData);
											echo "<strong>".$array['NIK']."</strong><br>".$array['Nama']."<br>";
											echo "Pengajuan : ".$data['NoTrMohon']."<br>";
											echo "Pada : ".TanggalIndo($data['TglPermohonan'])."<br>";
											?>
										</td>
										<td align="center">
											<?php echo TanggalIndo($data['PrediksiTglSelesai']);?>
										</td>
										<td>
											<?php 
											if($data['StatusPermohonan']=='WAITING'){
												echo "<font color='#f39c12'>Menunggu</font>";
											}elseif($data['StatusPermohonan']=='ON PROGRESS'){
												echo "<font color='#2ecc71'>Berkas Diproses</font>";
											}elseif($data['StatusPermohonan']=='REJECTED'){
												echo "<font color='#e74c3c'>Tinjau Ulang</font>";
											}elseif($data['StatusPermohonan']=='DONE'){
												echo "<font color='#3498db'>Selesai</font>";
											}
											;?>
										</td>
                                        <td width="150px" align="center">
											<?php /*if($data['StatusPermohonan']=='WAITING'){ 
												echo '<a href="PenomoranSurat.php?kd='.base64_encode($data['NoTrMohon']).'&aksi='.base64_encode('HapusTransaksi').'" onclick="return confirmation()">
													<span class="btn btn-sm btn-danger fa fa-close" title="Hapus Surat"></span></a>';
											}elseif($data['StatusPermohonan']=='ON PROGRESS'){
												echo '
												<a href="#" class="open_modal_tracking" data-notrmohon="'.$data['NoTrMohon'].'" data-namasurat="'.$data['Keterangan'].'" data-kodelokasi="'.$kode_lokasi_aktif.'">
												<span class="btn btn-sm btn-primary fa fa-share-alt" title="Lihat Progress Surat"></span></a>';
											}elseif($data['StatusPermohonan']=='REJECTED'){
												echo '<a href="TinjauUlang.php?id='.base64_encode($data['NoTrMohon']).'"><span class="btn btn-sm btn-warning fa fa-refresh" title="Tinjau ulang berkas"></span></a>';
											} */
											?>
											<!-- <a href="#" class='open_modal' data-notrmohon='<?php echo $data['NoTrMohon'];?>' data-kodelokasi='<?php echo $data['KodeLokasi'];?>' data-idpenduduk='<?php echo $data['IDPend'];?>' data-surat='<?php echo $data['JenisSurat'];?>'>
											<span class="btn btn-sm btn-primary fa fa-search" title="Lihat Dokumen"></span></a>
											
											<a href="#" class="open_modal_tracking" data-notrmohon='<?php echo $data['NoTrMohon'];?>' data-kodelokasi='<?php echo $data['KodeLokasi'];?>' data-idpenduduk='<?php echo $data['IDPend'];?>' data-surat='<?php echo $data['JenisSurat'];?>' data-user='<?php echo $login_id; ?>'>
											<span class="btn btn-sm btn-success fa fa-share-alt" title="Lihat Progress Surat"></span></a> -->
											
											<?php if(@$_REQUEST['ProsesSurat'] === 'AlihkanBerkas'){ ?>
											<a href="#" class='open_modal_verif' data-notrmohon='<?php echo $data['NoTrMohon'];?>' data-kodelokasi='<?php echo $data['KodeLokasi'];?>' data-idpenduduk='<?php echo $data['IDPend'];?>' data-surat='<?php echo $data['JenisSurat'];?>'>
											<span class="btn btn-sm btn-success fa fa-list" title="Penomoran"></span></a>
											
											<a href="ViewPrint.php?kode=<?php echo base64_encode($data['NoTrMohon']); ?>&loc=<?php echo base64_encode($data['KodeLokasi']); ?>&jns=<?php echo base64_encode($data['JenisSurat']); ?>" target="_blank">
											<span class="btn btn-sm btn-warning fa fa-print" title="Preview"></span></a>
											<?php } else { ?>
											<a href="#" class='open_modal' data-notrmohon='<?php echo $data['NoTrMohon'];?>' data-kodelokasi='<?php echo $data['KodeLokasi'];?>' data-idpenduduk='<?php echo $data['IDPend'];?>' data-surat='<?php echo $data['JenisSurat'];?>'>
											<span class="btn btn-sm btn-success fa fa-search" title="Lihat Dokumen"></span></a>
											<?php } ?>
											
											<a href="#" class="open_modal_tracking" data-notrmohon='<?php echo $data['NoTrMohon'];?>' data-kodelokasi='<?php echo $data['KodeLokasi'];?>' data-idpenduduk='<?php echo $data['IDPend'];?>' data-surat='<?php echo $data['JenisSurat'];?>' data-user='<?php echo $login_id;?>'>
											<span class="btn btn-sm btn-primary fa fa-share-alt" title="Lihat Progress Surat"></span></a>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									}
									?>
									
									</tbody>
									</form>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							  </div>
							</div>
						</div>
						<div class="tab-pane fade <?php if(@$_GET['aksi']=='tampil'){ echo 'in active show'; }?>" id="tambah-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Buat Permohonan Surat</h3>
							</div>
							<div class="card-body">
								<div class="row">
								  <div class="col-sm-8">
									<h5>1. Pilih Jenis Surat</h5>
									<form method="post" action="">
									<div class="form-group-material">
										<div class="input-group">
											<input type="hidden" name="NoTrMohon" value="<?php echo @$No_Transaksi; ?>">
											<select class="form-control" name="JenisSurat" autocomplete="off" required>
											<?php echo '<option value="">-- Pilih Jenis Surat --</option>';
												$list = @mysqli_query($koneksi, "SELECT * FROM masterpengurusansurat WHERE LEFT(KodeLokasi,3) = '".substr($kode_lokasi_aktif,0,3)."' ORDER BY JenisSurat ASC"); 
												while($daftar = @mysqli_fetch_array($list)){
													if($daftar['JenisSurat'] === @$Jenis_Surat){
														echo "<option value=\"".$daftar['JenisSurat']."\" selected>".ucwords($daftar['Keterangan'])."</option>\n";
													}else{
														echo "<option value=\"".$daftar['JenisSurat']."\">".ucwords($daftar['Keterangan'])."</option>\n";
													}
												}
											?>
											</select>
											<div class="input-group-append">
											<?php 
												if($numCek != 0 OR $numCek != null){ 
													echo '<button type="submit" class="btn btn-primary" name="GantiSurat">Buat Surat</button>';												
												}else{
													echo '<button type="submit" class="btn btn-primary" name="BuatSurat">Buat Surat</button>';
												}
											?>
											 
											</div>
										</div>
									</div>
									</form>
									<!--<div class="form-group-material">
									  <input id="register-username" type="text" name="registerUsername" required class="input-material">
									  <label for="register-username" class="label-material">Username</label>
									</div>
									<div class="form-group-material">
									  <input id="register-email" type="email" name="registerEmail" required class="input-material">
									  <label for="register-email" class="label-material">Email Address      </label>
									</div>-->
								  </div>
								  <div class="col-lg-8">
										<?php if($numCek != 0 OR $numCek != null){ ?>
										  <h5>2. Syarat Permohonan Surat</h5>
										  <div class="table-responsive">  
											<table class="table table-striped">
											  <thead>
												<tr>
												  <th>No</th>
												  <th>Nama Syarat</th>
												  <th>Keterangan</th>
												  <th>Dokumen</th>
												</tr>
											  </thead>
											  <tbody>
												<?php
												//AND b.NoTrMohon='$No_Transaksi'
												$no =1;
												$sql_syarat = @mysqli_query($koneksi, "SELECT * FROM mstsyaratsurat WHERE JenisSurat='$Jenis_Surat' AND LEFT(KodeLokasi,3)='".substr($kode_lokasi_aktif,0,3)."'"); 
												while($data_syarat = @mysqli_fetch_array($sql_syarat)){
												?>
												<tr>
												  <th scope="row"><?php echo $no++; ?></th>
												  <td><?php echo $data_syarat['NamaSyarat'];?></td>
												  <td><?php echo $data_syarat['Keterangan'];?></td>
												  <?php 
														$CekDok = @mysqli_query($koneksi, "SELECT NoTrMohon, UrlFile FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi' AND KodeSyarat='".$data_syarat['KodeSyarat']."'"); 
														$numCek = @mysqli_num_rows($CekDok); 
														$numCari = @mysqli_fetch_array($CekDok); 
													?>
												  <td width="100px">
													<?php if($numCek!=0){ ?>
														<span class="btn btn-success btn-sm fa fa-check"></span>
														<a href="PenomoranSurat.php?id=<?php echo base64_encode($data_syarat['KodeSyarat']);?>&file=<?php echo base64_encode($numCari['UrlFile']); ?>&aksi=<?php echo base64_encode('Hapus');?>" onclick="return confirm_hapus()"><span class="btn btn-danger btn-sm fa fa-close"></span></a>
													<?php }else{ ?>
														<a href="#" class='open_modal' data-notrmohon='<?php echo $No_Transaksi;?>' data-kodelokasi='<?php echo $kode_lokasi_aktif;?>' data-idpenduduk='<?php echo $id_penduduk_aktif;?>' data-namasyarat='<?php echo $data_syarat['NamaSyarat'];?>' data-kodesyarat='<?php echo $data_syarat['KodeSyarat'];?>'>
															<span class="btn btn-primary btn-sm fa fa-upload" title="Unggah Dokumen"></span>
														</a>
														
														
														<a href="#" class='open_modal_dokumen' data-notrmohon='<?php echo $No_Transaksi;?>' data-kodelokasi='<?php echo $kode_lokasi_aktif;?>' data-idpenduduk='<?php echo $id_penduduk_aktif;?>' data-namasyarat='<?php echo $data_syarat['NamaSyarat'];?>' data-kodesyarat='<?php echo $data_syarat['KodeSyarat'];?>'>
														<span class="btn btn-info btn-sm fa fa-list" title="Lihat Dokumen"></span>
														</a>
													<?php } ?>
												  </td>
												</tr>
												<?php } ?>
											  </tbody>
											</table>
										  </div>
										  <hr/>
										  <h5>3. Keterangan Permohonan</h5>
										  <form method="post" action="">
											<input  type="hidden" name="NoTrMohon" value="<?php echo $No_Transaksi; ?>">
											<div class="form-group-material">
											  <input type="text" name="Ket" class="form-control" placeholder="Keterangan">
											</div>
											
											<button type="submit" class="btn btn-primary" name="KirimPermohonan">Kirim Permohonan Surat</button>
											<button type="submit" class="btn btn-danger" name="BatalkanPermohonan">Batalkan Permohonan Surat</button>
										  </form>
										<?php } ?>
								  </div>
								</div>
							</div>
						</div>
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
	<!-- Modal Popup untuk Edit--> 
	<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
	
	<div id="ModalEditDokumen" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
	<div id="ModalEditTracking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="komponen/js/front.js"></script>
	<!-- Javascript untuk popup modal Edit--> 
	<script type="text/javascript">
	   // open modal lihat dokumen
	   $(document).ready(function () {
	   $(".open_modal").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  	   $.ajax({
					   url: "LihatDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		// open modal lihat progress
	   $(document).ready(function () {
	   $(".open_modal_tracking").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  var user  = $(this).data("user");
		  	   $.ajax({
					   url: "TrackingDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat,User: user},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		 // open modal lihat dokumen
	   $(document).ready(function () {
	   $(".open_modal_verif").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  	   $.ajax({
					   url: "VerifikasiPenomoran.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
	  /* // open modal unggah dokumensyarat 
		$(document).ready(function () {
	   $(".open_modal").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var nm_syarat = $(this).data("namasyarat");
		  var kd_syarat = $(this).data("kodesyarat");
			   $.ajax({
					   url: "UnggahDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,NamaSyarat: nm_syarat,KodeSyarat: kd_syarat},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		//open modal dokumensyarat
		$(document).ready(function () {
	    $(".open_modal_dokumen").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var nm_syarat = $(this).data("namasyarat");
		  var kd_syarat = $(this).data("kodesyarat");
			   $.ajax({
					   url: "LihatDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,NamaSyarat: nm_syarat,KodeSyarat: kd_syarat},
					   success: function (ajaxData){
					   $("#ModalEditDokumen").html(ajaxData);
					   $("#ModalEditDokumen").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		//open modal tracking
		$(document).ready(function () {
	    $(".open_modal_tracking").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var nm_surat = $(this).data("namasurat");
			   $.ajax({
					   url: "TrackingDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,NamaiSurat:nm_surat},
					   success: function (ajaxData){
					   $("#ModalEditTracking").html(ajaxData);
					   $("#ModalEditTracking").modal('show',{backdrop: 'true'});
				   }
				});
			});
		}); */
	</script>
	
	<script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>
	
	<?php
	if(isset($_POST['BuatSurat'])){
		// cek apakah sudah ada permohonan
		$cek = @mysqli_query($koneksi, "SELECT * from trpermohonanmasy where IDPend='$id_penduduk_aktif' AND JenisSurat='".$_POST['JenisSurat']."' AND (StatusPermohonan != 'REJECTED' OR StatusPermohonan != 'DONE')");
		$num = @mysqli_num_rows($cek);
		if($num > 0){
			echo '<script type="text/javascript">swal( "Permohonan Sudah Ada!", " Silahkan Tunggu Permohonan Selesai Diproses ", "error" ); </script>';
		}else{ 
			// membuat id otomatis
			$sql = @mysqli_query($koneksi, "SELECT MAX(RIGHT(NoTrMohon,8)) AS kode FROM trpermohonanmasy WHERE KodeLokasi='$kode_lokasi_aktif' AND LEFT(NoTrMohon,7)='MH-$Tahun'"); 
			$nums = @mysqli_num_rows($sql); 
			while($data = @mysqli_fetch_array($sql)){
			if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
			}
			// membuat kode user
			$bikin_kode = str_pad($kode, 8, "0", STR_PAD_LEFT);
			$kode_jadi = "MH-".$Tahun."-".$bikin_kode;
			
			//simpan trpermohonan masyarakat
			$SimpanData = @mysqli_query($koneksi, "INSERT INTO trpermohonanmasy (NoTrMohon,KodeLokasi,JenisSurat,IDPend,StatusPermohonan)VALUES('$kode_jadi','$kode_lokasi_aktif','".$_POST['JenisSurat']."','$id_penduduk_aktif','PraWAITING')"); 
			
			echo '<script language="javascript">document.location="PenomoranSurat.php?aksi=tampil"; </script>';	
		}
	}
	
	if(isset($_POST['GantiSurat'])){
		$cek = @mysqli_query($koneksi, "SELECT * from trpermohonanmasy where IDPend='$id_penduduk_aktif' AND JenisSurat='".$_POST['JenisSurat']."' AND (StatusPermohonan != 'REJECTED' OR StatusPermohonan != 'DONE')");
		$num = @mysqli_num_rows($cek);
		if($num > 0){
			echo '<script type="text/javascript">swal( "Permohonan Sudah Ada!", " Silahkan Tunggu Permohonan Selesai Diproses ", "error" ); </script>';
		}else{ 
			//hapus dokumensyarat mohon
			$HapusDokumenSyarat = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			if($HapusDokumenSyarat){
				//update trmohonmsy
				$UpdateData = @mysqli_query($koneksi, "UPDATE trpermohonanmasy SET JenisSurat='".$_POST['JenisSurat']."' WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi'"); 
			}
			
			echo '<script language="javascript">document.location="PenomoranSurat.php?aksi=tampil"; </script>';
		}
	}
	
	if(isset($_POST['KirimPermohonan'])){
		$cek = @mysqli_query($koneksi, "SELECT * from dokumensyaratmohon where NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
		$num = @mysqli_num_rows($cek);
		if($num === 0){
			echo '<script type="text/javascript">swal( "Maaf!", " Dokumen Persyaratan Anda Belum Lengkap ", "error" ); </script>';
		}else{ 
		
			$JamSekarang = date('H:i:s');
			$TanggalSekarang = date('Y-m-d');
			$JamLayananKantor = date('Y-m-d').' 12:00:00'; //jam tutup lyn hari ini
			
			$NamaHariIni = date('D', strtotime($HariIni)); //nama hari
			
			if(strtotime($HariIni) > strtotime($JamLayananKantor)){ //jika input diatas jam 12 siang maka diproses besok
				//Cek Ada berapa banyak hari sabtu
				$TglLibur = array();
				for($i=0; $i < $Standar_Waktu; $i++){
					$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
					$NmHari = date('D', strtotime($TglHari));
					$TglLibur[]=$NmHari;
				}
				//cari ada berapa banayak hari saturday
				$Count = array_count_values($TglLibur); 
				$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
				
				$TotalLamaPelayanan = $Standar_Waktu+$JmlTglLibur;
				$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
				
			}else{ //surat diproses hari ini
				//Cek Ada berapa banyak hari sabtu
				$StandarWaktuBaru  = $Standar_Waktu-1;
				$TglLibur = array();
				for($i=0; $i < $StandarWaktuBaru; $i++){
					$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
					$NmHari = date('D', strtotime($TglHari));
					$TglLibur[]=$NmHari;
				}
				//cari ada berapa banayak hari saturday
				$Count = array_count_values($TglLibur); 
				$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
				
				$TotalLamaPelayanan = $StandarWaktuBaru+$JmlTglLibur;
				$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
			}
			 
			//insert to progress surat & update trpermohonanmsy
			$UpdateTrPermohonan = @mysqli_query($koneksi, "UPDATE trpermohonanmasy SET TglPermohonan='$HariIni',PrediksiTglSelesai='$PrediksiTglSelesai', Keterangan1='".$_POST['Ket']."', StatusPermohonan='WAITING' WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			
			if($UpdateTrPermohonan){
				$InsertData = @mysqli_query($koneksi, "INSERT INTO progresssurat (NoUrutProgress,NoTrMohon,KodeLokasi,StatusProgress,Waktu,Tanggal,SendTo,UserSender,IsSend,WaktuSend,TglSend,IsConfirmed)
				VALUES('1','$No_Transaksi','$kode_lokasi_aktif','Pemohon/Masyarakat','$JamSekarang','$TanggalSekarang','Front Office (DESA)','$id_penduduk_aktif','1','$JamSekarang','$HariIni','0')");
				
				if($InsertData){
					echo '<script type="text/javascript">
					  sweetAlert({
						title: "Permohonan Terkirim!",
						text: " Permohonan surat akan diproses pada jam kerja aktif!",
						type: "success"
					  },
					  function () {
						window.location.href = "PenomoranSurat.php";
					  });
					  </script>';
				}else{
					echo '<script type="text/javascript">
						  sweetAlert({
							title: "Simpan Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "PenomoranSurat.php";
						  });
						  </script>';
				} 
			}
		}
	}
	
	if(isset($_POST['BatalkanPermohonan'])){
		//hapus dokumensyarat mohon
		$HapusDokumenSyarat = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
		if($HapusDokumenSyarat){
			$HapusTrPermohonan = @mysqli_query($koneksi, "DELETE FROM trpermohonanmasy WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			if($HapusTrPermohonan){
				echo '<script type="text/javascript">
				  sweetAlert({
					title: "Hapus Data Berhasil",
					text: " ",
					type: "success"
				  },
				  function () {
					window.location.href = "PenomoranSurat.php";
				  });
				  </script>';
			}else{
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "PenomoranSurat.php";
					  });
					  </script>';
			}
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "PenomoranSurat.php";
					  });
					  </script>';
		}
	}
	
	//Hapus Transaksi
	if(base64_decode(@$_GET['aksi'])==='HapusTransaksi'){
		//cek apakah status permohonan waiting
		$QueryCekStatus = @mysqli_query($koneksi, "SELECT NoTrMohon FROM trpermohonanmasy WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."' AND StatusPermohonan='WAITING'"); 
		$numCekStatus = @mysqli_num_rows($QueryCekData); 
		if($numCekStatus > 0){
			$HapusData = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'"); 
			
			if($HapusData){
				$HapusProgress = @mysqli_query($koneksi, "DELETE FROM progresssurat WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'"); 
				
				if($HapusProgress){
					$HapusTrans = @mysqli_query($koneksi, "DELETE FROM trpermohonanmasy WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'"); 
					
					if($HapusProgress){
						echo '<script language="javascript">document.location="PenomoranSurat.php"; </script>';
					}else{
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "PenomoranSurat.php";
						  });
						  </script>';
					}
				}else{
					echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "PenomoranSurat.php";
						  });
						  </script>';
				}
			}else{
				echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "PenomoranSurat.php";
						  });
						  </script>';
			}
		}else{
			echo '<script type="text/javascript">
			  sweetAlert({
				title: "Hapus Data Gagal!",
				text: " Permohonan Anda sedang dalam prosess verifikasi ",
				type: "error"
			  },
			  function () {
				window.location.href = "PenomoranSurat.php";
			  });
			  </script>';
		}
	}
	
	
	?>
  </body>
</html>