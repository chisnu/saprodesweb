<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $NoTrMohon	=@$_GET['NoTrMohon'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $IDPend	=@$_GET['IDPend'];
 $JenisSurat=@$_GET['JenisSurat'];
 // $Aksi	=@$_GET['Aksi']; //tinjau ulang digunakan jika dokumen di rejected 
?>

	
<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Cari Penduduk</h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
								<div class="form-group">
              <select id="CariPenduduk2" name="KodePersonTujuan" class="form-control" style="width: 100% !important;">
              <option value=""></option>
              <?php
                $menu = mysqli_query($koneksi,"SELECT NIK, Nama FROM datapenduduk");
                while($kode = mysqli_fetch_array($menu)){
                  echo "<option value=\"".$kode['NIK']."\" >".$kode['NIK']." : ".$kode['Nama']."</option>\n";
                }
              ?>
              </select>
            </div>
								<form method="post" action="">
									<div class="form-group input-group">						
										<input type="text" name="keyword" class="form-control" placeholder="Nama atau No KTP" value="<?php echo @$_REQUEST['keyword']; ?>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="submit">Cari</button>
										</span>
									</div>
										
								<div class="table-responsive">  
									<table class="table table-striped">
									  <thead>
										<tr>
										  <th>Nama</th>
										  <th>Kewarganegaraan</th>
										  <th>Tempat/Tanggal Lahir</th>
										  <th>Pekerjaan</th>
										  <th>Alamat</th>
										</tr>
									  </thead>
									  <tbody>
										<?php 
											$no =1;
											$sql_user = @mysqli_query($koneksi, "SELECT NIK, Nama, TempatLahir, TglLahir, Pekerjaan, Alamat FROM datapenduduk WHERE (NIK LIKE '%$keyword%' OR Nama LIKE '%$keyword%')"); 
											$jumlah = @mysqli_num_rows($sql_user);
											if($jumlah = 0){
												echo '<tr><td colspan="5" align="center"><br><h5>Tidak Ada Data</h5><br></td></tr>';
											}else{
											while($data_user = @mysqli_fetch_array($sql_syarat)){
										?>
												<tr>
												  <?php // cek apakah sudah lengkap
														echo '<td>';
														if($data_syarat['IsVerified'] === '1'){
															echo '<input type="checkbox" id="syarat" name="syarat[]" value="'.$data_syarat["KodeSyarat"].'" checked="checked" disabled />';
														} else {
															echo '<input type="checkbox" id="syarat" name="syarat[]" value="'.$data_syarat["KodeSyarat"].'"/>';
														}
														echo '
															  <input type="hidden" name="notrmohon[]" value="'.$NoTrMohon.'"/>
															  <input type="hidden" name="kodelokasi[]" value="'.$KodeLokasi.'"/>
															  <input type="hidden" name="idpend[]" value="'.$IDPend.'"/></td>';
														
														echo '<th scope="row">'.$no++.'</th>';
														echo '<td>'.$data_syarat["NamaSyarat"].'</td>';
												  
														$numLengkap = @mysqli_num_rows($sql_syarat); 
														if($numLengkap = 0){
															echo "<td><font color='#3498db'>Belum</font></td>";
														}else{
															echo "<td><font color='#3498db'>Lengkap</font></td>";
														}
												  ?>
												  <td width="50px">
														<a href="#" class='open_modal_image' data-notrmohon='<?php echo $NoTrMohon;?>' data-lokasi='<?php echo $KodeLokasi;?>'>
														<span class="btn btn-info btn-sm fa fa-search" title="Lihat Dokumen"></span>
														</a>
												  </td>
												</tr>
									  <?php } } ?>
									  </tbody>
									</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<div id="ModalEditImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
<script src="../library/select2master/js/select2.min.js"></script>
	  <script>
	  $(document).ready(function () {
		$("#CariPenduduk2").select2({
			placeholder: "NIK atau Nama Penduduk"
		});
	  });
	  </script>
<script type="text/javascript">
	   $(document).ready(function () {
	   $(".open_modal_image").click(function(e) {
		  // var kd_dok = $(this).data("kodedok");
		  // var id_pend  = $(this).data("idpend");
		  // var dok  = $(this).data("filedok");
		  var no_trmohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("lokasi");
			   $.ajax({
					   url: "DetilDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_trmohon,KodeLokasi: kode_lok},
					   success: function (ajaxData){
					   $("#ModalEditImage").html(ajaxData);
					   $("#ModalEditImage").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
	
	<script>
		function confirm_verif() {
			var answer = confirm("Apakah Anda yakin untuk memverifikasi data ini ?")
			if (answer == true){
				window.location = "VerifikasiSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
	
	<script>
		function cek(syarat){
			for(i=0; i < syarat.length; i++){
				syarat[i].checked = true;
			}
		}
		function uncek(syarat){
			for(i=0; i < syarat.length; i++){
				syarat[i].checked = false;
			}
		}
	</script>