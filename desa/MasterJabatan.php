<?php
include 'akses.php';
$fitur_id = 1;
include '../library/lock-menu.php';

$Page = 'MasterData';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM mstjabatan WHERE KodeJabatan='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
	$RowData = mysqli_fetch_assoc($Edit);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.blue.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Master Jabatan</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<ul class="nav nav-pills">
						<li <?php if(@$id==null){echo 'class="active"';} ?>>
							<a href="#home-pills" data-toggle="tab"><span class="btn btn-primary">Data Jabatan</span></a>&nbsp;
						</li>
						<li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary"><?php echo $Sebutan; ?></span></a>
						</li>
					</ul><br/>
				  <div class="card">
					<div class="tab-content">
						<div class="tab-pane fade <?php if(@$_GET['id']==null){ echo 'in active show'; }?>" id="home-pills">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Data Jabatan</h3>
							</div>
							<div class="card-body">							  
								<!--<div class="col-lg-4 offset-lg-8">
									<form method="post" action="">
										<div class="form-group input-group">						
											<input type="text" name="keyword" class="form-control" placeholder="Nama..." value="<?php echo @$_REQUEST['keyword']; ?>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="submit">Cari</button>
											</span>
										</div>
									</form>
								</div>-->
							  <div class="table-responsive">  
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>No</th>
									  <th>Nama Jabatan</th>
									  <th>Keterangan</th>
									  <th>Aksi</th>
									</tr>
								  </thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword=$_REQUEST['keyword'];
											$reload = "UserLogin.php?pagination=true&keyword=$keyword";
											$sql =  "SELECT * FROM userlogin WHERE ActualName LIKE '%$keyword%' AND KodeLokasi='$login_lokasi' AND IsDihapus='0' ORDER BY ActualName ASC";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "MasterJabatan.php?pagination=true";
											$sql =  "SELECT * FROM mstjabatan WHERE KodeLokasi='$login_lokasi' ORDER BY KodeJabatan ASC";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 20; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td>
												<strong><?php echo $data ['NamaJabatan']; ?></strong>
											</td>
											<td>
												<?php echo $data ['Keterangan']; ?>
											</td>
											<td width="100px" align="center">										
												<?php
												/* if($data ['KodeJabatan'] != 'JBT-00001' AND $data ['KodeJabatan'] != 'JBT-00002' AND $data ['KodeJabatan'] != 'JBT-00003'){
													echo '<a href="MasterJabatan.php?id='.base64_encode($data['KodeJabatan']).'" title="Edit"><i class="btn btn-warning btn-sm"><span class="fa fa-edit"></span></i></a>&nbsp;';
													
													echo '<a href="MasterJabatan.php?id='.base64_encode($data['KodeJabatan']).'&nm='.$data['NamaJabatan'].'&aksi='.base64_encode('Hapus').'" title="Hapus" onclick="return confirmation()"><i class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></i></a>';
												} else {
													echo '<a href="MasterJabatan.php?id='.base64_encode($data['KodeJabatan']).'" title="Edit"><i class="btn btn-warning btn-sm"><span class="fa fa-edit"></span></i></a>&nbsp;';
													
													echo '<a href="#" title="Data Sistem"><i class="btn btn-info btn-sm"><span class="fa fa-gear"></span></i></a>';
												} */ 
												
												echo '<a href="MasterJabatan.php?id='.base64_encode($data['KodeJabatan']).'" title="Edit"><i class="btn btn-warning btn-sm"><span class="fa fa-edit"></span></i></a>';
												
												?>
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										?>
									</tbody>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							  </div>
							</div>
						</div>
						<div class="tab-pane fade <?php if(@$_GET['id']!=null){ echo 'in active show'; }?>" id="tambah-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4"><?php echo $Sebutan; ?></h3>
							</div>
							<div class="card-body">
								<div class="row">
								  <div class="col-lg-6">
									  <form method="post" action="">
										<div class="form-group-material">
										  <input type="text" name="Nama" class="form-control" placeholder="Nama Jabatan" value="<?php echo @$RowData['NamaJabatan'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Keterangan" class="form-control" placeholder="Keterangan" value="<?php echo @$RowData['Keterangan'];?>" required>
										</div>
										<?php
										if(@$_GET['id']==null){
											echo '<button type="submit" class="btn btn-primary" name="Simpan">Simpan</button>';
										}else{
											echo '<input type="hidden" name="KodeJabatan" value="'.$RowData['KodeJabatan'].'"> ';
											echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
											echo '<a href="UserLogin.php"><span class="btn btn-warning">Batalkan</span></a>';
										}
										?>
									  </form>
								  </div>
								</div>
							</div>
						</div>
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	
	
	<?php
	if(isset($_POST['Simpan'])){
		/* include ('../library/kode-log-server.php'); */
		//kode jabatan
		$sql_jbt = mysqli_query($koneksi,'SELECT RIGHT(KodeJabatan,5) AS kode FROM mstjabatan WHERE KodeLokasi="'.$login_lokasi.'" ORDER BY KodeJabatan DESC LIMIT 1');  
		$nums_jbt = mysqli_num_rows($sql_jbt);
		 
		if($nums_jbt <> 0)
		 {
		 $data_jbt = mysqli_fetch_array($sql_jbt);
		 $kode_jbt = $data_jbt['kode'] + 1;
		 }else
		 {
		 $kode_jbt = 1;
		 }
		 
		//mulai bikin kode
		 $bikin_kode_jbt = str_pad($kode_jbt, 5, "0", STR_PAD_LEFT);
		 $kode_jadi_jbt = "JBT-".$bikin_kode_jbt;
		
		$query = mysqli_query($koneksi,"INSERT into mstjabatan (KodeJabatan,NamaJabatan,Keterangan,KodeLokasi) 
		VALUES ('$kode_jadi_jbt','".$_POST['Nama']."','".$_POST['Keterangan']."','$login_lokasi')");
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Master Jabatan : ".$_POST['Nama']."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterJabatan.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		}
	}
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		
		$query = mysqli_query($koneksi,"UPDATE mstjabatan SET NamaJabatan='".$_POST['Nama']."',Keterangan='".$_POST['Keterangan']."' WHERE KodeJabatan='".$_POST['KodeJabatan']."' AND KodeLokasi='$login_lokasi'");
		
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Edit Master Jabatan :".$_POST['Nama']."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterJabatan.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Edit Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		/* include ('../library/kode-log-server.php'); */
		$sql_jbt = mysqli_query($koneksi,'SELECT * FROM mstjabatan WHERE KodeLokasi="'.$login_lokasi.'" AND KodeJabatan="'.base64_decode($_GET['id']).'"');  
		$nums_jbt = mysqli_num_rows($sql_jbt);
		if($nums_jbt <> 0){
			echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " Data Jabatan dipakai Master Aparat! ",
							type: "error"
						  },
						  function () {
							window.location.href = "MasterJabatan.php";
						  });
						  </script>';
		} else {
			$query = mysqli_query($koneksi,"DELETE FROM mstjabatan WHERE KodeJabatan='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
			if($query){
				/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
				VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Master Jabatan : ".$_GET['nm']."','$login_id','$login_lokasi')"); */
				echo '<script language="javascript">document.location="MasterJabatan.php"; </script>';
			}else{
				echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " Hapus data di Master Aparatur terlebih dahulu! ",
							type: "error"
						  },
						  function () {
							window.location.href = "MasterJabatan.php";
						  });
						  </script>';
			}
		}
	}
	
	?>
  </body>
</html>