<nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="title">
              <p>Selamat Datang</p>
              <h1 class="h4"><?php echo $login_nama; 
			  echo '<br/><font color="#796AEE" size="3px">'; echo ucwords(strtolower($login_desa));echo '</font>';?></h1>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled">
			<li <?php if($Page=='index'){echo 'class="active"';} ?>><a href="index.php"> <i class="icon-home"></i>Dashboard </a></li>
			<li <?php if($Page=='MasterData'){echo 'class="active"';} ?> ><a href="#Master" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-list"></i>Master Data </a>
			  <ul id="Master" class="collapse list-unstyled ">
				<li><a href="MasterJabatan.php">Master Jabatan</a></li>
				<li><a href="MasterAparatur.php">Master Aparatur</a></li>
				<li><a href="MasterDusun.php">Master Dusun</a></li>
			  </ul>
			</li>
			<?php if(@$LetakMenu=='1'){ ?>	
				<li <?php if($Page=='LayananSurat'){echo 'class="active"';} ?>><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-envelope-o"></i>Front Office </a>
					  <ul id="exampledropdownDropdown" class="collapse list-unstyled nav nav-pills">
						<li><a href="#home-pills" data-toggle="tab">Data Permohonan</a></li>
						<li><a href="#tambah-user" data-toggle="tab">Buat Permohonan Surat</a></li>
					  </ul>
				</li>
			<?php }else{ ?>	
				<li <?php if($Page=='LayananSurat'){echo 'class="active"';} ?>><a href="LayananSurat.php"> <i class="fa fa-envelope-o"></i>Front Office </a></li>
			<?php } ?>
			<li <?php if($Page=='ProgressSurat'){echo 'class="active"';} ?> ><a href="#Progress" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-refresh"></i>Progress Surat</a>
			  <ul id="Progress" class="collapse list-unstyled ">
				<li><a href="VerifikasiSurat.php">Verifikasi</a></li>
				<!-- <li><a href="ProsesSurat.php">Pemrosesan</a></li> -->
				<li><a href="PengesahanSurat.php">Pengesahan</a></li>
				<!-- <li><a href="PenomoranSurat.php">Penomoran</a></li> -->
			  </ul>
			</li>
			<li <?php if($Page=='Kependudukan'){echo 'class="active"';} ?>><a href="#Kependudukan" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-sitemap"></i>Kependudukan </a>
				<ul id="Kependudukan" class="collapse list-unstyled ">
					<li><a href="DataPenduduk.php">Data Penduduk (B.1)</a></li>
					<!-- <li><a href="LaporanPenduduk.php">Laporan Penduduk</a></li>
					<li><a href="InputKelahiran.php">Input Kelahiran</a></li>
					<li><a href="InputKematian.php">Input Kematian</a></li>
					<li><a href="InputMutasiMasuk.php">Input Mutasi Masuk</a></li>
					<li><a href="InputMutasiKeluar.php">Input Mutasi Keluar</a></li> -->
				</ul>
			</li>
			<li <?php if($Page=='Pajak'){echo 'class="active"';} ?>><a href="#Pajak" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-dollar"></i>Pajak PBB </a>
				<ul id="Pajak" class="collapse list-unstyled ">
					<li><a href="MasterPajak.php">Data Objek Pajak</a></li>
					<li><a href="PenerimaanBayar.php">Penerimaan Bayar</a></li>
					<li><a href="LaporanBayarPBB.php">Laporan Bayar PBB</a></li>
					
				</ul>
			</li>
			<li <?php if($Page=='Register'){echo 'class="active"';} ?>><a href="Register.php"> <i class="fa fa-file-text-o"></i>Register </a></li>
			<li <?php if($Page=='Akun'){echo 'class="active"';} ?>><a href="Akun.php"> <i class="fa fa-user"></i>Profil </a></li>
			<li <?php if($Page=='UbahPassword'){echo 'class="active"';} ?>><a href="UbahPassword.php"> <i class="fa fa-lock"></i>Ubah Password </a></li>
			<!--<li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Example dropdown </a>
			  <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
				<li><a href="#">Page</a></li>
				<li><a href="#">Page</a></li>
				<li><a href="#">Page</a></li>
			  </ul>
			</li>
			<li><a href="login.html"> <i class="icon-interface-windows"></i>Login page </a></li>-->
			
			<li <?php if($Page=='Security'){echo 'class="active"';} ?>><a href="#Akun" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-lock"></i>Keamanan </a>
			  <ul id="Akun" class="collapse list-unstyled ">
				<li><a href="UserLogin.php">User Login</a></li>
				<li><a href="AksesLevel.php">Akses Level</a></li>
				<li><a href="SetIdentitas.php">Set Nomenklatur</a></li>
				<li><a href="SetPenandatanganan.php">Set Penandatanganan</a></li>
				<li><a href="ResetPassDesa.php">Reset Password</a></li>
			  </ul>
			</li>
          </ul>
		  
		  <!--<span class="heading">Extras</span>
          <ul class="list-unstyled">
            <li> <a href="#"> <i class="icon-flask"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-screen"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-mail"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-picture"></i>Demo </a></li>
          </ul>-->
        </nav>