<?php  
session_start();  
ob_start();  
include '../library/config.php';
include '../library/tgl-indo.php';
?>  	
<html xmlns="http://www.w3.org/1999/xhtml"> <!-- Bagian halaman HTML yang akan konvert -->  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />  
<title>SEPAKAT</title>   
</head>  
<body> 
	<table border="1" style="width: 100%" align="center">
		<tr>
			<td>
			<h2>REKAPITULASI DATA KERJASAMA TAHUN <?php echo base64_decode($_GET['kd']); ?></h2>
			</td>
		</tr>
	</table>
	<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="center">
		<thead>
            <tr>
                <th style="width: 20px; text-align: center; border: solid 1px #000;"><strong>No</strong></th>
                <th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>No.Dok. Kerjasama</strong></th>
                <th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Nama Program</strong></th>
                <th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Mitra Kerjasama</strong></th>
				<th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Bidang Kerjasama</strong></th>
				<th style="width: 20%; text-align: center; border: solid 1px #000;"><strong>Ruang Lingkup</strong></th>
				<th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Periode Kerjasama</strong></th>
				<th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Tindak Lanjut</strong></th>
				<th style="width: 10%; text-align: center; border: solid 1px #000;"><strong>Kendala</strong></th>
				<th style="width: 5%; text-align: center; border: solid 1px #000;"><strong>Ket</strong></th>
            </tr>
        </thead>
        <tbody>
            <?php
			$sql =  "SELECT * FROM kerjasama WHERE date_format(TanggalMulai, '%Y')='".$_POST['PrintTahun']."' ORDER BY KodeKerjasama DESC";
			$result = mysqli_query($koneksi,$sql);
			while($data=mysqli_fetch_array($result)){
			?>
			<tr>
                <td style="width: 20px; text-align: left; border: solid 1px #000">
                    <?php echo ++$no_urut;?>
                </td>
                <td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php 
						echo "No: ".$data ['NoPerjanjian1']; echo '<br/>';
						echo "No: ".$data ['NoPerjanjian2'];
						if($data ['NoPerjanjian3']!=null){
							echo '<br/>';
							echo "No: ".$data ['NoPerjanjian3'];
						}
						
						if($data ['NoPerjanjian4']!=null){
							echo '<br/>';
							echo "No: ".$data ['NoPerjanjian4'];
						}
					?>
                </td> 
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['NamaProgram']; ?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['MitraKerjasama']; ?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['BidangKerjasama']; ?>
                </td>
				<td style="width: 20%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['Kegiatan']; ?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data['PeriodeKerjasama']; echo '<br/>'; echo TanggalIndo($data ['TanggalMulai']); echo ' s/d '; echo TanggalIndo($data ['TanggalSelesai']);?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['TindakLanjut']; ?>
                </td>
				<td style="width: 10%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['Kendala']; ?>
                </td>
				<td style="width: 5%; text-align: left; border: solid 1px #000">
                    <?php echo $data ['Keterangan']; ?>
                </td>
            </tr>
			<?php } ?>
        </tbody>
    </table>
</body>  
</html><!-- Akhir halaman HTML yang akan di konvert -->  
<?php  
$filename="Rekapitulasi Kerjasama Tahun ".$_POST['PrintTahun'].".pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya  
//==========================================================================================================  
//Copy dan paste langsung script dibawah ini,untuk mengetahui lebih jelas tentang fungsinya silahkan baca-baca tutorial tentang HTML2PDF  
//==========================================================================================================  
$content = ob_get_clean();  
$content = '<page style="font-family: Cambria,"Times New Roman",serif;">'.($content).'</page>';  
 require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');  
 try  
 {  
  $html2pdf = new HTML2PDF('L','F4','en', false, 'ISO-8859-15',array(1,0));  
  $html2pdf->setDefaultFont('arial','2');  
  $html2pdf->writeHTML($content, isset($_GET['vuehtml']));  
  $html2pdf->Output($filename);  
 }  
 catch(HTML2PDF_exception $e) { echo $e; }  
?>  