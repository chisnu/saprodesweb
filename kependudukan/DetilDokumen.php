<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $KodeDok	=@$_GET['KodeDok'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $IDPend	=@$_GET['IDPend'];
 $NamaDokumen=@$_GET['NamaDokumen'];
 $NoUrutSyarat=@$_GET['NoUrutSyarat'];
 $NoTrMohon =@$_GET['NoTrMohon'];
 $UrlFile =@$_GET['UrlFile'];
?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Lihat Dokumen <?php echo $NamaDokumen; ?></h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
							<?php if(file_exists("../images/DokumenSyarat/".$UrlFile)){
								echo '<img src="../images/DokumenSyarat/'.$UrlFile.'" class="img img-responsive img-thumbnail" alt="Image User">';
							}else{
								echo '<img src="../images/DokumenSyarat/no-image.png" class="img img-responsive img-thumbnail" alt="Image User">';
							}
							?>	
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>