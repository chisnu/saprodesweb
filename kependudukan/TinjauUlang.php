<?php
include 'akses.php';
include '../library/tgl-indo.php';
$Tahun=date('Y');
$HariIni=date('Y-m-d H:i:s');

//cek apakah user sudah pernah membuat tr permohonan msyarakat
$QueryCekData = @mysqli_query($koneksi, "SELECT a.NoTrMohon,a.JenisSurat,b.StandarWaktuPelayanan,b.Keterangan FROM trpermohonanmasy a JOIN masterpengurusansurat b ON (a.JenisSurat)=(b.JenisSurat) AND (a.KodeLokasi=b.KodeLokasi) WHERE a.KodeLokasi='$kode_lokasi_aktif' AND a.IDPend='$id_penduduk_aktif' AND a.NoTrMohon='".base64_decode(@$_GET['id'])."' AND a.StatusPermohonan='REJECTED'"); 
$numCek = @mysqli_num_rows($QueryCekData); 
if($numCek > 0){
	while($CekData = @mysqli_fetch_array($QueryCekData)){
		$No_Transaksi = $CekData['NoTrMohon'];
		$Jenis_Surat = $CekData['JenisSurat'];
		$Standar_Waktu = $CekData['StandarWaktuPelayanan'];
		$Nama_Surat = $CekData['Keterangan'];
	}
}

//Hapus Data
if(base64_decode(@$_GET['aksi'])==='Hapus'){
	$HapusData = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND KodeSyarat='".base64_decode(@$_GET['kd'])."' AND NoTrMohon='$No_Transaksi'"); 
	echo '<script language="javascript">document.location="TinjauUlang.php?id='.base64_encode($No_Transaksi).'"; </script>';
}

if(base64_decode(@$_GET['aksi'])==='PindahDokumen'){
		$QueryCekData = @mysqli_query($koneksi, "SELECT MAX(NoUrutSyarat) as NoSekarang FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi'"); 
		$CekData = @mysqli_fetch_array($QueryCekData);
		$NoMax = $CekData['NoSekarang'];

		$NoSekarang = $NoMax+1; 
		
		$NmSyarat = base64_decode(@$_GET['NmSy']);
		$KodeDokPenduduk = base64_decode(@$_GET['KodeDok']);
		$KdSyarat = base64_decode(@$_GET['KdSyarat']);
		
		$PindahDokumen = @mysqli_query($koneksi, "INSERT INTO dokumensyaratmohon (NoTrMohon,NoUrutSyarat,KodeLokasi,NamaSyarat,KodeDokPenduduk,IDPend,IsVerified,KodeSyarat) VALUES (
		'$No_Transaksi','$NoSekarang','$kode_lokasi_aktif','$NmSyarat','$KodeDokPenduduk','$id_penduduk_aktif','0','$KdSyarat')");
		
		echo '<script language="javascript">document.location="TinjauUlang.php?id='.base64_encode($No_Transaksi).'"; </script>';
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="komponen/css/style.green.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "TinjauUlang.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Tinjau Ulang</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
				  <div class="card">
					<div class="card-header d-flex align-items-center">
					  <h3 class="h4">1. Peninjauan Ulang Surat <?php echo $Nama_Surat; ?></h3>
					</div>
					<div class="card-body">
						<div class="row">
						  <div class="col-lg-8">
								  <div class="table-responsive">  
									<table class="table table-striped">
									  <thead>
										<tr>
										  <th>No</th>
										  <th>Nama Syarat</th>
										  <th>Keterangan</th>
										  <th>Status</th>
										  <th>Dokumen</th>
										</tr>
									  </thead>
									  <tbody>
										<?php
										//AND b.NoTrMohon='$No_Transaksi'
										$no =1;
										$sql_syarat = @mysqli_query($koneksi, "SELECT * FROM mstsyaratsurat WHERE JenisSurat='$Jenis_Surat' AND KodeLokasi='$kode_lokasi_aktif'"); 
										while($data_syarat = @mysqli_fetch_array($sql_syarat)){
										?>
										<tr>
										  <th scope="row"><?php echo $no++; ?></th>
										  <td><?php echo $data_syarat['NamaSyarat'];?></td>
										  <td><?php echo $data_syarat['Keterangan'];?></td>
										  <?php 
												$CekDok = @mysqli_query($koneksi, "SELECT NoTrMohon,IsVerified,NoUrutSyarat,KodeDokPenduduk FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi' AND KodeSyarat='".$data_syarat['KodeSyarat']."'"); 
												$numCek = @mysqli_num_rows($CekDok); 
												$RowCekData = @mysqli_fetch_array($CekDok);
											?>
										  <td width="120px" align="center">
											<?php
												if($RowCekData['IsVerified']==0){
													echo "<font color='#e74c3c'><i class='fa fa-close'></i> Ditolak</font>";
												}else{
													echo '<font color="#2ecc71"><i class="fa fa-check"></i> Diterima</font>';
												}
											?>
										  </td>
										  <td width="100px">
											<?php if($numCek!=0){ ?>
												<a href="#" class='open_modal_image' data-kodedok='<?php echo $RowCekData['KodeDokPenduduk'];?>' data-lokasi='<?php echo $kode_lokasi_aktif;?>' data-idpend='<?php echo $id_penduduk_aktif;?>' data-dok='<?php echo $data_syarat['NamaSyarat'];?>' data-notrmohon='<?php echo $No_Transaksi;?>' data-nourut='<?php echo $RowCekData['NoUrutSyarat'];?>'>
												<span class="btn btn-success btn-sm fa fa-photo" title="Lihat Dokumen"></span>
												</a>
												
												<?php if($RowCekData['IsVerified']==0){ ?>
													<a href="TinjauUlang.php?id=<?php echo base64_encode($No_Transaksi);?>&kd=<?php echo base64_encode($data_syarat['KodeSyarat']);?>&aksi=<?php echo base64_encode('Hapus');?>"><span class="btn btn-danger btn-sm fa fa-close"></span></a>
												<?php } ?>
												
											<?php }else{ ?>
												<a href="#" class='open_modal' data-notrmohon='<?php echo $No_Transaksi;?>' data-kodelokasi='<?php echo $kode_lokasi_aktif;?>' data-idpenduduk='<?php echo $id_penduduk_aktif;?>' data-namasyarat='<?php echo $data_syarat['NamaSyarat'];?>' data-kodesyarat='<?php echo $data_syarat['KodeSyarat'];?>' data-aksi='TinjauUlang'>
													<span class="btn btn-primary btn-sm fa fa-upload" title="Unggah Dokumen"></span>
												</a>
												
												
												<a href="#" class='open_modal_dokumen' data-notrmohon='<?php echo $No_Transaksi;?>' data-kodelokasi='<?php echo $kode_lokasi_aktif;?>' data-idpenduduk='<?php echo $id_penduduk_aktif;?>' data-namasyarat='<?php echo $data_syarat['NamaSyarat'];?>' data-kodesyarat='<?php echo $data_syarat['KodeSyarat'];?>' data-aksi='TinjauUlang'>
												<span class="btn btn-info btn-sm fa fa-list" title="Lihat Dokumen"></span>
												</a>
											<?php } ?>
											
										  </td>
										</tr>
										<?php } ?>
									  </tbody>
									</table>
								  </div>
								  <hr/>
								  <h5>2. Kirim Berkas Permohonan</h5>
								  <form method="post" action="">
									<input  type="hidden" name="NoTrMohon" value="<?php echo $No_Transaksi; ?>">
									<div class="form-group-material">
									  <input type="text" name="Ket" class="form-control" placeholder="Keterangan">
									</div>
									
									<button type="submit" class="btn btn-primary" name="KirimPermohonan">Kirim Permohonan Surat</button>
									<button type="submit" class="btn btn-danger" name="BatalkanPermohonan">Batalkan Permohonan Surat</button>
								  </form>
						  </div>
						</div>
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
	<!-- Modal Popup untuk Edit--> 
	<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
	
	<div id="ModalEditDokumen" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
	<div id="ModalEditImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
    <!-- JavaScript files-->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>
    <script src="komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="komponen/js/front.js"></script>
	<!-- Javascript untuk popup modal Edit--> 
	<script type="text/javascript">
	   $(document).ready(function () {
	   $(".open_modal").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var nm_syarat = $(this).data("namasyarat");
		  var kd_syarat = $(this).data("kodesyarat");
		  var aksi = $(this).data("aksi");
			   $.ajax({
					   url: "UnggahDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,NamaSyarat: nm_syarat,KodeSyarat: kd_syarat,Aksi:aksi},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		//open modal dokumensyarat
		$(document).ready(function () {
	    $(".open_modal_dokumen").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var nm_syarat = $(this).data("namasyarat");
		  var kd_syarat = $(this).data("kodesyarat");
		  var aksi = $(this).data("aksi");
			   $.ajax({
					   url: "LihatDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,NamaSyarat: nm_syarat,KodeSyarat: kd_syarat,Aksi:aksi},
					   success: function (ajaxData){
					   $("#ModalEditDokumen").html(ajaxData);
					   $("#ModalEditDokumen").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});

		$(document).ready(function () {
		$(".open_modal_image").click(function(e) {
		  var kd_dok = $(this).data("kodedok");
		  var kode_lok = $(this).data("lokasi");
		  var id_pend  = $(this).data("idpend");
		  var dok  = $(this).data("dok");
		  var no_urut  = $(this).data("nourut");
		  var no_tr = $(this).data("notrmohon");
			   $.ajax({
					   url: "DetilDokumen.php",
					   type: "GET",
					   data : {KodeDok: kd_dok,KodeLokasi: kode_lok,IDPend: id_pend,NamaDokumen: dok,NoUrutSyarat:no_urut,NoTrMohon:no_tr},
					   success: function (ajaxData){
					   $("#ModalEditImage").html(ajaxData);
					   $("#ModalEditImage").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
	
	
	
	<?php	
	if(isset($_POST['KirimPermohonan'])){	
		$JamSekarang = date('H:i:s');
		$TanggalSekarang = date('Y-m-d');
		$JamLayananKantor = date('Y-m-d').' 12:00:00'; //jam tutup lyn hari ini
		
		$NamaHariIni = date('D', strtotime($HariIni)); //nama hari
		
		if(strtotime($HariIni) > strtotime($JamLayananKantor)){ //jika input diatas jam 12 siang maka diproses besok
			//Cek Ada berapa banyak hari sabtu
			$TglLibur = array();
			for($i=0; $i < $Standar_Waktu; $i++){
				$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
				$NmHari = date('D', strtotime($TglHari));
				$TglLibur[]=$NmHari;
			}
			//cari ada berapa banayak hari saturday
			$Count = array_count_values($TglLibur); 
			$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
			
			$TotalLamaPelayanan = $Standar_Waktu+$JmlTglLibur;
			$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
			
		}else{ //surat diproses hari ini
			//Cek Ada berapa banyak hari sabtu
			$StandarWaktuBaru  = $Standar_Waktu-1;
			$TglLibur = array();
			for($i=0; $i < $StandarWaktuBaru; $i++){
				$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
				$NmHari = date('D', strtotime($TglHari));
				$TglLibur[]=$NmHari;
			}
			//cari ada berapa banayak hari saturday
			$Count = array_count_values($TglLibur); 
			$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
			
			$TotalLamaPelayanan = $StandarWaktuBaru+$JmlTglLibur;
			$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
		}
		 
		//insert to progress surat & update trpermohonanmsy
		$UpdateTrPermohonan = @mysqli_query($koneksi, "UPDATE trpermohonanmasy SET TglPermohonan='$HariIni',PrediksiTglSelesai='$PrediksiTglSelesai', Keterangan1='".$_POST['Ket']."', StatusPermohonan='WAITING' WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
		
		if($UpdateTrPermohonan){
			$InsertData = @mysqli_query($koneksi, "UPDATE progresssurat SET StatusProgress='Front Office (DESA)',Waktu='$JamSekarang',Tanggal='$TanggalSekarang',SendTo='Front Office (DESA)',WaktuSend='$JamSekarang',TglSend='$HariIni',IsConfirmed='0' WHERE NoUrutProgress='1' AND NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			
			if($InsertData){
				echo '<script type="text/javascript">
				  sweetAlert({
					title: "Permohonan Terkirim!",
					text: " Permohonan surat akan diproses pada jam kerja aktif!",
					type: "success"
				  },
				  function () {
					window.location.href = "LayananSurat.php";
				  });
				  </script>';
			}else{
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Simpan Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "LayananSurat.php";
					  });
					  </script>';
			} 
		}
	}
	
	if(isset($_POST['BatalkanPermohonan'])){
		//hapus dokumensyarat mohon
		$HapusDokumenSyarat = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
		if($HapusDokumenSyarat){
			$HapusProgress = @mysqli_query($koneksi, "DELETE FROM progresssurat WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi'"); 
				
				if($HapusProgress){
					$HapusTrPermohonan = @mysqli_query($koneksi, "DELETE FROM trpermohonanmasy WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			
					if($HapusTrPermohonan){
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Berhasil",
							text: " ",
							type: "success"
						  },
						  function () {
							window.location.href = "LayananSurat.php";
						  });
						  </script>';
					}else{
						echo '<script type="text/javascript">
							  sweetAlert({
								title: "Hapus Data Gagal!",
								text: " ",
								type: "error"
							  },
							  function () {
								window.location.href = "LayananSurat.php";
							  });
							  </script>';
					}
				}else{
					echo '<script type="text/javascript">
							  sweetAlert({
								title: "Hapus Data Gagal!",
								text: " ",
								type: "error"
							  },
							  function () {
								window.location.href = "LayananSurat.php";
							  });
							  </script>';
				}
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "LayananSurat.php";
					  });
					  </script>';
		}
	}
?>
  </body>
</html>