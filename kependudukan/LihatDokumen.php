<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $NoTrMohon	=@$_GET['NoTrMohon'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $IDPend	=@$_GET['IDPend'];
 $NmSyarat	=@$_GET['NamaSyarat'];
 $KdSyarat	=@$_GET['KodeSyarat'];
 $Aksi	=@$_GET['Aksi']; //tinjau ulang digunakan jika dokumen di rejected 
?>

<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Lihat Dokumen Penduduk</h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
								<label>Dokumen untuk <strong><?php echo $NmSyarat;?></strong></label>
								<!--<form action="upload/processupload.php" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
									<input name="NoTrMohon" type="hidden" value="<?php echo $NoTrMohon;?>" />
									<input name="KodeLokasi" type="hidden" value="<?php echo $KodeLokasi;?>" />
									<input name="IDPend" type="hidden" value="<?php echo $IDPend;?>" />
									<input name="NamaSyarat" type="hidden" value="<?php echo $NmSyarat;?>" />
									<input name="KodeSyarat" type="hidden" value="<?php echo $KdSyarat;?>" />
									<br/>
									<div class="form-group-material">
									  <input type="text" name="Ket" class="form-control" placeholder="Keterangan">
									</div>
								</form>-->
								<div class="table-responsive">  
									<table class="table table-striped">
									  <thead>
										<tr>
										  <th>No</th>
										  <th>Nama Syarat</th>
										  <th>Keterangan</th>
										  <th>Dokumen</th>
										</tr>
									  </thead>
									  <tbody>
										<?php
										//AND b.NoTrMohon='$No_Transaksi'
										$no =1;
										$sql_syarat = @mysqli_query($koneksi, "SELECT NamaDokumen,Keterangan,KodeDokPenduduk,UrlFile FROM dokumenfisik WHERE IsVerified='1' AND IDPend='$IDPend' AND KodeLokasi='$KodeLokasi'"); 
										while($data_syarat = @mysqli_fetch_array($sql_syarat)){
										?>
										<tr>
										  <th scope="row"><?php echo $no++; ?></th>
										  <td><?php echo $data_syarat['NamaDokumen'];?></td>
										  <td><?php echo $data_syarat['Keterangan'];?></td>
										  <td align="center">
											<a href="#" class='open_modal_image' data-kodedok='<?php echo $data_syarat['KodeDokPenduduk'];?>' data-lokasi='<?php echo $KodeLokasi;?>' data-idpend='<?php echo $IDPend;?>' data-dok='<?php echo $data_syarat['NamaDokumen'];?>' data-url='<?php echo $data_syarat['UrlFile'];?>'>
												<span class="btn btn-primary btn-sm fa fa-photo" title="Lihat Dokumen"></span>
											</a>
											<?php if($Aksi=='TinjauUlang'){ ?>
												<a href="TinjauUlang.php?aksi=<?php echo base64_encode('PindahDokumen');?>&KodeDok=<?php echo base64_encode($data_syarat['KodeDokPenduduk']);?>&NmSy=<?php echo base64_encode($data_syarat['NamaDokumen']);?>&KdSyarat=<?php echo base64_encode($KdSyarat);?>&id=<?php echo base64_encode($NoTrMohon);?>">
											<?php }else{ ?>
												<a href="LayananSurat.php?aksi=<?php echo base64_encode('PindahDokumen');?>&KodeDok=<?php echo base64_encode($data_syarat['KodeDokPenduduk']);?>&NmSy=<?php echo base64_encode($data_syarat['NamaDokumen']);?>&KdSyarat=<?php echo base64_encode($KdSyarat);?>&FileSy=<?php echo base64_encode($data_syarat['UrlFile']);?>">
											<?php } ?>
												<span class="btn btn-warning btn-sm fa fa-exchange" title="Gunakan Dokumen"></span>
											</a>
										  </td>
										</tr>
										<?php } ?>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<div id="ModalEditImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
<script type="text/javascript">
	   $(document).ready(function () {
	   $(".open_modal_image").click(function(e) {
		  var kd_dok = $(this).data("kodedok");
		  var kode_lok = $(this).data("lokasi");
		  var id_pend  = $(this).data("idpend");
		  var dok  = $(this).data("dok");
		  var url  = $(this).data("url");
			   $.ajax({
					   url: "DetilDokumen.php",
					   type: "GET",
					   data : {KodeDok: kd_dok,KodeLokasi: kode_lok,IDPend: id_pend,NamaDokumen: dok,UrlFile: url},
					   success: function (ajaxData){
					   $("#ModalEditImage").html(ajaxData);
					   $("#ModalEditImage").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>