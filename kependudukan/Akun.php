<?php
include 'akses.php';
include '../library/tgl-indo.php';
$Page = 'Akun';
$Tahun=date('Y');
$HariIni=date('Y-m-d H:i:s');

//lihat data
function ResultListData($No,$JenisList,$koneksi){
	$query = @mysqli_query($koneksi, "SELECT Uraian FROM mstlistdata WHERE NoUrut='$No' AND JenisList='$JenisList'"); 
	$rowdata = @mysqli_fetch_array($query);
	return ($rowdata['Uraian']);
	//return ($No);
}
//cek apakah user sudah pernah membuat tr permohonan msyarakat
$QueryCekData = @mysqli_query($koneksi, "SELECT a.*,b.NamaDusun,c.NamaDesa,d.NamaKecamatan,e.NamaKab,e.NamaProv,f.NamaPendidikan,g.NamaPekerjaan FROM datapenduduk a LEFT JOIN mstdusun b ON (a.KodeDusun)=(b.KodeDusun) AND (a.KodeLokasi=b.KodeLokasi) JOIN mstlokasi h ON a.KodeLokasi=h.KodeLokasi JOIN mstdesa c ON h.KodeDesa=c.KodeDesa AND h.KodeKec=c.KodeKec AND h.KodeKab=c.KodeKab JOIN mstkecamatan d ON h.KodeKec=d.KodeKec AND h.KodeKab=d.KodeKab JOIN mstkab e ON h.KodeKab=e.KodeKab LEFT JOIN mstpendidikan f ON a.KodePendidikan=f.KodePendidikan LEFT JOIN mstpekerjaan g ON a.KodePekerjaan=g.KodePekerjaan WHERE a.KodeLokasi='$kode_lokasi_aktif' AND a.IDPend='$id_penduduk_aktif'"); 
$data = @mysqli_fetch_array($QueryCekData);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="komponen/css/style.green.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
		
		label{
			color:#796AEE;;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Profil</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables">
            <div class="container-fluid">
				<div class="row">
					<div class="col-lg-6">
					  <div class="card">
						<!--<div class="card-header d-flex align-items-center">
						  <h3 class="h4">Profil</h3>
						</div>-->
						<div class="card-body">	
							<label>Nama</label>
							<p><?php if($data['Nama'] == null OR $data['Nama'] === ""){ echo '-'; } else { echo $data['Nama']; } ?></p>
							
							<label>NIK</label>
							<p><?php if($data['NIK'] == null OR $data['NIK'] === ""){ echo '-'; } else { echo $data['NIK']; } ?></p>
							
							<label>No. KK</label>
							<p><?php if($data['NoKK'] == null OR $data['NoKK'] === ""){ echo '-'; } else { echo $data['NoKK']; } ?></p>
							
							<label>Tempat & Tanggal Lahir</label>
							<p><?php echo $data['TempatLahir']; echo ',&nbsp;';echo strtoupper(TanggalIndo($data['TanggalLahir']));?></p>
							
							<label>Alamat</label>
							<p><?php echo $data['Alamat']; echo " Dsn.".$data['NamaDusun']." Ds.".$data['NamaDesa']." Kec.".$data['NamaKecamatan']." Kab.".$data['NamaKab']."";?></p>
							
							<label>Agama</label>
							<p><?php if($data['Agama'] == null OR $data['Agama'] === ""){ echo '-'; } else { echo ResultListData($data['Agama'],'AGAMA',$koneksi); } ?></p>
							
							<label>Jenis Kelamin</label>
							<p><?php if($data['JenisKelamin']=='1'){echo 'Laki-laki';}else{echo 'Perempuan';}?></p>
						</div>
					  </div>
					</div>
					
					<div class="col-lg-6">
					  <div class="card">
						<!--<div class="card-header d-flex align-items-center">
						  <h3 class="h4">Data Register Permohonan</h3>
						</div>-->
						<div class="card-body">							  
							<label>Status Keluarga</label>
							<p><?php if($data['StatusKeluarga'] == null OR $data['StatusKeluarga'] === ""){ echo '-'; } else { echo ResultListData($data['StatusKeluarga'],'HUB_KELUARGA',$koneksi); } ?></p>
							
							<label>Status Perkawinan</label>
							<p><?php if($data['StatusPerkawinan'] == null OR $data['StatusPerkawinan'] === ""){ echo '-'; } else { echo ResultListData($data['StatusPerkawinan'],'STATUS_PERKAWINAN',$koneksi); } ?></p>
							
							<label>Anak Ke-</label>
							<p><?php if($data['AnakKe'] == null OR $data['AnakKe'] === ""){ echo '-'; } else { echo $data['AnakKe']; } ?></p>
							
							<label>Golongan Darah</label>
							<p><?php if($data['GolDarah'] == null OR $data['GolDarah'] === ""){ echo '-'; } else { echo ResultListData($data['GolDarah'],'GOL_DARAH',$koneksi); } ?></p>
							
							<label>Nama Ayah & Ibu</label>
							<p><?php if($data['NamaAyah'] == null OR $data['NamaAyah'] === ""){ echo '-'; } else { echo $data['NamaAyah']; }
							echo ' & '; if($data['NamaIbu'] == null OR $data['NamaIbu'] === ""){ echo '-'; } else { echo $data['NamaIbu']; } ?></p>
							
							<label>Kewarganegaraan</label>
							<p><?php if($data['WNI'] == null OR $data['WNI'] === ""){ echo '-'; } else { echo ResultListData($data['WNI'],'KEWARGANEGARAAN',$koneksi); } ?></p>
							
							<label>Pendidikan</label>
							<p><?php if($data['NamaPendidikan'] == null OR $data['NamaPendidikan'] === ""){ echo '-'; } else { echo $data['NamaPendidikan']; } ?></p>
							
							<label>Pekerjaan</label>
							<p><?php if($data['NamaPekerjaan'] == null OR $data['NamaPekerjaan'] === ""){ echo '-'; } else { echo $data['NamaPekerjaan']; } ?></p>
						</div>
					  </div>
					</div>
					
				</div>
            </div>
          </section> 
        </div>
      </div>
    </div>
	<div id="ModalEditTracking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
    <!-- JavaScript files-->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>
    <script src="komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="komponen/js/front.js"></script>
	<!-- Javascript untuk popup modal Edit--> 
	<script type="text/javascript">
		//open modal tracking
		$(document).ready(function () {
	    $(".open_modal_tracking").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var nm_surat = $(this).data("namasurat");
			   $.ajax({
					   url: "TrackingDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,NamaiSurat:nm_surat},
					   success: function (ajaxData){
					   $("#ModalEditTracking").html(ajaxData);
					   $("#ModalEditTracking").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
  </body>
</html>