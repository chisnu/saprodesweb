<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $NoTrMohon	=@$_GET['NoTrMohon'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $NmSurat	=@$_GET['NamaiSurat'];
?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Lihat Tracking Surat Permohonan <?php echo $NmSurat; ?></h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<?php
								//AND b.NoTrMohon='$No_Transaksi'
								$no =1;
								$sql_syarat = @mysqli_query($koneksi, "SELECT NoUrutProgress,StatusProgress,SendTo,CONVERT(varchar, WaktuSend, 108) AS WaktuKirim, CONVERT(varchar, TglSend, 105) AS TglKirim,IsConfirmed,CONVERT(varchar, WaktuConfirm, 108) AS WaktuKonfirmasi,CONVERT(varchar, TglConfirm, 105) AS TglKonfirmasi,UserSender,UserConfirm,Keterangan FROM progresssurat WHERE NoTrMohon='$NoTrMohon' AND KodeLokasi='$KodeLokasi' ORDER BY convert(int,NoUrutProgress) ASC"); 
								while($data = @mysqli_fetch_array($sql_syarat)){
							
							if($data['SendTo']=='Pemohon/Masyarakat'){
								echo '
								<div class="text-left">
									<div class="alert alert-warning">
										<i class="fa fa-clock-o"></i> '.$data['WaktuKirim'].' <i class="fa fa-calendar"></i> '.$data['TglKirim'].' 
										<p>Berkas Dialihkan dari '.$data['StatusProgress'].' ke '.$data['SendTo'].'</p>
										<p><font color="red">Nb. Surat permohonan anda telah selesai, silahkan Anda ambil di Kantor Kelurahan.</font></p>
									</div>
								</div>
								';
							}else{
							?>
							<div class="text-left">
								<div class="alert alert-info">
									<i class="fa fa-clock-o"></i> <?php echo $data['WaktuKirim'];?> <i class="fa fa-calendar"></i> <?php echo $data['TglKirim'];?> <!--<?php if($data['NoUrutProgress']!=1){ echo '<i class="fa fa-user"></i> '.$data['UserSender'].''; }else{echo '<i class="fa fa-user"></i> Anda';}?>-->
									<p>Berkas <?php if($data['NoUrutProgress']==1){ echo 'Dikirim Oleh';}else{echo 'Dialihkan dari';}?> <?php echo $data['StatusProgress']; if($data['NoUrutProgress']!=1){echo ' ke '; echo $data['SendTo'];}?></p>
								</div>
							</div>
							<?php //if($data['IsConfirmed']==1){ ?>
							<div class="text-right">
								<div class="alert alert-info">
									<i class="fa fa-clock-o"></i> <?php echo $data['WaktuKonfirmasi'];?> <i class="fa fa-calendar"></i> <?php echo $data['TglKonfirmasi'];?> <!--<i class="fa fa-user"></i> <?php echo $data['UserConfirm'];?>-->
									<p>Berkas Diterima Oleh Bagian <?php echo $data['SendTo']; ?></p>
								</div>
							</div>
							<?php //} ?>
								
							<?php }
							} ?>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>