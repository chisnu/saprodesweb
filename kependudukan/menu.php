<nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar">
				<?php if($kode_foto != null){
					echo '<img src="imageview-profil.php?KodeLokasi='.$kode_lokasi_aktif.'&IDPend='.$id_penduduk_aktif.'" class="img-fluid rounded-circle">';
				}else{
					echo '<img src="user-icon.jpg" alt="..." class="img-fluid rounded-circle">';
				}
				?>
			</div>
            <div class="title">
              <p>Selamat Datang</p>
              <h1 class="h4"><?php echo $nama_aktif;?></h1>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled">
			<li <?php if($Page=='index'){echo 'class="active"';} ?>><a href="index.php"> <i class="icon-home"></i>Dashboard </a></li>
			
		<?php if(@$LetakMenu=='1'){ ?>	
			<li <?php if($Page=='LayananSurat'){echo 'class="active"';} ?>><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-envelope-o"></i>Layanan Surat </a>
				  <ul id="exampledropdownDropdown" class="collapse list-unstyled nav nav-pills">
					<li><a href="#home-pills" data-toggle="tab">Data Permohonan</a></li>
					<li><a href="#tambah-user" data-toggle="tab">Buat Permohonan Surat</a></li>
				  </ul>
			</li>
		<?php }else{ ?>	
			<li <?php if($Page=='LayananSurat'){echo 'class="active"';} ?>><a href="LayananSurat.php"> <i class="fa fa-envelope-o"></i>Layanan Surat </a></li>
		<?php } ?>	
			
			<li <?php if($Page=='Register'){echo 'class="active"';} ?>><a href="Register.php"> <i class="fa fa-file-text-o"></i>Register </a></li>
			<li <?php if($Page=='Akun'){echo 'class="active"';} ?>><a href="Akun.php"> <i class="fa fa-user"></i>Profil </a></li>
			<li <?php if($Page=='UbahPassword'){echo 'class="active"';} ?>><a href="UbahPassword.php"> <i class="fa fa-lock"></i>Ubah Password </a></li>
			<!--<li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Example dropdown </a>
			  <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
				<li><a href="#">Page</a></li>
				<li><a href="#">Page</a></li>
				<li><a href="#">Page</a></li>
			  </ul>
			</li>
			<li><a href="login.html"> <i class="icon-interface-windows"></i>Login page </a></li>-->
          </ul>
		  <!--<span class="heading">Extras</span>
          <ul class="list-unstyled">
            <li> <a href="#"> <i class="icon-flask"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-screen"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-mail"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-picture"></i>Demo </a></li>
          </ul>-->
        </nav>