<?php
include ("../../library/config.php");
$NoTrMohon 		= @$_POST['NoTrMohon'];
//$IDPend 		= @$_POST['IDPend'];
$KodeLokasi 	= @$_POST['KodeLokasi'];
$NamaSyarat 	= @$_POST['NamaSyarat'];
$KodeSyarat 	= @$_POST['KodeSyarat'];
$Ket			= @$_POST['Ket'];
$Aksi			= @$_POST['Aksi']; //TinjauUlang

//buat no urut syarat
$QueryCekData = @mysqli_query($koneksi, "SELECT MAX(NoUrutSyarat) as NoSekarang FROM dokumensyaratmohon WHERE KodeLokasi='$KodeLokasi' AND NoTrMohon='$NoTrMohon'"); 
$CekData = @mysqli_fetch_array($QueryCekData);
$NoMax = $CekData['NoSekarang'];

$NoSekarang = $NoMax+1;

if(isset($_POST)){
	############ Edit settings ##############
	$ThumbSquareSize 		= 200; //Thumbnail will be 200x200
	$BigImageMaxSize 		= 500; //Image Maximum height or width
	$ThumbPrefix			= "thumb_"; //Normal thumb Prefix
	$DestinationDirectory	= '../../images/DokumenSyarat/'; //specify upload directory ends with / (slash)
	$Quality 				= 100; //jpeg quality
	##########################################
	
	//check if this is an ajax request
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	// check $_FILES['ImageFile'] not empty
	if(!isset($_FILES['ImageFile']) || !is_uploaded_file($_FILES['ImageFile']['tmp_name']))
	{
		die('Upload bermasalah,cek ulang extensi gambar yang di upload!'); // output error when above checks fail.
	}
	
	// Random number will be added after image name
	$RandomNumber 	= rand(0, 9999999999); 

	$ImageName 		= str_replace(' ','-',strtolower($_FILES['ImageFile']['name'])); //get image name
	$ImageSize 		= $_FILES['ImageFile']['size']; // get original image size
	$TempSrc	 	= $_FILES['ImageFile']['tmp_name']; // Temp name of image file stored in PHP tmp folder
	$ImageType	 	= $_FILES['ImageFile']['type']; //get file type, returns "image/png", image/jpeg, text/plain etc.
	
	/* $fp = fopen($TempSrc, 'r'); // open file (read-only, binary)
    $file_content = fread($fp, $ImageSize) or die("Tidak dapat membaca source file"); // read file
    $file_content = mysqli_real_escape_string($file_content) or die("Tidak dapat membaca source file"); // parse image ke string
    fclose($fp); // tuptup file */
	
	//Let's check allowed $ImageType, we use PHP SWITCH statement here
	switch(strtolower($ImageType))
	{
		case 'image/png':
			//Create a new image from file 
			$CreatedImage = @imagecreatefrompng($_FILES['ImageFile']['tmp_name']);
			break;
		case 'image/gif':
			$CreatedImage = @imagecreatefromgif($_FILES['ImageFile']['tmp_name']);
			break;			
		case 'image/jpeg':
		case 'image/pjpeg':
			$CreatedImage = @imagecreatefromjpeg($_FILES['ImageFile']['tmp_name']);
			break;
		default:
			die('Unsupported File!'); //output error and exit
	}
	
	//PHP getimagesize() function returns height/width from image file stored in PHP tmp folder.
	//Get first two values from image, width and height. 
	//list assign svalues to $CurWidth,$CurHeight
	list($CurWidth,$CurHeight)=getimagesize($TempSrc);
	
	//Get file extension from Image name, this will be added after random name
	$ImageExt = substr($ImageName, strrpos($ImageName, '.'));
  	$ImageExt = str_replace('.','',$ImageExt);
	
	//remove extension from filename
	$ImageName 		= preg_replace("/\\.[^.\\s]{3,4}$/", "", $ImageName); 
	
	//Construct a new name with random number and extension.
	//$NewImageName = $ImageName.'-'.$RandomNumber.'.'.$ImageExt;
	$NewImageName = $NoTrMohon.'-'.$NoSekarang.'.'.$ImageExt;
	
	//set the Destination Image
	$thumb_DestRandImageName 	= $DestinationDirectory.$ThumbPrefix.$NewImageName; //Thumbnail name with destination directory
	$DestRandImageName 			= $DestinationDirectory.$NewImageName; // Image with destination directory
	
	//Resize image to Specified Size by calling resizeImage function.
	if(resizeImage($CurWidth,$CurHeight,$BigImageMaxSize,$DestRandImageName,$CreatedImage,$Quality,$ImageType))
	{
		//Create a square Thumbnail right after, this time we are using cropImage() function
		if(!cropImage($CurWidth,$CurHeight,$ThumbSquareSize,$thumb_DestRandImageName,$CreatedImage,$Quality,$ImageType))
			{
				echo 'Error Creating thumbnail';
			}
		/*
		We have succesfully resized and created thumbnail image
		We can now output image to user's browser or store information in the database
		*/
		// $datastring = file_get_contents("../../Temp/$NewImageName");
		// $data         = unpack("H*hex", $datastring);
		// mssql_query("insert into sometable values ('someimage.JPG', 0x".$data['hex'].")");
		// $stmt = sqlsrv_query( $koneksi,"INSERT INTO DokumenSyaratMohon (NoTrMohon,NoUrutSyarat,KodeLokasi,NamaSyarat,Keterangan,UrlFile,IsVerified,KodeSyarat,FileDoc) VALUES ('$NoTrMohon','$NoSekarang', '$KodeLokasi', '$NamaSyarat', '$Ket', '$NewImageName', '0', '$KodeSyarat', 0x".$data['hex'].")");
		
		
		
		//echo '<img src="../image/foto-user/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-responsive">';
		//echo '<img src="../img/foto-campaign/'.$NewImageName.'" alt="Resized Image" title="Gambar Asli" class="img img-responsive">';
		//$UrlGambar = $_SERVER['DOCUMENT_ROOT'].'/saprodes/Temp/'.$NewImageName;
		//$UrlGambar = file_get_contents($TempSrc);
		// Insert info into database table!
		
		/* $query = "INSERT INTO DokumenSyaratMohon (NoTrMohon,NoUrutSyarat,KodeLokasi,NamaSyarat,Keterangan,UrlFile,IsVerified,KodeSyarat,FileDoc) VALUES (?,?,?,?,?,?,?,?,?)";
		$params = [$NoTrMohon, $NoSekarang, $KodeLokasi, $NamaSyarat, $Ket, $NewImageName, '0', $KodeSyarat, $UrlGambar];
		$stmt = sqlsrv_query( $koneksi, $query, $params);
		
		if( $stmt === false ) {
			die( print_r( sqlsrv_errors(), true));
		} */
		/* $SimpanData = @sqlsrv_query($koneksi, "INSERT INTO DokumenSyaratMohon (NoTrMohon,NoUrutSyarat,KodeLokasi,NamaSyarat,IDPend,Keterangan,UrlFile,IsVerified)VALUES('$NoTrMohon','$NoSekarang','$KodeLokasi','$NamaSyarat','$IDPend','$Ket','$NewImageName','0')") or die( print_r( sqlsrv_errors(), true));  */
		/* if($stmt){
			//hapus gambar
			unlink("../../Temp/$NewImageName");
			unlink("../../Temp/thumb_$NewImageName");
			if($Aksi=='TinjauUlang'){
				echo '<script language="javascript">alert("Data berhasil disimpan !"); document.location="TinjauUlang.php?id='.base64_encode($NoTrMohon).'"; </script>';
			}else{
				echo '<script language="javascript">alert("Data berhasil disimpan !"); document.location="LayananSurat.php?aksi=tampil"; </script>';
			}			//header("location:insert-dokumen.php?no=".base64_encode($NoTrMohon)."&lok=".base64_encode($KodeLokasi)."&urut=".base64_encode($NoSekarang)."&img=".base64_encode($NewImageName)."");	
		} */
		
		echo '<img src="../images/DokumenSyarat/'.$NewImageName.'" alt="Resized Image" title="Gambar Asli" class="img img-responsive">';
		
		$stmt = mysqli_query($koneksi,"INSERT INTO dokumensyaratmohon (NoTrMohon,NoUrutSyarat,KodeLokasi,NamaSyarat,Keterangan,UrlFile,IsVerified,KodeSyarat) VALUES ('$NoTrMohon', '$NoSekarang', '$KodeLokasi', '$NamaSyarat', '$Ket', '$NewImageName', b'0', '$KodeSyarat')");
		if($stmt){
			if($Aksi=='TinjauUlang'){
				echo '<script language="javascript">alert("Data berhasil disimpan !"); document.location="TinjauUlang.php?id='.base64_encode($NoTrMohon).'"; </script>';
			}else{
				echo '<script language="javascript">alert("Data berhasil disimpan !"); document.location="LayananSurat.php?aksi=tampil"; </script>';
			}		
		} 
		
	}else{
		die('Resize Error'); //output error
	}
}


// This function will proportionally resize image 
function resizeImage($CurWidth,$CurHeight,$MaxSize,$DestFolder,$SrcImage,$Quality,$ImageType)
{
	//Check Image size is not 0
	if($CurWidth <= 0 || $CurHeight <= 0) 
	{
		return false;
	}
	
	//Construct a proportional size of new image
	$ImageScale      	= min($MaxSize/$CurWidth, $MaxSize/$CurHeight); 
	$NewWidth  			= ceil($ImageScale*$CurWidth);
	$NewHeight 			= ceil($ImageScale*$CurHeight);
	$NewCanves 			= imagecreatetruecolor($NewWidth, $NewHeight);
	
	// Resize Image
	if(imagecopyresampled($NewCanves, $SrcImage,0, 0, 0, 0, $NewWidth, $NewHeight, $CurWidth, $CurHeight))
	{
		switch(strtolower($ImageType))
		{
			case 'image/png':
				imagepng($NewCanves,$DestFolder);
				break;
			case 'image/gif':
				imagegif($NewCanves,$DestFolder);
				break;			
			case 'image/jpeg':
			case 'image/pjpeg':
				imagejpeg($NewCanves,$DestFolder,$Quality);
				break;
			default:
				return false;
		}
	//Destroy image, frees memory	
	if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
	return true;
	}

}

//This function corps image to create exact square images, no matter what its original size!
function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType)
{	 
	//Check Image size is not 0
	if($CurWidth <= 0 || $CurHeight <= 0) 
	{
		return false;
	}
	
	//abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
	if($CurWidth>$CurHeight)
	{
		$y_offset = 0;
		$x_offset = ($CurWidth - $CurHeight) / 2;
		$square_size 	= $CurWidth - ($x_offset * 2);
	}else{
		$x_offset = 0;
		$y_offset = ($CurHeight - $CurWidth) / 2;
		$square_size = $CurHeight - ($y_offset * 2);
	}
	
	$NewCanves 	= imagecreatetruecolor($iSize, $iSize);	
	if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size))
	{
		switch(strtolower($ImageType))
		{
			case 'image/png':
				imagepng($NewCanves,$DestFolder);
				break;
			case 'image/gif':
				imagegif($NewCanves,$DestFolder);
				break;			
			case 'image/jpeg':
			case 'image/pjpeg':
				imagejpeg($NewCanves,$DestFolder,$Quality);
				break;
			default:
				return false;
		}
	//Destroy image, frees memory	
	if(is_resource($NewCanves)) {imagedestroy($NewCanves);} 
	return true;

	}
	  
}