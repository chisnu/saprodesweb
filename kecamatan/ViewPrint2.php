<?php 
require "../library/fpdf/fpdf.php";  //pastikan path atau alamat FPDF sesuai

function hitung_umur($tanggal_lahir){
	list($year,$month,$day) = explode("-",$tanggal_lahir);
	$year_diff = date("Y") - $year;
	$month_diff = date("m") - $month;
	$day_diff = date("d") - $day;
	if($month_diff < 0) $year_diff--;
		elseif(($month_diff == 0) && ($day_diff < 0)) $year_diff--;
	return $year_diff;
}

// Pendefinisian folder font pada FPDF
define('FPDF_FONTPATH', '../library/fpdf/font/');
// Seperti sebelunya, kita membuat class anakan dari class FPDF
class FPDF_AutoWrapTable extends FPDF {
	private $data = array();
  	private $options = array( 'filename' => '', 'destinationfile' => '', 'paper_size'=>'F4', 'orientation'=>'P' );
	
	function __construct($data = array(), $options = array()) {
    	parent::__construct();
    	$this->data = $data;
    	$this->options = $options;
	}
	
	function Footer(){
	} 
 
	public function rptDetailData () {
		include "../library/config.php";
		include "../library/tgl-indo.php";
		
		//get variable
		$kode = base64_decode(@$_GET['kode']);
		$jenis = base64_decode(@$_GET['jns']);
		$lokasi = base64_decode(@$_GET['loc']);
		$border = 0;
		$this->AddPage();
		$this->SetMargins(15,10,15,10);
		$this->SetAutoPageBreak(true,30);
		$this->AliasNbPages();
		
		//header
		$this->Image('..\images\Assets\logo.png',20,5,22); // logo
		$this->SetFont('Times','B','15'); $this->SetTextColor(0,0,0); // warna tulisan
		// font yang digunakan // membuat cell dg panjang 19 dan align center 'C'
		$this->Ln(); $this->Cell(205,0,'P E M E R I N T A H   K A B U P A T E N   N G A N J U K',0,0,'C');
		$this->Ln(); $this->Cell(205,12,'K E C A M A T A N  N G A N J U K',0,0,'C');
		$cari = @mysqli_query($koneksi, "Select NamaDesa, AlamatLokasi from mstdesa inner join mstlokasi on mstlokasi.KodeDesa = mstdesa.KodeDesa WHERE mstlokasi.KodeLokasi='".$lokasi."'") or die(@mysqli_error($cari));
		while($tampil = @mysqli_fetch_array($cari)){
			$namadesa = $tampil['NamaDesa']; $alamat = $tampil['AlamatLokasi'];
			$this->SetFont('Times','B','15');
			$this->Ln(); $this->Cell(205,1,'KELURAHAN '.$namadesa,0,0,'C');
		}
		$this->Ln(); $this->SetFont('Times','','11'); // font yang digunakan
		$this->Cell(200,12,$alamat,0,0,'C');
		
		$this->Ln(11); $this->SetLineWidth(0.75); // tebal garis
		$this->Cell(180,0,'','TB',0,'C',1);
		$this->Ln(1); $this->SetLineWidth(0.1); // tebal garis 
		$this->Cell(180,0,'','TB',0,'L',1);
		
		if($jenis === 'SKAW'){
			$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN AHLI WARIS',0,0,'C');
			$this->SetFont('Times','','11');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			// baris pendahuluan
			$this->Ln(5); $this->MultiCell(0,7.5,@$Keperluan);
			
			// baris isi
			$this->Ln(0);
			$this->SetWidths(array(7.5,5,160,7.5)); $this->SetAligns(array('J','L','J','J'));
			$Support = "Suport"; $No = 4; $Total = 20;
			for($i=$No;$i<$Total;$i++){
				$ke = $i+1; $urut = $i-3;
				$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
				$arrayHasil = @mysqli_fetch_array($cek);
				if($arrayHasil[$Support.$ke] != null){
					$bagi = explode("#",$arrayHasil[$Support.$ke]);
					$this->SetFont('Times','','11');
					$this->Ln(0);
					$this->Row( array( "", $urut.".", strtoupper($bagi[0]).". ".ucwords($bagi[2]).", ".ucwords(strtolower($bagi[1])).", Tempat/Tanggal Lahir : ".ucwords(strtolower($bagi[3])).", ".TanggalIndo($bagi[4]).", Pekerjaan : ".ucwords(strtolower($bagi[5])).", Alamat : ".ucwords(strtolower($bagi[6])), "" ) );
				}
			}
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(0); $this->MultiCell(0,7.5,@$Uraian);
			
			// baris tanda tangan
			$this->Ln(5);
			$this->SetWidths(array(10,85,40,40,5)); $this->SetAligns(array('J','L','J','L','J'));
			$this->Row( array( "", ucwords(strtolower($namadesa)).", ".TanggalIndo($Tgl), "", "", "" ) ); 
			$this->Row( array( "", "Kami para ahli waris,", "", "", "" ) );   
				$Support = "Suport"; $No = 4; $Total = 20;
				for($i=$No;$i<$Total;$i++){
					$ke = $i+1; $urut = $i-3;
					$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
					$arrayHasil = @mysqli_fetch_array($cek);
					if($arrayHasil[$Support.$ke] != null){
						$bagi = explode("#",$arrayHasil[$Support.$ke]);
						$this->SetFont('Times','','11');
						$this->Ln(0);
						if($urut % 2 == 0){
							$this->Row( array( "", $urut.". ".ucwords(strtolower($bagi[0])), "", $urut.". _______________" ) );
						} else {
							$this->Row( array( "", $urut.". ".ucwords(strtolower($bagi[0])), $urut.". _______________", "" ) );
						}
					}
				}
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2, Suport3, Suport4 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$this->Ln(5);
				$tgl2 = explode("#", $arraySah['Suport2']); $tgl3 = explode("#", $arraySah['Suport3']);
				$tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				$tglS3 = TanggalIndo(date("Y-m-d", strtotime($tgl3[0])));
				if($arraySah['Suport2'] == null AND $arraySah['Suport3'] == null){
					$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ... ", "" ) );   
				} elseif($arraySah['Suport2'] != null AND $arraySah['Suport3'] == null){
					$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ".$tglS2, "" ) );
				} elseif($arraySah['Suport2'] == null AND $arraySah['Suport3'] != null){
					if($arraySah['Suport3'] == null){
						$this->Row( array( "", "Nganjuk, ", "", "Nganjuk, ... ", "" ) );
					} else {
						$this->Row( array( "", "Nganjuk, ".$tglS3, "", "Nganjuk, ... ", "" ) );
					}
				} else {
					if($arraySah['Suport3'] == null){
						$this->Row( array( "", "Nganjuk, ", "", "Nganjuk, ".$tglS2, "" ) );
					} else {
						$this->Row( array( "", "Nganjuk, ".$tglS3, "", "Nganjuk, ".$tglS2, "" ) );
					}
				}
				
				$this->Ln(-1);
				if($arraySah['Suport1'] === '1') {
					$this->Row( array( "", "Nomor : ", "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
				} elseif($arraySah['Suport1'] === '2') {
					if($arraySah['Suport3'] == null){
						$this->Row( array( "", "Nomor : ", "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					} else {
						$this->Row( array( "", "Nomor : ".$tgl3[1], "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					}
				} else {
					$this->Row( array( "", "Nomor : ", "", "Nomor : ", "" ) ); 
				}
				
				$this->Ln(2.5);
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				} elseif($arraySah['Suport1'] === '2'){ 
					$this->Row( array( "", "Dikuatkan oleh Kecamatan Nganjuk,", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) );
					$this->Ln(-2); $this->Row( array( "", "*) Telah ditandatangani secara elektronik.", "", "", "" ) );
				}
				
				$this->Ln(-2.5);
				if($arraySah['Suport4'] !== null){
					$gambar = $this->Image($arraySah['Suport4'], 25, $this->GetY()+2.5, 30);
				} else {
					$gambar = "";
				}
			}
			   
			$this->Ln(-1);
			$this->Row( array( "", @$gambar, "", "Kepala Desa ".$namadesa, "" ) );
			$this->Ln(15);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'CAMAT' AND a.KodeLokasi = '001-001'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $camat = $arrayAparat['NamaAparat']; $nip1 = $arrayAparat['NIP']; }
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND a.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			$this->Ln(0); $this->Row( array( "", "", "", $lurah, "" ) );
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip2, "" ) );
			
		} elseif($jenis === 'SKBH' OR $jenis === 'SKD' OR $jenis === 'SKKB' OR $jenis === 'SKPS') {
			if($jenis === 'SKBH'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN KEHILANGAN',0,0,'C');
			} elseif($jenis === 'SKD'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN DOMISILI',0,0,'C');
			} elseif($jenis === 'SKKB'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN KELAKUAN BAIK',0,0,'C');
			} elseif($jenis === 'SKPS'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN PENDUDUK SEMENTARA',0,0,'C');
			}
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Suport1 = $tampil['Suport1']; $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			$this->Ln(5); $this->SetFont('Times','B','11');
				if($Suport1 === '1') {
					$this->Cell(180,0,'Nomor : '.$NoManual,0,0,'C');
				}else {
					$this->Cell(180,0,'Nomor : ',0,0,'C');
				}
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.Nama, a.NIK, a.TempatLahir, a.TanggalLahir, a.JenisKelamin, a.Agama, a.KodePekerjaan, a.Alamat, a.WNI from datapenduduk a join trpermohonanmasy b on b.IDPend = a.IDPend where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; $tempatlhr = $arrayPenduduk['TempatLahir']; $tgl = $arrayPenduduk['TanggalLahir']; $jk = $arrayPenduduk['JenisKelamin']; $agama = $arrayPenduduk['Agama']; $kerja = $arrayPenduduk['KodePekerjaan']; $alamat = $arrayPenduduk['Alamat']; $wni = $arrayPenduduk['WNI']; $nik = $arrayPenduduk['NIK']; }
			if($jenis === 'SKBH' OR $jenis === 'SKD'){
				$this->Ln(5); $this->MultiCell(0,7.5,"Yang bertanda tangan di bawah ini :");
				$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
				$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, b.NamaJabatan from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; $namajbt = $arrayAparat['NamaJabatan']; }
				$this->SetFont('Times','','11');
				$this->Row2( array( "", "Nama", ":", strtoupper($lurah), "" ) );
				$this->Row2( array( "", "NIP", ":", $nip2, "" ) );
				$this->Row2( array( "", "Jabatan", ":", ucwords($namajbt)." ".$namadesa, "" ) );
				
				// baris isi
				$this->Ln(0); $this->MultiCell(0,7.5,$Keperluan);
				$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
				
				$this->SetFont('Times','','11');
				$this->Row2( array( "", "Nama", ":", strtoupper($nama), "" ) );
				$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl), "" ) );
				$cek_jk = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'JNS_KEL' AND NoUrut = '".$jk."'"); 
				$num_jk = @mysqli_num_rows($cek_jk); 
				if($num_jk <> 0){
					while($arrayJk = @mysqli_fetch_array($cek_jk)){ $this->Row2( array( "", "Jenis Kelamin", ":", ucwords($arrayJk['Uraian']), "" ) ); }
				} else {
					$this->Row2( array( "", "Jenis Kelamin", ":", "-", "" ) );
				}
				
				$cek_wni = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'KEWARGANEGARAAN' AND NoUrut = '".$wni."'"); 
				$num_wni = @mysqli_num_rows($cek_wni); 
				if($num_wni <> 0){
					while($arrayWni = @mysqli_fetch_array($cek_wni)){ $this->Row2( array( "", "Kewarganegaraan", ":", ucwords($arrayWni['Uraian']), "" ) ); }
				} else {
					$this->Row2( array( "", "Kewarganegaraan", ":", "-", "" ) );
				}
				
				$cek_agm = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'AGAMA' AND NoUrut = '".$agama."'"); 
				$num_agm = @mysqli_num_rows($cek_agm); 
				if($num_agm <> 0){
					while($arrayAgm = @mysqli_fetch_array($cek_agm)){ $this->Row2( array( "", "Agama", ":", ucwords($arrayAgm['Uraian']), "" ) ); }
				} else {
					$this->Row2( array( "", "Agama", ":", "-", "" ) );
				}
				
				$cek_krj = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'PEKERJAAN' AND NoUrut = '".$kerja."'"); 
				$num_krj = @mysqli_num_rows($cek_krj); 
				if($num_krj <> 0){
					while($arrayKrj = @mysqli_fetch_array($cek_krj)){ $this->Row2( array( "", "Pekerjaan", ":", ucwords($arrayKrj['Uraian']), "" ) ); }
				} else {
					$this->Row2( array( "", "Pekerjaan", ":", "-", "" ) );
				}
				
				if($alamat == null){
					$this->Row2( array( "", "Alamat", ":", "-", "" ) );
				} else {
					$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($alamat)), "" ) );
				}
			} elseif($jenis === 'SKKB' OR $jenis === 'SKPS'){
				$this->Ln(5); $this->MultiCell(0,7.5,"Yang bertanda tangan di bawah ini Kepala Kelurahan ".$namadesa.", Kecamatan Nganjuk, Kabupaten Nganjuk menerangkan dengan sebenarnya bahwa :");
				$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
				
				$this->SetFont('Times','','11');
				$this->Row2( array( "", "Nama", ":", strtoupper($nama), "" ) );
				if($jenis === 'SKPS'){
					$this->Row2( array( "", "NIK", ":", $nik, "" ) );
				}
				$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl), "" ) );
				$cek_jk = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'JNS_KEL' AND NoUrut = '".$jk."'"); 
				$num_jk = @mysqli_num_rows($cek_jk); 
				if($num_jk <> 0){
					while($arrayJk = @mysqli_fetch_array($cek_jk)){ $this->Row2( array( "", "Jenis Kelamin", ":", ucwords($arrayJk['Uraian']), "" ) ); }
				} else {
					$this->Row2( array( "", "Jenis Kelamin", ":", "-", "" ) );
				}
				
				$cek_wni = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'KEWARGANEGARAAN' AND NoUrut = '".$wni."'"); 
				$num_wni = @mysqli_num_rows($cek_wni); 
				if($num_wni <> 0){
					while($arrayWni = @mysqli_fetch_array($cek_wni)){ $this->Row2( array( "", "Kewarganegaraan", ":", ucwords($arrayWni['Uraian']), "" ) ); }
				} else {
					$this->Row2( array( "", "Kewarganegaraan", ":", "-", "" ) );
				}
				
				$cek_agm = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'AGAMA' AND NoUrut = '".$agama."'"); 
				$num_agm = @mysqli_num_rows($cek_agm); 
				if($num_agm <> 0){
					while($arrayAgm = @mysqli_fetch_array($cek_agm)){ $this->Row2( array( "", "Agama", ":", ucwords($arrayAgm['Uraian']), "" ) ); }
				} else {
					$this->Row2( array( "", "Agama", ":", "-", "" ) );
				}
				
				$cek_krj = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'PEKERJAAN' AND NoUrut = '".$kerja."'"); 
				$num_krj = @mysqli_num_rows($cek_krj); 
				if($num_krj <> 0){
					while($arrayKrj = @mysqli_fetch_array($cek_krj)){ $this->Row2( array( "", "Pekerjaan", ":", ucwords($arrayKrj['Uraian']), "" ) ); }
				} else {
					$this->Row2( array( "", "Pekerjaan", ":", "-", "" ) );
				}
				
				if($alamat == null){
					$this->Row2( array( "", "Alamat", ":", "-", "" ) );
				} else {
					$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($alamat)), "" ) );
				}
			}
			
			if($jenis === 'SKPS'){
				$this->Ln(0); $this->MultiCell(0,7.5,$Keperluan);
			}
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(0); $this->MultiCell(0,7.5,$Uraian);
			
			// baris tanda tangan
			$this->Ln(5);
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$tgl2 = explode("#", $arraySah['Suport2']); $tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				if($arraySah['Suport2'] == null){
					$this->Row( array( "", "", "", "Nganjuk, ... ", "" ) );   
				} else {
					$this->Row( array( "", "", "", "Nganjuk, ".$tglS2, "" ) );
				}
				
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				}
			}
			   
			$this->Ln(-2);
			$this->Row( array( "", "Pemohon", "", "Kepala Desa ".$namadesa ) );
			$this->Ln(15);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			$this->Ln(0); $this->Row( array( "", ucwords(strtolower($nama)), "", ucwords(strtolower($lurah)), "" ) );
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip2, "" ) );
			
		} elseif($jenis === 'SKBM') {
			$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN BELUM MENIKAH',0,0,'C');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Suport1 = $tampil['Suport1']; $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			$this->Ln(5); $this->SetFont('Times','B','11');
				if($Suport1 === '1') {
					$this->Cell(180,0,'Nomor : '.$NoManual,0,0,'C');
				}else {
					$this->Cell(180,0,'Nomor : ',0,0,'C');
				}
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$this->Ln(5); $this->MultiCell(0,7.5,$Keperluan);
			$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.Nama, a.TempatLahir, a.TanggalLahir, a.JenisKelamin, a.Agama, a.KodePekerjaan, a.Alamat, a.WNI from datapenduduk a join trpermohonanmasy b on b.IDPend = a.IDPend where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; $tempatlhr = $arrayPenduduk['TempatLahir']; $tgl = $arrayPenduduk['TanggalLahir']; $jk = $arrayPenduduk['JenisKelamin']; $agama = $arrayPenduduk['Agama']; $kerja = $arrayPenduduk['KodePekerjaan']; $alamat = $arrayPenduduk['Alamat']; $wni = $arrayPenduduk['WNI']; }

			$this->SetFont('Times','','11');
			$this->Row2( array( "", "Nama", ":", strtoupper($nama), "" ) );
			$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl), "" ) );
			$cek_jk = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'JNS_KEL' AND NoUrut = '".$jk."'"); 
			$num_jk = @mysqli_num_rows($cek_jk); 
			if($num_jk <> 0){
				while($arrayJk = @mysqli_fetch_array($cek_jk)){ $this->Row2( array( "", "Jenis Kelamin", ":", ucwords($arrayJk['Uraian']), "" ) ); }
			} else {
				$this->Row2( array( "", "Jenis Kelamin", ":", "-", "" ) );
			}
			
			$cek_wni = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'KEWARGANEGARAAN' AND NoUrut = '".$wni."'"); 
			$num_wni = @mysqli_num_rows($cek_wni); 
			if($num_wni <> 0){
				while($arrayWni = @mysqli_fetch_array($cek_wni)){ $this->Row2( array( "", "Kewarganegaraan", ":", ucwords($arrayWni['Uraian']), "" ) ); }
			} else {
				$this->Row2( array( "", "Kewarganegaraan", ":", "-", "" ) );
			}
			
			$cek_agm = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'AGAMA' AND NoUrut = '".$agama."'"); 
			$num_agm = @mysqli_num_rows($cek_agm); 
			if($num_agm <> 0){
				while($arrayAgm = @mysqli_fetch_array($cek_agm)){ $this->Row2( array( "", "Agama", ":", ucwords($arrayAgm['Uraian']), "" ) ); }
			} else {
				$this->Row2( array( "", "Agama", ":", "-", "" ) );
			}
			
			$cek_krj = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'PEKERJAAN' AND NoUrut = '".$kerja."'"); 
			$num_krj = @mysqli_num_rows($cek_krj); 
			if($num_krj <> 0){
				while($arrayKrj = @mysqli_fetch_array($cek_krj)){ $this->Row2( array( "", "Pekerjaan", ":", ucwords($arrayKrj['Uraian']), "" ) ); }
			} else {
				$this->Row2( array( "", "Pekerjaan", ":", "-", "" ) );
			}
			
			if($alamat == null){
				$this->Row2( array( "", "Alamat", ":", "-", "" ) );
			} else {
				$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($alamat)), "" ) );
			}
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(0); $this->MultiCell(0,7.5,$Uraian);
			
			// baris tanda tangan
			$this->Ln(5);
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$tgl2 = explode("#", $arraySah['Suport2']); $tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				if($arraySah['Suport2'] == null){
					$this->Row( array( "", "", "", "Nganjuk, ... ", "" ) );   
				} else {
					$this->Row( array( "", "", "", "Nganjuk, ".$tglS2, "" ) );
				}
				
				$this->Ln(0);
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				}
			}
			   
			$this->Ln(-2);
			$this->Row( array( "", "Pemohon", "", "Kepala Desa ".$namadesa ) );
			$this->Ln(20);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			$this->Ln(0); $this->Row( array( "", ucwords(strtolower($nama)), "", ucwords(strtolower($lurah)), "" ) );
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip2, "" ) );
			
		} elseif($jenis === 'SKBPKB') {
			$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN BUKTI KEPEMILIKAN KENDARAAN BERMOTOR',0,0,'C');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Suport1 = $tampil['Suport1']; $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			$this->Ln(5); $this->SetFont('Times','B','11');
				if($Suport1 === '1') {
					$this->Cell(180,0,'Nomor : '.$NoManual,0,0,'C');
				}else {
					$this->Cell(180,0,'Nomor : ',0,0,'C');
				}
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$this->Ln(5); $this->MultiCell(0,7.5,"Yang bertanda tangan di bawah ini :");
			$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.Nama, a.TempatLahir, a.TanggalLahir, a.JenisKelamin, a.Agama, a.KodePekerjaan, a.Alamat, a.WNI from datapenduduk a join trpermohonanmasy b on b.IDPend = a.IDPend where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; $tempatlhr = $arrayPenduduk['TempatLahir']; $tgl = $arrayPenduduk['TanggalLahir']; $jk = $arrayPenduduk['JenisKelamin']; $agama = $arrayPenduduk['Agama']; $kerja = $arrayPenduduk['KodePekerjaan']; $alamat = $arrayPenduduk['Alamat']; $wni = $arrayPenduduk['WNI']; }

			$this->SetFont('Times','','11');
			$this->Row2( array( "", "Nama", ":", strtoupper($nama), "" ) );
			$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl), "" ) );
			$cek_jk = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'JNS_KEL' AND NoUrut = '".$jk."'"); 
			$num_jk = @mysqli_num_rows($cek_jk); 
			if($num_jk <> 0){
				while($arrayJk = @mysqli_fetch_array($cek_jk)){ $this->Row2( array( "", "Jenis Kelamin", ":", ucwords($arrayJk['Uraian']), "" ) ); }
			} else {
				$this->Row2( array( "", "Jenis Kelamin", ":", "-", "" ) );
			}
			
			$cek_krj = @mysqli_query($koneksi, "SELECT Uraian from mstlistdata where JenisList = 'PEKERJAAN' AND NoUrut = '".$kerja."'"); 
			$num_krj = @mysqli_num_rows($cek_krj); 
			if($num_krj <> 0){
				while($arrayKrj = @mysqli_fetch_array($cek_krj)){ $this->Row2( array( "", "Pekerjaan", ":", ucwords($arrayKrj['Uraian']), "" ) ); }
			} else {
				$this->Row2( array( "", "Pekerjaan", ":", "-", "" ) );
			}
			
			if($alamat == null){
				$this->Row2( array( "", "Alamat", ":", "-", "" ) );
			} else {
				$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($alamat)), "" ) );
			}
			
			$this->Ln(0); $this->MultiCell(0,7.5,"Dengan ini menerangkan bahwa Mobil/Motor sebagai berikut :");
			$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_bpkb = @mysqli_query($koneksi, "SELECT Keperluan from trpengurusansurat where NoTrMohon = '".$kode."' AND KodeLokasi = '".$lokasi."' AND JenisSurat = '".$jenis."'"); while($arrayBPKB = @mysqli_fetch_array($cek_bpkb)){ $bpkb = explode("#", $arrayBPKB['Keperluan']); }

			$this->SetFont('Times','','11');
			$this->Row2( array( "", "Merk", ":", ucwords($bpkb[0]), "" ) );
			$this->Row2( array( "", "Jenis Kendaraan/Tahun", ":", ucwords($bpkb[1]), "" ) );
			$this->Row2( array( "", "Warna", ":", ucwords($bpkb[2]), "" ) );
			$this->Row2( array( "", "No. Rangka", ":", ucwords($bpkb[3]), "" ) );
			$this->Row2( array( "", "No. Mesin", ":", ucwords($bpkb[4]), "" ) );
			$this->Row2( array( "", "No. BPKB", ":", ucwords($bpkb[5]), "" ) );
			$this->Row2( array( "", "Nopol", ":", ucwords($bpkb[6]), "" ) );
			$this->Row2( array( "", "Atas Nama", ":", ucwords($bpkb[7]), "" ) );
			$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($bpkb[8])), "" ) );
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(1); $this->MultiCell(0,7.5,$Uraian);
			
			// baris tanda tangan
			$this->Ln(5);
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$tgl2 = explode("#", $arraySah['Suport2']); $tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				if($arraySah['Suport2'] == null){
					$this->Row( array( "", "", "", "Nganjuk, ... ", "" ) );   
				} else {
					$this->Row( array( "", "", "", "Nganjuk, ".$tglS2, "" ) );
				}
				
				$this->Ln(0);
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				}
			}
			   
			$this->Ln(-2);
			$this->Row( array( "", "Pemohon", "", "Kepala Desa ".$namadesa ) );
			$this->Ln(15);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			$this->Ln(0); $this->Row( array( "", ucwords(strtolower($nama)), "", ucwords(strtolower($lurah)), "" ) );
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip2, "" ) );
		
		} elseif($jenis === 'SKKT') {
			$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN KEPEMILIKAN TANAH',0,0,'C');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Suport1 = $tampil['Suport1']; $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			$this->Ln(5); $this->SetFont('Times','B','11');
				if($Suport1 === '1') {
					$this->Cell(180,0,'Nomor : '.$NoManual,0,0,'C');
				}else {
					$this->Cell(180,0,'Nomor : ',0,0,'C');
				}
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$this->Ln(5); $this->MultiCell(0,7.5,"Yang bertanda tangan di bawah ini Kepala Kelurahan ".$namadesa.", Kecamatan Nganjuk, Kabupaten Nganjuk menerangkan dengan sebenarnya bahwa :");
			$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.Nama, a.TempatLahir, a.TanggalLahir, a.JenisKelamin, a.Agama, a.KodePekerjaan, a.Alamat, a.WNI from datapenduduk a join trpermohonanmasy b on b.IDPend = a.IDPend where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; $tempatlhr = $arrayPenduduk['TempatLahir']; $tgl = $arrayPenduduk['TanggalLahir']; $jk = $arrayPenduduk['JenisKelamin']; $agama = $arrayPenduduk['Agama']; $kerja = $arrayPenduduk['KodePekerjaan']; $alamat = $arrayPenduduk['Alamat']; $wni = $arrayPenduduk['WNI']; }

			$this->SetFont('Times','','11');
			$this->Row2( array( "", "Nama", ":", strtoupper($nama), "" ) );
			$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl), "" ) );
			if($alamat == null){
				$this->Row2( array( "", "Alamat", ":", "-", "" ) );
			} else {
				$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($alamat)), "" ) );
			}
			
			$this->Ln(0); $this->MultiCell(0,7.5,$Keperluan);
			
			// baris penutup
			$this->Ln(2.5); $this->MultiCell(0,7.5,$Uraian);
			
			// baris tanda tangan
			$this->Ln(5);
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$tgl2 = explode("#", $arraySah['Suport2']); $tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				if($arraySah['Suport2'] == null){
					$this->Row( array( "", "", "", "Nganjuk, ... ", "" ) );   
				} else {
					$this->Row( array( "", "", "", "Nganjuk, ".$tglS2, "" ) );
				}
				
				$this->Ln(0);
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				}
			}
			   
			$this->Ln(-2);
			$this->Row( array( "", "", "", "Kepala Desa ".$namadesa ) );
			$this->Ln(15);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			$this->Ln(0); $this->Row( array( "", "", "", ucwords(strtolower($lurah)), "" ) );
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip2, "" ) );
		
		} elseif($jenis === 'SKLHR' OR $jenis === 'SKMTN' OR $jenis === 'SKT') {
			if($jenis === 'SKLHR'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN KELAHIRAN',0,0,'C');
			} elseif($jenis === 'SKMTN'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN KEMATIAN',0,0,'C');
			} elseif($jenis === 'SKT'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT TUGAS',0,0,'C');
			}
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Suport1 = $tampil['Suport1']; $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			$this->Ln(5); $this->SetFont('Times','B','11');
				if($Suport1 === '1') {
					$this->Cell(180,0,'Nomor : '.$NoManual,0,0,'C');
				}else {
					$this->Cell(180,0,'Nomor : ',0,0,'C');
				}
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.Nama from datapenduduk a join trpermohonanmasy b on b.IDPend = a.IDPend where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; }
			
				// baris isi
				$this->Ln(5); $this->MultiCell(0,7.5,$Keperluan);
				$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
				$suport = @mysqli_query($koneksi, "SELECT Suport5 from trpengurusansurat where NoTrMohon = '".$kode."' AND KodeLokasi = '".$lokasi."' AND JenisSurat = '".$jenis."'"); while($arraySuport = @mysqli_fetch_array($suport)){ $bagi = explode("#", $arraySuport['Suport5']); }
				
				if($jenis === 'SKLHR'){
					$this->SetFont('Times','','11');
					$this->Row2( array( "", "Nama", ":", strtoupper($bagi[0]), "" ) );
					$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($bagi[3])).", ".TanggalIndo($bagi[4]), "" ) );
					$this->Row2( array( "", "Jenis Kelamin", ":", ucwords($bagi[1]), "" ) );
					$this->Row2( array( "", "Kewarganegaraan", ":", ucwords($bagi[2]), "" ) );
					$this->Row2( array( "", "Agama", ":", ucwords($bagi[11]), "" ) );
					$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($bagi[6])), "" ) );
				} elseif($jenis === 'SKMTN'){
					$this->SetFont('Times','','11');
					$this->Row2( array( "", "Nama", ":", strtoupper($bagi[0]), "" ) );
					$this->Row2( array( "", "Jenis Kelamin", ":", ucwords(strtolower($bagi[1])), "" ) );
					$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($bagi[6])), "" ) );
				} elseif($jenis === 'SKT'){
					$this->SetFont('Times','','11');
					$this->Row2( array( "", "Nama", ":", strtoupper($bagi[0]), "" ) );
					$this->Row2( array( "", "Jabatan", ":", ucwords(strtolower($bagi[1])), "" ) );
					$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($bagi[2])), "" ) );
					$this->Row2( array( "", "Keperluan", ":", ucwords(strtolower($bagi[3])), "" ) );
					$this->Row2( array( "", "Waktu", ":", ucwords(strtolower($bagi[4])), "" ) );
					$this->Row2( array( "", "Tempat", ":", ucwords(strtolower($bagi[5])), "" ) );
				}
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(0); $this->MultiCell(0,7.5,$Uraian);
			
			// baris tanda tangan
			$this->Ln(5);
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$tgl2 = explode("#", $arraySah['Suport2']); $tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				if($arraySah['Suport2'] == null){
					$this->Row( array( "", "", "", "Nganjuk, ... ", "" ) );   
				} else {
					$this->Row( array( "", "", "", "Nganjuk, ".$tglS2, "" ) );
				}
				
				$this->Ln(0);
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				}
			}
			   
			$this->Ln(-2);
			if($jenis === 'SKLHR'){
				$this->Row( array( "", "Pemohon", "", "Kepala Desa ".$namadesa ) );
			} elseif($jenis === 'SKMTN' OR $jenis === 'SKT'){
				$this->Row( array( "", "", "", "Kepala Desa ".$namadesa ) );
			}
			$this->Ln(15);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND a.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			if($jenis === 'SKLHR'){
				$this->Ln(0); $this->Row( array( "", $nama, "", $lurah, "" ) );
			} elseif($jenis === 'SKMTN' OR $jenis === 'SKT'){
				$this->Ln(0); $this->Row( array( "", "", "", $lurah, "" ) );
			}
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip2, "" ) );
			
		} elseif($jenis === 'SKP') {
			$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN PINDAH',0,0,'C');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Suport1 = $tampil['Suport1']; $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			$this->Ln(5); $this->SetFont('Times','B','11');
				if($Suport1 === '1') {
					$this->Cell(180,0,'Nomor : '.$NoManual,0,0,'C');
				}else {
					$this->Cell(180,0,'Nomor : ',0,0,'C');
				}
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$this->Ln(7.5); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.NIK, a.Nama, a.TempatLahir, a.TanggalLahir, a.JenisKelamin, a.Agama, a.KodePekerjaan, a.KodePendidikan, a.Alamat, a.WNI, a.StatusPerkawinan from datapenduduk a join trpermohonanmasy b on b.IDPend = a.IDPend where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; $tempatlhr = $arrayPenduduk['TempatLahir']; $tgl = $arrayPenduduk['TanggalLahir']; $jk = $arrayPenduduk['JenisKelamin']; $agama = $arrayPenduduk['Agama']; $kawin = $arrayPenduduk['StatusPerkawinan']; $kerja = $arrayPenduduk['KodePekerjaan']; $didik = $arrayPenduduk['KodePendidikan']; $alamat = $arrayPenduduk['Alamat']; $wni = $arrayPenduduk['WNI']; $nik = $arrayPenduduk['NIK']; }

			$this->SetFont('Times','','11');
			$this->Row2( array( "1.", "Nama", ":", strtoupper($nama), "" ) );
			$menu1 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='JNS_KEL' AND NoUrut='".$jk."'");
			while($kode1 = mysqli_fetch_array($menu1)){
				$this->Row2( array( "2.", "Jenis Kelamin", ":", ucwords(strtolower($kode1['Uraian'])), "" ) );
			}
			$this->Row2( array( "3.", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl), "" ) );
			$menu2 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' AND NoUrut='".$wni."'");
			while($kode2 = mysqli_fetch_array($menu2)){
				$this->Row2( array( "4.", "Kewarganegaraan", ":", ucwords($kode2['Uraian']), "" ) );
			}
			$menu3 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='AGAMA' AND NoUrut='".$agama."'");
			while($kode3 = mysqli_fetch_array($menu3)){
				$this->Row2( array( "5.", "Agama", ":", ucwords(strtolower($kode3['Uraian'])), "" ) );
			}
			$menu4 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='STATUS_PERKAWINAN' AND NoUrut='".$kawin."'");
			while($kode4 = mysqli_fetch_array($menu4)){
				$this->Row2( array( "6.", "Status Perkawinan", ":", ucwords(strtolower($kode4['Uraian'])), "" ) );
			}
			$menu5 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='PEKERJAAN' AND NoUrut='".$kerja."'");
			while($kode5 = mysqli_fetch_array($menu5)){
				$this->Row2( array( "7.", "Pekerjaan", ":", ucwords(strtolower($kode5['Uraian'])), "" ) );
			}
			$menu6 = mysqli_query($koneksi,"SELECT NamaPendidikan FROM mstpendidikan WHERE KodePendidikan='".$didik."'");
			while($kode6 = mysqli_fetch_array($menu6)){
				$this->Row2( array( "8.", "Pendidikan", ":", ucwords(strtolower($kode6['NamaPendidikan'])), "" ) );
			}
			if($alamat == null){
				$this->Row2( array( "9.", "Alamat Asal", ":", "-", "" ) );
			} else {
				$this->Row2( array( "9.", "Alamat Asal", ":", ucwords(strtolower($alamat)), "" ) );
			}
			$this->Row2( array( "10.", "No Identitas", ":", $nik, "" ) );
			$this->Row2( array( "11.", "Pindah Ke", ":", ucwords($Keperluan), "" ) );
			$this->Ln(1); $this->Row2( array( "12.", "Alasan Pindah", ":", ucwords($Uraian), "" ) );
			
			$Support = "Suport"; $No = 4; $Total = 20;
			for($i=$No;$i<$Total;$i++){
				$ke = $i+1; $urut = $i-3;
				$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
				$arrayHasil = @mysqli_fetch_array($cek);
				if($arrayHasil[$Support.$ke] != null){
					$max = $urut;
				}
			}
			$this->Ln(1); $this->Row2( array( "13.", "Pengikut", ":", $max." Orang, Yaitu : ", "" ) );
			
			// tabel pengikut
			$this->SetFont('Times','','9');
			$this->SetFillColor(255,255,255);
			$h = 7.5; $left = 15; $top = 80;
			$this->Cell(10, $h, 'No', 1, 0, 'C',true);
			$this->SetX($left += 10); $this->Cell(30, $h, 'Nama', 1, 0, 'C',true);
			$this->SetX($left += 30); $this->Cell(22, $h, 'Jenis Kelamin', 1, 0, 'C',true);
			$this->SetX($left += 22); $this->Cell(10, $h, 'Umur', 1, 0, 'C',true);
			$this->SetX($left += 10); $this->Cell(20, $h, 'Status', 1, 0, 'C',true);
			$this->SetX($left += 20); $this->Cell(30, $h, 'Pendidikan', 1, 0, 'C',true);
			$this->SetX($left += 30); $this->Cell(30, $h, 'No. KTP', 1, 0, 'C',true);
			$this->SetX($left += 30); $this->Cell(28, $h, 'Keterangan', 1, 1, 'C',true);
			
			$this->SetWidths(array(10,30,22,10,20,30,30,28));
			$this->SetAligns(array('L','L','C','C','C','L','L','L'));
			$Support = "Suport"; $No = 4; $Total = 20; 
			for($i=$No;$i<$Total;$i++){
				$ke = $i+1; $urut = $i-3;
				$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
				$arrayHasil = @mysqli_fetch_array($cek);
				if($arrayHasil[$Support.$ke] != null){
					$bagi = explode("#",$arrayHasil[$Support.$ke]);
					$this->Table( array( $urut.".", strtoupper($bagi[0]), $bagi[1], hitung_umur($bagi[4]), $bagi[8], $bagi[9], $bagi[10], $bagi[7] ) );
				}
			}
			
			// baris tanda tangan
			$this->Ln(5); $this->SetFont('Times','','11');
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$tgl2 = explode("#", $arraySah['Suport2']); $tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				if($arraySah['Suport2'] == null){
					$this->Row( array( "", "", "", "Nganjuk, ... ", "" ) );   
				} else {
					$this->Row( array( "", "", "", "Nganjuk, ".$tglS2, "" ) );
				}
				
				$this->Ln(0);
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				}
			}
			   
			$this->Ln(-2);
			$this->Row( array( "", "", "", "Kepala Desa ".$namadesa ) );
			
			$this->Ln(15);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			$this->Ln(0); $this->Row( array( "", "", "", $lurah, "" ) );
			
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip2, "" ) );
			
		} elseif($jenis === 'SKPTK' OR $jenis === 'SKTM' OR $jenis === 'SKU'){
			if($jenis === 'SKPTK'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN PENGHASILAN DAN TANGGUNGAN KELUARGA',0,0,'C');
			} elseif($jenis === 'SKTM'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN TIDAK MAMPU',0,0,'C');
			} elseif($jenis === 'SKU'){
				$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN USAHA',0,0,'C');
			}
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Suport1 = $tampil['Suport1']; $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			$this->Ln(5); $this->SetFont('Times','B','11');
				if($Suport1 === '1') {
					$this->Cell(180,0,'Nomor : '.$NoManual,0,0,'C');
				}else {
					$this->Cell(180,0,'Nomor : ',0,0,'C');
				}
			
			$this->SetFont('Times','','11');
			$this->Ln(5); $this->MultiCell(0,7.5,"Yang bertanda tangan di bawah ini :");
			$this->Ln(1); $this->SetWidths(array(7.5,50,2,113,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, b.NamaJabatan from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; $namajbt = $arrayAparat['NamaJabatan']; }
			$this->Row2( array( "", "Nama", ":", strtoupper($lurah), "" ) );
			$this->Row2( array( "", "NIP", ":", $nip2, "" ) );
			$this->Row2( array( "", "Jabatan", ":", ucwords($namajbt)." ".$namadesa, "" ) );
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->MultiCell(0,7.5,"Dengan ini menerangkan bahwa :");
			$this->Ln(1); $this->SetWidths(array(7.5,50,2,113,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.NIK, a.Nama, a.TempatLahir, a.TanggalLahir, a.JenisKelamin, a.Agama, a.KodePekerjaan, a.KodePendidikan, a.Alamat, a.WNI, a.StatusPerkawinan from datapenduduk a join trpermohonanmasy b on b.IDPend = a.IDPend where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; $tempatlhr = $arrayPenduduk['TempatLahir']; $tgl = $arrayPenduduk['TanggalLahir']; $jk = $arrayPenduduk['JenisKelamin']; $agama = $arrayPenduduk['Agama']; $kawin = $arrayPenduduk['StatusPerkawinan']; $kerja = $arrayPenduduk['KodePekerjaan']; $didik = $arrayPenduduk['KodePendidikan']; $alamat = $arrayPenduduk['Alamat']; $wni = $arrayPenduduk['WNI']; $nik = $arrayPenduduk['NIK']; }
			
			$this->Row2( array( "", "Nama", ":", strtoupper($nama), "" ) );
			$menu1 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='JNS_KEL' AND NoUrut='".$jk."'");
			while($kode1 = mysqli_fetch_array($menu1)){
				$this->Row2( array( "", "Jenis Kelamin", ":", ucwords(strtolower($kode1['Uraian'])), "" ) );
			}
			$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl)." (".hitung_umur($tgl)." th)", "" ) );
			$menu2 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' AND NoUrut='".$wni."'");
			while($kode2 = mysqli_fetch_array($menu2)){
				$this->Row2( array( "", "Kewarganegaraan", ":", ucwords($kode2['Uraian']), "" ) );
			}
			$menu5 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='PEKERJAAN' AND NoUrut='".$kerja."'");
			while($kode5 = mysqli_fetch_array($menu5)){
				$this->Row2( array( "", "Pekerjaan", ":", ucwords(strtolower($kode5['Uraian'])), "" ) );
			}
			if($alamat == null){
				$this->Row2( array( "", "Alamat", ":", "-", "" ) );
			} else {
				$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($alamat)), "" ) );
			}
			
			$this->SetFont('Times','','11');
			$this->Ln(0); $this->MultiCell(0,7.5,$Keperluan);
			
			if($jenis === 'SKPTK'){
			$Support = "Suport"; $No = 4; $Total = 20;
			for($i=$No;$i<$Total;$i++){
				$ke = $i+1; $urut = $i-3;
				$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
				$arrayHasil = @mysqli_fetch_array($cek);
				if($arrayHasil[$Support.$ke] != null){
					$max = $urut;
				}
			}
			
			// tabel pengikut
			$this->Ln(1); 
			$this->SetFont('Times','','9');
			$this->SetFillColor(255,255,255);
			$h = 7.5; $left = 15; $top = 80;
			$this->Cell(10, $h, 'No', 1, 0, 'C',true);
			$this->SetX($left += 10); $this->Cell(50, $h, 'Nama', 1, 0, 'C',true);
			$this->SetX($left += 50); $this->Cell(15, $h, 'Umur', 1, 0, 'C',true);
			$this->SetX($left += 15); $this->Cell(35, $h, 'Pendidikan', 1, 0, 'C',true);
			$this->SetX($left += 35); $this->Cell(35, $h, 'Pekerjaan', 1, 0, 'C',true);
			$this->SetX($left += 35); $this->Cell(35, $h, 'Keterangan', 1, 1, 'C',true);
			
			$this->SetWidths(array(10,50,15,35,35,35));
			$this->SetAligns(array('L','L','C','L','L','L'));
			$Support = "Suport"; $No = 4; $Total = 20; 
			for($i=$No;$i<$Total;$i++){
				$ke = $i+1; $urut = $i-3;
				$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
				$arrayHasil = @mysqli_fetch_array($cek);
				if($arrayHasil[$Support.$ke] != null){
					$bagi = explode("#",$arrayHasil[$Support.$ke]);
					$this->Table( array( $urut.".", strtoupper($bagi[0]), hitung_umur($bagi[4]), $bagi[9], $bagi[5], $bagi[7] ) );
				}
			}
			
			}
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(1); $this->MultiCell(0,7.5,$Uraian);
			
			// baris tanda tangan
			$this->Ln(5); $this->SetFont('Times','','11');
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$tgl2 = explode("#", $arraySah['Suport2']); $tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				if($arraySah['Suport2'] == null){
					$this->Row( array( "", "", "", "Nganjuk, ... ", "" ) );   
				} else {
					$this->Row( array( "", "", "", "Nganjuk, ".$tglS2, "" ) );
				}
				
				$this->Ln(0);
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				}
			}
			   
			$this->Ln(-2);
			$this->Row( array( "", "Pemohon", "", "Kepala Desa ".$namadesa ) );
			
			$this->Ln(15);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND a.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			$this->Ln(0); $this->Row( array( "", ucwords(strtolower($nama)), "", ucwords(strtolower($lurah)), "" ) );
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip2, "" ) );
			
		} elseif($jenis === 'SKB'){
			$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KESEPAKATAN BERSAMA',0,0,'C');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Suport1 = $tampil['Suport1']; $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			$this->Ln(5); $this->SetFont('Times','B','11');
				if($Suport1 === '1') {
					$this->Cell(180,0,'Nomor : '.$NoManual,0,0,'C');
				}else {
					$this->Cell(180,0,'Nomor : ',0,0,'C');
				}
			
			$this->SetFont('Times','','11');
			$this->Ln(5); $this->MultiCell(0,7.5,$Keperluan);
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.NIK, a.Nama, a.TempatLahir, a.TanggalLahir, a.JenisKelamin, a.Agama, a.KodePekerjaan, a.KodePendidikan, a.Alamat, a.WNI, a.StatusPerkawinan from datapenduduk a join trpermohonanmasy b on b.IDPend = a.IDPend where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; $tempatlhr = $arrayPenduduk['TempatLahir']; $tgl = $arrayPenduduk['TanggalLahir']; $jk = $arrayPenduduk['JenisKelamin']; $agama = $arrayPenduduk['Agama']; $kawin = $arrayPenduduk['StatusPerkawinan']; $kerja = $arrayPenduduk['KodePekerjaan']; $didik = $arrayPenduduk['KodePendidikan']; $alamat = $arrayPenduduk['Alamat']; $wni = $arrayPenduduk['WNI']; $nik = $arrayPenduduk['NIK']; }
			
			$this->Row2( array( "", "Nama", ":", strtoupper($nama), "" ) );
			$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl)." (".hitung_umur($tgl)." th)", "" ) );
			if($alamat == null){
				$this->Row2( array( "", "Alamat", ":", "-", "" ) );
			} else {
				$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($alamat)), "" ) );
			}
			$this->Ln(0); $this->MultiCell(0,7.5,"Dalam hal ini disebut sebagai PIHAK KESATU.");
			
			$this->Ln(2.5); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek = @mysqli_query($koneksi, "SELECT Suport5 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			$arrayHasil = @mysqli_fetch_array($cek);
			if($arrayHasil['Suport5'] != null){
					$bagi = explode("#",$arrayHasil['Suport5']);
					$this->Row2( array( "", "Nama", ":", strtoupper($bagi[0]), "" ) );
					$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($bagi[3])).", ".TanggalIndo($bagi[4])." (".hitung_umur($bagi[4])." th)", "" ) );
					$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($bagi[6])), "" ) );
			}
			$this->Ln(0); $this->MultiCell(0,7.5,"Dalam hal ini disebut sebagai PIHAK KEDUA.");
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(1); $this->MultiCell(0,7.5,$Uraian);
			
			// baris tanda tangan
			$this->Ln(5); $this->SetFont('Times','','11');
			$this->SetWidths(array(10,70,20,70,10)); $this->SetAligns(array('J','C','J','C','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$this->Ln(1);
				$this->Row( array( "", "Pihak Kesatu", "", "Pihak Kedua", "" ) );
				
				$this->Ln(15);
				$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
				$this->SetFont('Times','BU','11');
				$this->Ln(0); $this->Row( array( "", ucwords(strtolower($nama)), "", ucwords(strtolower($bagi[0])), "" ) );
				
				$this->Ln(1); $this->SetFont('Times','','11');
				$tgl2 = explode("#", $arraySah['Suport2']); $tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				if($arraySah['Suport2'] == null){
					$this->MultiCell(0,7.5,"Nganjuk, ... ",0,"C");
				} else {
					$this->MultiCell(0,7.5,"Nganjuk, ".$tglS2,0,"C");
				}
				
				$this->Ln(-1);
				if($arraySah['Suport1'] === '1'){
					$this->MultiCell(0,7.5,"Disahkan dan Dibenarkan oleh Kami,",0,"C");
				}
				
				$this->Ln(-2.5); $this->MultiCell(0,7.5,"Kepala Desa ".$namadesa,0,"C");
				$this->Ln(15);
				$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND a.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
				$this->SetFont('Times','BU','11');
				$this->MultiCell(0,7.5,$lurah,0,"C");
				$this->SetFont('Times','','11');
				$this->Ln(-3); $this->MultiCell(0,7.5,"NIP : ".$nip2,0,"C");
				
			}
			
		} elseif($jenis === 'SKPW'){
			$this->Ln(7.5); $this->SetFont('Times','BU','13'); $this->Cell(180,0,'SURAT KETERANGAN PERWALIAN',0,0,'C');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $Suport1 = $tampil['Suport1']; $Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); $Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); $Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; }
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$this->Ln(5); $this->MultiCell(0,7.5,"Yang bertanda tangan di bawah ini :");
			$this->Ln(1); $this->SetWidths(array(7.5,40,2,123,7.5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.NIK, a.Nama, a.TempatLahir, a.TanggalLahir, a.JenisKelamin, a.Agama, a.KodePekerjaan, a.KodePendidikan, a.Alamat, a.WNI, a.StatusPerkawinan from datapenduduk a join trpermohonanmasy b on b.IDPend = a.IDPend where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; $tempatlhr = $arrayPenduduk['TempatLahir']; $tgl = $arrayPenduduk['TanggalLahir']; $jk = $arrayPenduduk['JenisKelamin']; $agama = $arrayPenduduk['Agama']; $kawin = $arrayPenduduk['StatusPerkawinan']; $kerja = $arrayPenduduk['KodePekerjaan']; $didik = $arrayPenduduk['KodePendidikan']; $alamat = $arrayPenduduk['Alamat']; $wni = $arrayPenduduk['WNI']; $nik = $arrayPenduduk['NIK']; }
			
			$this->Row2( array( "", "Nama", ":", strtoupper($nama), "" ) );
			$menu1 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='JNS_KEL' AND NoUrut='".$jk."'");
			while($kode1 = mysqli_fetch_array($menu1)){
				$this->Row2( array( "", "Jenis Kelamin", ":", ucwords(strtolower($kode1['Uraian'])), "" ) );
			}
			$this->Row2( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl)." (".hitung_umur($tgl)." th)", "" ) );
			$menu2 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' AND NoUrut='".$wni."'");
			while($kode2 = mysqli_fetch_array($menu2)){
				$this->Row2( array( "", "Kewarganegaraan", ":", ucwords($kode2['Uraian']), "" ) );
			}
			$menu5 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='PEKERJAAN' AND NoUrut='".$kerja."'");
			while($kode5 = mysqli_fetch_array($menu5)){
				$this->Row2( array( "", "Pekerjaan", ":", ucwords(strtolower($kode5['Uraian'])), "" ) );
			}
			if($alamat == null){
				$this->Row2( array( "", "Alamat", ":", "-", "" ) );
			} else {
				$this->Row2( array( "", "Alamat", ":", ucwords(strtolower($alamat)), "" ) );
			}
			
			// baris isi
			$this->Ln(0); $this->MultiCell(0,7.5,"Adalah Orang Tua Kandung dan Selaku Perwalian dari :");
			$this->SetWidths(array(7.5,5,160,7.5)); $this->SetAligns(array('J','L','J','J'));
			$Support = "Suport"; $No = 4; $Total = 20;
			for($i=$No;$i<$Total;$i++){
				$ke = $i+1; $urut = $i-3;
				$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
				$arrayHasil = @mysqli_fetch_array($cek);
				if($arrayHasil[$Support.$ke] != null){
					$bagi = explode("#",$arrayHasil[$Support.$ke]);
					$this->SetFont('Times','','11');
					$this->Ln(2);
					$this->Row( array( "", $urut.".", strtoupper($bagi[0]).". ".ucwords($bagi[2]).", ".ucwords(strtolower($bagi[1])).", Tempat/Tanggal Lahir : ".ucwords(strtolower($bagi[3])).", ".TanggalIndo($bagi[4]).", Pekerjaan : ".ucwords(strtolower($bagi[5])).", Alamat : ".ucwords(strtolower($bagi[6])), "" ) );
				}
			}
			
			$this->SetFont('Times','','11');
			$this->Ln(2.5); $this->MultiCell(0,7.5,$Keperluan);
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(1); $this->MultiCell(0,7.5,$Uraian);
			
			// baris tanda tangan
			$this->Ln(5);
			$this->SetWidths(array(10,75,15,75,5)); $this->SetAligns(array('J','L','J','L','J'));
			$this->Row( array( "", ucwords(strtolower($namadesa)).", ".TanggalIndo($Tgl), "", "", "" ) ); 
			$this->Ln(-2); $this->Row( array( "", "Pemohon,", "", "", "" ) );   
			$this->Row( array( "", "1. ".ucwords(strtolower($nama)), "", "1. _______________", "" ) );
			
			$this->Ln(2.5); 
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Suport1, Suport2, Suport3, Suport4 from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$tgl2 = explode("#", $arraySah['Suport2']); $tgl3 = explode("#", $arraySah['Suport3']);
				$tglS2 = TanggalIndo(date("Y-m-d", strtotime($tgl2[0])));
				$tglS3 = TanggalIndo(date("Y-m-d", strtotime($tgl3[0])));
				if($arraySah['Suport2'] == null AND $arraySah['Suport3'] == null){
					$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ... ", "" ) );   
				} elseif($arraySah['Suport2'] != null AND $arraySah['Suport3'] == null){
					$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ".$tglS2, "" ) );
				} elseif($arraySah['Suport2'] == null AND $arraySah['Suport3'] != null){
					if($arraySah['Suport3'] == null){
						$this->Row( array( "", "Nganjuk, ", "", "Nganjuk, ... ", "" ) );
					} else {
						$this->Row( array( "", "Nganjuk, ".$tglS3, "", "Nganjuk, ... ", "" ) );
					}
				} else {
					if($arraySah['Suport3'] == null){
						$this->Row( array( "", "Nganjuk, ", "", "Nganjuk, ".$tglS2, "" ) );
					} else {
						$this->Row( array( "", "Nganjuk, ".$tglS3, "", "Nganjuk, ".$tglS2, "" ) );
					}
				}
				
				$this->Ln(-2);
				if($arraySah['Suport1'] === '1') {
					$this->Row( array( "", "Nomor : ", "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
				} elseif($arraySah['Suport1'] === '2') {
					if($arraySah['Suport3'] == null){
						$this->Row( array( "", "Nomor : ", "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					} else {
						$this->Row( array( "", "Nomor : ".$tgl3[1], "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					}
				} else {
					$this->Row( array( "", "Nomor : ", "", "Nomor : ", "" ) ); 
				}
				
				$this->Ln(1);
				if($arraySah['Suport1'] === '1'){
					$this->Row( array( "", "", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) ); 
				} elseif($arraySah['Suport1'] === '2'){ 
					$this->Row( array( "", "Dikuatkan oleh Kecamatan Nganjuk,", "", "Disahkan dan Dibenarkan oleh Kami,", "" ) );
					$this->Ln(-2); $this->Row( array( "", "*) Telah ditandatangani secara elektronik.", "", "", "" ) );
				}
				
				if($arraySah['Suport4'] !== null){
					$gambar = $this->Image($arraySah['Suport4'], 25, $this->GetY()+2.5, 30);
				} else {
					$gambar = "";
				}
			}
			   
			$this->Ln(-2);
			$this->Row( array( "", @$gambar, "", "Kepala Desa ".$namadesa, "" ) );
			$this->Ln(20);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'CAMAT' AND b.KodeLokasi = '001-001'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $camat = $arrayAparat['NamaAparat']; $nip1 = $arrayAparat['NIP']; }
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'KEPALA DESA' AND b.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $lurah = $arrayAparat['NamaAparat']; $nip2 = $arrayAparat['NIP']; }
			$this->SetFont('Times','BU','11');
			$this->Ln(0); $this->Row( array( "", "", "", ucwords(strtolower($lurah)), "" ) );
			$this->SetFont('Times','','11');
			$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip2, "" ) );
			
		} 
	}
	
	public function printPDF () {
		$this->SetAutoPageBreak(false);
	    $this->AliasNbPages();
	    $this->SetFont("Times", "B", 10);
	    //$this->AddPage();
 
	    $this->rptDetailData();
		$this->Output($this->options['filename'],$this->options['destinationfile']);
		
  	}
 
  	private $widths;
	private $aligns;
	
	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}
 
	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}
 
	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=7.5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			// $this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,7.5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	function Row2($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=4*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			// $this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,4,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	function Table($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
 
	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}
 
	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
} //end of class
 
$tabel = new FPDF_AutoWrapTable(@$data, @$options);
$tabel->printPDF();

?>

<script type="text/javascript">
	window.history.forward();
	function noBack() { window.history.forward(); }
</script>