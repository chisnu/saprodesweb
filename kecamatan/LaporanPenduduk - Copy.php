<?php
include 'akses.php';
/* $fitur_id = 2;
include '../library/lock-menu.php'; */
include '../library/tgl-indo.php';
$Page = 'Kependudukan';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE NIK='".base64_decode($_GET['id'])."' AND LEFT(KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
	$RowData = mysqli_fetch_array($Edit);
}

if(isset($_REQUEST['keyword']) && @$_REQUEST['keyword']<>""){
	$key = @$_REQUEST['keyword'];
} else {
	$key = date('Y-m');
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
		
		
		/* Style the form */
		#regForm {
		  background-color: #ffffff;
		  margin: 20px auto;
		  /* padding: 40px; */
		  width: 100%;
		  min-width: 300px;
		}

		/* Style the input fields */
		input {
		  padding: 10px;
		  width: 100%;
		  font-size: 17px;
		  font-family: Raleway;
		  border: 1px solid #aaaaaa;
		}

		/* Mark input boxes that gets an error on validation: */
		input.invalid {
		  background-color: #ffdddd;
		}

		/* Hide all steps by default: */
		.tab {
		  display: none;
		}

		/* Make circles that indicate the steps of the form: */
		.step {
		  height: 15px;
		  width: 15px;
		  margin: 0 2px;
		  background-color: #bbbbbb;
		  border: none; 
		  border-radius: 50%;
		  display: inline-block;
		  opacity: 0.5;
		}

		/* Mark the active step: */
		.step.active {
		  opacity: 1;
		}

		/* Mark the steps that are finished and valid: */
		.step.finish {
		  background-color: #4CAF50;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Laporan Penduduk</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<!-- <ul class="nav nav-pills">
						<li <?php if(@$id==null){echo 'class="active"';} ?>>
							<a href="#home-pills" data-toggle="tab"><span class="btn btn-primary">Data Penduduk</span></a>&nbsp;
						</li>
						<li>
							<a href="#import-user" data-toggle="tab"><span class="btn btn-primary">Import Penduduk</span></a>
						</li>
						<!-- <li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary"><?php /* echo $Sebutan; */ ?></span></a>
						</li> --
					</ul><br/> -->
				  <div class="card">
					<div class="tab-content">
						<div class="tab-pane fade <?php if(@$_GET['id']==null){ echo 'in active show'; }?>" id="home-pills">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Laporan Mutasi Penduduk</h3>
							</div>
							<div class="card-body">	
								<form method="post" action="">
							<div class="row">
								<div class="col-lg-8">
								<div class="form-group input-group">
									<input type="text" name="keyword" id="time1" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Pilih Bulan" required>
									<span class="input-group-btn">
										<button class="btn btn-primary" type="submit">Cari</button>&nbsp;
									</span>
									<a href="ViewLaporan.php?type=<?php echo base64_encode('LaporanPenduduk'); ?>&key=<?php echo base64_encode($key); ?>" class="btn btn-success" target="_blank">Cetak</a>
								</div>
								</div>
								<div class="col-lg-6">
										<!-- <div class="form-group input-group">						
											<input type="text" name="keyword" class="form-control" placeholder="Nama..." value="<?php echo @$_REQUEST['keyword']; ?>"> --
											<span class="input-group-btn">
												<button class="btn btn-primary" type="submit">Cari</button>
											</span>
										</div> -->
								</div>
							</div>
								</form>
							  <div class="table-responsive">  
								<table class="table table-striped">
								  <thead>
									<tr>
									  <!-- <th rowspan="2">No</th> -->
									  <th rowspan="2">Desa/Kelurahan</th>
									  <th colspan="3">Jumlah Awal</th>
									  <th colspan="3">Lahir Bulan Ini</th>
									  <th colspan="3">Mati Bulan Ini</th>
									  <th colspan="3">Datang Bulan Ini</th>
									  <th colspan="3">Pindah Bulan Ini</th>
									  <th colspan="3">Total</th>
									</tr>
									<tr>
									  <!-- Jumlah Awal -->
									  <th>LK</th>
									  <th>PR</th>
									  <th>JML</th>
									  <!-- Jumlah Lahir -->
									  <th>LK</th>
									  <th>PR</th>
									  <th>JML</th>
									  <!-- Jumlah Mati -->
									  <th>LK</th>
									  <th>PR</th>
									  <th>JML</th>
									  <!-- Jumlah Datang -->
									  <th>LK</th>
									  <th>PR</th>
									  <th>JML</th>
									  <!-- Jumlah Pindah -->
									  <th>LK</th>
									  <th>PR</th>
									  <th>JML</th>
									  <!-- Total -->
									  <th>LK</th>
									  <th>PR</th>
									  <th>JML</th>
									</tr>
								  </thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword= @$_REQUEST['keyword'];
											$reload = "LaporanPenduduk.php?keyword=$keyword";
											$sql =  "SELECT a.NamaDesa, b.KodeLokasi FROM mstdesa a JOIN mstlokasi b ON b.KodeDesa = a.KodeDesa ORDER BY a.KodeDesa";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "LaporanPenduduk.php";
											$keyword= date('Y-m');
											$sql =  "SELECT a.NamaDesa, b.KodeLokasi FROM mstdesa a JOIN mstlokasi b ON b.KodeDesa = a.KodeDesa ORDER BY a.KodeDesa";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 20; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<?php 
											// Lahir bulan ini
											$JmlLahirLK = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) = '".$keyword."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $LahirLK = mysqli_num_rows($JmlLahirLK);
											$JmlLahirPR = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) = '".$keyword."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $LahirPR = mysqli_num_rows($JmlLahirPR); 
											
											// Mati bulan ini
											$JmlMatiLK = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) = '".$keyword."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $MatiLK = mysqli_num_rows($JmlMatiLK); 
											$JmlMatiPR = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) = '".$keyword."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $MatiPR = mysqli_num_rows($JmlMatiPR); 
												
											// Datang bulan ini
											$JmlDtgLK = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '1' AND KodeLokasi = '".$data['KodeLokasi']."'"); $DtgLK = mysqli_num_rows($JmlDtgLK);
											$JmlDtgPR = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '2' AND KodeLokasi = '".$data['KodeLokasi']."'"); $DtgPR = mysqli_num_rows($JmlDtgPR); 
											
											// Pindah bulan ini
											$JmlKluLK = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$keyword."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $KluLK = mysqli_num_rows($JmlKluLK); $arrayKluLK = mysqli_fetch_array($JmlKluLK); 
											$JmlKluLK2 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$arrayKluLK['KodeMutasiKeluar']."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $KluLK2 = mysqli_num_rows($JmlKluLK2);
											$totalKluLK = $KluLK+$KluLK2; 
												
											$JmlKluPR = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$keyword."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $KluPR = mysqli_num_rows($JmlKluPR); $arrayKluPR = mysqli_fetch_array($JmlKluPR); 
											$JmlKluPR2 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$arrayKluPR['KodeMutasiKeluar']."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $KluPR2 = mysqli_num_rows($JmlKluPR2);
											$totalKluPR = $KluPR+$KluPR2; 
											
											// total sebelumnya
											// Lahir 
											$_JmlLahirLK = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) < '".$keyword."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $_LahirLK = mysqli_num_rows($_JmlLahirLK);
											$_JmlLahirPR = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) < '".$keyword."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $_LahirPR = mysqli_num_rows($_JmlLahirPR); 
											
											// Mati 
											$_JmlMatiLK = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) < '".$keyword."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $_MatiLK = mysqli_num_rows($_JmlMatiLK); 
											$_JmlMatiPR = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) < '".$keyword."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $_MatiPR = mysqli_num_rows($_JmlMatiPR); 
												
											// Datang 
											$_JmlDtgLK = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '1' AND KodeLokasi = '".$data['KodeLokasi']."'"); $_DtgLK = mysqli_num_rows($_JmlDtgLK);
											$_JmlDtgPR = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '2' AND KodeLokasi = '".$data['KodeLokasi']."'"); $_DtgPR = mysqli_num_rows($_JmlDtgPR); 
											
											// Pindah 
											$_JmlKluLK = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$keyword."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $_KluLK = mysqli_num_rows($_JmlKluLK); $_arrayKluLK = mysqli_fetch_array($_JmlKluLK); 
											$_JmlKluLK2 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$_arrayKluLK['KodeMutasiKeluar']."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $_KluLK2 = mysqli_num_rows($_JmlKluLK2);
											$_totalKluLK = $_KluLK+$_KluLK2; 
												
											$_JmlKluPR = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$keyword."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $_KluPR = mysqli_num_rows($_JmlKluPR); $_arrayKluPR = mysqli_fetch_array($_JmlKluPR); 
											$_JmlKluPR2 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$_arrayKluPR['KodeMutasiKeluar']."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$data['KodeLokasi']."'"); $_KluPR2 = mysqli_num_rows($_JmlKluPR2);
											$_totalKluPR = $_KluPR+$_KluPR2;
											
											$JmlAwalLK = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE JenisKelamin = '1' AND KodeLokasi = '".$data['KodeLokasi']."'"); $totAwalLK = mysqli_num_rows($JmlAwalLK); 
											$AwalLK = $totAwalLK+$_LahirLK-$_MatiLK+$_DtgLK-$_totalKluLK;
											
											$JmlAwalPR = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE JenisKelamin = '2' AND KodeLokasi = '".$data['KodeLokasi']."'"); $totAwalPR = mysqli_num_rows($JmlAwalPR);
											$AwalPR = $totAwalPR+$_LahirPR-$_MatiPR+$_DtgPR-$_totalKluPR;
											
											?>
											<!-- <td width="50px">
												<?php echo ++$no_urut; ?> 
											</td> -->
											<td>
												<?php echo $data['NamaDesa'];?> 
											</td>
											
											<td align="right">
												<?php echo number_format($AwalLK); ?>
											</td>
											<td align="right">
												<?php echo number_format($AwalPR); ?>
											</td>
											<td align="right">
												<?php echo number_format($AwalLK+$AwalPR); ?>
											</td>
											<!-- Jumlah Lahir -->
											<td align="right">
												<?php echo number_format($LahirLK); ?>
											</td>
											<td align="right">
												<?php echo number_format($LahirPR); ?>
											</td>
											<td align="right">
												<?php echo number_format($LahirLK+$LahirPR); ?>
											</td>
											<!-- Jumlah Mati -->
											<td align="right">
												<?php echo number_format($MatiLK); ?>
											</td>
											<td align="right">
												<?php echo number_format($MatiPR); ?>
											</td>
											<td align="right">
												<?php echo number_format($MatiLK+$MatiPR); ?>
											</td>
											<!-- Jumlah Masuk -->
											<td align="right">
												<?php echo number_format($DtgLK); ?>
											</td>
											<td align="right">
												<?php echo number_format($DtgPR); ?>
											</td>
											<td align="right">
												<?php echo number_format($DtgLK+$DtgPR); ?>
											</td>
											<!-- Jumlah Pindah -->
											<td align="right">
												<?php echo number_format($totalKluLK); ?>
											</td>
											<td align="right">
												<?php echo number_format($totalKluPR);?>
											</td>
											<td align="right">
												<?php echo number_format($totalKluLK+$totalKluPR); ?>
											</td>
											<!-- Total -->
											<td align="right"><strong><?php echo number_format($AwalLK+$LahirLK-$MatiLK+$DtgLK-$totalKluLK); ?></strong></td>
											<td align="right"><strong><?php echo number_format($AwalPR+$LahirPR-$MatiPR+$DtgPR-$totalKluPR); ?></strong></td>
											<td align="right"><strong><?php echo number_format(($AwalLK+$LahirLK-$MatiLK+$DtgLK-$totalKluLK) + ($AwalPR+$LahirPR-$MatiPR+$DtgPR-$totalKluPR)); ?></strong></td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										
										// load data laporan total
										// Lahir bulan ini
										$JmlLahirLK2 = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) = '".$keyword."' AND b.JenisKelamin = '1'"); $LahirLK2 = mysqli_num_rows($JmlLahirLK2);
										$JmlLahirPR2 = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) = '".$keyword."' AND b.JenisKelamin = '2'"); $LahirPR2 = mysqli_num_rows($JmlLahirPR2); 
																	
										// Mati bulan ini
										$JmlMatiLK2 = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) = '".$keyword."' AND b.JenisKelamin = '1'"); $MatiLK2 = mysqli_num_rows($JmlMatiLK2); 
										$JmlMatiPR2 = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) = '".$keyword."' AND b.JenisKelamin = '2'"); $MatiPR2 = mysqli_num_rows($JmlMatiPR2); 
																		
										// Datang bulan ini
										$JmlDtgLK2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '1'"); $DtgLK2 = mysqli_num_rows($JmlDtgLK2);
										$JmlDtgPR2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '2'"); $DtgPR2 = mysqli_num_rows($JmlDtgPR2); 
																	
										// Pindah bulan ini
										$JmlKluLK2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$keyword."' AND b.JenisKelamin = '1'"); $KluLK2 = mysqli_num_rows($JmlKluLK2); $arrayKluLK2 = mysqli_fetch_array($JmlKluLK2); 
										$JmlKluLK22 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$arrayKluLK2['KodeMutasiKeluar']."' AND b.JenisKelamin = '1'"); $KluLK22 = mysqli_num_rows($JmlKluLK22);
										$totalKluLK2 = $KluLK2+$KluLK22; 
																		
										$JmlKluPR2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$keyword."' AND b.JenisKelamin = '2'"); $KluPR2 = mysqli_num_rows($JmlKluPR2); $arrayKluPR2 = mysqli_fetch_array($JmlKluPR2); 
										$JmlKluPR22 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$arrayKluPR2['KodeMutasiKeluar']."' AND b.JenisKelamin = '2'"); $KluPR22 = mysqli_num_rows($JmlKluPR22);
										$totalKluPR2 = $KluPR2+$KluPR22; 
																	
										// total sebelumnya
										// Lahir 
										$_JmlLahirLK2 = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) < '".$keyword."' AND b.JenisKelamin = '1'"); $_LahirLK2 = mysqli_num_rows($_JmlLahirLK2);
										$_JmlLahirPR2 = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) < '".$keyword."' AND b.JenisKelamin = '2'"); $_LahirPR2 = mysqli_num_rows($_JmlLahirPR2); 
																	
										// Mati 
										$_JmlMatiLK2 = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) < '".$keyword."' AND b.JenisKelamin = '1'"); $_MatiLK2 = mysqli_num_rows($_JmlMatiLK2); 
										$_JmlMatiPR2 = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) < '".$keyword."' AND b.JenisKelamin = '2'"); $_MatiPR2 = mysqli_num_rows($_JmlMatiPR2); 
																		
										// Datang 
										$_JmlDtgLK2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '1'"); $_DtgLK2 = mysqli_num_rows($_JmlDtgLK2);
										$_JmlDtgPR2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '2'"); $_DtgPR2 = mysqli_num_rows($_JmlDtgPR2); 
																	
										// Pindah 
										$_JmlKluLK2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$keyword."' AND b.JenisKelamin = '1'"); $_KluLK2 = mysqli_num_rows($_JmlKluLK2); $_arrayKluLK2 = mysqli_fetch_array($_JmlKluLK2); 
										$_JmlKluLK22 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$_arrayKluLK2['KodeMutasiKeluar']."' AND b.JenisKelamin = '1'"); $_KluLK22 = mysqli_num_rows($_JmlKluLK22);
										$_totalKluLK2 = $_KluLK2+$_KluLK22; 
																		
										$_JmlKluPR2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$keyword."' AND b.JenisKelamin = '2'"); $_KluPR2 = mysqli_num_rows($_JmlKluPR2); $_arrayKluPR2 = mysqli_fetch_array($_JmlKluPR2); 
										$_JmlKluPR22 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$_arrayKluPR2['KodeMutasiKeluar']."' AND b.JenisKelamin = '2'"); $_KluPR22 = mysqli_num_rows($_JmlKluPR22);
										$_totalKluPR2 = $_KluPR2+$_KluPR22;
																	
										$JmlAwalLK2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE JenisKelamin = '1'"); $totAwalLK2 = mysqli_num_rows($JmlAwalLK2); 
										$AwalLK2 = $totAwalLK2+$_LahirLK2-$_MatiLK2+$_DtgLK2-$_totalKluLK2;
																	
										$JmlAwalPR2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE JenisKelamin = '2'"); $totAwalPR2 = mysqli_num_rows($JmlAwalPR2);
										$AwalPR2 = $totAwalPR2+$_LahirPR2-$_MatiPR2+$_DtgPR2-$_totalKluPR2;
										
										// end load
										
										echo '<tr class="odd gradeX"><td><strong>JUMLAH</strong></td>';
										echo '<td align="right"><strong>'.number_format($AwalLK2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($AwalPR2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($AwalLK2+$AwalPR2).'</strong></td>';
										
										echo '<td align="right"><strong>'.number_format($LahirLK2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($LahirPR2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($LahirLK2+$LahirPR2).'</strong></td>';
										
										echo '<td align="right"><strong>'.number_format($MatiLK2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($MatiPR2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($MatiLK2+$MatiPR2).'</strong></td>';
										
										echo '<td align="right"><strong>'.number_format($DtgLK2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($DtgPR2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($DtgLK2+$DtgPR2).'</strong></td>';
										
										echo '<td align="right"><strong>'.number_format($totalKluLK2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($totalKluPR2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($totalKluLK2+$totalKluPR2).'</strong></td>';
										
										echo '<td align="right"><strong>'.number_format($AwalLK2+$LahirLK2-$MatiLK2+$DtgLK2-$totalKluLK2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format($AwalPR2+$LahirPR2-$MatiPR2+$DtgPR2-$totalKluPR2).'</strong></td>';
										echo '<td align="right"><strong>'.number_format(($AwalLK2+$LahirLK2-$MatiLK2+$DtgLK2-$totalKluLK2) + ($AwalPR2+$LahirPR2-$MatiPR2+$DtgPR2-$totalKluPR2)).'</strong></td>';
										
										
										?>
										
									</tbody>
								</table><br><br>
								<!-- <div><?php echo paginate_one($reload, $page, $tpages); ?></div> -->
							  </div>
							</div>
						</div>
						<div class="tab-pane fade <?php if(@$_GET['id']!=null){ echo 'in active show'; }?>" id="tambah-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Data Utama<?php //echo $Sebutan; ?></h3>
							</div>
						<form method="post" action="">
							<div class="card-body">
								<div class="row">
								  <div class="col-lg-3">
										<div class="form-group-material">
										  <input type="text" name="NIK" class="form-control" placeholder="NIK" value="<?php echo @$RowData['NIK'];?>" readonly>
										  <input type="hidden" name="KodeLokasi" class="form-control" placeholder="Kode Lokasi" value="<?php echo @$RowData['KodeLokasi'];?>" readonly>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Nama" class="form-control" placeholder="Nama" value="<?php echo @$RowData['Nama'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="TempatLahir" class="form-control" placeholder="Tempat Lahir" value="<?php echo @$RowData['TempatLahir'];?>" required>
										</div>
										<div class="form-group-material">
										<?php if(@$RowData['TanggalLahir'] !== null){
											echo '<input type="text" name="TanggalLahir" id="time7" class="form-control" placeholder="Tanggal Lahir" value="'.substr($RowData['TanggalLahir'],0,11).'" required>';
										} else {
											echo '<input type="text" name="TanggalLahir" id="time7" class="form-control" placeholder="Tanggal Lahir" value="'.date('Y-m-d').'" required>';
										} ?>
										</div>
										<div class="form-group-material">
											<select name="JenisKelamin" class="form-control" required>
												<option value="" disabled selected>--- Jenis Kelamin ---</option>
												<option value="1" <?php if(@$RowData['JenisKelamin']==='1'){echo 'selected';} ?>>Laki-laki</option>
												<option value="2" <?php if(@$RowData['JenisKelamin']==='2'){echo 'selected';} ?>>Perempuan</option>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Agama" class="form-control" required>
												<option value="" disabled selected>--- Pilih Agama ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='AGAMA' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['Agama']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="StatusPenduduk" class="form-control" required>
												<option value="" disabled selected>--- StatusPenduduk ---</option>
												<option value="ADA" <?php if(@$RowData['StatusPenduduk']==='ADA'){echo 'selected';} ?>>Ada</option>
												<option value="PINDAH_MASUK" <?php if(@$RowData['StatusPenduduk']==='PINDAH_MASUK'){echo 'selected';} ?>>Pindah Masuk</option>
												<option value="PINDAH_KELUAR" <?php if(@$RowData['StatusPenduduk']==='PINDAH_KELUAR'){echo 'selected';} ?>>Pindah Keluar</option>
												<option value="MENINGGAL" <?php if(@$RowData['StatusPenduduk']==='MENINGGAL'){echo 'selected';} ?>>Meninggal</option>
											</select>
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group-material">
										  <input type="text" name="NamaAyah" class="form-control" placeholder="Nama Ayah" value="<?php echo @$RowData['NamaAyah'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NamaIbu" class="form-control" placeholder="Nama Ibu" value="<?php echo @$RowData['NamaIbu'];?>" required>
										</div>
										<div class="form-group-material">
											<select name="Kewarganegaraan" class="form-control" required>
												<option value="" disabled selected>--- Kewarganegaraan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['Agama']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										
										<div class="form-group-material">
											<select name="StatusKeluarga" class="form-control" required>
												<option value="" disabled selected>--- Status Keluarga ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='HUB_KELUARGA' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['StatusKeluarga']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="StatusPerkawinan" class="form-control" required>
												<option value="" disabled selected>--- Status Perkawinan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='STATUS_PERKAWINAN' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['StatusPerkawinan']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="GolDarah" class="form-control" required>
												<option value="" disabled selected>--- Gol Darah ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='GOL_DARAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['GolDarah']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group-material">
										  <input type="text" name="NoKK" class="form-control" placeholder="No KK" value="<?php echo @$RowData['NoKK'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NoUrutDiKK" class="form-control" placeholder="No Urut di KK" value="<?php echo @$RowData['NoUrutKK'];?>" required>
										</div>
										
										<div class="form-group-material">
										  <input type="text" name="NoAkteLahir" class="form-control" placeholder="No Akte Kelahiran" value="<?php echo @$RowData['NoAkteLahir'];?>">
										</div>
										<div class="form-group-material">
										<?php if(@$RowData['TanggalAkteLahir'] !== null){
											echo '<input type="text" name="TanggalAkteLahir" id="time2" class="form-control" placeholder="Tanggal Akte Lahir" value="'.substr($RowData['TanggalLahir'],0,11).'" required>';
										} else {
											echo '<input type="text" name="TanggalAkteLahir" id="time2" class="form-control" placeholder="Tanggal Akte Lahir" value="'.date('Y-m-d').'" required>';
										} ?>
										</div>
										<div class="form-group-material">
											<select name="Pendidikan" class="form-control" required>
												<option value="" disabled selected>--- Pendidikan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT KodePendidikan,NamaPendidikan FROM mstpendidikan ORDER BY KodePendidikan ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodePendidikan']===@$RowData['KodePendidikan']){
															echo "<option value=\"".$kode['KodePendidikan']."\" selected>".$kode['NamaPendidikan']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodePendidikan']."\" >".$kode['NamaPendidikan']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Pekerjaan" class="form-control" required>
												<option value="" disabled selected>--- Pekerjaan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT KodePekerjaan,NamaPekerjaan FROM mstpekerjaan ORDER BY KodePekerjaan ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodePekerjaan']===@$RowData['KodePekerjaan']){
															echo "<option value=\"".$kode['KodePekerjaan']."\" selected>".$kode['NamaPekerjaan']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodePekerjaan']."\" >".$kode['NamaPekerjaan']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="PekerjaanTambahan" class="form-control" placeholder="Pekerjaan Tambahan" value="<?php echo @$RowData['PekerjaanTambahan'];?>">
										</div>
									</div>
									<div class="col-lg-3">
										<!-- <div class="form-group-material">						
											<select id="KodeLokasi" name="KodeLokasi" class="form-control" required>	
												<option value="" disabled selected>--- Desa ---</option>
												<?php /*
													$menu = mysqli_query($koneksi,"SELECT a.KodeLokasi,b.NamaDesa FROM mstlokasi a JOIN mstdesa b ON a.KodeDesa=b.KodeDesa WHERE LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeLokasi']===@$RowData['KodeLokasi']){
															echo "<option value=\"".$kode['KodeLokasi']."\" selected >".$kode['NamaDesa']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeLokasi']."\" >".$kode['NamaDesa']."</option>\n";
														}
													} */
												?>
											</select>
										</div> -->
										<div class="form-group-material">
											<select id="KodeDusun" name="KodeDusun" class="form-control" required>	
												<option value="" disabled selected>--- Dusun ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT * FROM mstdusun WHERE KodeLokasi='".@$RowData['KodeLokasi']."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeDusun']==@$RowData['KodeDusun']){
															echo "<option value=\"".$kode['KodeDusun']."\" selected >".$kode['NamaDusun']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeDusun']."\" >".$kode['NamaDusun']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="RT" class="form-control" placeholder="RT" value="<?php echo @$RowData['RT'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="RW" class="form-control" placeholder="RW" value="<?php echo @$RowData['RW'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="NamaJalan" class="form-control" placeholder="Nama Jalan" value="<?php echo @$RowData['NamaJalan'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="NoRumah" class="form-control" placeholder="No Rumah" value="<?php echo @$RowData['NoRumah'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="Alamat" class="form-control" placeholder="Alamat" value="<?php echo @$RowData['Alamat'];?>">
										</div>
										<!-- <div class="form-group-material">
											<select name="StatusKepemilikanRumah" class="form-control">
												<option value="" disabled selected>--- Kepemilikan Rumah ---</option>
												<?php /*
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='KEPEMILIKAN_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['StatusPemilikRumah']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Dinding" class="form-control" >
												<option value="" disabled selected>--- Dinding ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='DINDING_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Dinding']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Lantai" class="form-control" >
												<option value="" disabled selected>--- Lantai ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='LANTAI_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Lantai']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Atap" class="form-control" >
												<option value="" disabled selected>--- Atap ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='ATAP_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Lantai']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="AsetLainnya" class="form-control" >
												<option value="" disabled selected>--- Aset Lainnya ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='ASET_KELUARGA' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Lantai']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Suku" class="form-control" placeholder="Suku" value="<?php echo @$RowData['Alamat']; */?>">
										</div> -->
								  </div>
								  &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button>
								</div>
							</div>
						</form>								
							<?php
							/* if(@$_GET['id']==null){
								echo '<button type="submit" class="btn btn-primary" name="Simpan">Simpan</button>';
							}else{
								echo '<input type="hidden" name="KodeJabatan" value="'.$RowData['KodeJabatan'].'"> ';
								echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
								echo '<a href="UserLogin.php"><span class="btn btn-warning">Batalkan</span></a>';
							} */
							?>
						</div>
						
						<div class="tab-pane fade" id="import-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Import Data Penduduk<?php //echo $Sebutan; ?></h3>
							</div>
							
							
						
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		 $("#KodeLokasi").change(function(){
			var KodeLokasi = $("#KodeLokasi").val();
			$.ajax({
				url: "../library/ambil-desa.php",
				data: "KodeLokasi="+KodeLokasi,
				cache: false,
				success: function(msg){
					$("#KodeDusun").html(msg);
				}
			});
		  });
		});
	</script>
	
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#time1').Zebra_DatePicker({format: 'Y-m'});
			$('#time2').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time7').Zebra_DatePicker({format: 'Y-m-d'});
			//$('#Datetime2').Zebra_DatePicker({format: 'Y-m-d H:i', direction: 1});
		});
	</script>
	
	<?php
	if(isset($_POST['Simpan'])){
		echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		
		/* include ('../library/kode-log-server.php');
		//kode jabatan
		$sql_jbt = mysqli_query($koneksi,'SELECT RIGHT(KodeJabatan,5) AS kode FROM mstjabatan WHERE KodeLokasi="'.$login_lokasi.'" ORDER BY KodeJabatan DESC LIMIT 1');  
		$nums_jbt = mysqli_num_rows($sql_jbt);
		 
		if($nums_jbt <> 0)
		 {
		 $data_jbt = mysqli_fetch_array($sql_jbt);
		 $kode_jbt = $data_jbt['kode'] + 1;
		 }else
		 {
		 $kode_jbt = 1;
		 }
		 
		//mulai bikin kode
		 $bikin_kode_jbt = str_pad($kode_jbt, 5, "0", STR_PAD_LEFT);
		 $kode_jadi_jbt = "JBT-".$bikin_kode_jbt;
		
		$query = mysqli_query($koneksi,"INSERT into mstjabatan (KodeJabatan,NamaJabatan,Keterangan,KodeLokasi) 
		VALUES ('$kode_jadi_jbt','".$_POST['Nama']."','".$_POST['Keterangan']."','$login_lokasi')");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Master Jabatan : ".$_POST['Nama']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		} */
	}
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		$nik = htmlspecialchars($_POST['NIK']); $lokasi = htmlspecialchars($_POST['KodeLokasi']); 
		$nama = htmlspecialchars($_POST['Nama']); $tlahir = htmlspecialchars($_POST['TempatLahir']); $tgllahir = $_POST['TanggalLahir']; $jk = htmlspecialchars($_POST['JenisKelamin']); $agama = htmlspecialchars($_POST['Agama']); $stats = htmlspecialchars($_POST['StatusPenduduk']);
		
		$ayah = htmlspecialchars($_POST['NamaAyah']); $ibu = htmlspecialchars($_POST['NamaIbu']); $warga = htmlspecialchars($_POST['Kewarganegaraan']); $statskel = htmlspecialchars($_POST['StatusKeluarga']); $statskaw = htmlspecialchars($_POST['StatusPerkawinan']); $goldarah = htmlspecialchars($_POST['GolDarah']);
		
		$nokk = htmlspecialchars($_POST['NoKK']); $nourut = htmlspecialchars($_POST['NoUrutDiKK']); $noakte = htmlspecialchars($_POST['NoAkteLahir']); $tglakte = $_POST['TanggalAkteLahir']; $didik = htmlspecialchars($_POST['Pendidikan']); $kerja = htmlspecialchars($_POST['Pekerjaan']); $kerja2 = htmlspecialchars($_POST['PekerjaanTambahan']);
		
		$dusun = htmlspecialchars($_POST['KodeDusun']); $rt = htmlspecialchars($_POST['RT']); $rw = htmlspecialchars($_POST['RW']); $jalan = $_POST['NamaJalan']; $norumah = htmlspecialchars($_POST['NoRumah']); $alamat = htmlspecialchars($_POST['Alamat']); 
		
		$query = mysqli_query($koneksi,"UPDATE datapenduduk SET Nama = '$nama', TempatLahir = '$tlahir', TanggalLahir = '$tgllahir', JenisKelamin = '$jk', Agama = '$agama', StatusPenduduk = '$stats', NamaAyah = '$ayah', NamaIbu = '$ibu', WNI = '$warga', StatusKeluarga = '$statskel', StatusPerkawinan = '$statskaw', GolDarah = '$goldarah', NoKK = '$nokk', NoUrutKK = '$nourut', NoAkteLahir = '$noakte', TanggalAkteLahir = '$tglakte', KodePendidikan = '$didik', KodePekerjaan = '$kerja', PekerjaanTambahan = '$kerja2', KodeDusun = '$dusun', RT = '$rt', RW = '$rw', NamaJalan = '$jalan', NoRumah = '$norumah', Alamat = '$alamat' WHERE KodeLokasi = '$lokasi' AND NIK = '$nik'");
		
		if($query){
			echo '<script language="javascript">document.location="DataPenduduk.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Edit Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "DataPenduduk.php";
				  });
				  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		include ('../library/kode-log-server.php');
		$query = mysqli_query($koneksi,"DELETE FROM mstjabatan WHERE KodeJabatan='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Master Jabatan : ".$_GET['nm']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " Hapus data di Master Aparatur terlebih dahulu! ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterJabatan.php";
					  });
					  </script>';
		}
	}
	
	?>
  </body>
</html>