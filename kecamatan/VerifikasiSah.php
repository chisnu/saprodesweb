<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $NoTrMohon	=@$_GET['NoTrMohon'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $IDPend	=@$_GET['IDPend'];
 $JenisSurat=@$_GET['JenisSurat'];
 // $Aksi	=@$_GET['Aksi']; //tinjau ulang digunakan jika dokumen di rejected 
?>

<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Pengesahan</h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
								<label>Dokumen untuk <strong><?php echo $JenisSurat;?></strong></label>
								<?php // cari user
								$CariData = @mysqli_query($koneksi, "SELECT Nama, NIK FROM datapenduduk WHERE IDPend = '".$IDPend."'"); 
								$array = @mysqli_fetch_array($CariData);
									echo "<br>Nama : ".$array['Nama']." (<strong>".$array['NIK']."</strong>)";
									echo "<br>Nomor : ".$NoTrMohon."<br><br>";
								?>
								<!--<form action="upload/processupload.php" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
									<input name="NoTrMohon" type="hidden" value="<?php echo $NoTrMohon;?>" />
									<input name="KodeLokasi" type="hidden" value="<?php echo $KodeLokasi;?>" />
									<input name="IDPend" type="hidden" value="<?php echo $IDPend;?>" />
									<input name="NamaSyarat" type="hidden" value="<?php echo $NmSyarat;?>" />
									<input name="KodeSyarat" type="hidden" value="<?php echo $KdSyarat;?>" />
									<br/>
									<div class="form-group-material">
									  <input type="text" name="Ket" class="form-control" placeholder="Keterangan">
									</div>
								</form>-->
								<form method="post" action="VerifikasiDokumen.php?type=<?php echo base64_encode('pengesahan'); ?>">
								<div class="table-responsive">  
									<table class="table table-striped">
									  <tbody>
										<?php 
											$no =1;
											$sql_syarat = @mysqli_query($koneksi, "SELECT StatusSurat FROM trpengurusansurat WHERE JenisSurat='$JenisSurat' AND KodeLokasi='$KodeLokasi' AND NoTrMohon='$NoTrMohon'"); 
											while($data_syarat = @mysqli_fetch_array($sql_syarat)){
										?>
												<tr>
												  <?php // cek apakah sudah lengkap
														echo '<td align="center">';
														$cek_surat = @mysqli_query($koneksi, "SELECT * from masterpengurusansurat where JenisSurat='".$JenisSurat."' AND LEFT(KodeLokasi,3)='".substr($KodeLokasi,0,3)."'");
														$num_surat = @mysqli_fetch_array($cek_surat);
														if($num_surat['StatusSurat'] === '3'){
															if($data_syarat['StatusSurat'] === '1'){
																if($data_syarat['IsAtasNamaTTD'] === '1'){
																	echo '<input type="radio" name="sah">&nbsp;Ditandatangani Camat</input><br>';
																	echo '<input type="radio" name="sah" checked="checked" disabled >&nbsp;Ditandatangani Atas Camat</input><br><br>';
																} elseif($data_syarat['IsAtasNamaTTD'] === '0'){
																	echo '<input type="radio" name="sah" checked="checked" disabled >&nbsp;Ditandatangani Camat</input><br>';
																	echo '<input type="radio" name="sah">&nbsp;Ditandatangani Atas Nama Camat</input><br><br>';
																}
																echo '<span class="btn btn-success">Sudah Disahkan</span>';
															} else {
																echo '<input type="radio" name="sah" value="1#0" checked="checked">&nbsp;Ditandatangani Camat</input><br>';
																echo '<input type="radio" name="sah" value="1#1">&nbsp;Ditandatangani Atas Nama Camat</input><br><br>';
																echo '<input name="NoTrMohon" type="hidden" value="'.$NoTrMohon.'" />';
																echo '<input name="KodeLokasi" type="hidden" value="'.$KodeLokasi.'" />';
																echo '<input name="JenisSurat" type="hidden" value="'.$JenisSurat.'" />';
																echo '<input name="StatusSurat" type="hidden" value="'.$num_surat['StatusSurat'].'" />';
																echo '<button class="btn btn-warning" name="Pengesahan">Ceklist dan Klik untuk Pengesahan</button>';
															}
														} elseif($num_surat['StatusSurat'] === '2'){
															if($data_syarat['StatusSurat'] === '2'){
																echo '<input type="checkbox" name="sah" value="'.$data_syarat["StatusSurat"].'" checked="checked" disabled />&nbsp;&nbsp;';
																echo '<span class="btn btn-success">Sudah Disahkan</span>';
															} else {
																echo '<input type="checkbox" name="sah" value="2"/>&nbsp;&nbsp;';
																echo '<input name="NoTrMohon" type="hidden" value="'.$NoTrMohon.'" />';
																echo '<input name="KodeLokasi" type="hidden" value="'.$KodeLokasi.'" />';
																echo '<input name="JenisSurat" type="hidden" value="'.$JenisSurat.'" />';
																echo '<input name="StatusSurat" type="hidden" value="'.$num_surat['StatusSurat'].'" />';
																echo '<button class="btn btn-warning" name="Pengesahan">Ceklist dan Klik untuk Pengesahan</button>';
															}
														} 
														echo '</td>';
												  ?>
												</tr>
										<?php } ?>
									  </tbody>
									</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>
<div id="ModalEditImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>

<script type="text/javascript">
	   $(document).ready(function () {
	   $(".open_modal_image").click(function(e) {
		  // var kd_dok = $(this).data("kodedok");
		  // var id_pend  = $(this).data("idpend");
		  // var dok  = $(this).data("filedok");
		  var no_trmohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("lokasi");
			   $.ajax({
					   url: "DetilDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_trmohon,KodeLokasi: kode_lok},
					   success: function (ajaxData){
					   $("#ModalEditImage").html(ajaxData);
					   $("#ModalEditImage").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
	
	<script>
		function confirm_verif() {
			var answer = confirm("Apakah Anda yakin untuk memverifikasi data ini ?")
			if (answer == true){
				window.location = "VerifikasiSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
	
	<script>
		function cek(syarat){
			for(i=0; i < syarat.length; i++){
				syarat[i].checked = true;
			}
		}
		function uncek(syarat){
			for(i=0; i < syarat.length; i++){
				syarat[i].checked = false;
			}
		}
	</script>