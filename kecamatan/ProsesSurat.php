<?php
include 'akses.php';
include '../library/tgl-indo.php';

$fitur_id = 7;
include '../library/lock-menu.php';

$Page = 'LayananSurat';
$Tahun=date('Y');
$HariIni=date('Y-m-d H:i:s');
$JamSekarang = date('H:i:s');
$TanggalSekarang = date('Y-m-d');
		

//cek apakah user sudah pernah membuat tr permohonan msyarakat
$QueryCekData = @mysqli_query($koneksi, "SELECT a.NoTrMohon,a.JenisSurat,b.StandarWaktuPelayanan FROM trpermohonanmasy a JOIN MasterPengurusanSurat b ON (a.JenisSurat)=(b.JenisSurat) WHERE LEFT(a.KodeLokasi,3)='".substr($kode_lokasi_aktif,0,3)."' AND a.IDPend='$id_penduduk_aktif' AND a.StatusPermohonan='PraWAITING'"); 
$numCek = @mysqli_num_rows($QueryCekData); 
if($numCek > 0){
	while($CekData = @mysqli_fetch_array($QueryCekData)){
		$No_Transaksi = $CekData['NoTrMohon'];
		$Jenis_Surat = $CekData['JenisSurat'];
		$Standar_Waktu = $CekData['StandarWaktuPelayanan'];
	}
}

//Hapus Data
/* if(base64_decode(@$_GET['aksi'])==='Hapus'){
	$file = '../images/DokumenSyarat/'.base64_decode(@$_GET['file']);
	$thumb = '../images/DokumenSyarat/thumb_'.base64_decode(@$_GET['file']);
	
	if(file_exists($file)){ unlink($file); }
	if(file_exists($thumb)){ unlink($thumb); }
	$HapusData = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND KodeSyarat='".base64_decode(@$_GET['id'])."' AND NoTrMohon='$No_Transaksi'"); 
	echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil"; </script>';
}

if(base64_decode(@$_GET['aksi'])==='PindahDokumen'){
		$QueryCekData = @mysqli_query($koneksi, "SELECT MAX(NoUrutSyarat) as NoSekarang FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi'"); 
		$CekData = @mysqli_fetch_array($QueryCekData);
		$NoMax = $CekData['NoSekarang'];

		$NoSekarang = $NoMax+1; 
		
		$NmSyarat = base64_decode(@$_GET['NmSy']);
		$KodeDokPenduduk = base64_decode(@$_GET['KodeDok']);
		$KdSyarat = base64_decode(@$_GET['KdSyarat']);
		
		$PindahDokumen = @mysqli_query($koneksi, "INSERT INTO dokumensyaratmohon (NoTrMohon,NoUrutSyarat,KodeLokasi,NamaSyarat,KodeDokPenduduk,IDPend,IsVerified,KodeSyarat) VALUES (
		'$No_Transaksi','$NoSekarang','$kode_lokasi_aktif','$NmSyarat','$KodeDokPenduduk','$id_penduduk_aktif','0','$KdSyarat')");
		
		echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil"; </script>';
	}
*/
	
	if(isset($_POST['CheckAll'])){
		if(@$_REQUEST['ProsesSurat'] === 'TerimaBerkas'){
			$cekbox = @$_POST['cekbox'];
			if($cekbox){
				foreach($cekbox as $value){
					$progress = @$_POST['Progress'.$value];
					
					$TerimaDokumen = @mysqli_query($koneksi, "UPDATE progresssurat SET IsSend=b'0' WHERE KodeLokasi='$login_lokasi' AND NoTrMohon='$value' AND NoUrutProgress='$progress' AND IsSend=b'1'");
					if($TerimaDokumen){
						$QueryCekData = @mysqli_query($koneksi, "SELECT MAX(NoUrutProgress) as NoSekarang, UserName FROM progresssurat WHERE KodeLokasi='$login_lokasi' AND NoTrMohon='$value'"); 
						$CekData = @mysqli_fetch_array($QueryCekData);
						$NoMax = $CekData['NoSekarang']; $NoSekarang = $NoMax+1; $UserMax = $CekData['UserName']; 
						
						// simpan penerimaan berkas
						@mysqli_query($koneksi, "INSERT INTO progresssurat (NoUrutProgress,NoTrMohon,KodeLokasi,StatusProgress,UserName,Waktu,Tanggal,IsSend,IsConfirmed,WaktuConfirm,TglConfirm,UserConfirm)VALUES('$NoSekarang','$value','$login_lokasi','Proses Berkas (DESA)','$UserMax','$TanggalSekarang $JamSekarang','$TanggalSekarang $JamSekarang',b'0',b'1','$TanggalSekarang $JamSekarang','$TanggalSekarang $JamSekarang','$login_id')");
						
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Sukses!",
							text: " Berkas Permohonan Surat telah diterima",
							type: "success"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';	
					} else {
						echo '<script language="javascript">document.location="ProsesSurat.php"; </script>';
					}
				}
							
			}
			else{
				echo '<script language="javascript">document.location="ProsesSurat.php"; </script>';	
			}
		} elseif(@$_REQUEST['ProsesSurat'] === 'AlihkanBerkas') {
			$cekbox = @$_POST['cekbox']; 
			if($cekbox){
				foreach($cekbox as $value){
					$progress = @$_POST['Progress'.$value];
					$tujuan = @$_POST['TujuanSurat'.$value];
					
					$UpdateData = @mysqli_query($koneksi, "UPDATE progresssurat SET IsSend=b'1',SendTo='$tujuan',WaktuSend='$TanggalSekarang $JamSekarang',TglSend='$TanggalSekarang $JamSekarang',UserSender='$login_id',IsConfirmed=b'0' WHERE KodeLokasi='$login_lokasi' AND NoTrMohon='$value' AND NoUrutProgress='$progress' AND StatusProgress='Proses Berkas (DESA)'");
					if($UpdateData){
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Berhasil!",
							text: " Permohonan akan diteruskan ke proses selanjutnya!",
							type: "success"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
						
						}
				}
			}
			else{
				echo '<script language="javascript">document.location="ProsesSurat.php"; </script>';	
			}
		}
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	<!-- Select2Master -->
	<link rel="stylesheet" href="../library/select2master/css/select2.css"/>
	<!-- CK Editor -->
	<script type="text/javascript" src="../library/ckeditor/ckeditor.js"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		
		function confirm_hapus() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php?aksi=tampil";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		
		function confirm_proses() {
			var answer = confirm("Apakah Anda yakin untuk memproses data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		function confirm_check() {
			var answer = confirm("Apakah Anda yakin untuk menerima atau mengalihkan data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
		function confirm_penduduk() {
			var answer = confirm("Apakah Anda yakin untuk menghapus data ini ?")
			if (answer == true){
				window.location = "ProsesSurat.php?aksi=tampil&";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Layanan Surat - Proses Surat</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
				    <ul class="nav nav-pills">
						<li>
							<a href="LayananSurat.php?ProsesSurat=AlihkanBerkas"><span class="btn btn-primary">Kembali</span></a>&nbsp;
						</li>
						
					</ul><br/>
				  <div class="card">
					
						<?php $sql =  @mysqli_query($koneksi, "SELECT a.NoTrMohon, a.TglPermohonan, a.Keterangan1, a.JenisSurat, a.KodeLokasi, a.IDPend, a.StatusPermohonan, b.Keterangan, a.PrediksiTglSelesai, b.KodeNomorSurat, b.StatusSurat FROM trpermohonanmasy a JOIN masterpengurusansurat b ON a.JenisSurat=b.JenisSurat WHERE (a.StatusPermohonan='WAITING' OR a.StatusPermohonan='ON PROGRESS' OR a.StatusPermohonan='DONE' OR a.StatusPermohonan='REJECTED') AND LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."' AND NoTrMohon='".base64_decode(@$_GET['kode'])."'");
						$array = @mysqli_fetch_array($sql);
						?>
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Proses <?php echo ucwords($array['Keterangan']); ?></h3>
							</div>
							<div class="card-body">
								<div class="row">
								  <div class="col-sm-8">
									<h5>Informasi Pemohon</h5>
									<?php $sql2 =  mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE IDPend='".$array['IDPend']."'");
									while($array2 = mysqli_fetch_array($sql2)){
										$nik = $array2['NIK'];
										$nama = $array2['Nama'];
										$tlahir = $array2['TempatLahir'];
										$tgllahir = $array2['TanggalLahir'];
										$alamat = $array2['Alamat'];
										$jk = $array2['JenisKelamin'];
										$lokasi = $array2['KodeLokasi'];
										$idlapor = $array2['IDPend'];
									} ?>
									
									<div class="table-responsive">  
									<table class="table table-striped">
									  <tbody>
										<tr><td width="35%">NIK / Nama</td><td width="1%">:</td><td><?php echo $nik." / ".strtoupper($nama);?></td></tr>
										<tr><td>Tempat & Tanggal Lahir</td><td>:</td><td><?php echo ucwords($tlahir).", ".TanggalIndo($tgllahir);?></td></tr>
									  </tbody>
									</table>
									</div><hr>
									
									<?php $tandatangan =  mysqli_query($koneksi, "SELECT Penandatanganan FROM mstlokasi WHERE KodeLokasi='001-000'");
									while($arrayTtd = mysqli_fetch_array($tandatangan)){
										$TTD = $arrayTtd['Penandatanganan'];
									} ?>
									
									<h5>Pengisian Surat</h5>
									<form method="post">
									<?php $sql3 =  mysqli_query($koneksi, "SELECT * FROM trpengurusansurat WHERE NoTrMohon='".$array['NoTrMohon']."' AND KodeLokasi='".$array['KodeLokasi']."' AND JenisSurat='".$array['JenisSurat']."'");
									$array3 = mysqli_fetch_array($sql3); ?>
									
									<!-- <div class="form-group-material">
										<?php /* if($array3['Tanggal'] == null){
											echo '<input type="text" name="TglSurat" id="time7" class="form-control" placeholder="Tanggal Surat" value="'.date("Y-m-d").'" required>';
										} else {
											echo '<input type="text" name="TglSurat" id="time7" class="form-control" placeholder="Tanggal Surat" value="'.substr($array3['Tanggal'],0,11).'" required>';
										} */ ?>
									</div> -->
									<div class="form-group-material">
										<?php if($array3['Keperluan'] == null){
											// keperluan, jika surat IUM baru
											if($array['JenisSurat'] === 'SIUM'){
												if($array['StatusSurat'] === '3'){
													echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>';
													echo 'Berdasarkan Peraturan Presiden Nomor 98 Tahun 2014 tentang perizinan untuk Usaha Mikro dan Kecil ( Lembar Negara Republik Indonesia Tahun 2014 Nomor 22 ); Peraturan Menteri Dalam Negeri Republik Indonesia Nomor 83 Tahun 2014 tentang Pedoman Pemberian Izin Usaha Mikro dan Kecil; Peraturan Bupati Nganjuk Nomor 20 Tahun 2015 tentang Pendelegasian Sebagian Kewenangan Bupati Kepada Camat dalam rangka pelaksanaan Izin Usaha Mikro, Surat Keterangan Usaha dari desa yang bersangkutan, serta berdasarkan keterangan dari Surat Pernyataan Pemohon, bersama ini menyatakan dan memberikan izin kepada :</textarea><br>';
													echo '<input type="text" name="Suport11" class="form-control" placeholder="Nama Pemohon" value="'.$nama.'" readonly><br>';
													echo '<input type="text" name="Suport12" class="form-control" placeholder="NIK" value="'.$nik.'" readonly><br>';
													echo '<input type="text" name="Suport13" class="form-control" placeholder="Alamat" value="'.$alamat.'" required><br>';
													echo '<input type="text" name="Suport14" class="form-control" placeholder="Nomor Telepon" required><br>';
												} else { echo ''; }
											} 
										} else {
											// keperluan, jika surat IUM update atau sudah ada
											if($array['JenisSurat'] === 'SIUM'){
												if($array['StatusSurat'] === '3'){
												$pecah = explode("#", $array3['Suport1']);
													echo '<textarea type="text" id="ckeditor" name="Keperluan" class="form-control" rows="10" placeholder="Keperluan" required>'.$array3['Keperluan'].'</textarea><br>';
													echo '<input type="text" name="Suport11" class="form-control" placeholder="Nama Pemohon" value="'.$pecah[0].'" readonly><br>';
													echo '<input type="text" name="Suport12" class="form-control" placeholder="NIK" value="'.$pecah[1].'" readonly><br>';
													echo '<input type="text" name="Suport13" class="form-control" placeholder="Alamat" value="'.$pecah[2].'" required><br>';
													echo '<input type="text" name="Suport14" class="form-control" placeholder="Nomor Telepon" value="'.$pecah[3].'" required><br>';
												} else { echo ''; }
											} 
										} ?>
										<script type="text/javascript">
											var editor = CKEDITOR.replace('ckeditor');
											CKFinder.setupCKEditor(editor, 'ckfinder/');    
										</script>
									</div>
									<div class="form-group-material">
										<?php if($array3['Uraian'] == null){
											// uraian, jika surat IUM baru
											if($array['JenisSurat'] === 'SIUM'){
												if($array['StatusSurat'] === '3'){
													echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>';
													echo 'Untuk mendirikan Usaha Mikro yang mencakup perizinan dasar berupa : menempati Lokasi/Domisili, melakukan kegiatan usaha baik produksi maupun penjualan barang dan jasa, dengan identitas :</textarea><br>';
													echo '<input type="text" name="Suport21" class="form-control" placeholder="Nama Perusahaan" required><br>';
													echo '<input type="text" name="Suport22" class="form-control" placeholder="Bentuk Perusahaan" required><br>';
													echo '<input type="text" name="Suport23" class="form-control" placeholder="Kegiatan Usaha" required><br>';
													echo '<input type="text" name="Suport24" class="form-control" placeholder="Sarana Usaha yang Digunakan" required><br>';
													echo '<input type="text" name="Suport25" class="form-control" placeholder="Alamat Usaha" required><br>';
													echo '<input type="text" name="Suport26" class="form-control" placeholder="Jumlah Modal Usaha" id="nominal" required><br>';
													echo '<input type="text" name="Suport27" class="form-control" placeholder="Nomor Pendaftaran" required>';
												} else { echo ''; }
											} 
										} else {
											// uraian, jika surat IUM update atau sudah ada
											if($array['JenisSurat'] === 'SIUM'){
												if($array['StatusSurat'] === '3'){
												$pecah = explode("#", $array3['Suport2']);
													echo '<textarea type="text" id="ckeditor2" name="Uraian" class="form-control" rows="10" placeholder="Uraian" required>'.$array3['Uraian'].'</textarea><br>';
													echo '<input type="text" name="Suport21" class="form-control" placeholder="Nama Perusahaan" value="'.$pecah[0].'" required><br>';
													echo '<input type="text" name="Suport22" class="form-control" placeholder="Bentuk Perusahaan" value="'.$pecah[1].'" required><br>';
													echo '<input type="text" name="Suport23" class="form-control" placeholder="Kegiatan Usaha" value="'.$pecah[2].'" required><br>';
													echo '<input type="text" name="Suport24" class="form-control" placeholder="Sarana Usaha yang Digunakan" value="'.$pecah[3].'" required><br>';
													echo '<input type="text" name="Suport25" class="form-control" placeholder="Alamat Usaha" value="'.$pecah[4].'" required><br>';
													echo '<input type="text" name="Suport26" class="form-control" placeholder="Jumlah Modal Usaha" id="nominal" value="'.number_format($pecah[5],0,"",".").'" required><br>';
													echo '<input type="text" name="Suport27" class="form-control" placeholder="Nomor Pendaftaran" value="'.$pecah[6].'"required>';
												} else { echo ''; }
											} 
										} ?>
										</textarea>
										<script type="text/javascript">
											var editor2 = CKEDITOR.replace('ckeditor2');
											CKFinder.setupCKEditor(editor2, 'ckfinder/');    
										</script>
									</div>
									<button type="submit" class="btn btn-primary" name="SimpanPengurusan">Simpan</button>&nbsp;
									<button type="submit" class="btn btn-warning" name="ResetPengurusan">Reset</button>
									
									<?php if(isset($_POST['SimpanPengurusan'])){
										// deklarasi post data
										if($array['JenisSurat'] === 'SIUM'){
											$modal =  @str_replace(".", "", $_POST['Suport26']);
											$suport1 = htmlspecialchars($_POST['Suport11'])."#".htmlspecialchars($_POST['Suport12'])."#".htmlspecialchars($_POST['Suport13'])."#".htmlspecialchars($_POST['Suport14']);
											$suport2 = htmlspecialchars($_POST['Suport21'])."#".htmlspecialchars($_POST['Suport22'])."#".htmlspecialchars($_POST['Suport23'])."#".htmlspecialchars($_POST['Suport24'])."#".htmlspecialchars($_POST['Suport25'])."#".htmlspecialchars($modal)."#".htmlspecialchars($_POST['Suport27']);
										}
										
										$tanggal = date("Y-m-d"); $keperluan = $_POST['Keperluan']; $uraian = $_POST['Uraian'];
										
										// cek apakah sudah ada permohonan
										$cek = @mysqli_query($koneksi, "SELECT * from trpengurusansurat where NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
										$num = @mysqli_num_rows($cek);
										if($num <> 0){
											// update transaksi pengurusan surat
											if($array['JenisSurat'] === 'SIUM'){
												$UpdateTrPengurusan = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET Tanggal='".$tanggal."', Uraian='".$uraian."', Keperluan='".$keperluan."', Suport1='".$suport1."', Suport2='".$suport2."', Penandatanganan='".$TTD."' WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
											}
											if($UpdateTrPengurusan){
												echo '<script type="text/javascript">
												  sweetAlert({
													title: "Update Surat Sukses!",
													text: " Data Surat Telah Diperbaharui!",
													type: "success"
												  },
												  function () {
													window.location.href = "ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'";
												  });
												  </script>';
												/* echo '<script language="javascript">document.location="ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'"; </script>'; */	
											}
										} else { 
											// buat nomor transaksi pengurusan surat
											$Pengurusan = @mysqli_query($koneksi, "SELECT MAX(NoTransaksiSurat) as NoSurat FROM trpengurusansurat WHERE KodeLokasi='".$array['KodeLokasi']."' AND JenisSurat='".$array['JenisSurat']."'"); 
											$nums = @mysqli_num_rows($pengurusan); 
											while($CekData = @mysqli_fetch_array($Pengurusan)){
												if($nums === 0){ $kode = 1; } else { $kode = $CekData['NoSurat'] + 1; }
											}
											$NoSekarang = $kode; 
											
											// simpan transaksi pengurusan surat
											if($array['JenisSurat'] === 'SIUM'){
												$SimpanTrPengurusan = @mysqli_query($koneksi, "INSERT INTO trpengurusansurat(NoTransaksiSurat,Tanggal,Uraian,Keperluan,Suport1,Suport2,KodeLokasi,JenisSurat,NoTrMohon,StatusSurat,Penandatanganan) VALUES ('$NoSekarang','".$tanggal."','".$uraian."','".$keperluan."','".$suport1."','".$suport2."','".$array['KodeLokasi']."','".$array['JenisSurat']."','".$array['NoTrMohon']."','0','".$TTD."')");
											}
											if($SimpanTrPengurusan){
												echo '<script type="text/javascript">
												  sweetAlert({
													title: "Simpan Surat Sukses!",
													text: " Data Surat Telah Dibuat!",
													type: "success"
												  },
												  function () {
													window.location.href = "ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'";
												  });
												  </script>';
												/* echo '<script language="javascript">document.location="ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'"; </script>'; */	
											}
										}
									} 
									if(isset($_POST['ResetPengurusan'])){
										// cek apakah sudah ada permohonan
										$cek = @mysqli_query($koneksi, "SELECT * from trpengurusansurat where NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
										$num = @mysqli_num_rows($cek);
										if($num <> 0){
											$UpdateTrPengurusan = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET Uraian=null, Keperluan=null, Suport1=null, Suport2=null WHERE NoTrMohon='".$array['NoTrMohon']."' AND JenisSurat='".$array['JenisSurat']."' AND KodeLokasi='".$array['KodeLokasi']."'");
											if($UpdateTrPengurusan){
												echo '<script type="text/javascript">
												  sweetAlert({
													title: "Reset Surat Sukses!",
													text: " Data Surat Telah Direset Ulang!",
													type: "success"
												  },
												  function () {
													window.location.href = "ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'";
												  });
												  </script>';
												/* echo '<script language="javascript">document.location="ProsesSurat.php?kode='.base64_encode($array['NoTrMohon']).'"; </script>'; */	
											}
										}
									}
									?>
									</form>
								  </div>
								  
								</div>
							</div>
						
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
	<!-- Modal Popup untuk Edit--> 
	<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
	
	<div id="ModalEditDokumen" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
	<div id="ModalEditTracking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Main File-->
    <script src="komponen/js/front.js"></script>
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#time1').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time2').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time7').Zebra_DatePicker({format: 'Y-m-d'});
			//$('#Datetime2').Zebra_DatePicker({format: 'Y-m-d H:i', direction: 1});
		});
	</script>
	<script src="../library/select2master/js/select2.min.js"></script>
	  <script>
	  $(document).ready(function () {
		$("#CariPenduduk").select2({
			placeholder: "NIK atau Nama Penduduk"
		});
	  });
	  $(document).ready(function () {
		$("#CariAyah").select2({
			placeholder: "NIK atau Nama Ayah"
		});
	  });
	  $(document).ready(function () {
		$("#CariIbu").select2({
			placeholder: "NIK atau Nama Ibu"
		});
	  });
	  </script>
	<!-- Javascript untuk popup modal Edit--> 
	<script type="text/javascript">
	   // open modal lihat dokumen
	   $(document).ready(function () {
	   $(".open_modal").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  	   $.ajax({
					   url: "LihatDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		// open modal lihat dokumen
	   $(document).ready(function () {
	   $(".open_modal_input").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  	   $.ajax({
					   url: "CariPenduduk.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		// open modal lihat progress
	   $(document).ready(function () {
	   $(".open_modal_tracking").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  var user  = $(this).data("user");
		  	   $.ajax({
					   url: "TrackingDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat,User: user},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
	  /* // open modal unggah dokumensyarat 
		$(document).ready(function () {
	   $(".open_modal").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var nm_syarat = $(this).data("namasyarat");
		  var kd_syarat = $(this).data("kodesyarat");
			   $.ajax({
					   url: "UnggahDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,NamaSyarat: nm_syarat,KodeSyarat: kd_syarat},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		//open modal dokumensyarat
		$(document).ready(function () {
	    $(".open_modal_dokumen").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var nm_syarat = $(this).data("namasyarat");
		  var kd_syarat = $(this).data("kodesyarat");
			   $.ajax({
					   url: "LihatDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,NamaSyarat: nm_syarat,KodeSyarat: kd_syarat},
					   success: function (ajaxData){
					   $("#ModalEditDokumen").html(ajaxData);
					   $("#ModalEditDokumen").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
		
		//open modal tracking
		$(document).ready(function () {
	    $(".open_modal_tracking").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var nm_surat = $(this).data("namasurat");
			   $.ajax({
					   url: "TrackingDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,NamaiSurat:nm_surat},
					   success: function (ajaxData){
					   $("#ModalEditTracking").html(ajaxData);
					   $("#ModalEditTracking").modal('show',{backdrop: 'true'});
				   }
				});
			});
		}); */
	</script>
	
	<script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>
	
	<script text="javascript">
			/* Tanpa Rupiah */
			var tanpa_rupiah = document.getElementById('nominal');
			
			tanpa_rupiah.addEventListener('keyup', function(e){ tanpa_rupiah.value = formatRupiah(this.value); });
			tanpa_rupiah.addEventListener('keydown', function(event){ limitCharacter(event); });
			
			/* Fungsi */
			function formatRupiah(bilangan, prefix)
			{
				var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
					split	= number_string.split(','),
					sisa 	= split[0].length % 3,
					rupiah 	= split[0].substr(0, sisa),
					ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
					
				if (ribuan) {
					separator = sisa ? '.' : '';
					rupiah += separator + ribuan.join('.');
				}
				
				rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
				return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
			}
			
			function limitCharacter(event)
			{
				key = event.which || event.keyCode;
				if ( key != 188 // Comma
					 && key != 8 // Backspace
					 && key != 9 // Tab
					 && key != 116 // F5
					 && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
					 && (key < 48 || key > 57) // Non digit
					 // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
					) 
				{
					event.preventDefault();
					return false;
				}
			}
			</script>
	
	<?php
	if(isset($_POST['BuatSurat'])){
		// cek apakah sudah ada permohonan
		$cek = @mysqli_query($koneksi, "SELECT * from trpermohonanmasy where IDPend='$id_penduduk_aktif' AND JenisSurat='".$_POST['JenisSurat']."' AND (StatusPermohonan != 'REJECTED' OR StatusPermohonan != 'DONE')");
		$num = @mysqli_num_rows($cek);
		if($num > 0){
			echo '<script type="text/javascript">swal( "Permohonan Sudah Ada!", " Silahkan Tunggu Permohonan Selesai Diproses ", "error" ); </script>';
		}else{ 
			// membuat id otomatis
			$sql = @mysqli_query($koneksi, "SELECT MAX(RIGHT(NoTrMohon,8)) AS kode FROM trpermohonanmasy WHERE KodeLokasi='$kode_lokasi_aktif' AND LEFT(NoTrMohon,7)='MH-$Tahun'"); 
			$nums = @mysqli_num_rows($sql); 
			while($data = @mysqli_fetch_array($sql)){
			if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
			}
			// membuat kode user
			$bikin_kode = str_pad($kode, 8, "0", STR_PAD_LEFT);
			$kode_jadi = "MH-".$Tahun."-".$bikin_kode;
			
			//simpan trpermohonan masyarakat
			$SimpanData = @mysqli_query($koneksi, "INSERT INTO trpermohonanmasy (NoTrMohon,KodeLokasi,JenisSurat,IDPend,StatusPermohonan)VALUES('$kode_jadi','$kode_lokasi_aktif','".$_POST['JenisSurat']."','$id_penduduk_aktif','PraWAITING')"); 
			
			echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil"; </script>';	
		}
	}
	
	if(isset($_POST['GantiSurat'])){
		$cek = @mysqli_query($koneksi, "SELECT * from trpermohonanmasy where IDPend='$id_penduduk_aktif' AND JenisSurat='".$_POST['JenisSurat']."' AND (StatusPermohonan != 'REJECTED' OR StatusPermohonan != 'DONE')");
		$num = @mysqli_num_rows($cek);
		if($num > 0){
			echo '<script type="text/javascript">swal( "Permohonan Sudah Ada!", " Silahkan Tunggu Permohonan Selesai Diproses ", "error" ); </script>';
		}else{ 
			//hapus dokumensyarat mohon
			$HapusDokumenSyarat = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			if($HapusDokumenSyarat){
				//update trmohonmsy
				$UpdateData = @mysqli_query($koneksi, "UPDATE trpermohonanmasy SET JenisSurat='".$_POST['JenisSurat']."' WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi'"); 
			}
			
			echo '<script language="javascript">document.location="ProsesSurat.php?aksi=tampil"; </script>';
		}
	}
	
	if(isset($_POST['KirimPermohonan'])){
		$cek = @mysqli_query($koneksi, "SELECT * from dokumensyaratmohon where NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
		$num = @mysqli_num_rows($cek);
		if($num === 0){
			echo '<script type="text/javascript">swal( "Maaf!", " Dokumen Persyaratan Anda Belum Lengkap ", "error" ); </script>';
		}else{ 
		
			$JamSekarang = date('H:i:s');
			$TanggalSekarang = date('Y-m-d');
			$JamLayananKantor = date('Y-m-d').' 12:00:00'; //jam tutup lyn hari ini
			
			$NamaHariIni = date('D', strtotime($HariIni)); //nama hari
			
			if(strtotime($HariIni) > strtotime($JamLayananKantor)){ //jika input diatas jam 12 siang maka diproses besok
				//Cek Ada berapa banyak hari sabtu
				$TglLibur = array();
				for($i=0; $i < $Standar_Waktu; $i++){
					$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
					$NmHari = date('D', strtotime($TglHari));
					$TglLibur[]=$NmHari;
				}
				//cari ada berapa banayak hari saturday
				$Count = array_count_values($TglLibur); 
				$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
				
				$TotalLamaPelayanan = $Standar_Waktu+$JmlTglLibur;
				$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
				
			}else{ //surat diproses hari ini
				//Cek Ada berapa banyak hari sabtu
				$StandarWaktuBaru  = $Standar_Waktu-1;
				$TglLibur = array();
				for($i=0; $i < $StandarWaktuBaru; $i++){
					$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
					$NmHari = date('D', strtotime($TglHari));
					$TglLibur[]=$NmHari;
				}
				//cari ada berapa banayak hari saturday
				$Count = array_count_values($TglLibur); 
				$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
				
				$TotalLamaPelayanan = $StandarWaktuBaru+$JmlTglLibur;
				$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
			}
			 
			//insert to progress surat & update trpermohonanmsy
			$UpdateTrPermohonan = @mysqli_query($koneksi, "UPDATE trpermohonanmasy SET TglPermohonan='$HariIni',PrediksiTglSelesai='$PrediksiTglSelesai', Keterangan1='".$_POST['Ket']."', StatusPermohonan='WAITING' WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			
			if($UpdateTrPermohonan){
				$InsertData = @mysqli_query($koneksi, "INSERT INTO progresssurat (NoUrutProgress,NoTrMohon,KodeLokasi,StatusProgress,Waktu,Tanggal,SendTo,UserSender,IsSend,WaktuSend,TglSend,IsConfirmed)
				VALUES('1','$No_Transaksi','$kode_lokasi_aktif','Pemohon/Masyarakat','$JamSekarang','$TanggalSekarang','Front Office (DESA)','$id_penduduk_aktif','1','$JamSekarang','$HariIni','0')");
				
				if($InsertData){
					echo '<script type="text/javascript">
					  sweetAlert({
						title: "Permohonan Terkirim!",
						text: " Permohonan surat akan diproses pada jam kerja aktif!",
						type: "success"
					  },
					  function () {
						window.location.href = "ProsesSurat.php";
					  });
					  </script>';
				}else{
					echo '<script type="text/javascript">
						  sweetAlert({
							title: "Simpan Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
				} 
			}
		}
	}
	
	if(isset($_POST['BatalkanPermohonan'])){
		//hapus dokumensyarat mohon
		$HapusDokumenSyarat = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
		if($HapusDokumenSyarat){
			$HapusTrPermohonan = @mysqli_query($koneksi, "DELETE FROM trpermohonanmasy WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'");
			if($HapusTrPermohonan){
				echo '<script type="text/javascript">
				  sweetAlert({
					title: "Hapus Data Berhasil",
					text: " ",
					type: "success"
				  },
				  function () {
					window.location.href = "ProsesSurat.php";
				  });
				  </script>';
			}else{
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "ProsesSurat.php";
					  });
					  </script>';
			}
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "ProsesSurat.php";
					  });
					  </script>';
		}
	}
	
	//Hapus Transaksi
	if(base64_decode(@$_GET['aksi'])==='HapusTransaksi'){
		//cek apakah status permohonan waiting
		$QueryCekStatus = @mysqli_query($koneksi, "SELECT NoTrMohon FROM trpermohonanmasy WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."' AND StatusPermohonan='WAITING'"); 
		$numCekStatus = @mysqli_num_rows($QueryCekData); 
		if($numCekStatus > 0){
			$HapusData = @mysqli_query($koneksi, "DELETE FROM dokumensyaratmohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'"); 
			
			if($HapusData){
				$HapusProgress = @mysqli_query($koneksi, "DELETE FROM progresssurat WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'"); 
				
				if($HapusProgress){
					$HapusTrans = @mysqli_query($koneksi, "DELETE FROM trpermohonanmasy WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'"); 
					
					if($HapusProgress){
						echo '<script language="javascript">document.location="ProsesSurat.php"; </script>';
					}else{
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
					}
				}else{
					echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
				}
			}else{
				echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "ProsesSurat.php";
						  });
						  </script>';
			}
		}else{
			echo '<script type="text/javascript">
			  sweetAlert({
				title: "Hapus Data Gagal!",
				text: " Permohonan Anda sedang dalam prosess verifikasi ",
				type: "error"
			  },
			  function () {
				window.location.href = "ProsesSurat.php";
			  });
			  </script>';
		}
	}
	
	
	?>
  </body>
</html>