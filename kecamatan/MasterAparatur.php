<?php
include 'akses.php';
$fitur_id = 2;
include '../library/lock-menu.php';

$Page = 'MasterData';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM mstaparat WHERE KodeAparat='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
	$RowData = mysqli_fetch_assoc($Edit);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	<!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
		
		.Zebra_DatePicker_Icon_Wrapper{
			width:100% !important;
		}
		
		.Zebra_DatePicker_Icon {
			top: 11px !important;
			right: 12px;
		}
		</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "MasterAparatur.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Master Aparatur</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<ul class="nav nav-pills">
						<li <?php if(@$id==null){echo 'class="active"';} ?>>
							<a href="#home-pills" data-toggle="tab"><span class="btn btn-primary">Data Aparatur</span></a>&nbsp;
						</li>
						<li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary"><?php echo $Sebutan; ?></span></a>
						</li>
					</ul><br/>
				  <div class="card">
					<div class="tab-content">
						<div class="tab-pane fade <?php if(@$_GET['id']==null){ echo 'in active show'; }?>" id="home-pills">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Data Aparatur</h3>
							</div>
							<div class="card-body">							  
								<!--<div class="col-lg-4 offset-lg-8">
									<form method="post" action="">
										<div class="form-group input-group">						
											<input type="text" name="keyword" class="form-control" placeholder="Nama..." value="<?php echo @$_REQUEST['keyword']; ?>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="submit">Cari</button>
											</span>
										</div>
									</form>
								</div>-->
							  <div class="table-responsive">  
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>No</th>
									  <th>Nama</th>
									  <th>NIP</th>
									  <th>Alamat</th>
									  <th>Jabatan</th>
									  <th>Status</th>
									  <th>Aksi</th>
									</tr>
								  </thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword=$_REQUEST['keyword'];
											$reload = "UserLogin.php?pagination=true&keyword=$keyword";
											$sql =  "SELECT * FROM userlogin WHERE ActualName LIKE '%$keyword%' AND KodeLokasi='$login_lokasi' AND IsDihapus='0' ORDER BY ActualName ASC";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "MasterAparatur.php?pagination=true";
											$sql =  "SELECT a.NamaAparat,a.NIP,a.Alamat,a.KodeAparat,a.IsAktif,b.NamaJabatan FROM mstaparat a JOIN mstjabatan b ON (a.KodeJabatan,a.KodeLokasi)=(b.KodeJabatan,b.KodeLokasi) WHERE a.KodeLokasi='$login_lokasi' ORDER BY KodeAparat ASC";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 20; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td>
												<strong><?php echo $data ['NamaAparat']; ?></strong>
											</td>
											<td>
												<?php echo $data ['NIP']; ?>
											</td>
											<td>
												<?php echo $data ['Alamat']; ?>
											</td>
											<td>
												<?php echo $data ['NamaJabatan']; ?>
											</td>
											<td align="center">
												<?php iF($data ['IsAktif']=='1'){
													echo '<a href="MasterAparatur.php?id='.base64_encode($data['KodeAparat']).'&aksi='.base64_encode('Nonaktif').'&nm='.$data['NamaAparat'].'" title="klik untuk set nonaktif aparatur"><font color="green">Aktif</font></a>';
												}else{
													echo '<a href="MasterAparatur.php?id='.base64_encode($data['KodeAparat']).'&aksi='.base64_encode('Aktif').'&nm='.$data['NamaAparat'].'" title="klik untuk set aktif aparatur"><font color="red">Nonaktif</font></a>';											
												} ?>
											</td>
											<td width="100px" align="center">										
												<?php
												echo '<a href="MasterAparatur.php?id='.base64_encode($data['KodeAparat']).'" title="Edit"><i class="btn btn-warning btn-sm"><span class="fa fa-edit"></span></i></a> ';
													
												/* echo '<a href="MasterAparatur.php?id='.base64_encode($data['KodeAparat']).'&nm='.$data['NamaAparat'].'&aksi='.base64_encode('Hapus').'" title="Hapus" onclick="return confirmation()"><i class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></i></a>'; */
												?>
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										?>
									</tbody>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							  </div>
							</div>
						</div>
						<div class="tab-pane fade <?php if(@$_GET['id']!=null){ echo 'in active show'; }?>" id="tambah-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4"><?php echo $Sebutan; ?></h3>
							</div>
							<div class="card-body">
								<div class="row">
								  <div class="col-lg-6">
									  <form method="post" action="">
										<div class="form-group-material">
										  <input type="text" name="Nama" class="form-control" placeholder="Nama Aparatur*" value="<?php echo @$RowData['NamaAparat'];?>" required>
										</div>
										<div class="form-group-material">
											<select name="KodeJabatan" class="form-control" required>
												<option value="" disabled selected>--- Pilih Jabatan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT KodeJabatan,NamaJabatan FROM mstjabatan WHERE KodeLokasi='$login_lokasi' ORDER BY KodeJabatan ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeJabatan']==@$RowData['KodeJabatan']){
															echo "<option value=\"".$kode['KodeJabatan']."\" selected>".$kode['NamaJabatan']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeJabatan']."\" >".$kode['NamaJabatan']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NIP" class="form-control" placeholder="NIP*" value="<?php echo @$RowData['NIP'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Pangkat" class="form-control" placeholder="Pangkat / Gol" value="<?php echo @$RowData['PangkatGol'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="NoPengangkatan" class="form-control" placeholder="No Pengangkatan" value="<?php echo @$RowData['NoPengangkatan'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="TglPengangkatan" id="time1" class="form-control" placeholder="Tanggal Pengangkatan*" value="<?php echo substr(@$RowData['TanggalPengangkatan'],0,11);?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="TglPemberhentian" id="time2" class="form-control" placeholder="Tanggal Pemberhentian" value="<?php echo substr(@$RowData['TanggalPemberhentian'],0,11);?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="TglLahir" id="time7" class="form-control" placeholder="Tanggal Lahir*" value="<?php echo substr(@$RowData['TanggalLahir'],0,10);?>" required> 
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group-material">
										  <input type="text" name="PendidikanTerakhir" class="form-control" placeholder="Pendidikan Terakhir*" value="<?php echo @$RowData['PendidikanTerakhir'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Alamat" class="form-control" placeholder="Alamat*" value="<?php echo @$RowData['Alamat'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NoTelp" class="form-control" placeholder="NoTelp*" value="<?php echo @$RowData['NoTelp'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="TempatLahir" class="form-control" placeholder="Tempat Lahir*" value="<?php echo @$RowData['TempatLahir'];?>" required>
										</div>
										
										<div class="form-group-material">
											<select name="JenisKelamin" class="form-control" required>
												<option value="" disabled selected>--- Jenis Kelamin ---</option>
												<option value="Laki-laki" <?php if(@$RowData['JenisKelamin']=='Laki-laki'){echo 'selected';} ?>>Laki-laki</option>
												<option value="Perempuan" <?php if(@$RowData['JenisKelamin']=='Perempuan'){echo 'selected';} ?>>Perempuan</option>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Agama" class="form-control" required>
												<option value="" disabled selected>--- Agama ---</option>
												<option value="Islam" <?php if(@$RowData['Agama']=='Islam'){echo 'selected';} ?>>Islam</option>
												<option value="Kristen" <?php if(@$RowData['Agama']=='Kristen'){echo 'selected';} ?>>Kristen</option>
												<option value="Katolik" <?php if(@$RowData['Agama']=='Katolik'){echo 'selected';} ?>>Katolik</option>
												<option value="Hindu" <?php if(@$RowData['Agama']=='Hindu'){echo 'selected';} ?>>Hindu</option>
												<option value="Budha" <?php if(@$RowData['Agama']=='Budha'){echo 'selected';} ?>>Budha</option>
												<option value="Konghucu" <?php if(@$RowData['Agama']=='Konghucu'){echo 'selected';} ?>>Kong Hu Cu</option>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Keterangan" class="form-control" placeholder="Keterangan" value="<?php echo @$RowData['Keterangan'];?>">
										</div>
										<div class="text-right">
										<?php
										if(@$_GET['id']==null){
											echo '<button type="submit" class="btn btn-primary" name="Simpan">Simpan</button>';
										}else{
											echo '<input type="hidden" name="KodeAparat" value="'.$RowData['KodeAparat'].'"> ';
											echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
											echo '<a href="MasterAparatur.php"><span class="btn btn-warning">Batalkan</span></a>';
										}
										?>
										</div>
									  </form>
								  </div>
								</div>
							</div>
						</div>
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	 <!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#time1').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time2').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time7').Zebra_DatePicker({format: 'Y-m-d'});
			//$('#Datetime2').Zebra_DatePicker({format: 'Y-m-d H:i', direction: 1});
		});
	</script>
	
	<?php
	if(isset($_POST['Simpan'])){
		/* include ('../library/kode-log-server.php'); */
		//kode jabatan
		$sql_jbt = mysqli_query($koneksi,'SELECT RIGHT(KodeAparat,5) AS kode FROM mstaparat WHERE KodeLokasi="'.$login_lokasi.'" ORDER BY KodeAparat DESC LIMIT 1');  
		$nums_jbt = mysqli_num_rows($sql_jbt);
		 
		if($nums_jbt <> 0)
		 {
		 $data_jbt = mysqli_fetch_array($sql_jbt);
		 $kode_jbt = $data_jbt['kode'] + 1;
		 }else
		 {
		 $kode_jbt = 1;
		 }
		 
		//mulai bikin kode
		 $bikin_kode_jbt = str_pad($kode_jbt, 5, "0", STR_PAD_LEFT);
		 $kode_jadi_jbt = "APR-".$bikin_kode_jbt;
		
		$query = mysqli_query($koneksi,"INSERT into mstaparat (KodeAparat,KodeLokasi,NamaAparat,NIP,PangkatGol,Alamat,NoTelp,Keterangan,IsAktif,JenisKelamin,TempatLahir,TanggalLahir,Agama,PendidikanTerakhir,NoPengangkatan,TanggalPengangkatan,TanggalPemberhentian,KodeJabatan) 
		VALUES ('$kode_jadi_jbt','$login_lokasi','".$_POST['Nama']."','".$_POST['NIP']."','".$_POST['Pangkat']."','".$_POST['Alamat']."','".$_POST['NoTelp']."','".$_POST['Keterangan']."','1','".$_POST['JenisKelamin']."','".$_POST['TempatLahir']."','".$_POST['TglLahir']."','".$_POST['Agama']."','".$_POST['PendidikanTerakhir']."','".$_POST['NoPengangkatan']."','".$_POST['TglPengangkatan']."','".$_POST['TglPemberhentian']."','".$_POST['KodeJabatan']."')");
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Master Aparatur : ".$_POST['Nama']."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterAparatur.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterAparatur.php";
				  });
				  </script>';
		}
	}
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		
		$query = mysqli_query($koneksi,"UPDATE mstaparat SET NamaAparat='".$_POST['Nama']."',NIP='".$_POST['NIP']."',PangkatGol='".$_POST['Pangkat']."',Alamat='".$_POST['Alamat']."',NoTelp='".$_POST['NoTelp']."',Keterangan='".$_POST['Keterangan']."',JenisKelamin='".$_POST['JenisKelamin']."',TempatLahir='".$_POST['TempatLahir']."',TanggalLahir='".$_POST['TglLahir']."',Agama='".$_POST['Agama']."',PendidikanTerakhir='".$_POST['PendidikanTerakhir']."',NoPengangkatan='".$_POST['NoPengangkatan']."',TanggalPengangkatan='".$_POST['TglPengangkatan']."',TanggalPemberhentian='".$_POST['TglPemberhentian']."',KodeJabatan='".$_POST['KodeJabatan']."' WHERE KodeAparat='".$_POST['KodeAparat']."' AND KodeLokasi='$login_lokasi'");
		
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Edit Master Aparatur :".$_POST['Nama']."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterAparatur.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Edit Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterAparatur.php";
				  });
				  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		/* include ('../library/kode-log-server.php'); */
		$query = mysqli_query($koneksi,"DELETE FROM mstaparat WHERE KodeAparat='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Master Aparatur : ".$_GET['nm']."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterAparatur.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " Aparatur telah digunakan di berbagai transaksi! ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterAparatur.php";
					  });
					  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Aktif'){
		/* include ('../library/kode-log-server.php'); */
		$query = mysqli_query($koneksi,"UPDATE mstaparat SET IsAktif='1' WHERE KodeAparat='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Update Data','Set Aktif Master Aparatur : ".$_GET['nm']."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterAparatur.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Set Aktif Aparatur Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterAparatur.php";
					  });
					  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Nonaktif'){
		/* include ('../library/kode-log-server.php'); */
		$query = mysqli_query($koneksi,"UPDATE mstaparat SET IsAktif=b'0' WHERE KodeAparat='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Update Data','Set Nonaktif Master Aparatur : ".$_GET['nm']."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterAparatur.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Set Nonaktif Aparatur Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterAparatur.php";
					  });
					  </script>';
		}
	}
	?>
  </body>
</html>