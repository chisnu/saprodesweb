<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 $NoTrMohon	=@$_GET['NoTrMohon'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $KodeSyarat=@$_GET['KodeSyarat'];
 // $NamaDokumen=@$_GET['NamaDokumen'];
 // $IDPend=@$_GET['IDPend'];
 
 $CariData = @mysqli_query($koneksi, "SELECT UrlFile, NamaSyarat FROM dokumensyaratmohon WHERE KodeLokasi='".$KodeLokasi."' AND NoTrMohon='".$NoTrMohon."' AND KodeSyarat='".$KodeSyarat."'"); 
 $array = @mysqli_fetch_array($CariData);
 
?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Detil <?php echo ucwords($array['NamaSyarat']); ?></h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
							<?php if(file_exists("../images/DokumenSyarat/".$array['UrlFile'])){
								echo '<img src="../images/DokumenSyarat/'.$array["UrlFile"].'" class="img img-responsive img-thumbnail" alt="Image User">';
							}else{
								echo '<img src="../images/DokumenSyarat/no-image.png" class="img img-responsive img-thumbnail" alt="Image User">';
							}
							?>		
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>