<?php include "akses.php"; 
include '../library/config.php';
$type = base64_decode(@$_GET['type']);
$Tahun=date('Y');
$HariIni=date('Y-m-d H:i:s');
$JamSekarang = date('H:i:s');
$TanggalSekarang = date('Y-m-d');

/* Sweet Alerts */
echo '<link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">';
echo '<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>';

if($type === 'pengesahan'){
	$sah = isset($_POST['sah']) ? $_POST['sah'] : 0;
	$bagi = explode("#", $sah);
	if($_POST['StatusSurat'] === '3'){
		$PengesahanSurat = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET StatusSurat='".$bagi[0]."', IsAtasNamaTTD='".$bagi[1]."' WHERE NoTrMohon='".$_POST['NoTrMohon']."' AND KodeLokasi='".$_POST['KodeLokasi']."' AND JenisSurat='".$_POST['JenisSurat']."'");
	} elseif($_POST['StatusSurat'] === '2') {
		$PengesahanSurat = @mysqli_query($koneksi, "UPDATE trpengurusansurat SET StatusSurat='".$sah."', Tanggal2='".date('Y-m-d')."' WHERE NoTrMohon='".$_POST['NoTrMohon']."' AND KodeLokasi='".$_POST['KodeLokasi']."' AND JenisSurat='".$_POST['JenisSurat']."'");
	}
	if($PengesahanSurat){
		echo '<script language="javascript">document.location="PengesahanSurat.php?ProsesSurat=AlihkanBerkas"; </script>';	
	}
} elseif($type === 'penyerahan'){
	$finish = isset($_POST['finish']) ? $_POST['finish'] : 0;
	$UpdateMohon = @mysqli_query($koneksi, "UPDATE trpermohonanmasy SET Penyerahan='1' WHERE KodeLokasi='".$_POST['KodeLokasi']."' AND NoTrMohon='".$_POST['NoTrMohon']."' AND IDPend='".$_POST['IDPend']."'");
	
	$QueryCekData = @mysqli_query($koneksi, "SELECT MAX(NoUrutProgress) as NoSekarang, UserName FROM progresssurat WHERE KodeLokasi='".$_POST['KodeLokasi']."' AND NoTrMohon='".$_POST['NoTrMohon']."'"); 
	$CekData = @mysqli_fetch_array($QueryCekData);
	$NoMax = $CekData['NoSekarang']; $NoSekarang = $NoMax+1; $UserMax = $CekData['UserName']; 
	
	// update progresssurat
	$UpdateMohon2 = @mysqli_query($koneksi, "UPDATE progresssurat SET SendTo='Pemohon/Masyarakat',WaktuSend='$TanggalSekarang $JamSekarang',TglSend='$TanggalSekarang $JamSekarang',UserSender='$login_id',IsConfirmed=b'0' WHERE KodeLokasi='".$_POST['KodeLokasi']."' AND NoTrMohon='".$_POST['NoTrMohon']."' AND NoUrutProgress='".$NoMax."'");
						
	// simpan progress penerimaan berkas
	$PenyerahanDokumen = @mysqli_query($koneksi, "INSERT INTO progresssurat (NoUrutProgress,NoTrMohon,KodeLokasi,StatusProgress,UserName,Waktu,Tanggal,IsSend,IsConfirmed,WaktuConfirm,TglConfirm,UserConfirm)VALUES('$NoSekarang','".$_POST['NoTrMohon']."','".$_POST['KodeLokasi']."','Pemohon/Masyarakat','$UserMax','$TanggalSekarang $JamSekarang','$TanggalSekarang $JamSekarang',b'0',b'1','$TanggalSekarang $JamSekarang','$TanggalSekarang $JamSekarang','$login_id')");
	if($PenyerahanDokumen){
		echo '<script language="javascript">document.location="LayananSurat.php?ProsesSurat=AlihkanBerkas"; </script>';	
	}
} else {
	$syarat = @$_POST['syarat']; $notrmohon = @$_POST['notrmohon']; $kodelokasi = @$_POST['kodelokasi']; $idpend = @$_POST['idpend']; 
	if($syarat){
		foreach($syarat as $key => $value){
			$UpdateData = @mysqli_query($koneksi, "UPDATE dokumensyaratmohon SET IsVerified=b'1' WHERE NoTrMohon='$notrmohon[$key]' AND KodeLokasi='$kodelokasi[$key]' AND KodeSyarat='$value'");
			if($UpdateData){
				// membuat id otomatis
				$sql = @mysqli_query($koneksi, "SELECT MAX(RIGHT(KodeDokPenduduk,1)) AS kode FROM dokumenfisik WHERE KodeLokasi='$kodelokasi[$key]' AND IDPend='$idpend[$key]' AND  LEFT(KodeDokPenduduk,11)='DK-$kodelokasi[$key]-'"); 
				$nums = @mysqli_num_rows($sql); 
				while($data = @mysqli_fetch_array($sql)){
				if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
				}
				// membuat kode user
				$bikin_kode = str_pad($kode, 1, "0", STR_PAD_LEFT);
				$kode_jadi = "DK-".$kodelokasi[$key]."-".$idpend[$key]."-".$bikin_kode;
				
				// array penggandaan data
				$CekLengkap = @mysqli_query($koneksi, "SELECT * FROM dokumensyaratmohon WHERE KodeLokasi='$kodelokasi[$key]' AND NoTrMohon='$notrmohon[$key]' AND KodeSyarat='$value'"); 
				while($arrayLengkap = @mysqli_fetch_array($CekLengkap)){ $nama = $arrayLengkap['NamaSyarat']; $file = $arrayLengkap['UrlFile']; $dokpenduduk = $arrayLengkap['KodeDokPenduduk']; }
				if($dokpenduduk == null){
					$update1 = @mysqli_query($koneksi, "INSERT INTO dokumenfisik (KodeDokPenduduk,NamaDokumen,UrlFile,IDPend,IsVerified,KodeLokasi)VALUES('$kode_jadi','$nama','$file','$idpend[$key]',b'1','$kodelokasi[$key]')");
					if($update1){
						@mysqli_query($koneksi, "UPDATE dokumensyaratmohon SET KodeDokPenduduk='$kode_jadi', IDPend='$idpend[$key]' WHERE KodeLokasi='$kodelokasi[$key]' AND NoTrMohon='$notrmohon[$key]' AND KodeSyarat='$value'");
					
						echo '<script type="text/javascript">
						sweetAlert({
							title: "Berhasil!",
							text: " Berkas Telah Terverifikasi!",
							type: "success"
						},
						function () {
							window.location.href = "VerifikasiSurat.php?ProsesSurat=AlihkanBerkas";
						});
					</script>';
					}
				} 
				echo '<script language="javascript">document.location="VerifikasiSurat.php?ProsesSurat=AlihkanBerkas"; </script>';	
													
			}
		}
	}
	else{
		echo '<script language="javascript">document.location="VerifikasiSurat.php?ProsesSurat=AlihkanBerkas"; </script>';	
	}
}
									
?>
