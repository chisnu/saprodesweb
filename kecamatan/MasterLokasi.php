<?php
include 'akses.php';
$fitur_id = 5;
include '../library/lock-menu.php';

$Page = 'MasterData';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data Identitas Lembaga';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM mstlokasi WHERE KodeLokasi='".base64_decode($_GET['id'])."'");
	$RowData = mysqli_fetch_assoc($Edit);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Master Lokasi</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<ul class="nav nav-pills">
						<li <?php if(@$id==null){echo 'class="active"';} ?>>
							<a href="MasterLokasi.php"><span class="btn btn-primary">Data Lokasi</span></a>&nbsp;
						</li>
						<!-- <li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary"><?php/* echo $Sebutan; */?></span></a>
						</li> -->
					</ul><br/>
				  <div class="card">
					<div class="tab-content">
						<div class="tab-pane fade <?php if(@$_GET['id']==null){ echo 'in active show'; }?>" id="home-pills">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Data User</h3>
							</div>							
							<div class="card-body">							  
								<!-- <div class="col-lg-4 offset-lg-8">
									<form method="post" action="">
										<div class="form-group input-group">						
											<input type="text" name="keyword" class="form-control" placeholder="Nama Desa" value="<?php echo @$_REQUEST['keyword']; ?>">
											<span class="input-group-btn">
												<button class="btn btn-primary" type="submit">Cari</button>
											</span>
										</div>
									</form>
								</div> -->
							  <div class="table-responsive">  
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>No</th>
									  <th>Kode Lokasi</th>
									  <th>Kecamatan</th>
									  <th>Desa</th>
									  <th>Kode Lokasi Surat</th>
									  <th>Aksi</th>
									</tr>
								  </thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword=$_REQUEST['keyword'];
											$reload = "MasterLokasi.php?pagination=true&keyword=$keyword";
											$sql =  "SELECT a.KodeLokasi,a.KodeLokasiSurat,b.NamaKecamatan,c.NamaDesa FROM mstlokasi a JOIN mstkecamatan b ON a.KodeKec=b.KodeKec JOIN mstdesa c ON a.KodeDesa=c.KodeDesa WHERE a.KodeKec='$login_kec' AND c.NamaDesa LIKE '%$keyword%' ORDER BY KodeLokasi ASC";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "MasterLokasi.php?pagination=true";
											$sql =  "SELECT a.KodeLokasi,a.KodeLokasiSurat,b.NamaKecamatan,c.NamaDesa FROM mstlokasi a JOIN mstkecamatan b ON a.KodeKec=b.KodeKec JOIN mstdesa c ON a.KodeDesa=c.KodeDesa WHERE a.KodeKec='$login_kec' ORDER BY KodeLokasi ASC";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 20; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td align="center">
												<strong><?php echo $data ['KodeLokasi']; ?></strong>
											</td>
											<td align="center">
												<?php echo $data ['NamaKecamatan']; ?>
											</td>
											<td align="left">
												<?php echo $data ['NamaDesa']; ?>
											</td>
											<td align="center">
												<?php echo $data ['KodeLokasiSurat']; ?>
											</td>
											<td width="100px" align="center">										
												<a href="MasterLokasi.php?id=<?php echo base64_encode($data['KodeLokasi']);?>" title='Edit'><i class='btn btn-warning btn-sm'><span class='fa fa-edit'></span></i></a> 
												
												<!-- <a href="MasterLokasi.php?id=<?php /* echo base64_encode($data['KodeLokasi']); ?>&aksi=<?php echo base64_encode('Hapus'); */ ?>" title="Hapus" onclick="return confirmation()"><i class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></i></a> -->
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										?>
									</tbody>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							  </div>
							</div>
						</div>
						<div class="tab-pane fade <?php if(@$_GET['id']!=null){ echo 'in active show'; }?>" id="tambah-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4"><?php echo $Sebutan; ?></h3>
							</div>
							<div class="card-body">
								<div class="row">
								  <div class="col-lg-6">
									  <?php $tampil = mysqli_query($koneksi,"SELECT * FROM mstlokasi WHERE KodeLokasi='".base64_decode(@$_GET['id'])."'");
									  while($row = mysqli_fetch_array($tampil)){ $data = explode("#", $row['AlamatLokasi']); $kode = $row['KodeLokasiSurat']; }
									  ?>
									  <form method="post" action="">
										<div class="form-group-material">
											<div class="form-group-material">
											  <input type="text" name="data1" class="form-control" placeholder="Nama Induk Instansi" value="<?php echo @$data[0];?>">
											</div>
										</div>
										<div class="form-group-material">
											<div class="form-group-material">
											  <input type="text" name="data2" class="form-control" placeholder="Lokasi Induk Instansi" value="<?php echo @$data[1];?>">
											</div>
										</div>
										<div class="form-group-material">
											<div class="form-group-material">
											  <input type="text" name="data3" class="form-control" placeholder="Nama Instansi Instansi" value="<?php echo @$data[2];?>">
											</div>
										</div>
										<div class="form-group-material">
											<div class="form-group-material">
											  <input type="text" name="data4" class="form-control" placeholder="Alamat Instansi" value="<?php echo @$data[3];?>">
											</div>
										</div>
										<div class="form-group-material">
											<div class="form-group-material">
											  <input type="text" name="kode_surat" class="form-control" placeholder="Kode Lokasi Surat" value="<?php echo @$kode;?>">
											</div>
										</div>
										<?php
										if(@$_GET['id']==null){
											/* echo '<div class="form-group-material">
												<select name="KodeDesa" class="form-control" required>	';
													$menu = mysqli_query($koneksi,"SELECT * FROM mstdesa WHERE KodeKec='$login_kec' ORDER BY NamaDesa ASC");
														while($kode = mysqli_fetch_array($menu)){
															echo "<option value=\"".$kode['KodeDesa']."\" >".$kode['NamaDesa']."</option>\n";
														}
											echo '</select>
											</div>';
											
											echo '<button type="submit" class="btn btn-primary" name="Simpan">Simpan</button>'; */
											echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
										}else{
											echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
											echo '<a href="MasterLokasi.php"><span class="btn btn-warning">Batalkan</span></a>';
										}
										?>
									  </form>
								  </div>
								</div>
							</div>
						</div>
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	
	
	<?php
	if(isset($_POST['Simpan'])){
		/* include ('../library/kode-log-server.php'); */

		//cek apakah kode lokasi desa sudah ada 
		$cek2 = mysqli_query($koneksi,"select KodeLokasi from mstlokasi where KodeKec='$login_kec' AND KodeDesa='".$_POST['KodeDesa']."'");
		$num2 = mysqli_num_rows($cek2);
		if($num2 == 1 ){
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Simpan Data Gagal!",
						text: " Kode Lokasi Sudah Ada! ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterLokasi.php";
					  });
					  </script>';
		}else{
			//buat kode lokasi
			$QueryKode = mysqli_query($koneksi,"SELECT MAX(RIGHT(KodeLokasi,3)) as Kode FROM mstlokasi WHERE LEFT(KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
			$num = mysqli_num_rows($QueryKode);
			
			if($num <> 0){
				$data_lok = mysqli_fetch_array($QueryKode);
				$kode_lok = $data_lok['Kode']+1;
			}else{
				$kode_lok = 1;
			}
			 
			//mulai bikin kode
			$bikin_kode_lok = str_pad($kode_lok, 3, "0", STR_PAD_LEFT);
			$kode_jadi_lok = substr($login_lokasi,0,3)."-".$bikin_kode_lok;
			
			
			$query = mysqli_query($koneksi,"INSERT INTO mstlokasi (KodeLokasi,KodeKab,KodeKec,KodeDesa,KodeLokasiSurat,AlamatLokasi) 
			VALUES ('$kode_jadi_lok','$login_kab','$login_kec','".$_POST['KodeDesa']."','".$_POST['KodeLokasiSurat']."','".$_POST['AlamatLokasiSurat']."')");
			if($query){
				mysqli_query($koneksi,"INSERT INTO mstjabatan (KodeJabatan,KodeLokasi,NamaJabatan,Keterangan)VALUES('JBT-00001','$kode_jadi_lok','BUPATI','-'),('JBT-00002','$kode_jadi_lok','CAMAT','-'),('JBT-00003','$kode_jadi_lok','KEPALA DESA','-'),('JBT-00004','$kode_jadi_lok','KETUA BPD','-'),('JBT-00005','$kode_jadi_lok','SEKRETARIS DESA','-')");
				
				/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
				VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Tambah Kode Lokasi : $kode_jadi_lok','$login_id','$login_lokasi')"); */

				echo '<script language="javascript">document.location="MasterLokasi.php";</script>';
			}else{
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Simpan Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterLokasi.php";
					  });
					  </script>';
			}
		}
	}
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		$Update = mysqli_query($koneksi,"UPDATE mstlokasi SET KodeLokasiSurat='".htmlspecialchars($_POST['kode_surat'])."', AlamatLokasi='".htmlspecialchars($_POST['data1'])."#".htmlspecialchars($_POST['data2'])."#".htmlspecialchars($_POST['data3'])."#".htmlspecialchars($_POST['data4'])."' WHERE KodeLokasi='".base64_decode($_GET['id'])."'");
		if($Update){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Edit Kode Lokasi Surat: ".base64_decode($_GET['id'])." - ".$_POST['KodeLokasiSurat']."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterLokasi.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Edit Data Gagal!",
						text: "  ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterLokasi.php";
					  });
					  </script>';
		}
		
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		/* include ('../library/kode-log-server.php'); */
		$query = mysqli_query($koneksi,"DELETE FROM mstlokasi WHERE KodeLokasi='".base64_decode($_GET['id'])."'");
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Hapus Kode Lokasi : ".base64_decode($_GET['id'])."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterLokasi.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " Data Telah Digunakan Dalam Berbagai Transaksi ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterLokasi.php";
					  });
					  </script>';
		}
	}	
	?>
  </body>
</html>