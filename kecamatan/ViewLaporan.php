<?php 
date_default_timezone_set('Asia/Jakarta');
require "../library/fpdf/fpdf2.php";  //pastikan path atau alamat FPDF sesuai

function hitung_umur($tanggal_lahir){
	list($year,$month,$day) = explode("-",$tanggal_lahir);
	$year_diff = date("Y") - $year;
	$month_diff = date("m") - $month;
	$day_diff = date("d") - $day;
	if($month_diff < 0) $year_diff--;
		elseif(($month_diff == 0) && ($day_diff < 0)) $year_diff--;
	return $year_diff;
}

// Pendefinisian folder font pada FPDF
define('FPDF_FONTPATH', '../library/fpdf/font/');
// Seperti sebelunya, kita membuat class anakan dari class FPDF
class FPDF_AutoWrapTable extends FPDF {
	private $data = array();
  	private $options = array( 'filename' => '', 'destinationfile' => '', 'paper_size'=>'f4', 'orientation'=>'l' );
	
	function __construct($data = array(), $options = array()) {
    	parent::__construct();
    	$this->data = $data;
    	$this->options = $options;
	}
	
	function Footer(){
	} 
 
	public function rptDetailData () {
		include "../library/config.php";
		include "../library/tgl-indo.php";
		
		//get variable
		$type = base64_decode(@$_GET['type']);
		$tahun = base64_decode(@$_GET['tahun']);
		$key = base64_decode(@$_GET['key']);
		$lokasi = base64_decode(@$_GET['lokasi']);
		$user = base64_decode(@$_GET['user']);
		
		$tandatangan =  mysqli_query($koneksi, "SELECT Penandatanganan FROM mstlokasi WHERE KodeLokasi='001-000'");
		while($arrayTtd = mysqli_fetch_array($tandatangan)){
			$bagiTTD = explode("#", $arrayTtd['Penandatanganan']);
		} 
		
		$border = 0;
		$this->AddPage();
		$this->SetMargins(20,25,20,10);
		$this->SetAutoPageBreak(true,10);
		$this->AliasNbPages();
		
		$tanggal_indo = TanggalIndo($key);
		$bulan = TanggalIndo($key);
		
		//header
			$this->Image('../images/Assets/logo.png',25,14,20); // logo
			$this->SetFont('Times','B','13'); $this->SetTextColor(0,0,0); // warna tulisan
			// font yang digunakan // membuat cell dg panjang 19 dan align center 'C'
		if($type === 'LaporanPenduduk' OR $type === 'LaporanPBB' OR $type === 'LaporanPBBDetilDesa' OR $type === 'LaporanPBBDetilPetugas'){
			// header
			$cari = @mysqli_query($koneksi, "Select AlamatLokasi from mstlokasi WHERE KodeLokasi = '001-000'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $alamat = explode("#", $tampil['AlamatLokasi']); }
			
			$this->Ln(10); $this->Cell(300,0,$alamat[0],0,0,'C');
			$this->Ln(0); $this->Cell(300,10,$alamat[1],0,0,'C');
			
			$this->SetFont('Times','B','12'); $this->Ln(0); $this->Cell(300,20,$alamat[2],0,0,'C');
			$this->Ln(0); $this->SetFont('Times','','11'); $this->Cell(300,30,$alamat[3].", ".$alamat[4],0,0,'C');
		} /* elseif($type === 'LaporanPBBDesa'){
			// header
			$this->Ln(10); $this->Cell(300,0,'PEMERINTAH KABUPATEN NGANJUK',0,0,'C');
			$this->Ln(0); $this->Cell(300,10,'KECAMATAN NGANJUK',0,0,'C');
			
			$cari = @mysqli_query($koneksi, "Select mstdesa.NamaDesa, mstlokasi.AlamatLokasi, mstlokasi.IsKelurahan from mstdesa inner join mstlokasi on mstlokasi.KodeDesa = mstdesa.KodeDesa WHERE mstlokasi.KodeLokasi='".$lokasi."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $namadesa = $tampil['NamaDesa']; $alamat = $tampil['AlamatLokasi']; $is_lurah = $tampil['IsKelurahan']; }
			if($is_lurah === '1'){
				$this->SetFont('Times','B','12'); $this->Ln(0); $this->Cell(300,20,'KELURAHAN '.$namadesa,0,0,'C');
			} else {
				$this->SetFont('Times','B','12'); $this->Ln(0); $this->Cell(300,20,'DESA '.$namadesa,0,0,'C');
			}
			$this->Ln(0); $this->SetFont('Times','','11'); $this->Cell(300,30,$alamat,0,0,'C');
		} */
			$this->Ln(20); $this->SetLineWidth(0.75); $this->Cell(290,0,'','TB',0,'C',1);
			$this->Ln(1); $this->SetLineWidth(0.1); $this->Cell(290,0,'','TB',0,'L',1);
			
		if($type === 'LaporanPenduduk'){
			$this->SetFont('Times','B','12'); $this->Ln(5); $this->Cell(290,0,'LAPORAN MUTASI PENDUDUK BULAN '.strtoupper($bulan),0,0,'C');
			$this->Ln(5); $this->SetFont('Times','B','9');
			$this->Cell(10,7,'','LTR',0,'L',0);
			$this->Cell(55,7,'','LTR',0,'L',0);
			$this->Cell(12.5,7,'','TB',0,'L',0);
			$this->Cell(12.5,7,'Jumlah Awal','TB',0,'C',0);
			$this->Cell(12.5,7,'','TRB',0,'L',0);
			$this->Cell(12.5,7,'','TB',0,'L',0);
			$this->Cell(12.5,7,'Lahir','TB',0,'C',0);
			$this->Cell(12.5,7,'','TRB',0,'L',0);
			$this->Cell(12.5,7,'','TB',0,'L',0);
			$this->Cell(12.5,7,'Mati','TB',0,'C',0);
			$this->Cell(12.5,7,'','TRB',0,'L',0);
			$this->Cell(12.5,7,'','TB',0,'L',0);
			$this->Cell(12.5,7,'Datang','TB',0,'C',0);
			$this->Cell(12.5,7,'','TRB',0,'L',0);
			$this->Cell(12.5,7,'','TB',0,'L',0);
			$this->Cell(12.5,7,'Pindah','TB',0,'C',0);
			$this->Cell(12.5,7,'','TRB',0,'L',0);
			$this->Cell(12.5,7,'','TB',0,'L',0);
			$this->Cell(12.5,7,'Total','TB',0,'C',0);
			$this->Cell(12.5,7,'','TRB',1,'L',0);
			
			$this->Cell(10,7,'No','LBR',0,'C',0);
			$this->Cell(55,7,'Desa/Kelurahan','LBR',0,'C',0);
			$this->Cell(12.5,7,'LK','TRB',0,'C',0);
			$this->Cell(12.5,7,'PR','TRB',0,'C',0);
			$this->Cell(12.5,7,'JML','TRB',0,'C',0);
			$this->Cell(12.5,7,'LK','TRB',0,'C',0);
			$this->Cell(12.5,7,'PR','TRB',0,'C',0);
			$this->Cell(12.5,7,'JML','TRB',0,'C',0);
			$this->Cell(12.5,7,'LK','TRB',0,'C',0);
			$this->Cell(12.5,7,'PR','TRB',0,'C',0);
			$this->Cell(12.5,7,'JML','TRB',0,'C',0);
			$this->Cell(12.5,7,'LK','TRB',0,'C',0);
			$this->Cell(12.5,7,'PR','TRB',0,'C',0);
			$this->Cell(12.5,7,'JML','TRB',0,'C',0);
			$this->Cell(12.5,7,'LK','TRB',0,'C',0);
			$this->Cell(12.5,7,'PR','TRB',0,'C',0);
			$this->Cell(12.5,7,'JML','TRB',0,'C',0);
			$this->Cell(12.5,7,'LK','TRB',0,'C',0);
			$this->Cell(12.5,7,'PR','TRB',0,'C',0);
			$this->Cell(12.5,7,'JML','TRB',1,'C',0);
			
			$this->SetFont('Times','I','8');
			$this->Cell(10,4,'1','LBR',0,'C',0);
			$this->Cell(55,4,'2','LBR',0,'C',0);
			$this->Cell(12.5,4,'3','TRB',0,'C',0);
			$this->Cell(12.5,4,'4','TRB',0,'C',0);
			$this->Cell(12.5,4,'5','TRB',0,'C',0);
			$this->Cell(12.5,4,'6','TRB',0,'C',0);
			$this->Cell(12.5,4,'7','TRB',0,'C',0);
			$this->Cell(12.5,4,'8','TRB',0,'C',0);
			$this->Cell(12.5,4,'9','TRB',0,'C',0);
			$this->Cell(12.5,4,'10','TRB',0,'C',0);
			$this->Cell(12.5,4,'11','TRB',0,'C',0);
			$this->Cell(12.5,4,'12','TRB',0,'C',0);
			$this->Cell(12.5,4,'13','TRB',0,'C',0);
			$this->Cell(12.5,4,'14','TRB',0,'C',0);
			$this->Cell(12.5,4,'15','TRB',0,'C',0);
			$this->Cell(12.5,4,'16','TRB',0,'C',0);
			$this->Cell(12.5,4,'17','TRB',0,'C',0);
			$this->Cell(12.5,4,'18','TRB',0,'C',0);
			$this->Cell(12.5,4,'19','TRB',0,'C',0);
			$this->Cell(12.5,4,'20','TRB',1,'C',0);
			
			$No = 1; $this->SetFont('Times','','8');
			$cari = @mysqli_query($koneksi, "SELECT a.NamaDesa, b.KodeLokasi FROM mstdesa a JOIN mstlokasi b ON b.KodeDesa = a.KodeDesa ORDER BY a.KodeDesa") or die(@mysqli_error($cari)); while($tampil = @mysqli_fetch_array($cari)){
				$loc = $tampil['KodeLokasi'];		

				// load data laporan
				// Lahir bulan ini
				$JmlLahirLK = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) = '".$key."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1'"); $LahirLK = mysqli_num_rows($JmlLahirLK);
				$JmlLahirPR = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) = '".$key."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1'"); $LahirPR = mysqli_num_rows($JmlLahirPR); 
											
				// Mati bulan ini
				$JmlMatiLK = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) = '".$key."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1'"); $MatiLK = mysqli_num_rows($JmlMatiLK); 
				$JmlMatiPR = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) = '".$key."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1'"); $MatiPR = mysqli_num_rows($JmlMatiPR); 
												
				// Datang bulan ini
				$JmlDtgLK = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '1' AND KodeLokasi = '".$tampil['KodeLokasi']."'"); $DtgLK = mysqli_num_rows($JmlDtgLK);
				$JmlDtgPR = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '2' AND KodeLokasi = '".$tampil['KodeLokasi']."'"); $DtgPR = mysqli_num_rows($JmlDtgPR); 
											
				// Pindah bulan ini
				// Trpindah laki
				$JmlKluLK = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $KluLK = mysqli_num_rows($JmlKluLK); 
												
					// anggota pindah laki
					$JmlKluLKag = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $KluLKag = mysqli_num_rows($JmlKluLKag);
											
				// Trpindah perempuan 											
				$JmlKluPR = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $KluPR = mysqli_num_rows($JmlKluPR); 
												
					// anggota pindah perempuan
					$JmlKluPRag = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $KluPRag = mysqli_num_rows($JmlKluPRag);
											
				// total laki
				$totalKluLK = $KluLK+$KluLKag; 
				// total perempuan
				$totalKluPR = $KluPR+$KluPRag;
											
				// total sebelumnya
				// Lahir 
				$_JmlLahirLK = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) < '".$key."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1'"); $_LahirLK = mysqli_num_rows($_JmlLahirLK);
				$_JmlLahirPR = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) < '".$key."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1'"); $_LahirPR = mysqli_num_rows($_JmlLahirPR); 
											
				// Mati 
				$_JmlMatiLK = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) < '".$key."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1'"); $_MatiLK = mysqli_num_rows($_JmlMatiLK); 
				$_JmlMatiPR = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) < '".$key."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1'"); $_MatiPR = mysqli_num_rows($_JmlMatiPR); 
												
				// Datang 
				$_JmlDtgLK = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '1' AND KodeLokasi = '".$tampil['KodeLokasi']."'"); $_DtgLK = mysqli_num_rows($_JmlDtgLK);
				$_JmlDtgPR = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '2' AND KodeLokasi = '".$tampil['KodeLokasi']."'"); $_DtgPR = mysqli_num_rows($_JmlDtgPR); 
											
				// Pindah 
				// Trpindah laki
				$_JmlKluLK = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $_KluLK = mysqli_num_rows($_JmlKluLK); 
												
					// anggota pindah laki
					$_JmlKluLKag = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '1' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $_KluLKag = mysqli_num_rows($_JmlKluLKag);
											
				// Trpindah perempuan 											
				$_JmlKluPR = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $_KluPR = mysqli_num_rows($_JmlKluPR); 
												
					// anggota pindah perempuan
					$_JmlKluPRag = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '2' AND a.KodeLokasi = '".$tampil['KodeLokasi']."' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $_KluPRag = mysqli_num_rows($_JmlKluPRag);
											
				// total laki
				$_totalKluLK = $_KluLK+$_KluLKag; 
				// total perempuan
				$_totalKluPR = $_KluPR+$_KluPRag;
				
				// Jumlah Awal Penduduk				
				$JmlAwalLK = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE JenisKelamin = '1' AND KodeLokasi = '".$tampil['KodeLokasi']."'"); $totAwalLK = mysqli_num_rows($JmlAwalLK); 
				$AwalLK = $totAwalLK+$_LahirLK-$_MatiLK+$_DtgLK-$_totalKluLK;
											
				$JmlAwalPR = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE JenisKelamin = '2' AND KodeLokasi = '".$tampil['KodeLokasi']."'"); $totAwalPR = mysqli_num_rows($JmlAwalPR);
				$AwalPR = $totAwalPR+$_LahirPR-$_MatiPR+$_DtgPR-$_totalKluPR;
				
				// end load
				
				$this->Cell(10,5,$No,1,0,'C',0);
				$this->Cell(55,5,$tampil['NamaDesa'],1,0,'L',0);
				$this->Cell(12.5,5,number_format($AwalLK),1,0,'R',0);
				$this->Cell(12.5,5,number_format($AwalPR),1,0,'R',0);
				$this->Cell(12.5,5,number_format($AwalLK+$AwalPR),1,0,'R',0);
				
				$this->Cell(12.5,5,number_format($LahirLK),1,0,'R',0);
				$this->Cell(12.5,5,number_format($LahirPR),1,0,'R',0);
				$this->Cell(12.5,5,number_format($LahirLK+$LahirPR),1,0,'R',0);
				
				$this->Cell(12.5,5,number_format($MatiLK),1,0,'R',0);
				$this->Cell(12.5,5,number_format($MatiPR),1,0,'R',0);
				$this->Cell(12.5,5,number_format($MatiLK+$MatiPR),1,0,'R',0);
				
				$this->Cell(12.5,5,number_format($DtgLK),1,0,'R',0);
				$this->Cell(12.5,5,number_format($DtgPR),1,0,'R',0);
				$this->Cell(12.5,5,number_format($DtgLK+$DtgPR),1,0,'R',0);
				
				$this->Cell(12.5,5,number_format($totalKluLK),1,0,'R',0);
				$this->Cell(12.5,5,number_format($totalKluPR),1,0,'R',0);
				$this->Cell(12.5,5,number_format($totalKluLK+$totalKluPR),1,0,'R',0);
				
				$this->SetFont('Times','B','8');
				$this->Cell(12.5,5,number_format($AwalLK+$LahirLK-$MatiLK+$DtgLK-$totalKluLK),1,0,'R',0);
				$this->Cell(12.5,5,number_format($AwalPR+$LahirPR-$MatiPR+$DtgPR-$totalKluPR),1,0,'R',0);
				$this->Cell(12.5,5,number_format(($AwalLK+$LahirLK-$MatiLK+$DtgLK-$totalKluLK) + ($AwalPR+$LahirPR-$MatiPR+$DtgPR-$totalKluPR)),1,1,'R',0);
				$this->SetFont('Times','','8');
				$No++;
			}
			
			$this->Cell(10,7.5,'',1,0,'C',0);
			$this->SetFont('Times','B','8');
				
				// load data laporan total
				// Lahir bulan ini
				$JmlLahirLK2 = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) = '".$key."' AND b.JenisKelamin = '1' AND a.IsAktif='1'"); $LahirLK2 = mysqli_num_rows($JmlLahirLK2);
				$JmlLahirPR2 = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) = '".$key."' AND b.JenisKelamin = '2' AND a.IsAktif='1'"); $LahirPR2 = mysqli_num_rows($JmlLahirPR2); 
											
				// Mati bulan ini
				$JmlMatiLK2 = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) = '".$key."' AND b.JenisKelamin = '1' AND a.IsAktif='1'"); $MatiLK2 = mysqli_num_rows($JmlMatiLK2); 
				$JmlMatiPR2 = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) = '".$key."' AND b.JenisKelamin = '2' AND a.IsAktif='1'"); $MatiPR2 = mysqli_num_rows($JmlMatiPR2); 
												
				// Datang bulan ini
				$JmlDtgLK2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '1'"); $DtgLK2 = mysqli_num_rows($JmlDtgLK2);
				$JmlDtgPR2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '2'"); $DtgPR2 = mysqli_num_rows($JmlDtgPR2); 
											
				// Pindah bulan ini
				$JmlKluLK2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '1'"); $KluLK2 = mysqli_num_rows($JmlKluLK2); $arrayKluLK2 = mysqli_fetch_array($JmlKluLK2); 
				$JmlKluLK22 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$arrayKluLK2['KodeMutasiKeluar']."' AND b.JenisKelamin = '1'"); $KluLK22 = mysqli_num_rows($JmlKluLK22);
				$totalKluLK2 = $KluLK2+$KluLK22; 
												
				$JmlKluPR2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '2'"); $KluPR2 = mysqli_num_rows($JmlKluPR2); $arrayKluPR2 = mysqli_fetch_array($JmlKluPR2); 
				$JmlKluPR22 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$arrayKluPR2['KodeMutasiKeluar']."' AND b.JenisKelamin = '2'"); $KluPR22 = mysqli_num_rows($JmlKluPR22);
				$totalKluPR2 = $KluPR2+$KluPR22; 
				
				// Pindah bulan ini
					// Trpindah laki
					$JmlKluLK2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '1' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $KluLK2 = mysqli_num_rows($JmlKluLK2); 
									
						// anggota pindah laki
						$JmlKluLKag2 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '1' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $KluLKag2 = mysqli_num_rows($JmlKluLKag2);
											
					// Trpindah perempuan 											
					$JmlKluPR2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '2' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $KluPR2 = mysqli_num_rows($JmlKluPR2); 
												
						// anggota pindah perempuan
						$JmlKluPRag2 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) = '".$key."' AND b.JenisKelamin = '2' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $KluPRag2 = mysqli_num_rows($JmlKluPRag2);
											
					// total laki
					$totalKluLK2 = $KluLK2+$KluLKag2; 
					// total perempuan
					$totalKluPR2 = $KluPR2+$KluPRag2;
											
				// total sebelumnya
				// Lahir 
				$_JmlLahirLK2 = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) < '".$key."' AND b.JenisKelamin = '1' AND a.IsAktif='1'"); $_LahirLK2 = mysqli_num_rows($_JmlLahirLK2);
				$_JmlLahirPR2 = mysqli_query($koneksi, "SELECT * FROM trkelahiran a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalLahir,7) < '".$key."' AND b.JenisKelamin = '2' AND a.IsAktif='1'"); $_LahirPR2 = mysqli_num_rows($_JmlLahirPR2); 
											
				// Mati 
				$_JmlMatiLK2 = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) < '".$key."' AND b.JenisKelamin = '1' AND a.IsAktif='1'"); $_MatiLK2 = mysqli_num_rows($_JmlMatiLK2); 
				$_JmlMatiPR2 = mysqli_query($koneksi, "SELECT * FROM trkematian a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalMeninggal,7) < '".$key."' AND b.JenisKelamin = '2' AND a.IsAktif='1'"); $_MatiPR2 = mysqli_num_rows($_JmlMatiPR2); 
												
				// Datang 
				$_JmlDtgLK2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '1'"); $_DtgLK2 = mysqli_num_rows($_JmlDtgLK2);
				$_JmlDtgPR2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE StatusPenduduk = 'PINDAH_MASUK' AND JenisKelamin = '2'"); $_DtgPR2 = mysqli_num_rows($_JmlDtgPR2); 
											
				// Pindah 
				$_JmlKluLK2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '1'"); $_KluLK2 = mysqli_num_rows($_JmlKluLK2); $_arrayKluLK2 = mysqli_fetch_array($_JmlKluLK2); 
				$_JmlKluLK22 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$_arrayKluLK2['KodeMutasiKeluar']."' AND b.JenisKelamin = '1'"); $_KluLK22 = mysqli_num_rows($_JmlKluLK22);
				$_totalKluLK2 = $_KluLK2+$_KluLK22; 
												
				$_JmlKluPR2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '2'"); $_KluPR2 = mysqli_num_rows($_JmlKluPR2); $_arrayKluPR2 = mysqli_fetch_array($_JmlKluPR2); 
				$_JmlKluPR22 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE KodeMutasiKeluar = '".$_arrayKluPR2['KodeMutasiKeluar']."' AND b.JenisKelamin = '2'"); $_KluPR22 = mysqli_num_rows($_JmlKluPR22);
				$_totalKluPR2 = $_KluPR2+$_KluPR22;
				
				// Pindah sebelumnya
					// Trpindah laki
					$_JmlKluLK2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '1' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $_KluLK2 = mysqli_num_rows($_JmlKluLK2); 
												
						// anggota pindah laki
						$_JmlKluLKag2 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '1' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $_KluLKag2 = mysqli_num_rows($_JmlKluLKag2);
											
					// Trpindah perempuan 											
					$_JmlKluPR2 = mysqli_query($koneksi, "SELECT * FROM mutasikeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '2' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $_KluPR2 = mysqli_num_rows($_JmlKluPR2); 
												
						// anggota pindah perempuan
						$_JmlKluPRag2 = mysqli_query($koneksi, "SELECT * FROM anggotakeluar a JOIN datapenduduk b ON (b.IDPend,b.KodeLokasi) = (a.IDPend,a.KodeLokasi) WHERE LEFT(a.TanggalPindah,7) < '".$key."' AND b.JenisKelamin = '2' AND a.IsAktif='1' AND b.StatusPenduduk = 'PINDAH_KELUAR'"); $_KluPRag2 = mysqli_num_rows($_JmlKluPRag2);
											
					// total laki
					$_totalKluLK2 = $_KluLK2+$_KluLKag2; 
					// total perempuan
					$_totalKluPR2 = $_KluPR2+$_KluPRag2;
											
				$JmlAwalLK2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE JenisKelamin = '1'"); $totAwalLK2 = mysqli_num_rows($JmlAwalLK2); 
				$AwalLK2 = $totAwalLK2+$_LahirLK2-$_MatiLK2+$_DtgLK2-$_totalKluLK2;
											
				$JmlAwalPR2 = mysqli_query($koneksi, "SELECT * FROM datapenduduk WHERE JenisKelamin = '2'"); $totAwalPR2 = mysqli_num_rows($JmlAwalPR2);
				$AwalPR2 = $totAwalPR2+$_LahirPR2-$_MatiPR2+$_DtgPR2-$_totalKluPR2;
				
				// end load
				
				$this->Cell(55,7.5,'JUMLAH',1,0,'L',0);
				$this->Cell(12.5,7.5,number_format($AwalLK2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($AwalPR2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($AwalLK2+$AwalPR2),1,0,'R',0);
				
				$this->Cell(12.5,7.5,number_format($LahirLK2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($LahirPR2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($LahirLK2+$LahirPR2),1,0,'R',0);
				
				$this->Cell(12.5,7.5,number_format($MatiLK2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($MatiPR2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($MatiLK2+$MatiPR2),1,0,'R',0);
				
				$this->Cell(12.5,7.5,number_format($DtgLK2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($DtgPR2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($DtgLK2+$DtgPR2),1,0,'R',0);
				
				$this->Cell(12.5,7.5,number_format($totalKluLK2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($totalKluPR2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($totalKluLK2+$totalKluPR2),1,0,'R',0);
				
				$this->Cell(12.5,7.5,number_format($AwalLK2+$LahirLK2-$MatiLK2+$DtgLK2-$totalKluLK2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format($AwalPR2+$LahirPR2-$MatiPR2+$DtgPR2-$totalKluPR2),1,0,'R',0);
				$this->Cell(12.5,7.5,number_format(($AwalLK2+$LahirLK2-$MatiLK2+$DtgLK2-$totalKluLK2) + ($AwalPR2+$LahirPR2-$MatiPR2+$DtgPR2-$totalKluPR2)),1,1,'R',0);
				
		} elseif($type === 'LaporanPBB') {
			$this->SetFont('Times','B','12'); $this->Ln(5); $this->Cell(290,0,'LAPORAN PERKEMBANGAN PBB TAHUN '.$tahun.' PER '.strtoupper($tanggal_indo),0,0,'C');
			$this->Ln(5); $this->SetFont('Times','B','9');
			$this->Cell(10,7,'','LTR',0,'L',0);
			$this->Cell(45,7,'','LTR',0,'L',0);
			$this->Cell(20,7,'','LTR',0,'L',0);
			$this->Cell(40,7,'','LTR',0,'L',0);
			$this->Cell(40,7,'','LT',0,'L',0);
			$this->Cell(40,7,'Setoran','TB',0,'C',0);
			$this->Cell(40,7,'','TR',0,'L',0);
			$this->Cell(40,7,'','LTR',0,'C',0);
			$this->Cell(15,7,'','LTR',1,'C',0);
			
			$this->Cell(10,7,'No','LBR',0,'C',0);
			$this->Cell(45,7,'Desa/Kelurahan','LBR',0,'C',0);
			$this->Cell(20,7,'SPPT','LBR',0,'C',0);
			$this->Cell(40,7,'Baku','LBR',0,'C',0);
			$this->Cell(40,7,'s.d. Minggu Lalu',1,0,'C',0);
			$this->Cell(40,7,'Minggu Ini',1,0,'C',0);
			$this->Cell(40,7,'s.d. Minggu Ini',1,0,'C',0);
			$this->Cell(40,7,'Sisa','LBR',0,'C',0);
			$this->Cell(15,7,'%','LBR',1,'C',0);
			
			$this->SetFont('Times','I','8');
			$this->Cell(10,4,'1','LBR',0,'C',0);
			$this->Cell(45,4,'2','LBR',0,'C',0);
			$this->Cell(20,4,'3','TRB',0,'C',0);
			$this->Cell(40,4,'4','TRB',0,'C',0);
			$this->Cell(40,4,'5','TRB',0,'C',0);
			$this->Cell(40,4,'6','TRB',0,'C',0);
			$this->Cell(40,4,'7','TRB',0,'C',0);
			$this->Cell(40,4,'8','TRB',0,'C',0);
			$this->Cell(15,4,'9','TRB',1,'C',0);
			
			$No = 1; $this->SetFont('Times','','8');
			$cari = @mysqli_query($koneksi, "SELECT a.NamaDesa, b.KodeLokasi FROM mstdesa a JOIN mstlokasi b ON b.KodeDesa = a.KodeDesa ORDER BY a.KodeDesa") or die(@mysqli_error($cari)); while($tampil = @mysqli_fetch_array($cari)){
				$loc = $tampil['KodeLokasi'];
				$Tahun = $tahun; 
				$HariIni = $key;
				$MingguLalu = date('Y-m-d', strtotime('-7 days', strtotime($HariIni)));

				// load data laporan
				// SPPT
				$SPPT = mysqli_query($koneksi, "SELECT COUNT(NoObjekPajak) as SPPT FROM masterobjekpajak WHERE Tahun = '".$Tahun."' AND KodeLokasi = '".$loc."'"); $JmlSPPT = mysqli_fetch_array($SPPT);
											
				// Baku
				$Baku = mysqli_query($koneksi, "SELECT SUM(Pokok) as Baku FROM masterobjekpajak WHERE Tahun = '".$Tahun."' AND KodeLokasi = '".$loc."'"); $JmlBaku = mysqli_fetch_array($Baku); $BakuPajak = $JmlBaku['Baku'];
											
				// Setor sd Minggu Lalu
				$SdMinLalu = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SdMinLalu FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (b.NoObjekPajak,b.KodeLokasi,b.Tahun) = (a.NoObjekPajak,a.KodeLokasi,a.Tahun) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$loc."' AND b.IsBayar = '1' AND b.Tanggal <= '".$MingguLalu."'"); 
				$JmlSdMinLalu = mysqli_fetch_array($SdMinLalu); $SdMinLalu2 = $JmlSdMinLalu['SdMinLalu'];
											
				// Setor selama Minggu ini
				$SetorMinIni = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SetorMinIni FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (b.NoObjekPajak,b.KodeLokasi,b.Tahun) = (a.NoObjekPajak,a.KodeLokasi,a.Tahun) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$loc."' AND b.IsBayar = '1' AND b.Tanggal > '".$MingguLalu."' AND b.Tanggal <= '".$HariIni."'"); 
				$JmlSetorMinIni = mysqli_fetch_array($SetorMinIni); $SetorMinIni2 = $JmlSetorMinIni['SetorMinIni'];
											
				// Setor sd Minggu ini
				$SdMinIni = $SdMinLalu2+$SetorMinIni2;
				// end load
				
				$this->Cell(10,5,$No,1,0,'C',0);
				$this->Cell(45,5,$tampil['NamaDesa'],1,0,'L',0);
				$this->Cell(20,5,number_format($JmlSPPT['SPPT']),1,0,'R',0);
				$this->Cell(40,5,number_format($BakuPajak),1,0,'R',0);
				$this->Cell(40,5,number_format($SdMinLalu2),1,0,'R',0);
				$this->Cell(40,5,number_format($SetorMinIni2),1,0,'R',0);
				$this->Cell(40,5,number_format($SdMinIni),1,0,'R',0);
				$this->SetFont('Times','B','8');
				$this->Cell(40,5,number_format($BakuPajak-$SdMinIni),1,0,'R',0);
				if($BakuPajak == 0){
					$this->Cell(15,5,number_format(0,2),1,1,'R',0);
				} else {
					$this->Cell(15,5,number_format(($SdMinIni/$BakuPajak)*100,2),1,1,'R',0); 
				}
				$this->SetFont('Times','','8');
				$No++;
			}
			
			$this->Cell(10,7,'',1,0,'C',0);
			$this->SetFont('Times','B','8');
			
			// load data laporan total
			// SPPT
			$SPPT2 = mysqli_query($koneksi, "SELECT COUNT(NoObjekPajak) as SPPT2 FROM masterobjekpajak WHERE Tahun = '".$Tahun."'"); $JmlSPPT2 = mysqli_fetch_array($SPPT2);
											
			// Baku
			$Baku2 = mysqli_query($koneksi, "SELECT SUM(Pokok) as Baku2 FROM masterobjekpajak WHERE Tahun = '".$Tahun."'"); $JmlBaku2 = mysqli_fetch_array($Baku2); $BakuPajak2 = $JmlBaku2['Baku2'];
											
			// Setor sd Minggu Lalu
			$SdMinLalu2 = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SdMinLalu2 FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (b.NoObjekPajak,b.Tahun) = (a.NoObjekPajak,a.Tahun) WHERE a.Tahun = '".$Tahun."' AND b.IsBayar = '1' AND b.Tanggal <= '".$MingguLalu."'"); $JmlSdMinLalu2 = mysqli_fetch_array($SdMinLalu2); $SdMinLalu22 = $JmlSdMinLalu2['SdMinLalu2'];
											
			// Setor selama Minggu ini
			$SetorMinIni2 = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SetorMinIni2 FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (b.NoObjekPajak,b.Tahun) = (a.NoObjekPajak,a.Tahun) WHERE a.Tahun = '".$Tahun."' AND b.IsBayar = '1' AND b.Tanggal > '".$MingguLalu."' AND b.Tanggal <= '".$HariIni."'"); 
			$JmlSetorMinIni2 = mysqli_fetch_array($SetorMinIni2); $SetorMinIni22 = $JmlSetorMinIni2['SetorMinIni2'];
											
			// Setor sd Minggu ini
			$SdMinIni22 = $SdMinLalu22+$SetorMinIni22;
			// end load
										
				$this->Cell(45,7,'TOTAL KESELURUHAN',1,0,'L',0);
				$this->Cell(20,7,number_format($JmlSPPT2['SPPT2']),1,0,'R',0);
				$this->Cell(40,7,number_format($BakuPajak2),1,0,'R',0);
				$this->Cell(40,7,number_format($SdMinLalu22),1,0,'R',0);
				$this->Cell(40,7,number_format($SetorMinIni22),1,0,'R',0);
				$this->Cell(40,7,number_format($SdMinIni22),1,0,'R',0);
				$this->SetFont('Times','B','8');
				$this->Cell(40,7,number_format($BakuPajak2-$SdMinIni22),1,0,'R',0);
				if($BakuPajak2 == 0){
					$this->Cell(15,7,number_format(0,2),1,1,'R',0);
				} else {
					$this->Cell(15,7,number_format(($SdMinIni22/$BakuPajak2)*100,2),1,1,'R',0); 
				}
				$this->SetFont('Times','','8');
				
		} elseif($type === 'LaporanPBBDesa' OR $type === 'LaporanPBBDetilDesa') {
			$this->SetFont('Times','B','12'); $this->Ln(5); $this->Cell(290,0,'LAPORAN PERKEMBANGAN PBB TAHUN '.$tahun.' PER '.strtoupper($tanggal_indo),0,0,'C');
			$cari = @mysqli_query($koneksi, "Select mstdesa.NamaDesa, mstlokasi.AlamatLokasi, mstlokasi.IsKelurahan from mstdesa inner join mstlokasi on mstlokasi.KodeDesa = mstdesa.KodeDesa WHERE mstlokasi.KodeLokasi='".$lokasi."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $namadesa = $tampil['NamaDesa']; $alamat = $tampil['AlamatLokasi']; $is_lurah = $tampil['IsKelurahan']; }
			if($is_lurah === '1'){
				$this->Ln(5); $this->Cell(290,0,'KELURAHAN '.$namadesa,0,0,'C');
			} else {
				$this->Ln(5); $this->Cell(290,0,'DESA '.$namadesa,0,0,'C');
			}
			$this->Ln(5); $this->SetFont('Times','B','9');
			$this->Cell(10,7,'','LTR',0,'L',0);
			$this->Cell(45,7,'','LTR',0,'L',0);
			$this->Cell(20,7,'','LTR',0,'L',0);
			$this->Cell(40,7,'','LTR',0,'L',0);
			$this->Cell(40,7,'','LT',0,'L',0);
			$this->Cell(40,7,'Setoran','TB',0,'C',0);
			$this->Cell(40,7,'','TR',0,'L',0);
			$this->Cell(40,7,'','LTR',0,'C',0);
			$this->Cell(15,7,'','LTR',1,'C',0);
			
			$this->Cell(10,7,'No','LBR',0,'C',0);
			$this->Cell(45,7,'Nama Petugas','LBR',0,'C',0);
			$this->Cell(20,7,'SPPT','LBR',0,'C',0);
			$this->Cell(40,7,'Baku','LBR',0,'C',0);
			$this->Cell(40,7,'s.d. Minggu Lalu',1,0,'C',0);
			$this->Cell(40,7,'Minggu Ini',1,0,'C',0);
			$this->Cell(40,7,'s.d. Minggu Ini',1,0,'C',0);
			$this->Cell(40,7,'Sisa','LBR',0,'C',0);
			$this->Cell(15,7,'%','LBR',1,'C',0);
			
			$this->SetFont('Times','I','8');
			$this->Cell(10,4,'1','LBR',0,'C',0);
			$this->Cell(45,4,'2','LBR',0,'C',0);
			$this->Cell(20,4,'3','TRB',0,'C',0);
			$this->Cell(40,4,'4','TRB',0,'C',0);
			$this->Cell(40,4,'5','TRB',0,'C',0);
			$this->Cell(40,4,'6','TRB',0,'C',0);
			$this->Cell(40,4,'7','TRB',0,'C',0);
			$this->Cell(40,4,'8','TRB',0,'C',0);
			$this->Cell(15,4,'9','TRB',1,'C',0);
			
			$No = 1; $this->SetFont('Times','','8');
			$cari = @mysqli_query($koneksi, "SELECT a.UserName, a.ActualName, b.KodeLokasi FROM userlogin a JOIN mstlokasi b ON b.KodeLokasi = a.KodeLokasi JOIN trbayarpajak c ON c.UserName = a.UserName WHERE a.KodeLokasi = '$lokasi' GROUP BY a.UserName ORDER BY a.UserName") or die(@mysqli_error($cari)); while($tampil = @mysqli_fetch_array($cari)){
				$loc = $tampil['KodeLokasi']; $nama = $tampil['ActualName']; $user = $tampil['UserName'];
				$Tahun = $tahun; 
				$HariIni = $key;
				$MingguLalu = date('Y-m-d', strtotime('-7 days', strtotime($HariIni)));

				// load data laporan
				// SPPT
				$SPPT = mysqli_query($koneksi, "SELECT COUNT(a.NoObjekPajak) as SPPT FROM masterobjekpajak a JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$loc."' AND b.UserName='".$user."'"); $JmlSPPT = mysqli_fetch_array($SPPT);
											
				// Baku
				$Baku = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as Baku FROM masterobjekpajak a JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$loc."' AND b.UserName='".$user."'"); $JmlBaku = mysqli_fetch_array($Baku); $BakuPajak = $JmlBaku['Baku'];
											
				// Setor sd Minggu Lalu
				$SdMinLalu = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SdMinLalu FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$loc."' AND b.UserName='".$user."' AND b.IsBayar = '1' AND b.Tanggal <= '".$MingguLalu."'"); 
				$JmlSdMinLalu = mysqli_fetch_array($SdMinLalu); $SdMinLalu2 = $JmlSdMinLalu['SdMinLalu'];
											
				// Setor selama Minggu ini
				$SetorMinIni = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SetorMinIni FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$loc."' AND b.UserName='".$user."' AND b.IsBayar = '1' AND b.Tanggal > '".$MingguLalu."' AND b.Tanggal <= '".$HariIni."'"); 
				$JmlSetorMinIni = mysqli_fetch_array($SetorMinIni); $SetorMinIni2 = $JmlSetorMinIni['SetorMinIni'];
											
				// Setor sd Minggu ini
				$SdMinIni = $SdMinLalu2+$SetorMinIni2;
				// end load
				
				$this->Cell(10,5,$No,1,0,'C',0);
				$this->Cell(45,5,ucwords($nama),1,0,'L',0);
				$this->Cell(20,5,number_format($JmlSPPT['SPPT']),1,0,'R',0);
				$this->Cell(40,5,number_format($BakuPajak),1,0,'R',0);
				$this->Cell(40,5,number_format($SdMinLalu2),1,0,'R',0);
				$this->Cell(40,5,number_format($SetorMinIni2),1,0,'R',0);
				$this->Cell(40,5,number_format($SdMinIni),1,0,'R',0);
				$this->SetFont('Times','B','8');
				$this->Cell(40,5,number_format($BakuPajak-$SdMinIni),1,0,'R',0);
				if($BakuPajak == 0){
					$this->Cell(15,5,number_format(0,2),1,1,'R',0);
				} else {
					$this->Cell(15,5,number_format(($SdMinIni/$BakuPajak)*100,2),1,1,'R',0); 
				}
				$this->SetFont('Times','','8');
				$No++;
			}
			
			$this->Cell(10,5,$No,1,0,'C',0);
			$this->SetFont('Times','','8');
			
			// load data yang belum distribusi
			// SPPT
			$SPPT_blm = mysqli_query($koneksi, "SELECT COUNT(a.NoObjekPajak) as SPPT_blm FROM masterobjekpajak a JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$lokasi."' AND (b.UserName is null OR b.UserName='')"); $JmlSPPT_blm = mysqli_fetch_array($SPPT_blm);
											
			// Baku
			$Baku_blm = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as Baku_blm FROM masterobjekpajak a JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$lokasi."' AND (b.UserName is null OR b.UserName='')"); $JmlBaku_blm = mysqli_fetch_array($Baku_blm); $BakuPajak_blm = $JmlBaku_blm['Baku_blm'];
											
			// Setor sd Minggu Lalu
			$SdMinLalu_blm = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SdMinLalu_blm FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$lokasi."' AND (b.UserName is null OR b.UserName='') AND b.IsBayar = '1' AND b.Tanggal <= '".$MingguLalu."'"); 
			$JmlSdMinLalu_blm = mysqli_fetch_array($SdMinLalu_blm); $SdMinLalu2_blm = $JmlSdMinLalu_blm['SdMinLalu_blm'];
											
			// Setor selama Minggu ini
			$SetorMinIni_blm = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SetorMinIni_blm FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$lokasi."' AND (b.UserName is null OR b.UserName='') AND b.IsBayar = '1' AND b.Tanggal > '".$MingguLalu."' AND b.Tanggal <= '".$HariIni."'"); 
			$JmlSetorMinIni_blm = mysqli_fetch_array($SetorMinIni_blm); $SetorMinIni2_blm = $JmlSetorMinIni_blm['SetorMinIni_blm'];
											
			// Setor sd Minggu ini
			$SdMinIni_blm = $SdMinLalu2_blm+$SetorMinIni2_blm;
			// end load
			
				$this->Cell(45,5,'Belum Didistribusikan',1,0,'L',0);
				$this->Cell(20,5,number_format($JmlSPPT_blm['SPPT_blm']),1,0,'R',0);
				$this->Cell(40,5,number_format($BakuPajak_blm),1,0,'R',0);
				$this->Cell(40,5,number_format($SdMinLalu2_blm),1,0,'R',0);
				$this->Cell(40,5,number_format($SetorMinIni2_blm),1,0,'R',0);
				$this->Cell(40,5,number_format($SdMinIni_blm),1,0,'R',0);
				$this->SetFont('Times','B','8');
				$this->Cell(40,5,number_format($BakuPajak_blm-$SdMinIni_blm),1,0,'R',0);
				if($BakuPajak_blm == 0){
					$this->Cell(15,5,number_format(0,2),1,1,'R',0);
				} else {
					$this->Cell(15,5,number_format(($SdMinIni_blm/$BakuPajak_blm)*100,2),1,1,'R',0); 
				}
				
			$this->Cell(10,7,'',1,0,'C',0);
			$this->SetFont('Times','B','8');
			
			// load data laporan total
			// SPPT
			$SPPT2 = mysqli_query($koneksi, "SELECT COUNT(a.NoObjekPajak) as SPPT2 FROM masterobjekpajak a JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$lokasi."'"); $JmlSPPT2 = mysqli_fetch_array($SPPT2);
											
			// Baku
			$Baku2 = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as Baku2 FROM masterobjekpajak a JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$lokasi."'"); $JmlBaku2 = mysqli_fetch_array($Baku2); $BakuPajak2 = $JmlBaku2['Baku2'];
											
			// Setor sd Minggu Lalu
			$SdMinLalu2 = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SdMinLalu2 FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$lokasi."' AND b.IsBayar = '1' AND b.Tanggal <= '".$MingguLalu."'"); $JmlSdMinLalu2 = mysqli_fetch_array($SdMinLalu2); $SdMinLalu22 = $JmlSdMinLalu2['SdMinLalu2'];
											
			// Setor selama Minggu ini
			$SetorMinIni2 = mysqli_query($koneksi, "SELECT SUM(a.Pokok) as SetorMinIni2 FROM masterobjekpajak a LEFT JOIN trbayarpajak b ON (a.NoObjekPajak,a.Tahun,a.KodeLokasi)=(b.NoObjekPajak,b.Tahun,b.KodeLokasi) WHERE a.Tahun = '".$Tahun."' AND a.KodeLokasi = '".$lokasi."' AND b.IsBayar = '1' AND b.Tanggal > '".$MingguLalu."' AND b.Tanggal <= '".$HariIni."'"); 
			$JmlSetorMinIni2 = mysqli_fetch_array($SetorMinIni2); $SetorMinIni22 = $JmlSetorMinIni2['SetorMinIni2'];
											
			// Setor sd Minggu ini
			$SdMinIni22 = $SdMinLalu22+$SetorMinIni22;
			// end load
										
				$this->Cell(45,7,'TOTAL KESELURUHAN',1,0,'L',0);
				$this->Cell(20,7,number_format($JmlSPPT2['SPPT2']),1,0,'R',0);
				$this->Cell(40,7,number_format($BakuPajak2),1,0,'R',0);
				$this->Cell(40,7,number_format($SdMinLalu22),1,0,'R',0);
				$this->Cell(40,7,number_format($SetorMinIni22),1,0,'R',0);
				$this->Cell(40,7,number_format($SdMinIni22),1,0,'R',0);
				$this->SetFont('Times','B','8');
				$this->Cell(40,7,number_format($BakuPajak2-$SdMinIni22),1,0,'R',0);
				if($BakuPajak2 == 0){
					$this->Cell(15,7,number_format(0,2),1,1,'R',0);
				} else {
					$this->Cell(15,7,number_format(($SdMinIni22/$BakuPajak2)*100,2),1,1,'R',0); 
				}
				$this->SetFont('Times','','8'); 
		} elseif($type === 'LaporanPBBDetilPetugas') {
			$this->SetFont('Times','B','12'); $this->Ln(5); $this->Cell(290,0,'LAPORAN PERKEMBANGAN PBB TAHUN '.$tahun.' PER '.strtoupper($tanggal_indo),0,0,'C');
			$cari = @mysqli_query($koneksi, "Select mstdesa.NamaDesa, mstlokasi.AlamatLokasi, mstlokasi.IsKelurahan from mstdesa inner join mstlokasi on mstlokasi.KodeDesa = mstdesa.KodeDesa WHERE mstlokasi.KodeLokasi='".$lokasi."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $namadesa = $tampil['NamaDesa']; $alamat = $tampil['AlamatLokasi']; $is_lurah = $tampil['IsKelurahan']; }
			if($is_lurah === '1'){
				$this->Ln(5); $this->Cell(290,0,'KELURAHAN '.$namadesa,0,0,'C');
			} else {
				$this->Ln(5); $this->Cell(290,0,'DESA '.$namadesa,0,0,'C');
			}
			$caridata = mysqli_query($koneksi, "SELECT ActualName FROM userlogin WHERE UserName='".base64_decode(@$_GET['user'])."' AND KodeLokasi='".base64_decode(@$_GET['lokasi'])."'");
			$array = mysqli_fetch_array($caridata);
			$this->SetFont('Times','','11'); $this->Ln(5); $this->Cell(290,0,'NAMA PETUGAS : '.strtoupper($array['ActualName']),0,0,'C');
			$this->Ln(5); $this->SetFont('Times','B','9');
			$this->Cell(10,7,'No',1,0,'C',0);
			$this->Cell(15,7,'Tahun',1,0,'C',0);
			$this->Cell(45,7,'NOP',1,0,'C',0);
			$this->Cell(50,7,'Nama WP',1,0,'C',0);
			$this->Cell(60,7,'Lokasi',1,0,'C',0);
			$this->Cell(40,7,'Pokok',1,0,'C',0);
			$this->Cell(70,7,'Keterangan',1,1,'C',0);
			
			$this->SetFont('Times','I','8');
			$this->Cell(10,4,'1','LBR',0,'C',0);
			$this->Cell(15,4,'2','LBR',0,'C',0);
			$this->Cell(45,4,'3','LBR',0,'C',0);
			$this->Cell(50,4,'4','TRB',0,'C',0);
			$this->Cell(60,4,'5','TRB',0,'C',0);
			$this->Cell(40,4,'6','TRB',0,'C',0);
			$this->Cell(70,4,'7','TRB',1,'C',0);
			
			$No = 1; $this->SetFont('Times','','8');
			$cari = @mysqli_query($koneksi, "SELECT * FROM masterobjekpajak a JOIN mstlokasi b ON b.KodeLokasi=a.KodeLokasi JOIN trbayarpajak c ON (c.NoObjekPajak,c.Tahun)=(a.NoObjekPajak,a.Tahun) WHERE c.UserName = '".$user."' AND a.Tahun = '".$tahun."' AND a.KodeLokasi ='".$lokasi."' ORDER BY a.NoObjekPajak") or die(@mysqli_error($cari)); while($tampil = @mysqli_fetch_array($cari)){
				
				$this->Cell(10,5,$No,1,0,'C',0);
				$this->Cell(15,5,$tampil['Tahun'],1,0,'C',0);
				$this->Cell(45,5,ucwords($tampil['NoObjekPajak']),1,0,'L',0);
				$this->Cell(50,5,ucwords($tampil['NamaWP']),1,0,'L',0);
				$this->Cell(60,5,ucwords($tampil['AlamatOP']),1,0,'L',0);
				$this->Cell(40,5,number_format($tampil['Pokok']),1,0,'R',0);
				
				$tanggalbaru = date("Y-m-d", strtotime($tampil['Tanggal']));
				if($tanggalbaru <= $key){
					if($tampil['IsBayar'] === '1'){ 
						if($tampil['Tanggal'] == null){
							$this->Cell(70,5,'Terbayar',1,1,'L',0);
						} else {
							$this->Cell(70,5,'Terbayar Pada '.TanggalIndo($tampil['Tanggal']),1,1,'L',0);
						}
					} else { 
						$this->Cell(70,5,'Belum Terbayar',1,1,'L',0);
					} 
				} else {
					$this->Cell(70,5,'Belum Terbayar',1,1,'L',0);
				} 
				
				$this->SetFont('Times','','8');
				$No++;
			}
			
			$this->SetFont('Times','B','8');
			// total bayar
				$cari1 = @mysqli_query($koneksi, "SELECT SUM(a.Pokok) as TotalBayar FROM masterobjekpajak a JOIN mstlokasi b ON b.KodeLokasi=a.KodeLokasi JOIN trbayarpajak c ON (c.NoObjekPajak,c.Tahun)=(a.NoObjekPajak,a.Tahun) WHERE c.UserName = '".$user."' AND a.Tahun = '".$tahun."' AND a.KodeLokasi ='".$lokasi."' AND c.IsBayar='1' AND c.Tanggal <= '".$key."' ORDER BY a.NoObjekPajak") or die(mysqli_error($cari1)); 
				$caridata1 = mysqli_fetch_array($cari1);
				$this->Cell(220,7,'TOTAL TERBAYAR',1,0,'R',0);
				$this->Cell(70,7,number_format($caridata1['TotalBayar']),1,1,'R',0);
			// total belum
				$cari2 = @mysqli_query($koneksi, "SELECT SUM(a.Pokok) as TotalBayar2 FROM masterobjekpajak a JOIN mstlokasi b ON b.KodeLokasi=a.KodeLokasi JOIN trbayarpajak c ON (c.NoObjekPajak,c.Tahun)=(a.NoObjekPajak,a.Tahun) WHERE c.UserName = '".$user."' AND a.Tahun = '".$tahun."' AND a.KodeLokasi ='".$lokasi."' ORDER BY a.NoObjekPajak") or die(mysqli_error($cari2)); 
				$caridata2 = mysqli_fetch_array($cari2);
				$this->Cell(220,7,'TOTAL BELUM TERBAYAR',1,0,'R',0);
				$this->Cell(70,7,number_format($caridata2['TotalBayar2']-$caridata1['TotalBayar']),1,1,'R',0);
			// total 
				$this->Cell(220,7,'TOTAL KESELURUHAN',1,0,'R',0);
				$this->Cell(70,7,number_format($caridata2['TotalBayar2']),1,1,'R',0);
				
				$this->SetFont('Times','','8');
			
		}
		
		/* if($type === 'LaporanPBBDesa' OR $type === 'LaporanPBBDetilDesa') {
			$this->Ln(5); $this->SetFont('Times','','11');
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, a.PangkatGol, c.IsKelurahan from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan join mstlokasi c on c.KodeLokasi = a.KodeLokasi where b.NamaJabatan = 'KEPALA DESA' AND a.KodeLokasi = '".$lokasi."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $desa = $arrayAparat['NamaAparat']; $gol = $arrayAparat['PangkatGol']; $nip = $arrayAparat['NIP']; $is_lurah = $arrayAparat['IsKelurahan']; }
				$this->Cell(225,7.5,"",0,0,'L',0); $this->Cell(100,5,"Nganjuk, ".TanggalIndo(date("Y-m-d")),0,1,'L',0);
				if($is_lurah === '1'){
					$this->Cell(225,7.5,"",0,0,'L',0); $this->Cell(100,5,"Lurah ".$namadesa,0,1,'L',0);
				} else {
					$this->Cell(225,7.5,"",0,0,'L',0); $this->Cell(100,5,"Kepala Desa ".$namadesa,0,1,'L',0);
				}
				$this->Ln(15);	
				$this->SetFont('Times','BU','11');
				$this->Cell(225,5,"",0,0,'L',0); $this->Cell(100,5,@$desa,0,1,'L',0);
				$this->SetFont('Times','','11');
				$this->Ln(-1); $this->Cell(225,5,"",0,0,'L',0); $this->Cell(100,5,"NIP : ".@$nip,0,1,'L',0);
		} elseif($type === 'LaporanPBB' OR $type === 'LaporanPenduduk') { 
			$this->Ln(5); $this->SetFont('Times','','11');
			$this->Cell(225,7.5,"",0,0,'L',0); $this->Cell(100,5,"Nganjuk, ".TanggalIndo(date("Y-m-d")),0,1,'L',0);
			$this->Cell(225,7.5,"",0,0,'L',0); $this->Cell(100,5,"Camat Nganjuk",0,1,'L',0);
			$this->Ln(15);	
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, a.PangkatGol from mstaparat a join mstjabatan b on b.KodeJabatan = a.KodeJabatan where b.NamaJabatan = 'CAMAT' AND a.KodeLokasi = '001-000'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ $camat = $arrayAparat['NamaAparat']; $gol = $arrayAparat['PangkatGol']; $nip = $arrayAparat['NIP']; }
				$this->SetFont('Times','BU','11');
				$this->Cell(225,5,"",0,0,'L',0); $this->Cell(100,5,@$camat,0,1,'L',0);
				$this->SetFont('Times','','11');
				$this->Ln(-1); $this->Cell(225,5,"",0,0,'L',0); $this->Cell(100,5,@$gol,0,1,'L',0);
				$this->Ln(-1); $this->Cell(225,5,"",0,0,'L',0); $this->Cell(100,5,"NIP : ".@$nip,0,1,'L',0);
		} */
		
			// Penandatanganan
			$this->Ln(5); $this->SetFont('Times','','11');
			$this->Cell(225,7.5,"",0,0,'L',0); $this->Cell(100,5,"Nganjuk, ".TanggalIndo(date("Y-m-d")),0,1,'L',0);
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, a.PangkatGol, b.NamaJabatan from mstaparat a join mstjabatan b on (b.KodeJabatan,b.KodeLokasi) = (a.KodeJabatan,a.KodeLokasi) where b.KodeLokasi = '001-000' AND b.KodeJabatan ='".$bagiTTD[0]."' AND a.KodeAparat='".$bagiTTD[1]."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ 
				$camat = $arrayAparat['NamaAparat']; $gol = $arrayAparat['PangkatGol']; $nip2 = $arrayAparat['NIP']; $jabat = $arrayAparat['NamaJabatan']; 
			}
			$this->Cell(225,7.5,"",0,0,'L',0); $this->Cell(100,5,@$jabat,0,1,'L',0);
			$this->Ln(15);	
				$this->SetFont('Times','BU','11');
				$this->Cell(225,5,"",0,0,'L',0); $this->Cell(100,5,@$camat,0,1,'L',0);
				$this->SetFont('Times','','11');
				$this->Ln(-1); $this->Cell(225,5,"",0,0,'L',0); $this->Cell(100,5,@$gol,0,1,'L',0);
				$this->Ln(-1); $this->Cell(225,5,"",0,0,'L',0); $this->Cell(100,5,"NIP : ".@$nip2,0,1,'L',0);
	}
	
	public function printPDF () {
		$this->SetAutoPageBreak(false);
	    $this->AliasNbPages();
	    $this->SetFont("Times", "B", 10);
	    //$this->AddPage();
 
	    $this->rptDetailData();
		$this->Output($this->options['filename'],$this->options['destinationfile']);
		
  	}
 
  	private $widths;
	private $aligns;
	
	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}
 
	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}
 
	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=7.5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			// $this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,7.5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	function Row2($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			// $this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	function Table($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
 
	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}
 
	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
	
	function MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false, $indent=0)
	{
    //Output text with automatic or explicit line breaks
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;

    $wFirst = $w-$indent;
    $wOther = $w;

    $wmaxFirst=($wFirst-2*$this->cMargin)*1000/$this->FontSize;
    $wmaxOther=($wOther-2*$this->cMargin)*1000/$this->FontSize;

    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 && $s[$nb-1]=="\n")
        $nb--;
    $b=0;
    if($border)
    {
        if($border==1)
        {
            $border='LTRB';
            $b='LRT';
            $b2='LR';
        }
        else
        {
            $b2='';
            if(is_int(strpos($border,'L')))
                $b2.='L';
            if(is_int(strpos($border,'R')))
                $b2.='R';
            $b=is_int(strpos($border,'T')) ? $b2.'T' : $b2;
        }
    }
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $ns=0;
    $nl=1;
        $first=true;
    while($i<$nb)
    {
        //Get next character
        $c=$s[$i];
        if($c=="\n")
        {
            //Explicit line break
            if($this->ws>0)
            {
                $this->ws=0;
                $this->_out('0 Tw');
            }
            $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $ns=0;
            $nl++;
            if($border && $nl==2)
                $b=$b2;
            continue;
        }
        if($c==' ')
        {
            $sep=$i;
            $ls=$l;
            $ns++;
        }
        $l+=$cw[$c];

        if ($first)
        {
            $wmax = $wmaxFirst;
            $w = $wFirst;
        }
        else
        {
            $wmax = $wmaxOther;
            $w = $wOther;
        }

        if($l>$wmax)
        {
            //Automatic line break
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
                if($this->ws>0)
                {
                    $this->ws=0;
                    $this->_out('0 Tw');
                }
                $SaveX = $this->x; 
                if ($first && $indent>0)
                {
                    $this->SetX($this->x + $indent);
                    $first=false;
                }
                $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                    $this->SetX($SaveX);
            }
            else
            {
                if($align=='J')
                {
                    $this->ws=($ns>1) ? ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
                    $this->_out(sprintf('%.3f Tw',$this->ws*$this->k));
                }
                $SaveX = $this->x; 
                if ($first && $indent>0)
                {
                    $this->SetX($this->x + $indent);
                    $first=false;
                }
                $this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
                    $this->SetX($SaveX);
                $i=$sep+1;
            }
            $sep=-1;
            $j=$i;
            $l=0;
            $ns=0;
            $nl++;
            if($border && $nl==2)
                $b=$b2;
        }
        else
            $i++;
    }
    //Last chunk
    if($this->ws>0)
    {
        $this->ws=0;
        $this->_out('0 Tw');
    }
    if($border && is_int(strpos($border,'B')))
        $b.='B';
    $this->Cell($w,$h,substr($s,$j,$i),$b,2,$align,$fill);
    $this->x=$this->lMargin;
    }
} //end of class
 
$tabel = new FPDF_AutoWrapTable(@$data, @$options);
$tabel->printPDF();

?>

<script type="text/javascript">
	window.history.forward();
	function noBack() { window.history.forward(); }
</script>