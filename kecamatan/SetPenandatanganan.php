<?php
include 'akses.php';
$fitur_id = 27;
include '../library/lock-menu.php';

$Page = 'Setting';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM mstlokasi WHERE KodeLokasi='".base64_decode($_GET['id'])."'");
	$RowData = mysqli_fetch_assoc($Edit);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Setting Penandatanganan Surat</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<div class="card">
					
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Aparat Penandatanganan Surat</h3>
							</div>							
							
							<div class="card-body">
								<div class="row">
								  <div class="col-lg-6">
									  <?php $tampil = mysqli_query($koneksi,"SELECT * FROM mstlokasi WHERE KodeLokasi='".$login_lokasi."'");
									  while($row = mysqli_fetch_array($tampil)){ $data = $row['Penandatanganan']; $bagi = explode("#", $data); }
									  ?>
									  <form method="post" action="">
										<div class="form-group-material">
											<select class="form-control" id="jabat" name="_jabat" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Jabatan --</option>';
													$list = mysqli_query($koneksi, "SELECT * FROM mstjabatan WHERE KodeLokasi='".$login_lokasi."' ORDER BY NamaJabatan ASC"); 
													while($daftar = mysqli_fetch_array($list)){
														if($data == null OR $data === ""){
															echo "<option value=\"".$daftar['KodeJabatan']."\" >".$daftar['NamaJabatan']."</option>\n";
														}else{
															if($daftar['KodeJabatan'] === $bagi[0]){
																echo "<option value=\"".$daftar['KodeJabatan']."\" selected='selected'>".$daftar['NamaJabatan']."</option>\n";
															}else{
																echo "<option value=\"".$daftar['KodeJabatan']."\" >".$daftar['NamaJabatan']."</option>\n";
															}
														}
													}
												?>
                                            </select>
										</div>
										<div class="form-group-material">
											<select class="form-control" id="aparat" name="_aparat" autocomplete="off" required>
                                                <?php echo '<option value="">-- Pilih Aparat --</option>';
												if($data != null OR $data !== ""){
													$list = mysqli_query($koneksi, "SELECT * FROM mstaparat WHERE KodeLokasi='".$login_lokasi."' AND KodeAparat='".$bagi[1]."' AND IsAktif=b'1' ORDER BY NamaAparat ASC"); 
													while($daftar = mysqli_fetch_array($list)){
														if($daftar['KodeAparat'] === $bagi[1]){
															echo "<option value=\"".$daftar['KodeAparat']."\" selected='selected'>".$daftar['NamaAparat']."</option>\n";
														}
													}
												}
												?>
                                            </select>
										</div>
										
										<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button>
									  </form>
								   </div>
								</div>
							</div>
						
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	
	<!-- membuat dropdown bertingkat tab I -->
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		  $("#jabat").change(function(){
			var KodeJabat = $("#jabat option:selected").val();
			var KodeLokasi = "<?php echo $login_lokasi;?>";
			$.ajax({ url: "../library/ambil-aparat.php", data: "jabat="+KodeJabat+"&lokasi="+KodeLokasi, cache: false, success: function(msg){ $("#aparat").html(msg); } });
		  });
		});
	</script>
	
	<?php
	if(isset($_POST['Simpan'])){
		/* include ('../library/kode-log-server.php'); */

		//cek apakah kode lokasi desa sudah ada 
		$cek2 = mysqli_query($koneksi,"select KodeLokasi from mstlokasi where KodeKec='$login_kec' AND KodeDesa='".$_POST['KodeDesa']."'");
		$num2 = mysqli_num_rows($cek2);
		if($num2 == 1 ){
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Simpan Data Gagal!",
						text: " Kode Lokasi Sudah Ada! ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterLokasi.php";
					  });
					  </script>';
		}else{
			//buat kode lokasi
			$QueryKode = mysqli_query($koneksi,"SELECT MAX(RIGHT(KodeLokasi,3)) as Kode FROM mstlokasi WHERE LEFT(KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
			$num = mysqli_num_rows($QueryKode);
			
			if($num <> 0){
				$data_lok = mysqli_fetch_array($QueryKode);
				$kode_lok = $data_lok['Kode']+1;
			}else{
				$kode_lok = 1;
			}
			 
			//mulai bikin kode
			$bikin_kode_lok = str_pad($kode_lok, 3, "0", STR_PAD_LEFT);
			$kode_jadi_lok = substr($login_lokasi,0,3)."-".$bikin_kode_lok;
			
			
			$query = mysqli_query($koneksi,"INSERT INTO mstlokasi (KodeLokasi,KodeKab,KodeKec,KodeDesa,KodeLokasiSurat,AlamatLokasi) 
			VALUES ('$kode_jadi_lok','$login_kab','$login_kec','".$_POST['KodeDesa']."','".$_POST['KodeLokasiSurat']."','".$_POST['AlamatLokasiSurat']."')");
			if($query){
				mysqli_query($koneksi,"INSERT INTO mstjabatan (KodeJabatan,KodeLokasi,NamaJabatan,Keterangan)VALUES('JBT-00001','$kode_jadi_lok','BUPATI','-'),('JBT-00002','$kode_jadi_lok','CAMAT','-'),('JBT-00003','$kode_jadi_lok','KEPALA DESA','-'),('JBT-00004','$kode_jadi_lok','KETUA BPD','-'),('JBT-00005','$kode_jadi_lok','SEKRETARIS DESA','-')");
				
				/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
				VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Tambah Kode Lokasi : $kode_jadi_lok','$login_id','$login_lokasi')"); */

				echo '<script language="javascript">document.location="MasterLokasi.php";</script>';
			}else{
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Simpan Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterLokasi.php";
					  });
					  </script>';
			}
		}
	}
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		$Update = mysqli_query($koneksi,"UPDATE mstlokasi SET Penandatanganan='".htmlspecialchars($_POST['_jabat'])."#".htmlspecialchars($_POST['_aparat'])."' WHERE KodeLokasi='".$login_lokasi."' ");
		if($Update){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Edit Kode Lokasi Surat: ".base64_decode($_GET['id'])." - ".$_POST['KodeLokasiSurat']."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="SetPenandatanganan.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Update Data Gagal!",
						text: "  ",
						type: "error"
					  },
					  function () {
						window.location.href = "SetPenandatanganan.php";
					  });
					  </script>';
		}
		
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		/* include ('../library/kode-log-server.php'); */
		$query = mysqli_query($koneksi,"DELETE FROM mstlokasi WHERE KodeLokasi='".base64_decode($_GET['id'])."'");
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Hapus Kode Lokasi : ".base64_decode($_GET['id'])."','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="MasterLokasi.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " Data Telah Digunakan Dalam Berbagai Transaksi ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterLokasi.php";
					  });
					  </script>';
		}
	}	
	?>
  </body>
</html>