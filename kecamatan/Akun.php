<?php
include 'akses.php';
include '../library/tgl-indo.php';
$Page = 'Akun';
$Tahun=date('Y');
$HariIni=date('Y-m-d H:i:s');

//lihat data
function ResultListData($No,$JenisList,$koneksi){
	$query = @mysqli_query($koneksi, "SELECT Uraian FROM mstlistdata WHERE NoUrut='$No' AND JenisList='$JenisList'"); 
	$rowdata = @mysqli_fetch_array($query);
	return ($rowdata['Uraian']);
	//return ($No);
}
//cek apakah user sudah pernah membuat tr permohonan msyarakat
$QueryCekData = @mysqli_query($koneksi, "SELECT * FROM userlogin WHERE UserName='$login_id' AND KodeLokasi='$login_lokasi'"); 
$data = @mysqli_fetch_array($QueryCekData);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
		
		label{
			color:#796AEE;;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Profil</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables">
            <div class="container-fluid">
				<div class="row">
					<div class="col-lg-6">
					  <div class="card">
						<!--<div class="card-header d-flex align-items-center">
						  <h3 class="h4">Profil</h3>
						</div>-->
						<div class="card-body">	
							<label>Nama</label>
							<p><?php if($data['ActualName'] == null OR $data['ActualName'] === ""){ echo '-'; } else { echo $data['ActualName']; } ?></p>
							
							<label>Alamat</label>
							<p><?php if($data['Address'] == null OR $data['Address'] === ""){ echo '-'; } else { echo $data['Address']; } ?></p>
							
							<label>Phone</label>
							<p><?php if($data['Phone'] == null OR $data['Phone'] === ""){ echo '-'; } else { echo $data['Phone']; }
							/* if($data['HPNo'] == null OR $data['HPNo'] === ""){ echo ''; } else { echo $data['HPNo']; } */ ?></p>
							
							<label>Email</label>
							<p><?php if($data['Email'] == null OR $data['Email'] === ""){ echo '-'; } else { echo $data['Email']; } ?></p>
						</div>
					  </div>
					</div>
					
					<!-- <div class="col-lg-6">
					  <div class="card">
						<!--<div class="card-header d-flex align-items-center">
						  <h3 class="h4">Data Register Permohonan</h3>
						</div>--
						<div class="card-body">							  
							<label>Status Keluarga</label>
							<p><?php echo ResultListData($data['StatusKeluarga'],'HUB_KELUARGA',$koneksi);?></p>
							
							<label>Status Perkawinan</label>
							<p><?php echo ResultListData($data['StatusPerkawinan'],'STATUS_PERKAWINAN',$koneksi);?></p>
							
							<label>Anak Ke-</label>
							<p><?php echo $data['AnakKe'];?></p>
							
							<label>Golongan Darah</label>
							<p><?php echo ResultListData($data['GolDarah'],'GOL_DARAH',$koneksi);?></p>
							
							<label>Nama Ayah & Ibu</label>
							<p><?php echo $data['NamaAyah']; echo ' & '; echo $data['NamaIbu']; ?></p>
							
							<label>Kewarganegaraan</label>
							<p><?php echo ResultListData($data['WNI'],'KEWARGANEGARAAN',$koneksi);?></p>
							
							<label>Pendidikan</label>
							<p><?php echo $data['NamaPendidikan'];?></p>
							
							<label>Pekerjaan</label>
							<p><?php echo $data['NamaPekerjaan'];?></p>
						</div>
					  </div>
					</div> -->
					
				</div>
            </div>
          </section> 
        </div>
      </div>
    </div>
	<div id="ModalEditTracking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>
	<!-- Javascript untuk popup modal Edit--> 
	<script type="text/javascript">
		//open modal tracking
		$(document).ready(function () {
	    $(".open_modal_tracking").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var nm_surat = $(this).data("namasurat");
			   $.ajax({
					   url: "TrackingDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,NamaiSurat:nm_surat},
					   success: function (ajaxData){
					   $("#ModalEditTracking").html(ajaxData);
					   $("#ModalEditTracking").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
  </body>
</html>