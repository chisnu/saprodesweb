<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 include '../library/config.php';
 include '../library/tgl-indo.php';
 
 $NoTrMohon	=@$_GET['NoTrMohon'];
 $KodeLokasi=@$_GET['KodeLokasi'];
 $IDPend	=@$_GET['IDPend'];
 $JenisSurat=@$_GET['JenisSurat'];
  $KetSurat=@$_GET['KetSurat'];
 $User=@$_GET['User'];

?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
		  <h4 id="exampleModalLabel" class="modal-title">Lihat Tracking Surat Permohonan <?php echo $KetSurat; ?></h4>
		  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
		</div>
        <div class="modal-body">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<div id="upload-wrapper">
							<div align="center">
								<label>Dokumen atas User</label>
								<?php // cari user
								$CariData = @mysqli_query($koneksi, "SELECT Nama, NIK FROM datapenduduk WHERE IDPend = '".$IDPend."'"); 
								$array = @mysqli_fetch_array($CariData);
									echo "<br>Nama : ".$array['Nama']." (<strong>".$array['NIK']."</strong>)";
									echo "<br>Nomor : ".$NoTrMohon."<br><br>";
								?>
								<div class="table-responsive">  
									<table class="table table-striped">
									  <thead>
										<tr>
										  <th>No</th>
										  <th>Status</th>
										  <!-- <th>Tujuan</th> -->
										  <th>Tanggal Terima</th>
										  <th>Tanggal Pengalihan</th>
										  <th>User</th>
										</tr>
									  </thead>
									  <tbody>
										<?php 
											$sql_syarat = @mysqli_query($koneksi, "SELECT * FROM progresssurat WHERE NoTrMohon='".$NoTrMohon."' AND KodeLokasi='".$KodeLokasi."' ORDER BY TglConfirm"); 
											while($data_syarat = @mysqli_fetch_array($sql_syarat)){
										?>
												<tr>
												  <th scope="row"><?php echo $data_syarat['NoUrutProgress']; ?></th>
												  <td><?php echo $data_syarat['StatusProgress'];?></td>
												  <!-- <td><?php echo $data_syarat['SendTo'];?></td> -->
												  <td align="center"><?php echo TanggalIndo($data_syarat['TglConfirm'])."<br>".date('H:i',strtotime($data_syarat['WaktuConfirm']));?></td>
												  <?php if($data_syarat['TglSend'] !== null){
													echo "<td align='center'>".TanggalIndo($data_syarat['TglSend'])."<br>".date('H:i',strtotime($data_syarat['WaktuSend']))."</td>";
												  } else {
													echo "<td align='center'>-</td>";
												  }
												  
												  // cek user
													if($data_syarat['StatusProgress'] === 'Pemohon/Masyarakat'){
														$CekUser = @mysqli_query($koneksi, "SELECT Nama FROM datapenduduk WHERE KodeLokasi='$KodeLokasi' AND IDPend='$IDPend'"); 
														$UserCari = @mysqli_fetch_array($CekUser);
														$NamaCari = $UserCari['Nama'];
													} else {
														$CekUser = @mysqli_query($koneksi, "SELECT ActualName FROM UserLogin WHERE KodeLokasi='$KodeLokasi' AND UserName='$User'");
														$UserCari = @mysqli_fetch_array($CekUser);
														$NamaCari = $UserCari['ActualName'];
													}
														
													echo "<td>".$NamaCari."</td>";
												  ?>
												</tr>
										<?php } ?>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>