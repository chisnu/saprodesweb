<?php
include 'akses.php';
$fitur_id = 19;
include '../library/lock-menu.php';

$Page = 'AkunDesa';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	$Disable = 'disabled';
	$Edit = mysqli_query($koneksi,"SELECT * FROM userlogin WHERE UserName='".base64_decode($_GET['id'])."' AND KodeLokasi='".base64_decode($_GET['lok'])."'");
	$RowData = mysqli_fetch_assoc($Edit);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">User Login Desa/Kelurahan</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<ul class="nav nav-pills">
						<li <?php if(@$id==null){echo 'class="active"';} ?>>
							<a href="#home-pills" data-toggle="tab"><span class="btn btn-primary">Data User Desa</span></a>&nbsp;
						</li>
						<li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary"><?php echo $Sebutan; ?></span></a>
						</li>
					</ul><br/>
				  <div class="card">
					<div class="tab-content">
						<div class="tab-pane fade <?php if(@$_GET['id']==null){ echo 'in active show'; }?>" id="home-pills">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Data User Desa</h3>
							</div>
							<div class="card-body">							  
								<div class="col-lg-4 offset-lg-8">
									<form method="post" action="">
										<div class="form-group input-group">						
											<select name="keyword" class="form-control" required>	
												<option value="" disabled selected>--- Semua Desa ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT a.KodeLokasi,b.NamaDesa FROM mstlokasi a JOIN mstdesa b ON a.KodeDesa=b.KodeDesa WHERE LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeLokasi']==@$_REQUEST['keyword']){
															echo "<option value=\"".$kode['KodeLokasi']."\" selected >".$kode['NamaDesa']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeLokasi']."\" >".$kode['NamaDesa']."</option>\n";
														}
													}
												?>
											</select>
											<span class="input-group-btn">
												<button class="btn btn-primary" type="submit">Cari</button>
											</span>
										</div>
									</form>
								</div>
							  <div class="table-responsive">  
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>No</th>
									  <th>Nama</th>
									  <th>Lokasi</th>
									  <th>Phone</th>
									  <th>Email</th>
									  <th>Aksi</th>
									</tr>
								  </thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword=$_REQUEST['keyword'];
											$reload = "UserLoginDesa.php?pagination=true&keyword=$keyword";
											$sql =  "SELECT * FROM userlogin WHERE KodeLokasi='$keyword' AND IsDihapus='0'  AND LevelUser='Desa' ORDER BY ActualName ASC";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "UserLoginDesa.php?pagination=true";
											$sql =  "SELECT * FROM userlogin WHERE LEFT(KodeLokasi,3)='".substr($login_lokasi,0,3)."' AND IsDihapus='0' AND LevelUser='Desa' ORDER BY ActualName ASC";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 20; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td>
												<strong><?php echo $data ['ActualName']; ?></strong>
											</td>
											<td>
												<?php /* echo $data ['Address']; */ ?>
												<?php $caridesa = @mysqli_query($koneksi, "SELECT a.NamaDesa FROM mstdesa a JOIN mstlokasi b ON b.KodeDesa = a.KodeDesa WHERE b.KodeLokasi = '".$data['KodeLokasi']."'"); $desa = @mysqli_fetch_array($caridesa); 
												echo $desa['NamaDesa']; ?> 
											</td>
											<td align="center">
												<?php echo $data ['Phone']; ?>
											</td>
											<td>
												<?php echo $data ['Email']; ?>
											</td>
											<td width="100px" align="center">											
												<a href="UserLoginDesa.php?id=<?php echo base64_encode($data['UserName']);?>&lok=<?php echo base64_encode($data['KodeLokasi']);?>" title='Edit'><i class='btn btn-warning btn-sm'><span class='fa fa-edit'></span></i></a> 
											
												<?php
													if($data['LevelID'] === '1' AND $data['UserName'] === 'admin'){
														echo '';
													} else {
														echo '<a href="UserLoginDesa.php?id='.base64_encode($data['UserName']).'&lok='.base64_encode($data['KodeLokasi']).'&aksi='.base64_encode('Hapus').'" title="Hapus" onclick="return confirmation()"><i class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></i></a>';
													}
												
												?>
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										?>
									</tbody>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							  </div>
							</div>
						</div>
						<div class="tab-pane fade <?php if(@$_GET['id']!=null){ echo 'in active show'; }?>" id="tambah-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4"><?php echo $Sebutan; ?></h3>
							</div>
							<div class="card-body">
								<div class="row">
								  <div class="col-lg-6">
									  <form method="post" action="">
										<div class="form-group-material">
										  <input type="text" name="Nama" class="form-control" placeholder="Nama" value="<?php echo @$RowData['ActualName'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Alamat" class="form-control" placeholder="Alamat" value="<?php echo @$RowData['Address'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NoHp" class="form-control" placeholder="Telephone" value="<?php echo @$RowData['Phone'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="email" name="Email" class="form-control" placeholder="Email" value="<?php echo @$RowData['Email'];?>" required>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group-material">
										  <input type="text" name="Username" class="form-control" placeholder="Username" value="<?php echo @$RowData['UserName'];?>" required <?php echo @$Readonly;?>>
										</div>
										<div class="form-group-material">
										  <input type="password" name="Password" class="form-control" placeholder="Password" value="<?php echo base64_decode(@$RowData['UserPsw']);?>" required>
										</div>
										<div class="form-group-material">
											<select id="KodeLokasi" name="KodeLokasi" class="form-control" required>	
												<option value="" disabled selected>--- Pilih Desa ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT a.KodeLokasi,b.NamaDesa FROM mstlokasi a JOIN mstdesa b ON a.KodeDesa=b.KodeDesa WHERE LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeLokasi']==@$RowData['KodeLokasi']){
															echo "<option value=\"".$kode['KodeLokasi']."\" selected >".$kode['NamaDesa']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeLokasi']."\" >".$kode['NamaDesa']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select id="LevelID" name="LevelID" class="form-control" required>	
												<?php
													$menu = mysqli_query($koneksi,"SELECT * FROM accesslevel WHERE IsAktif='1' AND KodeLokasi='".@$RowData['KodeLokasi']."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['LevelID']==@$RowData['LevelID']){
															echo "<option value=\"".$kode['LevelID']."\" selected >".$kode['LevelName']."</option>\n";
														}else{
															echo "<option value=\"".$kode['LevelID']."\" >".$kode['LevelName']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="text-right">
										<?php
										if(@$_GET['id']==null){
											echo '<button type="submit" class="btn btn-primary" name="Simpan">Simpan</button>';
										}else{
											echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
											echo '<a href="UserLoginDesa.php"><span class="btn btn-warning">Batalkan</span></a>';
										}
										?>
										</div>
									  </form>
								  </div>
								</div>
							</div>
						</div>
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		 $("#KodeLokasi").change(function(){
			var KodeLokasi = $("#KodeLokasi").val();
			$.ajax({
				url: "../library/ambil-LevelID.php",
				data: "KodeLokasi="+KodeLokasi,
				cache: false,
				success: function(msg){
					$("#LevelID").html(msg);
				}
			});
		  });
		});
	</script>
	<!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	
	
	<?php
	if(isset($_POST['Simpan'])){
		/* include ('../library/kode-log-server.php'); */
		
		$UserName			= @$_POST['Username'];
		$ActualName		 	= @$_POST['Nama'];
		$Address		 	= @$_POST['Alamat'];
		$Phone			 	= @$_POST['NoHp'];
		//$HPNo			 	= $_POST['HPNo'];
		$Email			 	= @$_POST['Email'];
		$LevelID		 	= @$_POST['LevelID'];
		$KodeLokasi		 	= @$_POST['KodeLokasi'];
		
		$UserPsw = base64_encode($_POST['Password']);
		//cek apakah username ada yang sama atau tidak
		$cek2 = mysqli_query($koneksi,"select UserName from userlogin where UserName='$UserName' AND KodeLokasi='$KodeLokasi'");
		$num2 = mysqli_num_rows($cek2);
		if($num2 == 1 ){
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Simpan Data Gagal!",
						text: " Username sudah ada, Silahkan ganti username Anda! ",
						type: "error"
					  },
					  function () {
						window.location.href = "UserLoginDesa.php";
					  });
					  </script>';
		}else{
			$query = mysqli_query($koneksi,"INSERT into userlogin (UserName,UserPsw,ActualName,Address,Phone,Email,LevelID,KodeLokasi,IsDihapus,LevelUser) 
			VALUES ('$UserName','$UserPsw','$ActualName','$Address','$Phone','$Email','$LevelID','$KodeLokasi',b'0','Desa')");
			if($query){
				/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
				VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Tambah User Login Desa : $UserName - $KodeLokasi','$login_id','$login_lokasi')"); */
				echo '<script language="javascript">document.location="UserLoginDesa.php";</script>';
			}else{
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Simpan Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "UserLoginDesa.php";
					  });
					  </script>';
			}
		}
	}
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		
		$UserName			= @$_POST['Username'];
		$ActualName		 	= @$_POST['Nama'];
		$Address		 	= @$_POST['Alamat'];
		$Phone			 	= @$_POST['NoHp'];
		//$HPNo			 	= $_POST['HPNo'];
		$Email			 	= @$_POST['Email'];
		$LevelID		 	= @$_POST['LevelID'];
		$KodeLokasi		 	= @$_POST['KodeLokasi'];
		
		$UserPsw = base64_encode($_POST['Password']);
		//cek apakah username ada yang sama atau tidak
		$query = mysqli_query($koneksi,"UPDATE userlogin SET ActualName='$ActualName',Address='$Address',Phone='$Phone',Email='$Email',UserPsw='$UserPsw',LevelID='$LevelID' WHERE UserName='$UserName' AND KodeLokasi='$KodeLokasi'");
		
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Edit User Login Desa: $UserName - $KodeLokasi','$login_id','$login_lokasi')"); */
			echo '<script language="javascript">document.location="UserLoginDesa.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Edit Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "UserLoginDesa.php";
				  });
				  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		/* include ('../library/kode-log-server.php'); */
		$sql = mysqli_query($koneksi,"SELECT UserName FROM trbayarpajak WHERE UserName='".base64_decode($_GET['id'])."' AND KodeLokasi='".base64_decode($_GET['lok'])."'");  
		$nums = mysqli_num_rows($sql);
		if($nums > 0){
			echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " Data Telah Digunakan Dalam Berbagai Transaksi ",
							type: "error"
						  },
						  function () {
							window.location.href = "UserLoginDesa.php";
						  });
						  </script>';
		} else {
			$query = mysqli_query($koneksi,"UPDATE userlogin SET IsDihapus='1' WHERE UserName='".base64_decode($_GET['id'])."' AND KodeLokasi='".base64_decode($_GET['lok'])."'");
			if($query){
				/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
				VALUES ('$kode_jadi_log','$DateTime','Hapus Data','User Login Desa : ".base64_decode($_GET['id'])." - ".base64_decode($_GET['lok'])."','$login_id','$login_lokasi')"); */
				echo '<script language="javascript">document.location="UserLoginDesa.php"; </script>';
			}else{
				echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " Data Telah Digunakan Dalam Berbagai Transaksi ",
							type: "error"
						  },
						  function () {
							window.location.href = "UserLoginDesa.php";
						  });
						  </script>';
			}
		}
	}
	/* if(isset($_POST['BuatSurat'])){
		// membuat id otomatis
		$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(NoTrMohon,8)) AS kode FROM TrPermohonanMasy WHERE KodeLokasi='$kode_lokasi_aktif' AND LEFT(NoTrMohon,7)='MH-$Tahun'", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true)); 
		$nums = @sqlsrv_num_rows($sql); 
		while($data = @sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)){
		if($nums === 0){ $kode = 1; }else{ $kode = $data['kode'] + 1; }
		}
		// membuat kode user
		$bikin_kode = str_pad($kode, 8, "0", STR_PAD_LEFT);
		$kode_jadi = "MH-".$Tahun."-".$bikin_kode;
		
		//simpan trpermohonan masyarakat
		$SimpanData = @sqlsrv_query($dbconnect, "INSERT INTO TrPermohonanMasy (NoTrMohon,KodeLokasi,JenisSurat,IDPend,StatusPermohonan)VALUES('$kode_jadi','$kode_lokasi_aktif','".$_POST['JenisSurat']."','$id_penduduk_aktif','PraWAITING')") or die( print_r( sqlsrv_errors(), true)); 
		
		echo '<script language="javascript">document.location="LayananSurat.php?aksi=tampil"; </script>';	
	}
	
	if(isset($_POST['GantiSurat'])){
		//hapus dokumensyarat mohon
		$HapusDokumenSyarat = @sqlsrv_query($dbconnect, "DELETE FROM DokumenSyaratMohon WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true));
		if($HapusDokumenSyarat){
			//update trmohonmsy
			$UpdateData = @sqlsrv_query($dbconnect, "UPDATE TrPermohonanMasy SET JenisSurat='".$_POST['JenisSurat']."' WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='$No_Transaksi'") or die( print_r( sqlsrv_errors(), true)); 
		}
		
		echo '<script language="javascript">document.location="LayananSurat.php?aksi=tampil"; </script>';
	}
	
	if(isset($_POST['KirimPermohonan'])){	
		$JamSekarang = date('H:i:s');
		$TanggalSekarang = date('Y-m-d');
		$JamLayananKantor = date('Y-m-d').' 12:00:00'; //jam tutup lyn hari ini
		
		$NamaHariIni = date('D', strtotime($HariIni)); //nama hari
		
		if(strtotime($HariIni) > strtotime($JamLayananKantor)){ //jika input diatas jam 12 siang maka diproses besok
			//Cek Ada berapa banyak hari sabtu
			$TglLibur = array();
			for($i=0; $i < $Standar_Waktu; $i++){
				$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
				$NmHari = date('D', strtotime($TglHari));
				$TglLibur[]=$NmHari;
			}
			//cari ada berapa banayak hari saturday
			$Count = array_count_values($TglLibur); 
			$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
			
			$TotalLamaPelayanan = $Standar_Waktu+$JmlTglLibur;
			$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
			
		}else{ //surat diproses hari ini
			//Cek Ada berapa banyak hari sabtu
			$StandarWaktuBaru  = $Standar_Waktu-1;
			$TglLibur = array();
			for($i=0; $i < $StandarWaktuBaru; $i++){
				$TglHari = date('Y-m-d', strtotime('+'.$i.' days', strtotime($HariIni)));
				$NmHari = date('D', strtotime($TglHari));
				$TglLibur[]=$NmHari;
			}
			//cari ada berapa banayak hari saturday
			$Count = array_count_values($TglLibur); 
			$JmlTglLibur = @$Count['Sat']*2; //*2 karena instansi libur sabtu dan minggu
			
			$TotalLamaPelayanan = $StandarWaktuBaru+$JmlTglLibur;
			$PrediksiTglSelesai	 = date('Y-m-d H:i:s', strtotime('+'.$TotalLamaPelayanan.' days', strtotime($HariIni)));
		}
		 
		//insert to progress surat & update trpermohonanmsy
		$UpdateTrPermohonan = @sqlsrv_query($dbconnect, "UPDATE TrPermohonanMasy SET TglPermohonan='$HariIni',PrediksiTglSelesai='$PrediksiTglSelesai', Keterangan1='".$_POST['Ket']."', StatusPermohonan='WAITING' WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif' ", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true));
		
		if($UpdateTrPermohonan){
			$InsertData = @sqlsrv_query($dbconnect, "INSERT INTO ProgressSurat (NoUrutProgress,NoTrMohon,KodeLokasi,StatusProgress,Waktu,Tanggal,SendTo,UserSender,IsSend,WaktuSend,TglSend,IsConfirmed)
			VALUES('1','$No_Transaksi','$kode_lokasi_aktif','Pemohon/Masyarakat','$JamSekarang','$TanggalSekarang','Front Office (DESA)','$id_penduduk_aktif','1','$JamSekarang','$HariIni','0')", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true));
			
			if($InsertData){
				echo '<script type="text/javascript">
				  sweetAlert({
					title: "Permohonan Terkirim!",
					text: " Permohonan surat akan diproses pada jam kerja aktif!",
					type: "success"
				  },
				  function () {
					window.location.href = "LayananSurat.php";
				  });
				  </script>';
			}else{
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Simpan Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "LayananSurat.php";
					  });
					  </script>';
			} 
		}
	}
	
	if(isset($_POST['BatalkanPermohonan'])){
		//hapus dokumensyarat mohon
		$HapusDokumenSyarat = @sqlsrv_query($dbconnect, "DELETE FROM DokumenSyaratMohon WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true));
		if($HapusDokumenSyarat){
			$HapusTrPermohonan = @sqlsrv_query($dbconnect, "DELETE FROM TrPermohonanMasy WHERE NoTrMohon='$No_Transaksi' AND KodeLokasi='$kode_lokasi_aktif'", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true));
			if($HapusTrPermohonan){
				echo '<script type="text/javascript">
				  sweetAlert({
					title: "Hapus Data Berhasil",
					text: " ",
					type: "success"
				  },
				  function () {
					window.location.href = "LayananSurat.php";
				  });
				  </script>';
			}else{
				echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "LayananSurat.php";
					  });
					  </script>';
			}
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " ",
						type: "error"
					  },
					  function () {
						window.location.href = "LayananSurat.php";
					  });
					  </script>';
		}
	}
	
	//Hapus Transaksi
	if(base64_decode(@$_GET['aksi'])==='HapusTransaksi'){
		//cek apakah status permohonan waiting
		$QueryCekStatus = @sqlsrv_query($dbconnect, "SELECT NoTrMohon FROM TrPermohonanMasy WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."' AND StatusPermohonan='WAITING'", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true)); 
		$numCekStatus = @sqlsrv_num_rows($QueryCekData); 
		if($numCekStatus > 0){
			$HapusData = @sqlsrv_query($dbconnect, "DELETE FROM DokumenSyaratMohon WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true)); 
			
			if($HapusData){
				$HapusProgress = @sqlsrv_query($dbconnect, "DELETE FROM ProgressSurat WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true)); 
				
				if($HapusProgress){
					$HapusTrans = @sqlsrv_query($dbconnect, "DELETE FROM TrPermohonanMasy WHERE KodeLokasi='$kode_lokasi_aktif' AND NoTrMohon='".base64_decode(@$_GET['kd'])."'", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true)); 
					
					if($HapusProgress){
						echo '<script language="javascript">document.location="LayananSurat.php"; </script>';
					}else{
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "LayananSurat.php";
						  });
						  </script>';
					}
				}else{
					echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "LayananSurat.php";
						  });
						  </script>';
				}
			}else{
				echo '<script type="text/javascript">
						  sweetAlert({
							title: "Hapus Data Gagal!",
							text: " ",
							type: "error"
						  },
						  function () {
							window.location.href = "LayananSurat.php";
						  });
						  </script>';
			}
		}else{
			echo '<script type="text/javascript">
			  sweetAlert({
				title: "Hapus Data Gagal!",
				text: " Permohonan Anda sedang dalam prosess verifikasi ",
				type: "error"
			  },
			  function () {
				window.location.href = "LayananSurat.php";
			  });
			  </script>';
		}
	} */
	
	
	?>
  </body>
</html>