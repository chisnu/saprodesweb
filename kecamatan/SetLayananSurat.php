<?php
include 'akses.php';
$fitur_id = 18;
include '../library/lock-menu.php';

$Page = 'Setting';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM masterpengurusansurat WHERE JenisSurat='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
	$RowData = mysqli_fetch_assoc($Edit);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Set Layanan Surat</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
				  <div class="card">
					<?php if(@$_GET['aksi']==null){ ?>	
						<div class="card-header d-flex align-items-center">
						  <h3 class="h4">Data Layanan Surat</h3>
						</div>							
						<div class="card-body">							  
							<!--<div class="col-lg-4 offset-lg-8">
								<form method="post" action="">
									<div class="form-group input-group">						
										<input type="text" name="keyword" class="form-control" placeholder="Cari" value="<?php echo @$_REQUEST['keyword']; ?>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="submit">Cari</button>
										</span>
									</div>
								</form>
							</div>-->
						  <div class="table-responsive">  
							<table class="table table-striped">
							  <thead>
								<tr>
								  <th>No</th>
								  <th>Jenis Surat</th>
								  <th>Nama Surat</th>
								  <th>Waktu Pelayanan</th>
								  <th>Kode Nomor Surat</th>
								  <th>Aksi</th>
								</tr>
							  </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "SetLayananSurat.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT * FROM masterpengurusansurat WHERE JenisSurat!='SKT' Keterangan LIKE '%$keyword%' ORDER BY JenisSurat ASC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "SetLayananSurat.php?pagination=true";
										$sql =  "SELECT * FROM masterpengurusansurat WHERE JenisSurat!='SKT' ORDER BY JenisSurat ASC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 20; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
								<tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
									<tr class="odd gradeX">
										<td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td align="left">
											<?php echo $data ['JenisSurat']; ?>
										</td>
										<td align="left">
											<?php echo $data ['Keterangan']; ?>
										</td>
										<td align="center">
											<?php echo $data ['StandarWaktuPelayanan']; ?> Hari
										</td>
										<td align="left">
											<?php echo $data ['KodeNomorSurat']; ?>
										</td>
										<td width="100px" align="center">										
											<a href="SetLayananSurat.php?id=<?php echo base64_encode($data['JenisSurat']);?>&aksi=Edit" title='Edit'><i class='btn btn-warning btn-sm'><span class='fa fa-edit'></span></i></a> 
											
											<a href="SetLayananSurat.php?id=<?php echo base64_encode($data['JenisSurat']);?>&aksi=Syarat" title='Set Syarat Surat'><i class='btn btn-info btn-sm'><span class='fa fa-paperclip'></span></i></a> 
										</td>
									</tr>
									<?php
										$i++; 
										$count++;
									}
									?>
								</tbody>
							</table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
						  </div>
						</div>
					<?php }elseif(@$_GET['aksi']=='Edit'){ ?>	
						<div class="card-header d-flex align-items-center">
						  <h3 class="h4"><?php echo $Sebutan; ?></h3>
						</div>
						<div class="card-body">
							<div class="row">
							  <div class="col-lg-6">
								  <form method="post" action="">
									<div class="form-group-material">
										<div class="form-group-material">
										  <input type="text" name="JenisSurat" class="form-control" value="<?php echo @$RowData['JenisSurat'];?>" readonly>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Keterangan" class="form-control" placeholder="Nama Surat" value="<?php echo @$RowData['Keterangan'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="number" name="StandarWaktuPelayanan" class="form-control" placeholder="Waktu Pelayanan *Hari" value="<?php echo @$RowData['StandarWaktuPelayanan'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="KodeNomerSurat" class="form-control" placeholder="Kode Nomer Surat" value="<?php echo @$RowData['KodeNomorSurat'];?>" required>
										</div>
									</div>
									<?php
										echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
										echo '<a href="SetLayananSurat.php"><span class="btn btn-warning">Batalkan</span></a>';
									?>
								  </form>
							  </div>
							</div>
						</div>
					<?php }elseif(@$_GET['aksi']=='Syarat'){ ?>
						<div class="card-header d-flex align-items-center">
						  <h3 class="h4">Set Syarat <?php echo @$RowData['Keterangan']; ?></h3>
						</div>
						<div class="card-body">
							<div class="row">
							  <div class="col-lg-8">
								  <div class="table-responsive">  
									<table class="table table-striped">
									  <thead>
										<tr>
										  <th>No</th>
										  <th>Nama Syarat</th>
										  <th>Keterangan</th>
										  <th>Aksi</th>
										</tr>
									  </thead>
									<tbody>
									<?php 
									$no = 1;
									$QuerySyarat = mysqli_query($koneksi,"SELECT * FROM mstsyaratsurat WHERE KodeLokasi='$login_lokasi' AND JenisSurat='".$RowData['JenisSurat']."' ");
									while($RowSyarat=mysqli_fetch_array($QuerySyarat)){
									?>
										<tr>
											<td><?php echo $no++; ?></td>
											<td><?php echo $RowSyarat['NamaSyarat']; ?></td>
											<td><?php echo $RowSyarat['Keterangan']; ?></td>
											<td align="center"><a href="SetLayananSurat.php?id=<?php echo base64_encode($RowSyarat['JenisSurat']);?>&KodeSyarat=<?php echo base64_encode($RowSyarat['KodeSyarat']);?>&aksi=Hapus" title='Hapus'><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a></td>
										</tr>
									<?php } ?>
										<form method="post" action="">
										<tr>
											<td><i class="btn btn-success btn-sm"><span class='fa fa-plus'></span></i></td>
											<td>
												<div class="form-group-material">
												  <input type="text" name="NamaSyarat" class="form-control" placeholder="Nama Syarat" required>
												</div>
											</td>
											<td>
												<div class="form-group-material">
												  <input type="text" name="Keterangan" class="form-control" placeholder="Keterangan">
												  <input type="hidden" name="JenisSurat" value="<?php echo $RowData['JenisSurat']; ?>">
												</div>
											</td>
											<td>
												<button type="submit" class="btn btn-primary" name="Simpan">Simpan</button>
											</td>
										</tr>
										</form>
									</tbody>
									</table>
								  </div>
							  </div>
							</div>
						</div>
					<?php } ?>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	
	
	<?php
	if(isset($_POST['Simpan'])){
		include ('../library/kode-log-server.php');
		//kode jabatan
		$sql_jbt = mysqli_query($koneksi,'SELECT RIGHT(KodeSyarat,3) AS kode FROM mstsyaratsurat WHERE KodeLokasi="'.$login_lokasi.'" AND JenisSurat="'.$_POST['JenisSurat'].'" ORDER BY KodeSyarat DESC LIMIT 1');  
		$nums_jbt = mysqli_num_rows($sql_jbt);
		 
		if($nums_jbt <> 0)
		 {
		 $data_jbt = mysqli_fetch_array($sql_jbt);
		 $kode_jbt = $data_jbt['kode'] + 1;
		 }else
		 {
		 $kode_jbt = 1;
		 }
		 
		//mulai bikin kode
		 $bikin_kode_jbt = str_pad($kode_jbt, 3, "0", STR_PAD_LEFT);
		 $kode_jadi_jbt = "SYR-".$bikin_kode_jbt;
		
		$query = mysqli_query($koneksi,"INSERT INTO mstsyaratsurat (KodeSyarat,KodeLokasi,JenisSurat,NamaSyarat,Keterangan) 
		VALUES ('$kode_jadi_jbt','$login_lokasi','".$_POST['JenisSurat']."','".$_POST['NamaSyarat']."','".$_POST['Keterangan']."')");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Tambah Syarat Surat : ".$_POST['JenisSurat']."-".$_POST['NamaSyarat']." ','$login_id','$login_lokasi')");

			echo '<script language="javascript">document.location="SetLayananSurat.php?id='.base64_encode($_POST['JenisSurat']).'&aksi=Syarat";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "SetLayananSurat.php?id='.base64_encode($_POST['JenisSurat']).'&aksi=Syarat";
				  });
				  </script>';
		}
	}
	
	if(isset($_POST['SimpanEdit'])){
		include ('../library/kode-log-server.php');
		$Update = mysqli_query($koneksi,"UPDATE masterpengurusansurat SET Keterangan='".$_POST['Keterangan']."',StandarWaktuPelayanan='".$_POST['StandarWaktuPelayanan']."',KodeNomorSurat='".$_POST['KodeNomerSurat']."' WHERE JenisSurat='".$_POST['JenisSurat']."' AND KodeLokasi='$login_lokasi'");
		if($Update){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Edit Set Layanan Surat: ".base64_decode($_GET['id'])." - ".$_POST['Keterangan']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="SetLayananSurat.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Edit Data Gagal!",
						text: "  ",
						type: "error"
					  },
					  function () {
						window.location.href = "SetLayananSurat.php";
					  });
					  </script>';
		}
		
	}
	
	if(@$_GET['aksi']=='Hapus'){
		include ('../library/kode-log-server.php');
		$query = mysqli_query($koneksi,"DELETE FROM mstsyaratsurat WHERE JenisSurat='".base64_decode($_GET['id'])."' AND KodeSyarat='".base64_decode($_GET['KodeSyarat'])."' AND KodeLokasi='$login_lokasi'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Master Syarat Surat : ".base64_decode($_GET['id'])." - ".base64_decode($_GET['KodeSyarat'])."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="SetLayananSurat.php?id='.$_GET['id'].'&aksi=Syarat"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " Aparatur telah digunakan di berbagai transaksi! ",
						type: "error"
					  },
					  function () {
						window.location.href = "SetLayananSurat.php?id='.$_GET['id'].'&aksi=Syarat";
					  });
					  </script>';
		}
	}
	?>
  </body>
</html>