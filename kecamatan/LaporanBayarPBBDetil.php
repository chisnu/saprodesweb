<?php
include 'akses.php';
include '../library/tgl-indo.php';

$fitur_id = 16;
include '../library/lock-menu.php';

$Page = 'Pajak';
$Tahun=date('Y');
$DateTime=date('Y-m-d H:i:s');

if(@$_GET['id']==null){
	$Sebutan = 'Tambah Data';
}else{
	$Sebutan = 'Edit Data';	
	$Readonly = 'readonly';
	
	$Edit = mysqli_query($koneksi,"SELECT * FROM datapenduduk WHERE NIK='".base64_decode($_GET['id'])."' AND LEFT(KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
	$RowData = mysqli_fetch_array($Edit);
}

$carilokasi = mysqli_query($koneksi,"SELECT * FROM mstlokasi a JOIN mstdesa b on a.KodeDesa = b.KodeDesa WHERE a.KodeLokasi='".base64_decode($_GET['lokasi'])."'");
$datalokasi = mysqli_fetch_array($carilokasi);

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
		
		
		/* Style the form */
		#regForm {
		  background-color: #ffffff;
		  margin: 20px auto;
		  /* padding: 40px; */
		  width: 100%;
		  min-width: 300px;
		}

		/* Style the input fields */
		input {
		  padding: 10px;
		  width: 100%;
		  font-size: 17px;
		  font-family: Raleway;
		  border: 1px solid #aaaaaa;
		}

		/* Mark input boxes that gets an error on validation: */
		input.invalid {
		  background-color: #ffdddd;
		}

		/* Hide all steps by default: */
		.tab {
		  display: none;
		}

		/* Make circles that indicate the steps of the form: */
		.step {
		  height: 15px;
		  width: 15px;
		  margin: 0 2px;
		  background-color: #bbbbbb;
		  border: none; 
		  border-radius: 50%;
		  display: inline-block;
		  opacity: 0.5;
		}

		/* Mark the active step: */
		.step.active {
		  opacity: 1;
		}

		/* Mark the steps that are finished and valid: */
		.step.finish {
		  background-color: #4CAF50;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Laporan Perkembangan PBB</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
					<!-- <ul class="nav nav-pills">
						<li <?php if(@$id==null){echo 'class="active"';} ?>>
							<a href="#home-pills" data-toggle="tab"><span class="btn btn-primary">Data Penduduk</span></a>&nbsp;
						</li>
						<li>
							<a href="#import-user" data-toggle="tab"><span class="btn btn-primary">Import Penduduk</span></a>
						</li>
						<!-- <li>
							<a href="#tambah-user" data-toggle="tab"><span class="btn btn-primary"><?php /* echo $Sebutan; */ ?></span></a>
						</li> --
					</ul><br/> -->
				  <div class="card">
					<div class="tab-content">
						<div class="tab-pane fade <?php if(@$_GET['id']==null){ echo 'in active show'; }?>" id="home-pills">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Laporan PBB <?php echo $datalokasi['NamaDesa']; ?></h3>
							</div>
							<div class="card-body">	
								<!-- <form method="post" action="">
							<div class="row">
								<div class="col-lg-8">
								<div class="form-group input-group">
									<input type="text" name="tahun" id="time1" class="form-control" value="<?php echo @$_REQUEST['tahun']; ?>" placeholder="Pilih Tahun PBB" required>&nbsp;&nbsp;
									<input type="text" name="keyword" id="time2" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Pilih Sampai Tanggal" required>
									<span class="input-group-btn">
										<button class="btn btn-primary" type="submit">Cari</button>&nbsp;
									</span>
								</div>
								</div>
								<div class="col-lg-6">
										<!-- <div class="form-group input-group">						
											<input type="text" name="keyword" class="form-control" placeholder="Nama..." value="<?php echo @$_REQUEST['keyword']; ?>"> --
											<span class="input-group-btn">
												<button class="btn btn-primary" type="submit">Cari</button>
											</span>
										</div>
								</div>-->
							<div class="row">
								<div class="col-lg-8">
								<div class="table-responsive">  
									<table class="table table-striped">
									  <tbody>
										<tr><td width="40%">Tahun Pajak PBB</td><td width="1%">:</td><td><?php echo base64_decode(@$_GET['tahun']);?></td></tr>
										<?php $caridata = mysqli_query($koneksi, "SELECT ActualName FROM userlogin WHERE UserName='".base64_decode(@$_GET['user'])."' AND KodeLokasi='".base64_decode(@$_GET['lokasi'])."'");
										$array = mysqli_fetch_array($caridata);?>
										<tr><td>Nama Petugas</td><td>:</td><td><?php echo ucwords($array['ActualName'])." (".base64_decode(@$_GET['user']).")";?></td></tr>
										<tr><td>Per Tanggal</td><td>:</td><td><?php echo TanggalIndo(base64_decode(@$_GET['key']));?></td></tr>
									  </tbody>
									</table>
									</div></div><hr>
								
							</div>
								<!-- </form> -->
							  <div class="table-responsive">  
								<table class="table table-striped" width="100%">
								  <thead>
									<tr>
									  <th>No</th>
									  <th>NOP</th>
									  <th>Tahun</th>
									  <th>Alamat OP</th>
									  <!-- <th>Luas Bumi/Bangunan</th> -->
									  <th>Pokok</th>
									  <th>Lokasi</th>
									  <th>Petugas</th>
									  <th>Aksi</th>
									</tr>
								  </thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										$tahun=base64_decode(@$_GET['tahun']);
										//jika tidak ada pencarian pakai ini
											if(@$tahun === null){
												$reload = "LaporanBayarPBBDetil.php?pagination=true&user=".@$_GET['user']."&lokasi=".@$_GET['lokasi']."&key=".@$_GET['key'];
												$sql =  "SELECT * FROM masterobjekpajak a JOIN mstlokasi b ON b.KodeLokasi=a.KodeLokasi JOIN trbayarpajak c ON (c.NoObjekPajak,c.Tahun)=(a.NoObjekPajak,a.Tahun) WHERE c.UserName = '".base64_decode(@$_GET['user'])."' AND a.Tahun = '".date('Y')."' AND a.KodeLokasi ='".base64_decode(@$_GET['lokasi'])."' ORDER BY a.NoObjekPajak";
											} else {
												$reload = "LaporanBayarPBBDetil.php?pagination=true&user=".@$_GET['user']."&lokasi=".@$_GET['lokasi']."&tahun=".@$_GET['tahun']."&key=".@$_GET['key'];
												$sql =  "SELECT * FROM masterobjekpajak a JOIN mstlokasi b ON b.KodeLokasi=a.KodeLokasi JOIN trbayarpajak c ON (c.NoObjekPajak,c.Tahun)=(a.NoObjekPajak,a.Tahun) WHERE c.UserName = '".base64_decode(@$_GET['user'])."' AND a.Tahun = '$tahun' AND a.KodeLokasi ='".base64_decode(@$_GET['lokasi'])."' ORDER BY a.NoObjekPajak";
											}
											$result = mysqli_query($koneksi,$sql);
										
										//pagination config start
										$rpp = 20; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php if($tcount == null OR $tcount === 0){
											echo '<tr class="odd gradeX"><td colspan="9" align="center"><br><h5>Tidak Ada Data</h5><br></td></tr>';
										} else {
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td>
												<?php echo $data ['NoObjekPajak']."<br><strong>".strtoupper($data['NamaWP'])."</strong>"; ?>
											</td>
											<td>
												<?php echo $data ['Tahun']; ?>
											</td>
											<td>
												<?php echo ucwords($data ['AlamatOP']); ?>
											</td>
											<!-- <td align="right">
												<?php /* echo number_format($data ['LuasBumi'])."<br>".number_format($data ['LuasBangunan']); */ ?>
											</td> -->
											<td align="right">
												<?php echo "<strong>".number_format($data ['Pokok'])."</strong>"; ?>
											</td>
											<td>
												<?php $CariData = @mysqli_query($koneksi, "SELECT NamaDesa FROM mstdesa WHERE KodeDesa= '".$data['KodeDesa']."'"); 
												$array = @mysqli_fetch_array($CariData);
												echo $array ['NamaDesa']; ?>
											</td>
											<td>
												<?php if($data['UserName'] == null OR $data['UserName'] === ''){ echo '-'; } else { echo ucwords($data ['UserName']); } ?>
											</td>
											<td>	
												<?php $tanggalbaru = date("Y-m-d", strtotime($data['Tanggal']));
												if($tanggalbaru <= base64_decode(@$_GET['key'])){
													if($data['IsBayar'] === '1'){ 
														echo '<a href="#"><span class="btn btn-sm btn-success" title="Terbayar">Terbayar</span><br>';
														if($data['Tanggal'] == null){
															echo 'Tidak Ada Tanggal';
														} else {
															echo 'Pada '.TanggalIndo($data['Tanggal']);
														}
													} else { 
														echo '<a href="#"><span class="btn btn-sm btn-danger" title="Belum Terbayar">Belum Terbayar</span><br>';
													} 
												} else {
													echo '<a href="#"><span class="btn btn-sm btn-danger" title="Belum Terbayar">Belum Terbayar</span><br>';
												} ?>
												<!-- <a href="MasterPajak.php?id=<?php /* echo base64_encode($data['NoObjekPajak']); ?>&kode=<?php echo base64_encode($data['KodeLokasi']); */ ?>"><span class="btn btn-sm btn-warning fa fa-edit" title="Edit"></span></a>
												<a href="LayananSurat.php"><span class="btn btn-sm btn-info fa fa-search" title="Detail"></span></a> -->
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										}
										?>
									</tbody>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div><br>
								<a href="ViewLaporan.php?type=<?php echo base64_encode('LaporanPBBDetilPetugas'); ?>&user=<?php echo @$_GET['user']; ?>&lokasi=<?php echo @$_GET['lokasi']; ?>&tahun=<?php echo base64_encode($tahun); ?>&key=<?php echo @$_GET['key']; ?>" class="btn btn-success" target="_blank">Cetak Laporan</a>
							  </div>
							</div>
						</div>
						<div class="tab-pane fade <?php if(@$_GET['id']!=null){ echo 'in active show'; }?>" id="tambah-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Data Utama<?php //echo $Sebutan; ?></h3>
							</div>
						<form method="post" action="">
							<div class="card-body">
								<div class="row">
								  <div class="col-lg-3">
										<div class="form-group-material">
										  <input type="text" name="NIK" class="form-control" placeholder="NIK" value="<?php echo @$RowData['NIK'];?>" readonly>
										  <input type="hidden" name="KodeLokasi" class="form-control" placeholder="Kode Lokasi" value="<?php echo @$RowData['KodeLokasi'];?>" readonly>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Nama" class="form-control" placeholder="Nama" value="<?php echo @$RowData['Nama'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="TempatLahir" class="form-control" placeholder="Tempat Lahir" value="<?php echo @$RowData['TempatLahir'];?>" required>
										</div>
										<div class="form-group-material">
										<?php if(@$RowData['TanggalLahir'] !== null){
											echo '<input type="text" name="TanggalLahir" id="time7" class="form-control" placeholder="Tanggal Lahir" value="'.substr($RowData['TanggalLahir'],0,11).'" required>';
										} else {
											echo '<input type="text" name="TanggalLahir" id="time7" class="form-control" placeholder="Tanggal Lahir" value="'.date('Y-m-d').'" required>';
										} ?>
										</div>
										<div class="form-group-material">
											<select name="JenisKelamin" class="form-control" required>
												<option value="" disabled selected>--- Jenis Kelamin ---</option>
												<option value="1" <?php if(@$RowData['JenisKelamin']==='1'){echo 'selected';} ?>>Laki-laki</option>
												<option value="2" <?php if(@$RowData['JenisKelamin']==='2'){echo 'selected';} ?>>Perempuan</option>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Agama" class="form-control" required>
												<option value="" disabled selected>--- Pilih Agama ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='AGAMA' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['Agama']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="StatusPenduduk" class="form-control" required>
												<option value="" disabled selected>--- StatusPenduduk ---</option>
												<option value="ADA" <?php if(@$RowData['StatusPenduduk']==='ADA'){echo 'selected';} ?>>Ada</option>
												<option value="PINDAH_MASUK" <?php if(@$RowData['StatusPenduduk']==='PINDAH_MASUK'){echo 'selected';} ?>>Pindah Masuk</option>
												<option value="PINDAH_KELUAR" <?php if(@$RowData['StatusPenduduk']==='PINDAH_KELUAR'){echo 'selected';} ?>>Pindah Keluar</option>
												<option value="MENINGGAL" <?php if(@$RowData['StatusPenduduk']==='MENINGGAL'){echo 'selected';} ?>>Meninggal</option>
											</select>
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group-material">
										  <input type="text" name="NamaAyah" class="form-control" placeholder="Nama Ayah" value="<?php echo @$RowData['NamaAyah'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NamaIbu" class="form-control" placeholder="Nama Ibu" value="<?php echo @$RowData['NamaIbu'];?>" required>
										</div>
										<div class="form-group-material">
											<select name="Kewarganegaraan" class="form-control" required>
												<option value="" disabled selected>--- Kewarganegaraan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['Agama']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										
										<div class="form-group-material">
											<select name="StatusKeluarga" class="form-control" required>
												<option value="" disabled selected>--- Status Keluarga ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='HUB_KELUARGA' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['StatusKeluarga']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="StatusPerkawinan" class="form-control" required>
												<option value="" disabled selected>--- Status Perkawinan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='STATUS_PERKAWINAN' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['StatusPerkawinan']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="GolDarah" class="form-control" required>
												<option value="" disabled selected>--- Gol Darah ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='GOL_DARAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']===@$RowData['GolDarah']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group-material">
										  <input type="text" name="NoKK" class="form-control" placeholder="No KK" value="<?php echo @$RowData['NoKK'];?>" required>
										</div>
										<div class="form-group-material">
										  <input type="text" name="NoUrutDiKK" class="form-control" placeholder="No Urut di KK" value="<?php echo @$RowData['NoUrutKK'];?>" required>
										</div>
										
										<div class="form-group-material">
										  <input type="text" name="NoAkteLahir" class="form-control" placeholder="No Akte Kelahiran" value="<?php echo @$RowData['NoAkteLahir'];?>">
										</div>
										<div class="form-group-material">
										<?php if(@$RowData['TanggalAkteLahir'] !== null){
											echo '<input type="text" name="TanggalAkteLahir" id="time2" class="form-control" placeholder="Tanggal Akte Lahir" value="'.substr($RowData['TanggalLahir'],0,11).'" required>';
										} else {
											echo '<input type="text" name="TanggalAkteLahir" id="time2" class="form-control" placeholder="Tanggal Akte Lahir" value="'.date('Y-m-d').'" required>';
										} ?>
										</div>
										<div class="form-group-material">
											<select name="Pendidikan" class="form-control" required>
												<option value="" disabled selected>--- Pendidikan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT KodePendidikan,NamaPendidikan FROM mstpendidikan ORDER BY KodePendidikan ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodePendidikan']===@$RowData['KodePendidikan']){
															echo "<option value=\"".$kode['KodePendidikan']."\" selected>".$kode['NamaPendidikan']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodePendidikan']."\" >".$kode['NamaPendidikan']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Pekerjaan" class="form-control" required>
												<option value="" disabled selected>--- Pekerjaan ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT KodePekerjaan,NamaPekerjaan FROM mstpekerjaan ORDER BY KodePekerjaan ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodePekerjaan']===@$RowData['KodePekerjaan']){
															echo "<option value=\"".$kode['KodePekerjaan']."\" selected>".$kode['NamaPekerjaan']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodePekerjaan']."\" >".$kode['NamaPekerjaan']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="PekerjaanTambahan" class="form-control" placeholder="Pekerjaan Tambahan" value="<?php echo @$RowData['PekerjaanTambahan'];?>">
										</div>
									</div>
									<div class="col-lg-3">
										<!-- <div class="form-group-material">						
											<select id="KodeLokasi" name="KodeLokasi" class="form-control" required>	
												<option value="" disabled selected>--- Desa ---</option>
												<?php /*
													$menu = mysqli_query($koneksi,"SELECT a.KodeLokasi,b.NamaDesa FROM mstlokasi a JOIN mstdesa b ON a.KodeDesa=b.KodeDesa WHERE LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeLokasi']===@$RowData['KodeLokasi']){
															echo "<option value=\"".$kode['KodeLokasi']."\" selected >".$kode['NamaDesa']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeLokasi']."\" >".$kode['NamaDesa']."</option>\n";
														}
													} */
												?>
											</select>
										</div> -->
										<div class="form-group-material">
											<select id="KodeDusun" name="KodeDusun" class="form-control" required>	
												<option value="" disabled selected>--- Dusun ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT * FROM mstdusun WHERE KodeLokasi='".@$RowData['KodeLokasi']."'");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeDusun']==@$RowData['KodeDusun']){
															echo "<option value=\"".$kode['KodeDusun']."\" selected >".$kode['NamaDusun']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeDusun']."\" >".$kode['NamaDusun']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="RT" class="form-control" placeholder="RT" value="<?php echo @$RowData['RT'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="RW" class="form-control" placeholder="RW" value="<?php echo @$RowData['RW'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="NamaJalan" class="form-control" placeholder="Nama Jalan" value="<?php echo @$RowData['NamaJalan'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="NoRumah" class="form-control" placeholder="No Rumah" value="<?php echo @$RowData['NoRumah'];?>">
										</div>
										<div class="form-group-material">
										  <input type="text" name="Alamat" class="form-control" placeholder="Alamat" value="<?php echo @$RowData['Alamat'];?>">
										</div>
										<!-- <div class="form-group-material">
											<select name="StatusKepemilikanRumah" class="form-control">
												<option value="" disabled selected>--- Kepemilikan Rumah ---</option>
												<?php /*
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='KEPEMILIKAN_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['StatusPemilikRumah']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Dinding" class="form-control" >
												<option value="" disabled selected>--- Dinding ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='DINDING_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Dinding']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Lantai" class="form-control" >
												<option value="" disabled selected>--- Lantai ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='LANTAI_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Lantai']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="Atap" class="form-control" >
												<option value="" disabled selected>--- Atap ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='ATAP_RUMAH' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Lantai']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
											<select name="AsetLainnya" class="form-control" >
												<option value="" disabled selected>--- Aset Lainnya ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoUrut,Uraian FROM mstlistdata WHERE JenisList='ASET_KELUARGA' ORDER BY NoUrut ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['NoUrut']==@$RowData['Lantai']){
															echo "<option value=\"".$kode['NoUrut']."\" selected>".$kode['Uraian']."</option>\n";
														}else{
															echo "<option value=\"".$kode['NoUrut']."\" >".$kode['Uraian']."</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group-material">
										  <input type="text" name="Suku" class="form-control" placeholder="Suku" value="<?php echo @$RowData['Alamat']; */?>">
										</div> -->
								  </div>
								  &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button>
								</div>
							</div>
						</form>								
							<?php
							/* if(@$_GET['id']==null){
								echo '<button type="submit" class="btn btn-primary" name="Simpan">Simpan</button>';
							}else{
								echo '<input type="hidden" name="KodeJabatan" value="'.$RowData['KodeJabatan'].'"> ';
								echo '<button type="submit" class="btn btn-primary" name="SimpanEdit">Simpan</button> &nbsp;';
								echo '<a href="UserLogin.php"><span class="btn btn-warning">Batalkan</span></a>';
							} */
							?>
						</div>
						
						<div class="tab-pane fade" id="import-user">
							<div class="card-header d-flex align-items-center">
							  <h3 class="h4">Import Data Penduduk<?php //echo $Sebutan; ?></h3>
							</div>
							
							
						
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>	
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		 $("#KodeLokasi").change(function(){
			var KodeLokasi = $("#KodeLokasi").val();
			$.ajax({
				url: "../library/ambil-desa.php",
				data: "KodeLokasi="+KodeLokasi,
				cache: false,
				success: function(msg){
					$("#KodeDusun").html(msg);
				}
			});
		  });
		});
	</script>
	
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#time1').Zebra_DatePicker({format: 'Y'});
			$('#time2').Zebra_DatePicker({format: 'Y-m-d'});
			$('#time7').Zebra_DatePicker({format: 'Y-m-d'});
			//$('#Datetime2').Zebra_DatePicker({format: 'Y-m-d H:i', direction: 1});
		});
	</script>
	
	<?php
	if(isset($_POST['Simpan'])){
		echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		
		/* include ('../library/kode-log-server.php');
		//kode jabatan
		$sql_jbt = mysqli_query($koneksi,'SELECT RIGHT(KodeJabatan,5) AS kode FROM mstjabatan WHERE KodeLokasi="'.$login_lokasi.'" ORDER BY KodeJabatan DESC LIMIT 1');  
		$nums_jbt = mysqli_num_rows($sql_jbt);
		 
		if($nums_jbt <> 0)
		 {
		 $data_jbt = mysqli_fetch_array($sql_jbt);
		 $kode_jbt = $data_jbt['kode'] + 1;
		 }else
		 {
		 $kode_jbt = 1;
		 }
		 
		//mulai bikin kode
		 $bikin_kode_jbt = str_pad($kode_jbt, 5, "0", STR_PAD_LEFT);
		 $kode_jadi_jbt = "JBT-".$bikin_kode_jbt;
		
		$query = mysqli_query($koneksi,"INSERT into mstjabatan (KodeJabatan,NamaJabatan,Keterangan,KodeLokasi) 
		VALUES ('$kode_jadi_jbt','".$_POST['Nama']."','".$_POST['Keterangan']."','$login_lokasi')");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Master Jabatan : ".$_POST['Nama']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Simpan Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "MasterJabatan.php";
				  });
				  </script>';
		} */
	}
	
	if(isset($_POST['SimpanEdit'])){
		/* include ('../library/kode-log-server.php'); */
		$nik = htmlspecialchars($_POST['NIK']); $lokasi = htmlspecialchars($_POST['KodeLokasi']); 
		$nama = htmlspecialchars($_POST['Nama']); $tlahir = htmlspecialchars($_POST['TempatLahir']); $tgllahir = $_POST['TanggalLahir']; $jk = htmlspecialchars($_POST['JenisKelamin']); $agama = htmlspecialchars($_POST['Agama']); $stats = htmlspecialchars($_POST['StatusPenduduk']);
		
		$ayah = htmlspecialchars($_POST['NamaAyah']); $ibu = htmlspecialchars($_POST['NamaIbu']); $warga = htmlspecialchars($_POST['Kewarganegaraan']); $statskel = htmlspecialchars($_POST['StatusKeluarga']); $statskaw = htmlspecialchars($_POST['StatusPerkawinan']); $goldarah = htmlspecialchars($_POST['GolDarah']);
		
		$nokk = htmlspecialchars($_POST['NoKK']); $nourut = htmlspecialchars($_POST['NoUrutDiKK']); $noakte = htmlspecialchars($_POST['NoAkteLahir']); $tglakte = $_POST['TanggalAkteLahir']; $didik = htmlspecialchars($_POST['Pendidikan']); $kerja = htmlspecialchars($_POST['Pekerjaan']); $kerja2 = htmlspecialchars($_POST['PekerjaanTambahan']);
		
		$dusun = htmlspecialchars($_POST['KodeDusun']); $rt = htmlspecialchars($_POST['RT']); $rw = htmlspecialchars($_POST['RW']); $jalan = $_POST['NamaJalan']; $norumah = htmlspecialchars($_POST['NoRumah']); $alamat = htmlspecialchars($_POST['Alamat']); 
		
		$query = mysqli_query($koneksi,"UPDATE datapenduduk SET Nama = '$nama', TempatLahir = '$tlahir', TanggalLahir = '$tgllahir', JenisKelamin = '$jk', Agama = '$agama', StatusPenduduk = '$stats', NamaAyah = '$ayah', NamaIbu = '$ibu', WNI = '$warga', StatusKeluarga = '$statskel', StatusPerkawinan = '$statskaw', GolDarah = '$goldarah', NoKK = '$nokk', NoUrutKK = '$nourut', NoAkteLahir = '$noakte', TanggalAkteLahir = '$tglakte', KodePendidikan = '$didik', KodePekerjaan = '$kerja', PekerjaanTambahan = '$kerja2', KodeDusun = '$dusun', RT = '$rt', RW = '$rw', NamaJalan = '$jalan', NoRumah = '$norumah', Alamat = '$alamat' WHERE KodeLokasi = '$lokasi' AND NIK = '$nik'");
		
		if($query){
			echo '<script language="javascript">document.location="DataPenduduk.php";</script>';
		}else{
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Edit Data Gagal!",
					text: " ",
					type: "error"
				  },
				  function () {
					window.location.href = "DataPenduduk.php";
				  });
				  </script>';
		}
	}
	
	if(base64_decode(@$_GET['aksi'])=='Hapus'){
		include ('../library/kode-log-server.php');
		$query = mysqli_query($koneksi,"DELETE FROM mstjabatan WHERE KodeJabatan='".base64_decode($_GET['id'])."' AND KodeLokasi='$login_lokasi'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeLokasi) 
			VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Master Jabatan : ".$_GET['nm']."','$login_id','$login_lokasi')");
			echo '<script language="javascript">document.location="MasterJabatan.php"; </script>';
		}else{
			echo '<script type="text/javascript">
					  sweetAlert({
						title: "Hapus Data Gagal!",
						text: " Hapus data di Master Aparatur terlebih dahulu! ",
						type: "error"
					  },
					  function () {
						window.location.href = "MasterJabatan.php";
					  });
					  </script>';
		}
	}
	
	?>
  </body>
</html>