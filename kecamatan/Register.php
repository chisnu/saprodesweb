<?php
include 'akses.php';
include '../library/tgl-indo.php';

$fitur_id = 17;
include '../library/lock-menu.php';

$Page = 'Register';
$Tahun=date('Y');
$HariIni=date('Y-m-d H:i:s');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include 'title.php';?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../komponen/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../komponen/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../komponen/css/style.red.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../komponen/css/custom.css">
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<style>
		 th {
			text-align: center;
		}
	</style>
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda yakin menghapus data ini ?")
			if (answer == true){
				window.location = "LayananSurat.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <?php include 'header.php';?>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include 'menu.php';?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Register</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
         <section class="tables"> 
            <div class="container-fluid">
                <div class="col-lg-12">
				  <div class="card">
					<div class="card-header d-flex align-items-center">
					  <h3 class="h4">Data Register Permohonan</h3>
					</div>
					<div class="card-body">							  
						<div class="col-lg-4 offset-lg-8">
							<form method="post" action="">
								<div class="form-group input-group">						
									<input type="text" name="keyword" class="form-control" placeholder="Nama Surat" value="<?php echo @$_REQUEST['keyword']; ?>">
									<span class="input-group-btn">
										<button class="btn btn-primary" type="submit">Cari</button>
									</span>
								</div>
							</form>
						</div>
					  <div class="table-responsive">  
						<table class="table table-striped">
						  <thead>
							<tr>
							  <th>No</th>
							  <!-- <th>Tgl. Transaksi</th> -->
							  <th>Nama Surat</th>
							  <!-- <th>Estimasi Selesai</th> -->
							  <th>Status Permohonan</th>
							  <th>Keterangan</th>
							  <th>Aksi</th>
							</tr>
						  </thead>
						<?php 
							//procedure paging
							include '../library/pagination1.php';
							// mengatur variabel reload dan sql
							$kosong=null;
							if((isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>"")){
								// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
								$keyword= @$_REQUEST['keyword'];
								$reload = "Register.php?pagination=true&keyword=$keyword";
								$sql =  "SELECT a.NoTrMohon, a.TglPermohonan, a.Keterangan1, a.JenisSurat, a.KodeLokasi, a.IDPend, a.StatusPermohonan, b.Keterangan, 
								a.PrediksiTglSelesai FROM trpermohonanmasy a JOIN masterpengurusansurat b ON a.JenisSurat=b.JenisSurat JOIN progresssurat c ON c.NoTrMohon = a.NotrMohon AND c.KodeLokasi = a.KodeLokasi WHERE (a.StatusPermohonan='REJECTED' OR a.StatusPermohonan='DONE') AND b.Keterangan LIKE '%$keyword%' AND LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."' AND c.StatusProgress = 'Front Office (KEC)' GROUP BY a.NoTrMohon ORDER BY a.TglPermohonan DESC";
								$result = mysqli_query($koneksi, $sql);
							}else{
								//jika tidak ada pencarian pakai ini
								$reload = "Register.php?pagination=true";
								$sql =  "SELECT a.NoTrMohon, a.TglPermohonan, a.Keterangan1, a.JenisSurat, a.KodeLokasi, a.IDPend, a.StatusPermohonan, b.Keterangan, 
								a.PrediksiTglSelesai FROM trpermohonanmasy a JOIN masterpengurusansurat b ON a.JenisSurat=b.JenisSurat JOIN progresssurat c ON c.NoTrMohon = a.NotrMohon AND c.KodeLokasi = a.KodeLokasi WHERE (a.StatusPermohonan='REJECTED' OR a.StatusPermohonan='DONE') AND LEFT(a.KodeLokasi,3)='".substr($login_lokasi,0,3)."' AND c.StatusProgress = 'Front Office (KEC)' GROUP BY a.NoTrMohon ORDER BY a.TglPermohonan DESC";
								$result = mysqli_query($koneksi, $sql);
							}
						
							//pagination config start
							$rpp = 10; // jumlah record per halaman
							$page = intval(@$_GET["page"]);
							if($page<=0) $page = 1;  
							$tcount = mysqli_num_rows($result);
							$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
							$count = 0;
							$i = ($page-1)*$rpp;
							$no_urut = ($page-1)*$rpp;
							//pagination config end	
							
							?>
							
							<tbody>
							<?php if($tcount == null OR $tcount === 0){
								echo '<tr class="odd gradeX"><td colspan="6" align="center"><br><h5>Tidak Ada Data</h5><br></td></tr>';
							} else {
							while(($count<$rpp) && ($i<$tcount)) {
								mysqli_data_seek($result,$i);
							$data = mysqli_fetch_array($result);
							?>
							<tr class="odd gradeX">
								<td width="50px">
									<?php echo ++$no_urut;?> 
								</td>
								<!-- <td align="center">
									<?php /* echo TanggalIndo($data['TglPermohonan']); */?>
								</td> -->
								<td>
									<?php echo $data['Keterangan']." atas user :<br>";
									// cari user
									$CariData = @mysqli_query($koneksi, "SELECT Nama, NIK FROM datapenduduk WHERE IDPend = '".$data['IDPend']."'"); 
									$array = @mysqli_fetch_array($CariData);
									echo "<strong>".$array['NIK']."</strong> : ".$array['Nama']."<br>";
									echo "Pengajuan : ".$data['NoTrMohon']."<br>";
									echo "Pada : ".TanggalIndo($data['TglPermohonan'])."<br>";
									?>
								</td>
								<!-- <td align="center">
									<?php /* echo TanggalIndo($data['PrediksiTglSelesai']); */?>
								</td> -->
								<td>
									<?php 
									if($data['StatusPermohonan']=='WAITING'){
										echo "<font color='#f39c12'>Menunggu</font>";
									}elseif($data['StatusPermohonan']=='ON PROGRESS'){
										echo "<font color='#2ecc71'>Berkas Diproses</font>";
									}elseif($data['StatusPermohonan']=='REJECTED'){
										echo "<font color='#e74c3c'>Gagal Terbit</font>";
									}elseif($data['StatusPermohonan']=='DONE'){
										echo "<font color='#3498db'>Selesai</font>";
									}
									
									$CariData2 = @mysqli_query($koneksi, "SELECT Tanggal, NoManualSurat FROM trpengurusansurat WHERE KodeLokasi = '".$data['KodeLokasi']."' AND NoTrMohon = '".$data['NoTrMohon']."'"); 
									$array2 = @mysqli_fetch_array($CariData2);
									echo "<br>Pada : ".TanggalIndo($array2['Tanggal']);
									echo "<br>No Surat : ".$array2['NoManualSurat'];
									?>
								</td>
								<td>
									<?php if($data['StatusPermohonan']=='REJECTED'){
										$CariData3 = @mysqli_query($koneksi, "SELECT Keterangan FROM trpengurusansurat WHERE NoTrMohon='".$data['NoTrMohon']."' AND KodeLokasi='".$data['KodeLokasi']."' AND JenisSurat='".$data['JenisSurat']."'"); 
										$array3 = @mysqli_fetch_array($CariData3);
										echo ucwords($array3['Keterangan']);
									} else {
										echo '-';
									}
									
									?>
								</td>
								<td align="right">
									<?php if($data['StatusPermohonan']=='WAITING'){ 
										echo '<a href="LayananSurat.php?kd='.base64_encode($data['NoTrMohon']).'&aksi='.base64_encode('HapusTransaksi').'" onclick="return confirmation()">
											<span class="btn btn-sm btn-danger fa fa-close" title="Hapus Surat"></span></a>';
									}elseif($data['StatusPermohonan']=='ON PROGRESS'){
										echo '
										<a href="#" class="open_modal_tracking" data-notrmohon="'.$data['NoTrMohon'].'" data-kodelokasi="'.$data['KodeLokasi'].'" data-idpenduduk="'.$data['IDPend'].'" data-surat="'.$data['JenisSurat'].'" data-ket="'.$data['Keterangan'].'" data-user="'.$login_id.'">
										<span class="btn btn-sm btn-primary fa fa-share-alt" title="Lihat Progress Surat"></span></a>';
									}elseif($data['StatusPermohonan']=='REJECTED'){
										echo '
										<a href="#" class="open_modal_tracking" data-notrmohon="'.$data['NoTrMohon'].'" data-kodelokasi="'.$data['KodeLokasi'].'" data-idpenduduk="'.$data['IDPend'].'" data-surat="'.$data['JenisSurat'].'" data-ket="'.$data['Keterangan'].'" data-user="'.$login_id.'">
										<span class="btn btn-sm btn-primary fa fa-share-alt" title="Lihat Progress Surat"></span></a>';
									}elseif($data['StatusPermohonan']=='DONE'){
										echo '&nbsp;<a href="ViewPrint.php?kode='.base64_encode($data['NoTrMohon']).'&loc='.base64_encode($data['KodeLokasi']).'&jns='.base64_encode($data['JenisSurat']).'" target="_blank">
										<span class="btn btn-sm btn-warning fa fa-print" title="Preview"></span></a>'; 
										echo '
										<a href="#" class="open_modal_tracking" data-notrmohon="'.$data['NoTrMohon'].'" data-kodelokasi="'.$data['KodeLokasi'].'" data-idpenduduk="'.$data['IDPend'].'" data-surat="'.$data['JenisSurat'].'" data-ket="'.$data['Keterangan'].'" data-user="'.$login_id.'">
										<span class="btn btn-sm btn-primary fa fa-share-alt" title="Lihat Progress Surat"></span></a>';
									}
									?>
								</td>
							</tr>
							<?php
								$i++; 
								$count++;
							}
							}
							?>
							</tbody>
						</table>
						<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
					  </div>
					</div>
                  </div>
                </div>
            </div>
          </section> 
        </div>
      </div>
    </div>
	<div id="ModalEditTracking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
    <!-- JavaScript files-->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>
    <script src="../komponen/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../komponen/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../komponen/vendor/chart.js/Chart.min.js"></script>
    <script src="../komponen/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../komponen/js/charts-home.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="../komponen/js/front.js"></script>
	<!-- Javascript untuk popup modal Edit--> 
	<script type="text/javascript">
			// open modal lihat progress
	   $(document).ready(function () {
	   $(".open_modal_tracking").click(function(e) {
		  var no_mohon = $(this).data("notrmohon");
		  var kode_lok = $(this).data("kodelokasi");
		  var id_pend  = $(this).data("idpenduduk");
		  var jenis_surat  = $(this).data("surat");
		  var ket_surat  = $(this).data("ket");
		  var user  = $(this).data("user");
		  	   $.ajax({
					   url: "TrackingDokumen.php",
					   type: "GET",
					   data : {NoTrMohon: no_mohon,KodeLokasi: kode_lok,IDPend: id_pend,JenisSurat: jenis_surat,KetSurat: ket_surat,User: user},
					   success: function (ajaxData){
					   $("#ModalEditTracking").html(ajaxData);
					   $("#ModalEditTracking").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
  </body>
</html>