<?php 
date_default_timezone_set('Asia/Jakarta');
require "../library/fpdf/fpdf.php";  // pastikan path atau alamat FPDF sesuai

// fungsi hitung umur
function hitung_umur($tanggal_lahir){
	list($year,$month,$day) = explode("-",$tanggal_lahir);
	$year_diff = date("Y") - $year;
	$month_diff = date("m") - $month;
	$day_diff = date("d") - $day;
	if($month_diff < 0) $year_diff--;
		elseif(($month_diff == 0) && ($day_diff < 0)) $year_diff--;
	return $year_diff;
}

// pendefinisian folder font pada FPDF
define('FPDF_FONTPATH', '../library/fpdf/font/');
// seperti sebelunya, kita membuat class anakan dari class FPDF
class FPDF_AutoWrapTable extends FPDF {
	private $data = array();
  	private $options = array( 'filename' => '', 'destinationfile' => '', 'paper_size'=>'f4', 'orientation'=>'p' );
	
	function __construct($data = array(), $options = array()) {
    	parent::__construct();
    	$this->data = $data;
    	$this->options = $options;
	}
	
	function Footer(){
	} 
 
	public function rptDetailData () {
		include "../library/config.php";
		include "../library/tgl-indo.php";
		include "../library/terbilang.php";
		
		//get variable
		$kode = base64_decode(@$_GET['kode']); $jenis = base64_decode(@$_GET['jns']); $lokasi = base64_decode(@$_GET['loc']);
		
		$this->AddPage();
		$this->SetAutoPageBreak(true,30);
		$this->AliasNbPages();
		
		// cek jenis surat 
		if($jenis === 'SIUM'){
			// header
			$this->SetMargins(36,33,34,55);
			$this->Image('../images/Assets/logo.png',35,30,22.5); // logo
			$this->SetFont('Times','B','15'); $this->SetTextColor(0,0,0); // warna tulisan
			// font yang digunakan // membuat cell dg panjang 19 dan align center 'C'
			$cari = @mysqli_query($koneksi, "Select AlamatLokasi from mstlokasi WHERE KodeLokasi = '001-000'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $alamat = explode("#", $tampil['AlamatLokasi']); }
			
			$this->Ln(0); $this->Cell(170,48,$alamat[0],0,0,'C');
			$this->Ln(30); $this->Cell(170,0,$alamat[1],0,0,'C');
			
			$this->SetFont('Times','B','13'); $this->Ln(0); $this->Cell(170,11,$alamat[2],0,0,'C');
			$this->Ln(0); $this->SetFont('Times','','12'); $this->Cell(170,22.5,$alamat[3],0,0,'C');
			$this->Ln(0); $this->Cell(170,32.5,$alamat[4],0,0,'C');
			
			$this->Ln(20); $this->SetLineWidth(0.75); $this->Cell(145,0,'','TB',0,'C',1);
			$this->Ln(1); $this->SetLineWidth(0.1); $this->Cell(145,0,'','TB',0,'L',1);
		} else {
			//header
			$this->SetMargins(30,20,20,20);
			$this->Image('../images/Assets/logo.png',30,17.5,21); // logo
			$this->SetFont('Times','B','15'); $this->SetTextColor(0,0,0); // warna tulisan
			// font yang digunakan // membuat cell dg panjang 19 dan align center 'C'
			$cari = @mysqli_query($koneksi, "Select mstdesa.NamaDesa, mstlokasi.AlamatLokasi, mstlokasi.IsKelurahan from mstdesa inner join mstlokasi on mstlokasi.KodeDesa = mstdesa.KodeDesa WHERE mstlokasi.KodeLokasi='".$lokasi."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ $namadesa = $tampil['NamaDesa']; $alamat = explode("#", $tampil['AlamatLokasi']); $is_lurah = $tampil['IsKelurahan']; }
			
			$this->Ln(0); $this->Cell(185,23.5,$alamat[0],0,0,'C');
			$this->Ln(18.5); $this->Cell(185,0,$alamat[1],0,0,'C');
				
			/* if($is_lurah === '1'){
				$this->SetFont('Times','B','15'); $this->Ln(0); $this->Cell(190,13.5,'KELURAHAN '.$namadesa,0,0,'C');
			} else {
				$this->SetFont('Times','B','15'); $this->Ln(0); $this->Cell(190,13.5,'DESA '.$namadesa,0,0,'C');
			} */
			
			$this->SetFont('Times','B','15'); $this->Ln(0); $this->Cell(185,13.5,$alamat[2],0,0,'C');
			$this->Ln(12.5); $this->SetFont('Times','','11'); $this->Cell(185,0,$alamat[3],0,0,'C');
			
			$this->Ln(5); $this->SetLineWidth(0.75); $this->Cell(165,0,'','TB',0,'C',1);
			$this->Ln(1); $this->SetLineWidth(0.1); $this->Cell(165,0,'','TB',0,'L',1);
		}
		
		
		if($jenis === 'SIUM') {
			$this->Ln(5); $this->SetFont('Times','BU','13'); $this->Cell(150,0,'SURAT IZIN USAHA MIKRO',0,0,'C');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ 
				$Status = $tampil['StatusSurat']; 
				$Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); 
				$Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); 
				$Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; $IsTTD = $tampil['IsAtasNamaTTD']; 
				$bagiTTD = explode("#", $tampil['Penandatanganan']); 
			}
			
			$this->Ln(5); $this->SetFont('Times','B','11');
			if($Status === '1') {
				if($NoManual == null){
					$this->Cell(150,0,'Nomor : ',0,0,'C');
				} else {
					$this->Cell(150,0,'Nomor : '.$NoManual,0,0,'C');
				}
			} else {
				$this->Cell(150,0,'Nomor : ',0,0,'C');
			}
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$this->Ln(5); $this->MultiCell(0,5,$Keperluan,0,'J',0,14); 
			
			// data pemohon
			$this->Ln(1); $this->SetWidths(array(50,5,90)); $this->SetAligns(array('L','J','L'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT Suport1 FROM trpengurusansurat where NoTrMohon = '".$kode."' AND KodeLokasi = '".$lokasi."' AND JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $pecah = explode("#", $arrayPenduduk['Suport1']); }
			$this->SetFont('Times','','11');
			$this->Row( array( "Nama", ":", strtoupper($pecah[0]) ) );
			$this->Row( array( "Nomor KTP", ":", $pecah[1]) );
			$this->Row( array( "Alamat", ":", ucwords(strtolower($pecah[2])) ) );
			$this->Row( array( "Nomor Telepon", ":", $pecah[3] ) );
			
			$this->Ln(1); $this->MultiCell(0,5,$Uraian,0,'J',0,14); 
			
			// data surat
			$this->Ln(1); $this->SetWidths(array(50,5,90)); $this->SetAligns(array('L','J','L'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT Suport2 FROM trpengurusansurat where NoTrMohon = '".$kode."' AND KodeLokasi = '".$lokasi."' AND JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $pecah2 = explode("#", $arrayPenduduk['Suport2']); }
			$this->SetFont('Times','','11');
			$this->Row( array( "Nama Perusahaan", ":", strtoupper($pecah2[0]) ) );
			$this->Row( array( "Bentuk Perusahaan", ":", ucwords(strtolower($pecah2[1])) ) );
			$this->Row( array( "Kegiatan Usaha", ":", ucwords(strtolower($pecah2[2])) ) );
			$this->Row( array( "Sarana Usaha yang Digunakan", ":", ucwords(strtolower($pecah2[3])) ) );
			$this->Row( array( "Alamat Usaha", ":", ucwords(strtolower($pecah2[4])) ) );
			$this->Row( array( "Jumlah Modal Usaha", ":", "Rp. ".str_replace(",", ".", number_format($pecah2[5])).",-  (".ucwords(terbilang($pecah2[5]))." Rupiah)" ) );
			$this->Row( array( "Nomor Pendaftaran", ":", $pecah2[6] ) );
			
			// baris tanda tangan
			$this->Ln(1);
			$this->SetWidths(array(60,15,25,45)); $this->SetAligns(array('L','R','L','L','L'));
			if($Status === '1'){
				$this->Row( array( "", "", "Ditetapkan di", ": Nganjuk" ) );   
				$this->Ln(-2.5); $this->Row( array( "", "", "Pada Tanggal", ": ".TanggalIndo($Tgl) ) );
			} else {
				$this->Row( array( "", "", "Ditetapkan di", ": ... " ) );   
				$this->Ln(-2.5); $this->Row( array( "", "", "Pada Tanggal", ": ..." ) );   
			}
			$this->Ln(-1); $this->Cell(75,0,'','',0,'L',0); $this->SetLineWidth(0.1); $this->Cell(60,0,'','',0,'L',1);
			$this->Ln(0); $this->SetWidths(array(60,15,65,5)); $this->SetAligns(array('L','R','L','L'));
			
			$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, a.PangkatGol, b.NamaJabatan from mstaparat a join mstjabatan b on (b.KodeJabatan,b.KodeLokasi) = (a.KodeJabatan,a.KodeLokasi) where b.KodeLokasi = '001-000' AND b.KodeJabatan ='".$bagiTTD[0]."' AND a.KodeAparat='".$bagiTTD[1]."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ 
				$camat = $arrayAparat['NamaAparat']; $gol = $arrayAparat['PangkatGol']; $nip = $arrayAparat['NIP']; $jabat = $arrayAparat['NamaJabatan']; 
			}
			if($IsTTD === '1'){
				$this->Row( array( "", "a.n.", $jabat, "" ) );
			} else {
				$this->Row( array( "", "", $jabat, "" ) );
			}
			
			$this->Ln(12.5);
			if($IsTTD === '1'){
				$this->SetFont('Times','BU','11');
				$this->Ln(0); $this->Row( array( "", "", "_______________", "" ) );
			} else {
				$this->SetFont('Times','BU','11');
				$this->Ln(0); $this->Row( array( "", "", ucwords($camat), "" ) );
				$this->SetFont('Times','','11');
				$this->Ln(-3); $this->Row( array( "", "", $gol, "" ) );
				$this->Ln(-3); $this->Row( array( "", "", "NIP : ".$nip, "" ) );
			}
			
		} elseif($jenis === 'SKAW'){
			$this->Ln(5); $this->SetFont('Times','BU','13'); $this->Cell(165,0,'SURAT KETERANGAN AHLI WARIS',0,0,'C');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ 
				$Status = $tampil['StatusSurat']; 
				$Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); 
				$Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); 
				$Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; $IsTTD = $tampil['IsAtasNamaTTD'];
				$bagiTTD = explode("#", $tampil['Penandatanganan']); 
			}
			
			// baris pendahuluan
			$this->SetFont('Times','','11'); $this->Ln(5); $this->MultiCell(0,7,$Keperluan,0,'J',0,14); 
			
			// baris isi
			$this->Ln(1);
			$this->SetWidths(array(7.5,157.5)); $this->SetAligns(array('L','J'));
			$Support = "Suport"; $No = 0; $Total = 20;
			for($i=$No;$i<$Total;$i++){
				$ke = $i+1; $urut = $i+1;
				$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
				$arrayHasil = @mysqli_fetch_array($cek);
				if($arrayHasil[$Support.$ke] != null){
					$bagi = explode("#",$arrayHasil[$Support.$ke]);
					$this->SetFont('Times','','11');
					$this->Ln(0);
					$this->Row( array( $urut.".", strtoupper($bagi[0]).". ".ucwords($bagi[2]).", Jenis Kelamin : ".ucwords(strtolower($bagi[1])).", Tempat/Tanggal Lahir : ".ucwords(strtolower($bagi[3])).", ".TanggalIndo($bagi[4]).", Pekerjaan : ".ucwords(strtolower($bagi[5])).", Alamat : ".ucwords(strtolower($bagi[6])) ) );
				}
			}
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(1); $this->MultiCell(0,7,$Uraian,0,'J',0,14); 
			
			// baris tanda tangan
			$this->Ln(2.5);
			/* $this->SetWidths(array(5,80,40,40,0)); $this->SetAligns(array('J','L','J','L','J'));
			$this->Row( array( "", ucwords(strtolower($namadesa)).", ".TanggalIndo($Tgl), "", "", "" ) ); 
			$this->Ln(-2.5); $this->Row( array( "", "Kami para ahli waris,", "", "", "" ) );   
				$Support = "Suport"; $No = 0; $Total = 20;
				for($i=$No;$i<$Total;$i++){
					$ke = $i+1; $urut = $i+1;
					$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
					$arrayHasil = @mysqli_fetch_array($cek);
					if($arrayHasil[$Support.$ke] != null){
						$bagi = explode("#",$arrayHasil[$Support.$ke]);
						$this->SetFont('Times','','11');
						$this->Ln(0);
						if($urut % 2 == 0){
							$this->Row( array( "", $urut.". ".ucwords(strtolower($bagi[0])), "", $urut.". _______________", "" ) );
						} else {
							$this->Row( array( "", $urut.". ".ucwords(strtolower($bagi[0])), $urut.". _______________", "", "" ) );
						}
					}
				} */
			$this->SetWidths(array(5,70,10,70,10)); $this->SetAligns(array('J','L','R','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Tanggal, NoManualSurat2, Tanggal2, StatusSurat, Barcode from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$this->Ln(2.5);
				if($arraySah['StatusSurat'] === '1'){
					if($arraySah['Tanggal'] == null){
						$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ... ", "" ) );  
					} else {
						$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ".TanggalIndo($arraySah['Tanggal']), "" ) );
					}
				} elseif($arraySah['StatusSurat'] === '2'){
					if($arraySah['Tanggal2'] == null AND $arraySah['Tanggal'] == null){
						$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ... ", "" ) );  
					} elseif($arraySah['Tanggal2'] == null){
						$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ".TanggalIndo($arraySah['Tanggal']), "" ) );  
					} else {
						$this->Row( array( "", "Nganjuk, ".TanggalIndo($arraySah['Tanggal2']), "", "Nganjuk, ".TanggalIndo($arraySah['Tanggal']), "" ) );
					}
				} else {
					$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ... ", "" ) );   
				}
				
				$this->Ln(-2.5);
				if($arraySah['StatusSurat'] === '1') {
					if($arraySah['NoManualSurat'] == null){
						$this->Row( array( "", "Nomor : ", "", "Nomor : ", "" ) );  
					} else {
						$this->Row( array( "", "Nomor : ", "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					}
				} elseif($arraySah['StatusSurat'] === '2') {
					if($arraySah['NoManualSurat2'] == null AND $arraySah['NoManualSurat'] == null){
						$this->Row( array( "", "Nomor : ", "", "Nomor : ", "" ) );  
					} elseif($arraySah['NoManualSurat2'] == null){
						$this->Row( array( "", "Nomor : ", "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					} else {
						$this->Row( array( "", "Nomor : ".$arraySah['NoManualSurat2'], "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					}
				} else {
					$this->Row( array( "", "Nomor : ", "", "Nomor : ", "" ) ); 
				}
				
				$this->Ln(2.5);
				$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, a.PangkatGol, b.NamaJabatan from mstaparat a join mstjabatan b on (b.KodeJabatan,b.KodeLokasi) = (a.KodeJabatan,a.KodeLokasi) where b.KodeLokasi = '".$lokasi."' AND b.KodeJabatan ='".$bagiTTD[0]."' AND a.KodeAparat='".$bagiTTD[1]."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ 
					$kades = $arrayAparat['NamaAparat']; $gol = $arrayAparat['PangkatGol']; $nip = $arrayAparat['NIP']; $jabat = $arrayAparat['NamaJabatan']; 
				}
				$cek_aparat2 = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, a.PangkatGol, b.NamaJabatan from mstaparat a join mstjabatan b on (b.KodeJabatan,b.KodeLokasi) = (a.KodeJabatan,a.KodeLokasi) where b.KodeLokasi = '001-000' AND b.KodeJabatan ='".$bagiTTD[2]."' AND a.KodeAparat='".$bagiTTD[3]."'"); while($arrayAparat2 = @mysqli_fetch_array($cek_aparat2)){ 
					$camat = $arrayAparat2['NamaAparat']; $gol2 = $arrayAparat2['PangkatGol']; $nip2 = $arrayAparat2['NIP']; $jabat2 = $arrayAparat2['NamaJabatan']; 
				}
				if($arraySah['StatusSurat'] === '2'){ 
					if($arraySah['Barcode'] == null){
						$gambar = "";
						if($is_lurah === '1'){
							if($IsTTD === '1'){
								$this->Ln(-2); $this->Row( array( "", $jabat2, "a.n.", $jabat." ".$namadesa, "" ) );
							} else {
								$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
							}
						} else {
							if($IsTTD === '1'){
								$this->Ln(-2); $this->Row( array( "", $jabat2, "a.n.", $jabat." ".$namadesa, "" ) );
							} else {
								$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
							}
						}
						$this->Ln(-2); $this->Row( array( "", "*) Telah ditandatangani secara elektronik.", "", "", "" ) );
					} else {
						$gambar = $this->Image($arraySah['Barcode'], 35, $this->GetY()+10, 30);
						if($is_lurah === '1'){
							if($IsTTD === '1'){
								$this->Ln(-2); $this->Row( array( "", $jabat2, "a.n.", $jabat." ".$namadesa, "" ) );
							} else {
								$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
							}
						} else {
							if($IsTTD === '1'){
								$this->Ln(-2); $this->Row( array( "", $jabat2, "a.n.", $jabat." ".$namadesa, "" ) );
							} else {
								$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
							}
						}
						$this->Ln(-2); $this->Row( array( "", "*) Telah ditandatangani secara elektronik.", "", "", "" ) );
						$this->Row( array( "", @$gambar, "", "", "" ) );
					}
				} else {
					$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
				}
			}
			   
			$this->Ln(7.5);
			if($IsTTD === '1'){
				$this->SetFont('Times','BU','11');
				$this->Ln(0); $this->Row( array( "", "", "", "_______________", "" ) );
			} else {
				$this->SetFont('Times','BU','11');
				$this->Ln(0); $this->Row( array( "", "", "", $kades, "" ) );
				$this->SetFont('Times','','11');
				$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip, "" ) );
			}
			
		}  elseif($jenis === 'SKPW'){
			$this->Ln(5); $this->SetFont('Times','BU','13'); $this->Cell(165,0,'SURAT KETERANGAN PERWALIAN',0,0,'C');
			$cari = @mysqli_query($koneksi, "Select * from trpengurusansurat WHERE NoTrMohon='".$kode."' AND KodeLokasi='".$lokasi."' AND JenisSurat='".$jenis."'") or die(@mysqli_error($cari));
			while($tampil = @mysqli_fetch_array($cari)){ 
				$Status = $tampil['StatusSurat']; 
				$Keperluan = @str_replace("&nbsp;", " ", @strip_tags($tampil['Keperluan'])); 
				$Uraian = @str_replace("&nbsp;", " ", @strip_tags($tampil['Uraian'])); 
				$Tgl = $tampil['Tanggal']; $NoManual = $tampil['NoManualSurat']; $IsTTD = $tampil['IsAtasNamaTTD'];
				$bagiTTD = explode("#", $tampil['Penandatanganan']); 
			}
			
			// baris pendahuluan
			$this->SetFont('Times','','11');
			$this->Ln(5); $this->MultiCell(0,7,"Yang bertanda tangan di bawah ini :",0,'J',0,14);
			$this->Ln(1); $this->SetWidths(array(5,40,5,110,5)); $this->SetAligns(array('J','L','J','L','J'));
			$cek_penduduk = @mysqli_query($koneksi, "SELECT a.NIK, a.Nama, a.TempatLahir, a.TanggalLahir, a.JenisKelamin, a.Agama, a.KodePekerjaan, a.KodePendidikan, a.Alamat, a.WNI, a.StatusPerkawinan from datapenduduk a join trpermohonanmasy b on (b.KodeLokasi,b.IDPend) = (a.KodeLokasi,a.IDPend) where b.NoTrMohon = '".$kode."' AND b.KodeLokasi = '".$lokasi."' AND b.JenisSurat = '".$jenis."'"); 
			while($arrayPenduduk = @mysqli_fetch_array($cek_penduduk)){ $nama = $arrayPenduduk['Nama']; $tempatlhr = $arrayPenduduk['TempatLahir']; $tgl = $arrayPenduduk['TanggalLahir']; $jk = $arrayPenduduk['JenisKelamin']; $agama = $arrayPenduduk['Agama']; $kawin = $arrayPenduduk['StatusPerkawinan']; $kerja = $arrayPenduduk['KodePekerjaan']; $didik = $arrayPenduduk['KodePendidikan']; $alamat = $arrayPenduduk['Alamat']; $wni = $arrayPenduduk['WNI']; $nik = $arrayPenduduk['NIK']; }
			
			$this->Row( array( "", "Nama", ":", strtoupper($nama), "" ) );
			$menu1 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='JNS_KEL' AND NoUrut='".$jk."'");
			while($kode1 = mysqli_fetch_array($menu1)){
				$this->Row( array( "", "Jenis Kelamin", ":", ucwords(strtolower($kode1['Uraian'])), "" ) );
			}
			$this->Row( array( "", "Tempat/Tanggal Lahir", ":", ucwords(strtolower($tempatlhr)).", ".TanggalIndo($tgl)." (".hitung_umur($tgl)." th)", "" ) );
			if($wni == null OR $wni === ''){
				$this->Row( array( "", "Kewarganegaraan", ":", "-", "" ) );
			} else {
				$menu2 = mysqli_query($koneksi,"SELECT Uraian FROM mstlistdata WHERE JenisList='KEWARGANEGARAAN' AND NoUrut='".$wni."'");
				while($kode2 = mysqli_fetch_array($menu2)){
					$this->Row( array( "", "Kewarganegaraan", ":", ucwords($kode2['Uraian']), "" ) );
				}
			}
			if($kerja == null OR $kerja === ''){
				$this->Row( array( "", "Pekerjaan", ":", "-", "" ) );
			} else {
				$menu5 = mysqli_query($koneksi,"SELECT NamaPekerjaan from mstpekerjaan where KodePekerjaan = '".$kerja."'");
				while($kode5 = mysqli_fetch_array($menu5)){
					$this->Row( array( "", "Pekerjaan", ":", ucwords($kode5['NamaPekerjaan']), "" ) );
				}
			}
			if($alamat == null OR $alamat === ''){
				$this->Row( array( "", "Alamat", ":", "-", "" ) );
			} else {
				$this->Row( array( "", "Alamat", ":", ucwords($alamat), "" ) );
			}
			
			// baris isi
			$this->Ln(1); $this->MultiCell(0,7,$Keperluan,0,'J',0,0);
			$this->SetWidths(array(7.5,157.5)); $this->SetAligns(array('L','J'));
			$Support = "Suport"; $No = 1; $Total = 20;
			for($i=$No;$i<$Total;$i++){
				$ke = $i; $urut = $i;
				$cek = @mysqli_query($koneksi, "SELECT ".$Support.$ke." from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
				$arrayHasil = @mysqli_fetch_array($cek);
				if($arrayHasil[$Support.$ke] != null){
					$bagi = explode("#",$arrayHasil[$Support.$ke]);
					$this->SetFont('Times','','11');
					$this->Ln(0);
					$this->Row( array( $urut.".", strtoupper($bagi[0]).". ".ucwords($bagi[2]).", Jenis Kelamin : ".ucwords(strtolower($bagi[1])).", Tempat/Tanggal Lahir : ".ucwords(strtolower($bagi[3])).", ".TanggalIndo($bagi[4]).", Pekerjaan : ".ucwords(strtolower($bagi[5])).", Alamat : ".ucwords(strtolower($bagi[6])) ) );
				}
			}
			
			// baris penutup
			$this->SetFont('Times','','11');
			$this->Ln(1); $this->MultiCell(0,7,$Uraian,0,'J',0,14);
			
			// baris tanda tangan
			$this->Ln(2.5);
			$this->SetWidths(array(5,70,10,70,10)); $this->SetAligns(array('J','L','J','L','J'));
			$this->Row( array( "", ucwords(strtolower($namadesa)).", ".TanggalIndo($Tgl), "", "", "" ) ); 
			$this->Ln(-2); $this->Row( array( "", "Pemohon,", "", "", "" ) );   
			$this->Row( array( "", "1. ".ucwords(strtolower($nama)), "", "1. _______________", "" ) ); // baris tanda tangan 
			
			$this->SetWidths(array(5,70,10,70,10)); $this->SetAligns(array('J','L','R','L','J'));
			$cek_sah = @mysqli_query($koneksi, "SELECT NoManualSurat, Tanggal, NoManualSurat2, Tanggal2, StatusSurat, Barcode from trpengurusansurat where NoTrMohon='".$kode."' AND JenisSurat='".$jenis."' AND KodeLokasi='".$lokasi."'");
			while($arraySah = @mysqli_fetch_array($cek_sah)){
				$this->Ln(5);
				if($arraySah['StatusSurat'] === '1'){
					if($arraySah['Tanggal'] == null){
						$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ... ", "" ) );  
					} else {
						$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ".TanggalIndo($arraySah['Tanggal']), "" ) );
					}
				} elseif($arraySah['StatusSurat'] === '2'){
					if($arraySah['Tanggal2'] == null AND $arraySah['Tanggal'] == null){
						$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ... ", "" ) );  
					} elseif($arraySah['Tanggal2'] == null){
						$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ".TanggalIndo($arraySah['Tanggal']), "" ) );  
					} else {
						$this->Row( array( "", "Nganjuk, ".TanggalIndo($arraySah['Tanggal2']), "", "Nganjuk, ".TanggalIndo($arraySah['Tanggal']), "" ) );
					}
				} else {
					$this->Row( array( "", "Nganjuk, ... ", "", "Nganjuk, ... ", "" ) );   
				}
				
				$this->Ln(-2.5);
				if($arraySah['StatusSurat'] === '1') {
					if($arraySah['NoManualSurat'] == null){
						$this->Row( array( "", "Nomor : ", "", "Nomor : ", "" ) );  
					} else {
						$this->Row( array( "", "Nomor : ", "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					}
				} elseif($arraySah['StatusSurat'] === '2') {
					if($arraySah['NoManualSurat2'] == null AND $arraySah['NoManualSurat'] == null){
						$this->Row( array( "", "Nomor : ", "", "Nomor : ", "" ) );  
					} elseif($arraySah['NoManualSurat2'] == null){
						$this->Row( array( "", "Nomor : ", "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					} else {
						$this->Row( array( "", "Nomor : ".$arraySah['NoManualSurat2'], "", "Nomor : ".$arraySah['NoManualSurat'], "" ) );
					}
				} else {
					$this->Row( array( "", "Nomor : ", "", "Nomor : ", "" ) ); 
				}
				
				$this->Ln(2.5);
				$cek_aparat = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, a.PangkatGol, b.NamaJabatan from mstaparat a join mstjabatan b on (b.KodeJabatan,b.KodeLokasi) = (a.KodeJabatan,a.KodeLokasi) where b.KodeLokasi = '".$lokasi."' AND b.KodeJabatan ='".$bagiTTD[0]."' AND a.KodeAparat='".$bagiTTD[1]."'"); while($arrayAparat = @mysqli_fetch_array($cek_aparat)){ 
					$kades = $arrayAparat['NamaAparat']; $gol = $arrayAparat['PangkatGol']; $nip = $arrayAparat['NIP']; $jabat = $arrayAparat['NamaJabatan']; 
				}
				$cek_aparat2 = @mysqli_query($koneksi, "SELECT a.NamaAparat, a.NIP, a.PangkatGol, b.NamaJabatan from mstaparat a join mstjabatan b on (b.KodeJabatan,b.KodeLokasi) = (a.KodeJabatan,a.KodeLokasi) where b.KodeLokasi = '001-000' AND b.KodeJabatan ='".$bagiTTD[2]."' AND a.KodeAparat='".$bagiTTD[3]."'"); while($arrayAparat2 = @mysqli_fetch_array($cek_aparat2)){ 
					$camat = $arrayAparat2['NamaAparat']; $gol2 = $arrayAparat2['PangkatGol']; $nip2 = $arrayAparat2['NIP']; $jabat2 = $arrayAparat2['NamaJabatan']; 
				}
				if($arraySah['StatusSurat'] === '2'){ 
					if($arraySah['Barcode'] == null){
						$gambar = "";
						if($is_lurah === '1'){
							if($IsTTD === '1'){
								$this->Ln(-2); $this->Row( array( "", $jabat2, "a.n.", $jabat." ".$namadesa, "" ) );
							} else {
								$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
							}
						} else {
							if($IsTTD === '1'){
								$this->Ln(-2); $this->Row( array( "", $jabat2, "a.n.", $jabat." ".$namadesa, "" ) );
							} else {
								$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
							}
						}
						$this->Ln(-2); $this->Row( array( "", "*) Telah ditandatangani secara elektronik.", "", "", "" ) );
					} else {
						$gambar = $this->Image($arraySah['Barcode'], 35, $this->GetY()+10, 30);
						if($is_lurah === '1'){
							if($IsTTD === '1'){
								$this->Ln(-2); $this->Row( array( "", $jabat2, "a.n.", $jabat." ".$namadesa, "" ) );
							} else {
								$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
							}
						} else {
							if($IsTTD === '1'){
								$this->Ln(-2); $this->Row( array( "", $jabat2, "a.n.", $jabat." ".$namadesa, "" ) );
							} else {
								$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
							}
						}
						$this->Ln(-2); $this->Row( array( "", "*) Telah ditandatangani secara elektronik.", "", "", "" ) );
						$this->Row( array( "", @$gambar, "", "", "" ) );
					}
				} else {
					$this->Ln(-2); $this->Row( array( "", $jabat2, "", $jabat." ".$namadesa, "" ) );
				}
			}
			   
			$this->Ln(7.5);
			if($IsTTD === '1'){
				$this->SetFont('Times','BU','11');
				$this->Ln(0); $this->Row( array( "", "", "", "_______________", "" ) );
			} else {
				$this->SetFont('Times','BU','11');
				$this->Ln(0); $this->Row( array( "", "", "", $kades, "" ) );
				$this->SetFont('Times','','11');
				$this->Ln(-2.5); $this->Row( array( "", "", "", "NIP : ".$nip, "" ) );
			}
		
		}
	}
	
	public function printPDF () {
		$this->SetAutoPageBreak(false);
	    $this->AliasNbPages();
	    $this->SetFont("Times", "B", 10);
	    //$this->AddPage();
 
	    $this->rptDetailData();
		$this->Output($this->options['filename'],$this->options['destinationfile']);
		
  	}
 
  	private $widths;
	private $aligns;
	
	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}
 
	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}
 
	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=7.5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			// $this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,7.5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	function Row2($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			// $this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	function Table($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}
 
	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}
 
	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
	
	function MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false, $indent=0)
	{
    //Output text with automatic or explicit line breaks
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;

    $wFirst = $w-$indent;
    $wOther = $w;

    $wmaxFirst=($wFirst-2*$this->cMargin)*1000/$this->FontSize;
    $wmaxOther=($wOther-2*$this->cMargin)*1000/$this->FontSize;

    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 && $s[$nb-1]=="\n")
        $nb--;
    $b=0;
    if($border)
    {
        if($border==1)
        {
            $border='LTRB';
            $b='LRT';
            $b2='LR';
        }
        else
        {
            $b2='';
            if(is_int(strpos($border,'L')))
                $b2.='L';
            if(is_int(strpos($border,'R')))
                $b2.='R';
            $b=is_int(strpos($border,'T')) ? $b2.'T' : $b2;
        }
    }
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $ns=0;
    $nl=1;
        $first=true;
    while($i<$nb)
    {
        //Get next character
        $c=$s[$i];
        if($c=="\n")
        {
            //Explicit line break
            if($this->ws>0)
            {
                $this->ws=0;
                $this->_out('0 Tw');
            }
            $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $ns=0;
            $nl++;
            if($border && $nl==2)
                $b=$b2;
            continue;
        }
        if($c==' ')
        {
            $sep=$i;
            $ls=$l;
            $ns++;
        }
        $l+=$cw[$c];

        if ($first)
        {
            $wmax = $wmaxFirst;
            $w = $wFirst;
        }
        else
        {
            $wmax = $wmaxOther;
            $w = $wOther;
        }

        if($l>$wmax)
        {
            //Automatic line break
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
                if($this->ws>0)
                {
                    $this->ws=0;
                    $this->_out('0 Tw');
                }
                $SaveX = $this->x; 
                if ($first && $indent>0)
                {
                    $this->SetX($this->x + $indent);
                    $first=false;
                }
                $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                    $this->SetX($SaveX);
            }
            else
            {
                if($align=='J')
                {
                    $this->ws=($ns>1) ? ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
                    $this->_out(sprintf('%.3f Tw',$this->ws*$this->k));
                }
                $SaveX = $this->x; 
                if ($first && $indent>0)
                {
                    $this->SetX($this->x + $indent);
                    $first=false;
                }
                $this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
                    $this->SetX($SaveX);
                $i=$sep+1;
            }
            $sep=-1;
            $j=$i;
            $l=0;
            $ns=0;
            $nl++;
            if($border && $nl==2)
                $b=$b2;
        }
        else
            $i++;
    }
    //Last chunk
    if($this->ws>0)
    {
        $this->ws=0;
        $this->_out('0 Tw');
    }
    if($border && is_int(strpos($border,'B')))
        $b.='B';
    $this->Cell($w,$h,substr($s,$j,$i),$b,2,$align,$fill);
    $this->x=$this->lMargin;
    }
} //end of class
 
$tabel = new FPDF_AutoWrapTable(@$data, @$options);
$tabel->printPDF();

?>

<script type="text/javascript">
	window.history.forward();
	function noBack() { window.history.forward(); }
</script>