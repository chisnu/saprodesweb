<?php
session_start();
date_default_timezone_set('Asia/Jakarta');
include "../library/config.php";
$_SESSION = array();
include("../library/captcha/simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();
?>

<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <title>SIPOLAN | Kec.Nganjuk Kab.Nganjuk</title>
  <!-- Favicon -->
  <link rel="shortcut icon" href="img/title.png">

  <meta name="keywords" content="" />
  <meta name="description" content="" />

  <link href="../library/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="../library/bootstrap/js/bootstrap.min.js"></script>
  <!-- Sweet Alerts -->
  <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<!------ Include the above in your HEAD tag ---------->
	<style>
	.panel-heading {
		padding: 5px 15px;
	}

	.panel-footer {
		padding: 1px 15px;
		color: #A0A0A0;
	}

	.profile-img {
		width: 96px;
		height: 96px;
		margin: 0 auto 10px;
		display: block;
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		border-radius: 50%;
	}
	
	body {
		background-color:#363636;
	}
	</style>
</head>

<body>
    <div class="container" style="margin-top:40px">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading text-center">
						<h3>Administrator</h3>
					</div>
					<div class="panel-body">
						<form action="LoginAdmin.php" method="POST">
							<fieldset>
								<div class="row">
									<div class="center-block">
										<img class="profile-img"
											src="img/login.png" alt="">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-10  col-md-offset-1 ">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span> 
												<input class="form-control" placeholder="Username" name="username" type="text" maxlength="25" autofocus required>
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-lock"></i>
												</span>
												<input class="form-control" placeholder="Password" name="password" type="password" maxlength="15" required>
											</div>
										</div>
										<div class="form-group">
											<select name="LevelUser" class="form-control" id="colorselector" required>
												<option value='' disabled selected>--- Pengguna ---</option>
												<option value='Kecamatan'>Kecamatan</option>
												<option value='Desa'>Desa</option>
											</select>
											<!-- Material inline 1
											<div class="form-check form-check-inline" id="colorselector">
												<input type="radio" class="form-check-input" id="materialInline1" name="LevelUser" value='Kecamatan'>
												<label class="form-check-label" for="materialInline1">Kecamatan</label>
												&nbsp;&nbsp;
												<input type="radio" class="form-check-input" id="materialInline2" name="LevelUser" value='Desa'>
												<label class="form-check-label" for="materialInline2">Desa/Kelurahan</label>
											</div> -->
										</div>
										<div class="output">											  
										  <div id="Desa" class="colors yellow">
											<select name="KodeLokasi" class="form-control">	
												<option value="" disabled selected>--- Desa ---</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT a.KodeLokasi,b.NamaDesa FROM mstlokasi a JOIN mstdesa b ON (a.KodeDesa,a.KodeKec)=(b.KodeDesa,b.KodeKec) WHERE b.KodeKec='3518140'  GROUP BY b.NamaDesa ORDER BY a.KodeLokasi ASC");
													while($kode = mysqli_fetch_array($menu)){
														echo "<option value=\"".$kode['KodeLokasi']."\" >".$kode['NamaDesa']."</option>\n";
													}
												?>
											</select>
										  </div>
										</div>
										
										<div class="form-group">
											<?php echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA code">';?>
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-info-sign"></i>
												</span>
												<input class="form-control" placeholder="Masukkan Kode" name="captcha" type="text" maxlength="5" required>
												<input type="hidden" name="KodeCaptcha" value="<?php echo $_SESSION['captcha']['code']; ?>">
											</div>
										</div>
										<div class="form-group">
											<input type="submit" name="submit" class="btn btn-lg btn-primary btn-block" value="Sign in">
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer text-right">
						<a href="index.php">Kembali</a>
					</div>
                </div>
			</div>
		</div>
	</div>
<script src="js/jquery.min.js"></script>
<script>
	$(function() {
	  $('.colors').hide();
	  
	  $('#colorselector').change(function(){
		$('.colors').hide();
		$('#' + $(this).val()).show();
	  });
	  
	});
</script>
<!-- Sweet Alerts -->
<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>

</body>
<?php
if(isset($_POST['submit'])){
	if($_POST['KodeCaptcha']==$_POST['captcha']){
		if(empty($_POST['username']) || empty ($_POST ['password'])) {
			echo '<script type="text/javascript">
				  sweetAlert({
					title: "Maaf!",
					text: " Username atau Password Kosong ",
					type: "error"
				  },
				  function () {
					window.location.href = "login.php";
				  });
				  </script>';
			@session_destroy();
		}else{		
			// Variabel username dan password
			$username = @stripslashes($_POST['username']);
			$password = @stripslashes(base64_encode($_POST['password']));
			$level	  = $_POST['LevelUser'];
			$KodeLokasi = $_POST['KodeLokasi'];
			$waktu    = time()+25200; //(GMT+7)
			$expired  = 300000;
			
			// Mencegah MySQL injection dan XSS
			$check_user = @htmlspecialchars(addslashes($username));
			$check_pass = @htmlspecialchars(addslashes($password));
			
			// SQL query untuk memeriksa apakah user terdapat di database?
			$query = "SELECT * FROM userlogin WHERE UserName = '$check_user' AND UserPsw = '$check_pass' AND IsDihapus='0' ";
			if($level=='Desa'){
				$query .= " AND KodeLokasi='$KodeLokasi' AND LevelUser='Desa' ";
			}else{
				$query .= " AND LevelUser='Kecamatan' ";
			}
			
			$query = @mysqli_query($koneksi, $query);
			$cari = @mysqli_num_rows($query); 
				
				if($cari === 0){
					echo '<script type="text/javascript">
						  sweetAlert({
							title: "Login Gagal!",
							text: " Username / Password Salah atau Pengguna Tidak Aktif ",
							type: "error"
						  },
						  function () {
							window.location.href = "LoginAdmin.php";
						  });
						  </script>';
					@session_destroy();
				}else{
					$row = mysqli_fetch_array($query);
					
					@$_SESSION['user_login'] = $row['UserName'];
					@$_SESSION['user_pass'] = $row['UserPsw'];
					@$_SESSION['user_level'] = $row['LevelUser'];
					@$_SESSION['user_lokasi'] = $row['KodeLokasi'];
					@$_SESSION['timeout'] = $waktu + $expired; // Membuat Sesi Waktu
					//@$_SESSION['_status'] = $row['StatusPerson']; // Status
					if($level=='Desa'){
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Login Sukses!",
							text: " Anda Berhasil Login ",
							type: "success"
						  },
						  function () {
							window.location.href = "../desa/index.php";
						  });
						  </script>';
					}else{
						echo '<script type="text/javascript">
						  sweetAlert({
							title: "Login Sukses!",
							text: " Anda Berhasil Login ",
							type: "success"
						  },
						  function () {
							window.location.href = "../kecamatan/index.php";
						  });
						  </script>';
					}
				}
				@sqlsrv_close(); // Menutup koneksi
			
		}
	}else{
		echo '<script type="text/javascript">
			  sweetAlert({
				title: "Login Gagal!",
				text: " Kode Captcha Salah! ",
				type: "error"
			  },
			  function () {
				window.location.href = "LoginAdmin.php";
			  });
			  </script>';
		@session_destroy();
	}
}
?>
</html>