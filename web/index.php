<?php
session_start();
date_default_timezone_set('Asia/Jakarta');
$_SESSION = array();
include("../library/captcha/simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();
include '../library/config.php';

$useragent=$_SERVER['HTTP_USER_AGENT']; 
if(preg_match('/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){ 
   //jika menggunakan browser versi mobile maka alihkan ke file web versi mobile anda 
   $LetakLogin = "logo";
}else{ 
    //jika tidak versi mobile maka pakai versi standar 
	$LetakLogin = "login";
}

function Encrypt($StrData,$KeyCode){
	$Encrypt=StrXOR(strrev($StrData),$KeyCode);	
	return $Encrypt;
}

function Decrypt($StrData,$KeyCode){
	$Decrypt=str_replace('~^&',"'",$StrData);
	$Decrypt=strrev(StrXOR($StrData,$KeyCode));
	return $Decrypt;
}	


function StrXOR($StrData,$KeyCode){
	$StrRet="";
	$kPos=1;
	for ($i=1;$i<=strlen($StrData);$i++){
		$StrRet=$StrRet.chr(ord(substr($StrData,$i-1,1)) ^ ord(substr($KeyCode,$kPos-1,1)));
		$kPos++;
		
		if ($kPos > strlen($KeyCode)){
			$kPos=1;
			
		}
	}
	return $StrRet;
}

?>

<!DOCTYPE HTML>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <title>SIPOLAN | Kec.Nganjuk Kab.Nganjuk</title>
  <!-- Favicon -->
  <link rel="shortcut icon" href="img/title.png">

  <meta name="keywords" content="" />
  <meta name="description" content="" />

  <!-- css -->
  <link rel="stylesheet" href="css/bootstrap.css" />
  <link rel="stylesheet" href="css/bootstrap-responsive.css" />
  <link rel="stylesheet" href="css/prettyPhoto.css" />
  <link rel="stylesheet" href="css/sequence.css" />
  <link rel="stylesheet" href="css/style.css" />
	<!-- Sweet Alerts -->
    <link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
  <style>
	.login {
		float: right;
	}
	
	.box p {
		padding: 0 30px;
		text-align: justify;
		margin-top: 16px;
		font-size: 14px;
		text-transform: none;
		line-height: 24px;
	}
	
	.sweet-alert button {
		background-color: #8CD4F5 !important;
		color: #f31717;
		border: none;
		box-shadow: none;
		font-size: 17px;
		font-weight: 500;
		-webkit-border-radius: 4px;
		border-radius: 5px;
		padding: 10px 32px;
		margin: 26px 5px 0 5px;
		cursor: pointer;
	}
  </style>
  <!-- =======================================================
    Theme Name: Bootslander
    Theme URL: https://bootstrapmade.com/bootslander-free-bootstrap-landing-page-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
	======================================================= -->
</head>

<body>

  <!-- main wrap -->
  <div class="main-wrap">

    <!-- header -->
    <header>
      <!-- top area -->
      <div class="top-nav">
        <div class="wrapper">
          <div class="logo">
            <a href="login.php">
              <!-- your logo image -->
              <img src="img/logo-sipolan.png" alt="" />
            </a>
          </div>
		<?php if($LetakLogin=='login'){ 
			echo '
			<a href="login.php" class="text-center">
			  <div class="login">
				<p>Login</p>
			  </div>
		  </a>
			';
		} 
		?>
          
        </div>
      </div>
      <!-- end top area -->
    </header>
    <!-- end of header-->

    <!-- section intro -->
    <section id="intro">
      <div class="featured">
        <div class="wrapper">

          <div class="row-fluid">
            <!-- slider -->
            <div class="span12">

              <div id="sequence-theme">
                <div id="sequence">
                  <img class="prev" src="img/slides/bt-prev.png" alt="Previous Frame" />
                  <img class="next" src="img/slides/bt-next.png" alt="Next Frame" />
                  <ul>
                    <li class="animate-in">
                      <h2 class="title">Layanan Online</h2>
                      <h5 class="subtitle">Buat permohonan surat tanpa harus datang ke Kantor Kelurahan / Kecamatan</h5>
                      <img class="model" src="img/slides/img4.png" alt="" />
                    </li>
                    <li>
                      <h2 class="title">Lebih Dari 10 Surat</h2>
                      <h5 class="subtitle">Melayani permohonan surat lebih dari 10 model surat</h5>
                      <img class="model" src="img/slides/img3.png" alt="" />
                    </li>
                    <li>
                      <h2 class="title">Efisiensi Waktu & Biaya</h2>
                      <h5 class="subtitle">Lihat tracking surat permohonan Anda secara real time</h5>
                      <img class="model" src="img/slides/img1.png" alt="" />
                    </li>
					<li>
                      <h2 class="title">Dapat Diakses Dimanapun & Kapanpun</h2>
                      <h5 class="subtitle">System online dapat Anda akses melalui Smartphone / Laptop</h5>
                      <img class="model" src="img/slides/img2.png" alt="" />
                    </li>
                  </ul>
                </div>
                <ul class="nav">
                  <li><img src="img/slides/thumb4.png" alt="Thumbnail" /></li>
                  <li><img src="img/slides/thumb3.png" alt="Thumbnail" /></li>
                  <li><img src="img/slides/thumb1.png" alt="Thumbnail" /></li>
                  <li><img src="img/slides/thumb2.png" alt="Thumbnail" /></li>
                </ul>
              </div>

            </div>
            <!-- end slider -->
          </div>

        </div>
      </div>
    </section>
    <!-- end section intro -->
	<?php if($LetakLogin=='logo'){ 
		 echo ' <section id="bottom">
      <div class="bottom-cta">
        <div class="wrapper">
		  <a href="login.php" class="btn btn-red btn-large">Login Kependudukan</a>
        </div>
      </div>
    </section>';
		 }?>
	 <!-- section bottom -->
   
	
    <!-- section main content -->
    <section id="main-content">
      <div class="content-wrap">
        <!-- tagline -->
        <div class="tagline">
          <div class="wrapper">
             <!-- boxes -->
          <div class="boxes">
			<div class="headline">
              <h3><span>Sistem Aplikasi Program Layanan Masyarakat (SIPOLAN)</span></h3>
			</div>
            <div class="row-fluid">

              <!-- box 1 -->
              <div class="span4">
                <div class="box">
                  <div class="icon">
                    <img src="img/icons/icon-1.png" class="" alt="" />
                  </div>
                  <h4>Tentang</h4>
                  <p> SIPOLAN ONLINE adalah sistem layanan masyarakat online yang diperuntukan bagi seluruh masyarakat Kec.Nganjuk guna mempermudah proses pelayanan baik dari kantor desa / kecamatan. <br/><br/></p>
                  <!--<a href="#" class="textlink">Learn more</a>-->
                </div>
              </div>

              <!-- box 2 -->
              <div class="span4">
                <div class="box">
                  <div class="icon">
                    <img src="img/icons/icon-2.png" class="" alt="" />
                  </div>
                  <h4>Efisiensi Waktu & Biaya</h4>
                  <p> Dengan hadirnya SIPOLAN ONLINE ini diharapkan pelayanan surat masyarakat dapat terbantu sepenuhnya baik dari segi waktu & biaya yang banyak masyarakat keluhkan. <br/><br/><br/></p>
                  <!--<a href="#" class="textlink">Learn more</a>-->
                </div>
              </div>

              <!-- box 3 -->
              <div class="span4">
                <div class="box">
                  <div class="icon">
                    <img src="img/icons/icon-3.png" class="" alt="" />
                  </div>
                  <h4>Akses Mudah</h4>
                  <p> SIPOLAN ONLINE dibangun dengan platform website, sehingga sangat mudah diakses melalui Android, IOS, Windows, dll. Khusus untuk aplikasi Kantor Kelurahan / Kecamatan hanya dapat diakses melalui platform Windows. </p>
                  <!--<a href="#" class="textlink">Learn more</a>-->
                </div>
              </div>

            </div>
			
          </div>
          <!-- end boxes -->
			
			<!--### Subtitle ###-->
            <h2>MAKLUMAT/ JANJI PELAYANAN KECAMATAN NGANJUK</h2>
            <!-- CTA -->
            <div class="text-left">
            <!--<div class="cta">
              <div class="btn-group">
              <div class="btn-group">
                <a href="https://bootstrapmade.com/buy/?template=Bootslander" class="btn btn-green btn-large"><i class="icon-shopping-cart icon-white"></i> Buy this template</a>
                <a href="https://bootstrapmade.com/bootslander-free-bootstrap-landing-page-template/#download" class="btn btn-red btn-large">Try demo version</a>
              </div>-->
			  <p>
				<table>
					<tbody>
					<tr>
					  <td>1.&nbsp;</td>
					  <td>MEMBERIKAN PELAYANAN YANG TERBAIK BAGI  SELURUH WARGA MASYARAKAT;</td>
					</tr>
					<tr>
					  <td>2.&nbsp;</td>
					  <td>MEMBERIKAN KEMUDAHAN DAN KEPASTIAN DALAM PROSEDUR PELAYANAN (KEPASTIAN PELAYANAN);</td>
					</tr>
					<tr>
					  <td>3.&nbsp;</td>
					  <td>MENYELESAIKAN PELAYANAN SESUAI DENGAN JADWAL WAKTU YANG TELAH DITETAPKAN (KEPASTIAN WAKTU PENYELESAIAN);</td>
					</tr>
					<tr>
					  <td>4.&nbsp;</td>
					  <td>RAMAH, SALAM, SAPA, SENYUM, SOPAN DAN SANTUN DALAM MEMBERIKAN PELAYANAN.</td>
					</tr>
					</tbody>
				</table>

			  </p>
            </div>
          </div>
        </div>
        <!-- end tagline -->

        <!-- wrapper -->
        <div class="wrapper">
          <!-- recent portfolio -->
          <div class="row-fluid portfolio">
            <div class="headline">
              <h2><span>Daftar Layanan Surat Online</span></h2>
			</div>
           <div class="table-responsive">  
				<table class="table table-striped">
				  <thead>
					<tr>
					  <th>No</th>
					  <th>Jenis Surat</th>
					  <th>Estimasi Pelayanan</th>
					</tr>
				  </thead>
				  <tbody>
					<?php
					//AND b.NoTrMohon='$No_Transaksi'
					$no =1;
					$sql_syarat = @mysqli_query($koneksi, "SELECT * FROM masterpengurusansurat WHERE JenisSurat !='SKT' ORDER BY Keterangan"); 
					while($data_syarat = @mysqli_fetch_array($sql_syarat)){
					?>
					<tr>
					  <th scope="row"><?php echo $no++; ?></th>
					  <td><?php echo $data_syarat['Keterangan'];?></td>
					  <td><?php echo $data_syarat['StandarWaktuPelayanan'];?> Hari Kerja</td>
					</tr>
					<?php } ?>
				  </tbody>
				</table>
			  </div>
          </div>
          <!-- end portfolio -->


          <!-- testimonials 
          <div class="row-fluid testimonials">
            <div class="headline">
              <h2><span>What people are saying</span></h2></div>
            <ul>
              <li class="span4">
                <div class="testimonial">
                  <img src="img/dummies/user-1.png" alt="" class="img-circle" />
                  <p>
                    &ldquo;Lorem ipsum dolor sit amet, veritus molestie et his. Summo dissentiet duo an. Et duo vitae atomorum, eripuit eruditi definitiones nec ut.&rdquo;
                  </p>
                  <span>&#45;&#45; Mike lamouz, <a href="#">Net designer</a></span>
                </div>
              </li>
              <li class="span4">
                <div class="testimonial">
                  <img src="img/dummies/user-2.png" alt="" class="img-circle" />
                  <p>
                    &ldquo;Lorem ipsum dolor sit amet, veritus molestie et his. Summo dissentiet duo an. Et duo vitae atomorum, eripuit eruditi definitiones nec ut.&rdquo;
                  </p>
                  <span>&#45;&#45; Leslie samarov, <a href="#">JIK Company</a></span>
                </div>
              </li>
              <li class="span4">
                <div class="testimonial">
                  <img src="img/dummies/user-3.png" alt="" class="img-circle" />
                  <p>
                    &ldquo;Lorem ipsum dolor sit amet, veritus molestie et his. Summo dissentiet duo an. Et duo vitae atomorum, eripuit eruditi definitiones nec ut.&rdquo;
                  </p>
                  <span>&#45;&#45; Jonathan does, <a href="#">App Studio</a></span>
                </div>
              </li>

            </ul>
          </div>-->

        </div>
        <!-- end wrapper -->

      </div>
    </section>
    <!-- end main content section -->

    <!-- section bottom --
    <section id="bottom">
      <div class="bottom-cta">
        <div class="wrapper">
          <h3 class="title">Download Aplikasi Saprodes Versi Kantor Kelurahan & Kecamatan Disini!</h3>
          <h2>Aplikasi ini hanya digunakan untuk kantor kelurahan & kecamatan (Bukan untuk umum)</h2>
		  
		  <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-red btn-large">Download Saprodes</a>
                      <!-- Modal--
		  <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
			<div role="document" class="modal-dialog modal-sm">
			  <div class="modal-content" style="background-color: #666464;">
				<div class="modal-header">
				  <h4 id="exampleModalLabel" class="modal-title"><font color="#fff">Download Saprodes Desktop</font></h4>
				</div>
				<div class="modal-body">
				  <form method="post" action="">
					<div class="form-group">
					  <select class="form-control" name="JenisSurat" autocomplete="off" required>
						<?php/*  echo '<option value="">-- Pilih Desa / Kelurahan --</option>';
							$list = @sqlsrv_query($dbconnect, "SELECT a.KodeLokasi,b.NamaDesa FROM MstLokasi a JOIN MstDesa b ON a.KodeDesa=b.KodeDesa AND a.KodeKec=b.KodeKec AND a.KodeKab=b.KodeKab ORDER BY b.NamaDesa ASC") or die( print_r( sqlsrv_errors(), true)); 
							while($daftar = @sqlsrv_fetch_array($list, SQLSRV_FETCH_ASSOC)){
								echo "<option value=\"".$daftar['KodeLokasi']."\">".ucwords($daftar['NamaDesa'])."</option>\n";
							} */
						?>
						</select>
					</div>
					<div class="form-group">    
					  <input type="text" placeholder="Username" name="Username" class="form-control" required>
					</div>
					<div class="form-group">    
					  <input type="password" placeholder="Password" name="Password" class="form-control" required>
					</div>
					<div class="form-group">
						<?php //echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA code">';?>
						<div class="input-group">
							<span class="input-group-addon">
								<i class="glyphicon glyphicon-info-sign"></i>
							</span>
							<input class="form-control" placeholder="Masukkan Kode" name="captcha" type="text" maxlength="5" required>
							<input type="hidden" name="KodeCaptcha" value="<?php // echo $_SESSION['captcha']['code']; ?>">
						</div>
					</div>
					<div class="form-group">       
					  <button type="submit" name="Download" class="btn btn-red">Download</button>
					</div>
				  </form>
				</div>
				<div class="modal-footer"  style="background-color: #666464;">
				  <button type="button" data-dismiss="modal" class="btn btn-warning">tutup</button>
				</div>
			  </div>
			</div>
		  </div>
		  
        </div>
      </div>
    </section> -->
    <!-- end section bottom -->
	<!--
	Button trigger modal 
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
	  Launch demo modal
	</button>

	<!-- Modal 
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-body">
			<form method="post" class="contactForm">

              <div id="sendmessage">Your message has been sent. Thank you!</div>
              <div id="errormessage"></div>

              <div class="row">
					<div class="span12 field form-group">
					  <input type="text" name="name" placeholder="* Enter your full name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
					  <div class="validation"></div>
					</div>
					<div class="span4 field form-group">
					  <input type="text" name="email" placeholder="* Enter your email address" data-rule="email" data-msg="Please enter a valid email" />
					  <div class="validation"></div>
					</div>
					
              </div>
            </form>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary">Save changes</button>
		  </div>
		</div>
	  </div>
	</div>
	-->
    <!-- footer -->
    <footer>
      <div class="footer">
        <div class="wrapper">
          <!--<div class="social">
            <a href="#" class="fb"> </a> <a href="#" class="tw"> </a>
          </div>-->
          <div class="subfooter">
            <!--<ul>
              <li><a href="#">Home</a> &#45; </li>
              <li><a href="#">Terms conditions</a> &#45; </li>
              <li><a href="#">Contact</a></li>
            </ul>-->
			<p class="copyright">Office : Jl. Dermojoyo 21 Nganjuk | ( 0358 ) 322753 &#169; Copyright. All rights reserved</p>

          </div>
          <div class="credits">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Bootslander
            -->
            Develope by <a href="https://afindo-inf.com/" target="_BLANK">Afindo Informatika - Jombang</a>
          </div>
        </div>
      </div>
    </footer>


  </div>
  <!-- end main wrap -->
  <!-- Javascript Libraries -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/sequence.jquery.js"></script>
  <script src="js/jquery-hover-effect.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Custom Javascript File -->
  <script src="js/custom.js"></script>
	<!-- Sweet Alerts -->
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
</body>
<?php /*

if(isset($_POST['Download'])){
	/ $CekSql = @sqlsrv_query($dbconnect, "SELECT * FROM Userlogin WHERE UserName='admin'", array(), array( "Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true)); 
	$data = @sqlsrv_fetch_array($CekSql, SQLSRV_FETCH_ASSOC); 
	if($_POST['KodeCaptcha']==$_POST['captcha']){
		$Key='<^v^> SIM - ZIN <^v^>';
		$kunci = Encrypt($_POST['Password'],$Key);

		//cek apakah terdaftar  pada user login
		$CekSql = @sqlsrv_query($dbconnect, "SELECT * FROM Userlogin WHERE UserName='".$_POST['Username']."' AND UserPsw='$kunci'", array(), array("Scrollable" => 'static')) or die( print_r( sqlsrv_errors(), true)); 
		$numCek = @sqlsrv_num_rows($CekSql); 
		if($numCek == 1){
			echo '<script language="javascript">document.location="../Download/Setup_AIPharsyst_v1.exe"; </script>';
		}else{
			echo '<script type="text/javascript">
			  sweetAlert({
				title: " Tidak Ada User Ditemukan!",
				text: " ",
				type: "error"
			  },
			  function () {
				window.location.href = "index.php";
			  });
			  </script>';
		}
	}else{
		echo '<script type="text/javascript">
			  sweetAlert({
				title: " Kode Captcha Salah ",
				text: " ",
				type: "error"
			  },
			  function () {
				window.location.href = "index.php";
			  });
			  </script>';
	}
} */
?>
</html>
